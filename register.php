<?php 
  include 'inc/header.php';
  // if(!isset($_SESSION, $_SESSION['superadmin_token']) || empty($_SESSION['superadmin_token']) || strlen($_SESSION['superadmin_token']) != 30){
  //   redirect('../','error','Please login first.');
  // }
?>

  <div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="../../index2.html" class="h1"><b>Admin</b>LTE</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form action="./process/login" method="post" enctype='multipart/form-data'>
        <div class="input-group mb-3">
          <input type="text" name="full-name" class="form-control" placeholder="Full name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="re-password" class="form-control" placeholder="Retype password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="file" name="image" class="form-control" placeholder="image">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-image"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <div class="form-group" style="max-width: 100%" data-select2-id="55">
            <label>Role</label>
              <select class="form-group form-control" aria-hidden="true">
                <option>Admin</option>
                <option>User</option>
              </select>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
                <input type="search" class="form-control" placeholder="User Role">
              
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" name="register" value="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="./" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>

<!-- <script type="text/javascript">
  var calendar = $('#calendar').fullCalendar('getCalendar');

calendar.on('dayClick', function(date, jsEvent, view) {
  console.log('clicked on ' + date.format());
})
</script> -->

<?php 
    $scripts = '
<script src="'.VENDOR_URL.'bootstrap/assets/dist/js/bootstrap.min.js"></script>
<script src="'.VENDOR_URL.'fastclick/lib/fastclick.js"></script>
<script src="'.VENDOR_URL.'nprogress/nprogress.js"></script>
<script src="'.JS_URL.'custom.min.js"></script>
<script src="'.JS_URL.'datatables.min.js"></script>';
    include 'inc/footer.php';
?>
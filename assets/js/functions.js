/*Misc Functions*/

function printDiv(divId){
	console.log(divId);

	var prtContent = document.getElementsByClassName(divId);
	var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
	console.log(prtContent[0]);
	WinPrint.document.write(prtContent[0].innerHTML);
	WinPrint.document.close();
	WinPrint.focus();
	WinPrint.print();
	WinPrint.close();
}
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
function endsWithNumber( str ){
  return isNaN(str.slice(-1)) ? false : true;
}
function getClassData(element){
	console.log(element);
	return element

}
function camelize(str) {
	return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
		return index === 0 ? word.toLowerCase() : word.toUpperCase();
	}).replace(/\s+/g, '');
}
function postData(){
	console.log('1');
	$('#manage-class-form').click();
}

function getCommon(arr1, arr2) {
  var common = [];                   // Array to contain common elements
  if (Array.isArray(arr1) && Array.isArray(arr2)) {
  	for(var i=0 ; i<arr1.length ; ++i) {
  		for(var j=0 ; j<arr2.length ; ++j) {
	      if(arr1[i] == arr2[j]) {       // If element is in both the arrays
	        common.push(arr1[i]);        // Push to common array
	    }
	}
}
}
  return common;                     // Return the common elements
}

function arrayColumn(array, columnName) {
	return array.map(function(value,index) {
		return value[columnName];
	})
}
function removeElements(elements){
	

	if(elements.length){
		for(var i = 0; i < elements.length; i++){
			console.log('elements[i].parentNode');
			elements[i].parentNode.removeChild(elements[i]);
		}
	}else{
		console.log('elements.parentNode>>',elements);

		//elements.parentNode.removeChild(elements);
	}
	
}


function unique(array){
	return array.filter(function(el, index, arr) {
		return index == arr.indexOf(el);
	});
}

function displayInputField(){
	$('.inputFields').removeAttr('style');
	$('#addAction').attr('style','display:none');
}

function PrintDiv(divToPrint) {    
	//console.log("divToPrint>>",document.getElementById("divToPrint"));
	var divToPrint = document.getElementById(divToPrint);
	var popupWin = window.open('', '_blank', 'width=300,height=300');
	console.log('popupWin>>',popupWin);
	popupWin.document.open();
	popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
	popupWin.document.close();
}

function showGeneralInfoForm(){
	var staff_generalInfo_details = $("#staff_generalInfo_details");
	var bank_acc_details = $("#bank_acc_details");
	var personalInfo_details = $("#personalInfo_details");
	staff_generalInfo_details.css("display", "block");
	bank_acc_details.css("display", "none");
	personalInfo_details.css("display", "none");
}

function showPersonalInfoForm(a = false){
	var staff_generalInfo_details = $("#staff_generalInfo_details");
	var bank_acc_details = $("#bank_acc_details");
	var personalInfo_details = $("#personalInfo_details");

	submitButton = $("#staff_bank_detail_add");
	updateButton = $("#staff_bank_detail_update");
	console.log(submitButton);
	console.log(updateButton);
	

	if (updateButton.length && a == false) {
		updateButton.click();
	}

	staff_generalInfo_details.css("display", "none");
	bank_acc_details.css("display", "none");
	personalInfo_details.css("display", "block");
}

function showBankInfoForm(a = false){
	
	var staff_generalInfo_details = $("#staff_generalInfo_details");
	var bank_acc_details = $("#bank_acc_details");
	var personalInfo_details = $("#personalInfo_details");

	submitButton = $("#staff_generalInfo_detail_add");
	updateButton = $("#staff_generalInfo_detail_update");
	console.log(submitButton.length);
	//console.log(updateButton);

	if (submitButton.length && a == false) {
		console.log('inside click');
		submitButton.click();
	}

	staff_generalInfo_details.css("display", "none");
	bank_acc_details.css("display", "block");
	personalInfo_details.css("display", "none");
}

function showStudentPersonalInfoForm($updateForm){
	
	var studentPersonalInfoForm = $("#studentPersonalInfoForm");
	var studentFatherDetailForm = $("#studentFatherDetailForm");
	var studentMotherDetailForm = $("#studentMotherDetailForm");
	var studentGuardianDetailForm = $("#studentGuardianDetailForm");

	studentPersonalInfoForm.css("display", "block");
	studentFatherDetailForm.css("display", "none");
	studentMotherDetailForm.css("display", "none");
	studentGuardianDetailForm.css("display", "none");

	if ($updateForm == true) {
		$("#student_personal_detail_update").click();
	}

}

function showFatherDetailForm($updateForm){
	var studentPersonalInfoForm = $("#studentPersonalInfoForm");
	var studentFatherDetailForm = $("#studentFatherDetailForm");
	var studentMotherDetailForm = $("#studentMotherDetailForm");
	var studentGuardianDetailForm = $("#studentGuardianDetailForm");

	studentPersonalInfoForm.css("display", "none");
	studentFatherDetailForm.css("display", "block");
	studentMotherDetailForm.css("display", "none");
	studentGuardianDetailForm.css("display", "none");

	if ($updateForm == true) {
		$("#student_personal_detail_update").click();
	}
	
}
function showMotherDetailForm($updateForm){
	
	//$("#student_mother_detail_update").click();

	var studentPersonalInfoForm = $("#studentPersonalInfoForm");
	var studentFatherDetailForm = $("#studentFatherDetailForm");
	var studentMotherDetailForm = $("#studentMotherDetailForm");
	var studentGuardianDetailForm = $("#studentGuardianDetailForm");

	studentPersonalInfoForm.css("display", "none");
	studentFatherDetailForm.css("display", "none");
	studentMotherDetailForm.css("display", "block");
	studentGuardianDetailForm.css("display", "none");
	if ($updateForm == true) {
		$("#student_father_detail_update").click();
	}
	
}
function showGuardianDetailForm($updateForm){
	

	var studentPersonalInfoForm = $("#studentPersonalInfoForm");
	var studentFatherDetailForm = $("#studentFatherDetailForm");
	var studentMotherDetailForm = $("#studentMotherDetailForm");
	var studentGuardianDetailForm = $("#studentGuardianDetailForm");

	studentPersonalInfoForm.css("display", "none");
	studentFatherDetailForm.css("display", "none");
	studentMotherDetailForm.css("display", "none");
	studentGuardianDetailForm.css("display", "block");
	if ($updateForm == true) {
		$("#student_mother_detail_update").click();
	}
	
}

function userTypeDropdownChange(){
	console.log(2);
	var userType = $("#userTypeDropdown");

	if (userType.val() == 'Admin') {
		console.log(userType.val());

	}else if(userType.val() == 'Admin'){
		console.log(userType.val());

	}else if(userType.val() == 'Student'){
		console.log(userType.val());

	}else if(userType.val() == 'Teacher'){
		console.log(userType.val());

	}else if(userType.val() == 'Receptionist'){
		console.log(userType.val());

	}else if(userType.val() == 'Librarian'){
		console.log(userType.val());

	}
}

function addFinePopup(params){
	var dialogId = params[0];
	console.log(params);
	document.getElementsByClassName("popup")[0].id = dialogId;
	sessionStorage.setItem("userIdForAddingFine", params[1]);
	$(document).ready(function(){
		$('#xyz').attr('value',params[1]);
	});	
	console.log(userIdForAddingFine);
}

function changePrivilegeDomElements(privilege){
	//console.log(privilege.length);
	var priv = privilege.substr(0,privilege.length-2);

	var privStatus = privilege.substr(privilege.length-1,1);
	console.log(priv);
	console.log(privStatus);
	var clickedRadioId = "#radio-"+priv+"-"+privStatus;
	var clickedInputId = "#"+priv+"-"+privStatus;

	var clickedRadioClass = ".radio-"+priv;
	var clickedInputClass = ".input-"+priv;


	//console.log($(clickedRadioId).attr('checked'));
	var radioClass = ".radio-"+priv;
	console.log(clickedRadioClass);
	console.log(clickedInputId);

	$('#').attr('checked');
	if ($(clickedRadioId).attr('checked')) {
		console.log('inChecked');
		//console.log(radioClass).addClass("fa-disabled");
		//$(radioClass).addClass("fa-disabled");
		/*$(clickedRadioClass).removeClass("fa-disabled");*/
		$(clickedRadioClass).addClass("fa-disabled");
		$(clickedRadioId).removeClass("fa-disabled");
		$(clickedInputClass).removeAttr('checked');
		$(clickedInputId).attr('checked','checked');

	}else{
		console.log('notChecked');

		$(clickedRadioClass).addClass("fa-disabled");
		$(clickedRadioId).removeClass("fa-disabled");
		$(clickedInputClass).removeAttr('checked');
		$(clickedInputId).attr('checked','checked');
		//$(clickedRadioId).removeClass("fa-disabled");
	}
	/*var radioId = 
	$("#test")*/
}

function myFunction(current_select){
	
	if (current_select) {
		console.log(current_select.value);

		var staffData = current_select.value.replace(/'/g, '"');
		//var str = text.replace(/'/g, '"');

		var staffDataObj = JSON.parse(staffData);
		console.log(staffDataObj);
		var staff_name = staffDataObj['staffData'][1];
		var user_id = staffDataObj['staffData'][2];

		console.log(staff_name);
		document.getElementById('staff_name').value = staff_name;
		document.getElementById('xyz').value = user_id;	
	}
}

format = function date2str(x, y) {
	var z = {
		M: x.getMonth() + 1,
		d: x.getDate(),
		h: x.getHours(),
		m: x.getMinutes(),
		s: x.getSeconds()
	};
	y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
		return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
	});

	return y.replace(/(y+)/g, function(v) {
		return x.getFullYear().toString().slice(-v.length)
	});
}

/*Result*/
function showgpa(id){
	$(document).ready(function() {
		console.log(id);

		$('.gpa'+id).removeAttr("style");
		$('.percentage'+id).attr("style","display: none");
		$('.gpa').removeAttr("style");
		$('.percentage').attr("style","display: none");

		$("#resultViewBtn"+id).html("Percentage View");
		$("#resultViewBtn"+id).attr("onclick","showPercentage('"+id+"')");
		$("#printingMarksheetButton").attr("value","print-marksheet-gpa");
		$("#printingLedgerMarksheetButton").attr("value","print-ledger-marksheet-gpa");


	});
}

function showPercentage(id){
	$(document).ready(function() {
		console.log(id);
		$('.gpa'+id).attr("style","display: none");
		$('.percentage'+id).removeAttr("style");
		$('.percentage').removeAttr("style");
		$('.gpa').attr("style","display: none");
		
		//$('.percentage').attr("style","display: block");

		$("#resultViewBtn"+id).html("GPA View");
		$("#resultViewBtn"+id).attr("onclick","showgpa('"+id+"')");
		$("#printingMarksheetButton").attr("value","print-marksheet-percentage");
		$("#printingLedgerMarksheetButton").attr("value","print-ledger-marksheet-percentage");

	});
}

function showMarks(id){
	$(document).ready(function() {
		console.log(id);
	   //$(".percentageRelative"+id).attr("style","display: block");
	   	if ($("#resultViewBtn"+id).attr("onClick") == "showPercentage('"+id+"')") { //relative marks not shown
	   		$(".percentageRelative"+id).attr("style","display: none");
	   		$(".percentage"+id).attr("style","display: block");
	   		$(".gpa"+id).attr("style","display: none");


	   	}else{
	   		$(".percentageRelative"+id).attr("style","display: none");
	   		$(".percentage"+id).attr("style","display: none");
	   		$(".gpa"+id).attr("style","display: block");

	   	}

	   	$(".grandTotalRelative"+id).attr("style","display: none");
	   	$(".grandTotal"+id).attr("style","display: block");

	   	$(".gpaRelative"+id).attr("style","display: none");

	   	$(".exam_marks"+id).attr("style","display: block");
	   	$(".exam_relative_marks"+id).attr("style","display: none");
	   	$("#marksViewBtn"+id).html("Relative Marks");
	   	$("#marksViewBtn"+id).attr("onclick","showRelativeMarks('"+id+"')");
	   });
}

function showRelativeMarks(id){
	$(document).ready(function() {
		console.log(id);

		if ($("#resultViewBtn"+id).attr("onClick") == "showPercentage('"+id+"')") { //relative marks not shown
			$(".percentageRelative"+id).attr("style","display: none");
			$(".percentage"+id).attr("style","display: none");
			$(".gpaRelative"+id).attr("style","display: block");


		}else{
			$(".percentageRelative"+id).attr("style","display: block");
			$(".percentage"+id).attr("style","display: none");
			$(".gpaRelative"+id).attr("style","display: none");

		}

		$(".grandTotalRelative"+id).attr("style","display: block");
		$(".grandTotal"+id).attr("style","display: none");

		//$(".percentageRelative"+id).attr("style","display: none");
		$(".gpa"+id).attr("style","display: none");

		$(".exam_marks"+id).attr("style","display: none");
		$(".exam_relative_marks"+id).attr("style","display: block");
		$("#marksViewBtn"+id).html("Exam Marks");
		$("#marksViewBtn"+id).attr("onclick","showMarks('"+id+"')");
	});
}
/*Update*/
function updateGrading(sliderId){
	console.log(sliderId);
	$( function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 100,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$( "#amount" ).val(ui.values[ 0 ] + "%" + " - " + ui.values[ 1 ] + "%" );
			}
		});
		$( "#amount").val( $( "#slider-range" ).slider( "values", 0 ) + "%" + " - " + $( "#slider-range" ).slider( "values", 1 ) + "%" );
	} );
}

function UpdateTotalAmount(feeId,$size) {
	var total = 0;
	var gn, elem;
  //var total = Number(document.getElementById('total_amount').value) ;
  var total = parseInt(document.getElementById('total_amount').value)  ;

  //console.log(total);
  gn = 'fee-amount-' + feeId;
  elem = document.getElementById(gn);
  console.log(elem);
  if (elem.checked == true) {
  	console.log(total);
  	total += parseInt(elem.value); 
  	console.log(total);
  }else{ 
  	if (elem.checked == false) {
  		total -= parseInt(elem.value);

  	}
  }
  
  //console.log(total);
  document.getElementById('total_amount').value = total ;

  document.getElementById('total').innerText = 'Rs. '+total+' /-';
}

/*Display popup*/
function displayUploadDocumentPopup(dialogId){
	
	document.getElementsByClassName("uploadDocumentPopup")[0].id = dialogId;
}


function displayContentDetailPopup(params){
	var dialogId = params[0];
	var content_type = params[1];
	var available_for = params[2];
	var class_name = params[3];
	var content_description = params[4];

	document.getElementsByClassName("popup")[0].id = dialogId;
	console.log(params);
	
	document.getElementById("content_type").innerHTML = content_type;
	document.getElementById("available_for").innerHTML = available_for;
	document.getElementById("class_name").innerHTML = class_name;
	document.getElementById("content_description").innerHTML = content_description;
}

function displayPopup(params){
	var dialogId = params[0];
	var complainDescription = params[1];
	var complainNote = params[2];
	var actionTaken = params[3];
	document.getElementsByClassName("popup")[0].id = dialogId;

	document.getElementById("complain_description").innerHTML = complainDescription;
	document.getElementById("complain_note").innerHTML = complainNote;
	document.getElementById("action_taken").innerHTML = actionTaken;
}

function onComplainEdit(params){
	var dialogId = params[0];
	var complainDetailId = params[1];
	var complainDetails = params[2];
	console.log(params);
	console.log(complainDetails);

	action_taken = complainDetails[0].action_taken;
	remarks = complainDetails[0].remarks;
	status = complainDetails[0].status;
	console.log('action_taken>>');
	document.getElementsByClassName("popupEdit")[0].id = dialogId;

	document.getElementById("action_taken").value = action_taken;
	document.getElementById("remarks").value = remarks;
	document.getElementById("status").value = status;
	document.getElementById("complainDetailId").value = complainDetailId;
}

function onEnquiryEdit(params){
	var dialogId = params[0];
	var enquiryDetailId = params[1];
	var enquiryDetails = params[2];
	console.log(params);
	console.log(enquiryDetails);

	action_taken = enquiryDetails[0].action_taken;
	remarks = enquiryDetails[0].remarks;
	status = enquiryDetails[0].status;
	console.log('action_taken>>');
	document.getElementsByClassName("popupEdit")[0].id = dialogId;

	document.getElementById("action_taken").value = action_taken;
	document.getElementById("remarks").value = remarks;
	document.getElementById("status").value = status;
	document.getElementById("enquiryDetailId").value = enquiryDetailId;
}

function onCallLogEdit(params){
	var dialogId = params[0];
	var callLogDetailId = params[1];
	var callLogDetails = params[2];
	console.log(params);
	console.log(callLogDetails);

	action_taken = callLogDetails[0].action_taken;
	remarks = callLogDetails[0].remarks;
	status = callLogDetails[0].status;
	console.log('action_taken>>');
	document.getElementsByClassName("popupEdit")[0].id = dialogId;

	document.getElementById("action_taken").value = action_taken;
	document.getElementById("remarks").value = remarks;
	document.getElementById("status").value = status;
	document.getElementById("callLogDetailId").value = callLogDetailId;
}

function openPopupText(id,text,headerText){
	console.log(text);
	document.getElementsByClassName("popup")[0].id = id;
	document.getElementsByClassName("close")[0].onClick = id;
	document.getElementById('modal_body').innerHTML = text;
	document.getElementById('sender').innerHTML = headerText + '<button type="button" class="close" data-dismiss="modal">&times;</button></h4>';

	document.getElementById(id).style.display='block';
	//document.getElementsByClassName('navbar-wrapper').style.zIndex = '10';
	$('.navbar-wrapper').css('z-index', '10');     
}

function viewNoticeDetail(params){
	console.log(1);
	var dialogId = params[0];
	var notice_title = params[1];
	var notice_text = params[2];
	var publish_on = params[3];
	var notice_till = params[4];

	document.getElementById("notice_title").innerHTML = notice_title;
	document.getElementById("notice_text").innerHTML = notice_text;
	document.getElementById("publish_on").innerHTML = 'As on : ' + publish_on;
	document.getElementsByClassName("popup")[0].id = dialogId;
	console.log(params);
	
}

/*Fees*/
function displayFeesStructurePopup(dialogId){
	
	document.getElementsByClassName("popup")[0].id = dialogId;
}

function feesDetailPopup(dialogId){
	
	document.getElementsByClassName("popupFeesDetail")[0].id = dialogId;
}

/*Fine*/
function addFinePopup(params){
	var dialogId = params[0];
	//console.log(params);
	document.getElementsByClassName("popup")[0].id = dialogId;
	
	$(document).ready(function(){
		$('#xyz').attr('value',params[1]);
	});
}

function editFinePopup(params){
	var dialogId = params[0];
	console.log(params[5]);
	document.getElementsByClassName("popupedit")[0].id = dialogId;

	var mydate = new Date(params[5]);
	var month = ["01", "02", "03", "04", "05", "06","07", "08", "09", "10", "11", "12"][mydate.getMonth()];
	console.log(mydate);
	fine_id = params[1]/1.675;
	fine_title = params[2];
	fine_amount = params[3];
	fine_description = params[4];
	fine_incurred_date = mydate.getFullYear() +'-'+ month +'-' + mydate.getDate();
	added_date = params[5];
	updated_date = params[6];
	console.log(mydate.getDate());
	document.getElementById("fine_title").value = fine_title;
	document.getElementById("fine_amount").value = fine_amount;
	document.getElementById("fine_discount").value = fine_discount;
	document.getElementById("fine_description").innerHTML = fine_description;
	document.getElementById("fine_incurred_date").value = fine_incurred_date;
	document.getElementById("xyz").value = fine_id*1.675;

}

function viewFinePopup(params){
	var dialogId = params[0];
	//console.log(params);
	document.getElementsByClassName("popup")[0].id = dialogId;
	console.log(params[2].length);
	if (params[2] != null) {
		for (var i = 0; i<params[2].length; i++) {
			
			var mydate = new Date(params[2][i].fine_incurred_date);
			var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][mydate.getMonth()];
			var month_b = ["1", "2", "3", "4", "5", "6","7", "8", "9", "10", "11", "12"][mydate.getMonth()];

			fine_id = params[2][i].fine_id;
			user_id = params[2][i].user_id;
			fine_title = params[2][i].fine_title;
			fine_amount = params[2][i].fine_amount;
			fine_discount = params[2][i].fine_discount;
			fine_description = params[2][i].fine_description;
			fine_incurred_date = mydate.getFullYear() +'-'+ month +'-' + mydate.getDate();
			added_date = params[2][i].added_date;
			updated_date = params[2][i].updated_date;
			student_name = params[3];
			edit_fine_url = params[2][i].edit_fine_url;
			console.log(params[2][i]);
			edit_url = "editFinePopup(['"+params[2][i].edit_fine_url+"','"+fine_id*1.675+"','"+fine_title+"','"+fine_amount+"','"+fine_description+"','"+fine_incurred_date+"','"+added_date+"','"+updated_date+"'])";
			//del_url = "return confirm("+"'Are you sure you want to delete this fine info ?'"+")";
			delete mydate;
			console.log(edit_url);
			document.getElementById("exampleModalLabel").innerHTML = 'Student Fine info of ' + student_name;
			var parenttbl = document.getElementById("myTableBody");

			var tableRow = document.createElement('tr');

			var fine_title_td = document.createElement('td');
			var fine_amount_td = document.createElement('td');
			var fine_discount_td = document.createElement('td');
			var fine_description_td = document.createElement('td');
			var fine_date_td = document.createElement('td');
			var actionButton = document.createElement('td');
			console.log(fine_incurred_date);
			fine_title_td.innerHTML = fine_title;
			fine_amount_td.innerHTML = fine_amount;
			fine_discount_td.innerHTML = fine_discount;

			fine_description_td.innerHTML = fine_description;
			fine_date_td.innerHTML = fine_incurred_date;

			parenttbl.appendChild(tableRow);
			tableRow.appendChild(fine_title_td);
			tableRow.appendChild(fine_amount_td);
			tableRow.appendChild(fine_description_td);
			tableRow.appendChild(fine_date_td);


			if (params[2] != null) {
				edit_url = "editFinePopup(['"+params[2][i].edit_fine_url+"','"+fine_id*1.675+"','"+fine_title+"','"+fine_amount+"','"+fine_description+"','"+fine_incurred_date+"','"+added_date+"','"+updated_date+"'])";
				del_url = "onDeleteBtnClick(this,'"+ "process/fine"+params[2][i].del_fine_url + "','are you sure you want to delete this fine?');";

				var editFine_button = document.createElement('button');
				var deleteFine_button = document.createElement('a');

				editFine_button.setAttribute('class','btn btn-primary btn-xs pull-right checkbox-toggle');
				editFine_button.setAttribute('data-toggle','modal');
				editFine_button.setAttribute('data-target','#'+params[2][i].edit_fine_url);
				editFine_button.setAttribute('onClick',edit_url);
				editFine_button.innerHTML = 'Edit Fine';

				deleteFine_button.setAttribute('class','btn btn-danger btn-xs pull-right checkbox-toggle');
				deleteFine_button.setAttribute('onClick',del_url);
				deleteFine_button.setAttribute('data-toggle','modal');
				deleteFine_button.setAttribute('data-target','#checkPasswordModal');

				//deleteFine_button.setAttribute('href','process/fine'+params[2][i].del_fine_url);
				deleteFine_button.innerHTML = 'Delete';

				tableRow.appendChild(actionButton).appendChild(editFine_button);
				tableRow.appendChild(actionButton).appendChild(deleteFine_button);

			}
		}
	}else{
		student_name = params[3];
		document.getElementById("exampleModalLabel").innerHTML = 'Student Timeline of ' + student_name;

	}

}

/*Exam*/
function displaySingleExamDetailPopup(params){

	var dialogId = params[0];
	var subject = params[1];
	var full_marks = params[2];
	var pass_marks = params[3];
	var starting_date = params[4];
	var ending_date = params[4];

	
	document.getElementsByClassName("popup")[0].id = dialogId;

	document.getElementById("subject").innerHTML = subject;
	document.getElementById("full_marks").innerHTML = full_marks;
	document.getElementById("pass_marks").innerHTML = pass_marks;
	document.getElementById("starting_date").innerHTML = starting_date;
	document.getElementById("ending_date").innerHTML = ending_date;
}

function displayMultipleExamDetailPopup(params){
	
	var dialogId = params[0];
	var exam_details = params[1];
	var items;
	console.log(params[1]);

	//console.log(params[1][0].exam_id);
	document.getElementsByClassName("popup")[0].id = dialogId;
	for (var i = 0; i<params[1].length; i++) {
		
		theory_subject = params[1][i].subject_name+' ('+params[1][i].subject_code+')';
		practical_subject = params[1][i].practical_subject_name+' ('+params[1][i].practical_subject_code+')';
		full_marks_theory = params[1][i].full_marks_theory;
		pass_marks_theory = params[1][i].pass_marks_theory;
		full_marks_practical = params[1][i].full_marks_practical;
		pass_marks_practical = params[1][i].pass_marks_practical;
		starting_date_theory = params[1][i].starting_date_theory;
		ending_date_theory = params[1][i].ending_date_theory;
		starting_date_practical = params[1][i].starting_date_practical;
		ending_date_practical = params[1][i].ending_date_practical;
		result_detail = params[1][i].result_detail;
		add_result_url = params[1][i].add_result_url;
		edit_result_url = params[1][i].edit_result_url;

		var parenttbl = document.getElementById("myTableBody");

		var tableRow = document.createElement('tr');

		var theory_subject_td = document.createElement('td');
		var full_marks_theory_td = document.createElement('td');
		var pass_marks_theory_td = document.createElement('td');
		var starting_date_theory_td = document.createElement('td');
		var ending_date_theory_td = document.createElement('td');

		var practical_subject_td = document.createElement('td');
		var full_marks_practical_td = document.createElement('td');
		var pass_marks_practical_td = document.createElement('td');

		var starting_date_practical_td = document.createElement('td');
		var ending_date_practical_td = document.createElement('td');
		var actionButton = document.createElement('td');
		var elementid = document.getElementsByTagName("td").length;
	    //subject_td.setAttribute('id',elementid);
	    
	    theory_subject_td.innerHTML = theory_subject;
	    full_marks_theory_td.innerHTML = full_marks_theory;
	    pass_marks_theory_td.innerHTML = pass_marks_theory;

	    if (full_marks_practical == null || full_marks_practical == '') {
	    	full_marks_practical = '-';
	    }

	    if (pass_marks_practical == null || pass_marks_practical == '') {
	    	pass_marks_practical = '-';
	    }

	    if (params[1][i].practical_subject_name == null || params[1][i].practical_subject_name == '' && params[1][i].practical_subject_code == null || params[1][i].practical_subject_code == '') {
	    	practical_subject = '-';
	    	full_marks_practical = '-';
	    	pass_marks_practical = '-';
	    	starting_date_practical = '-';
	    	ending_date_practical = '-';
	    }
	    
	    practical_subject_td.innerHTML = practical_subject;
	    full_marks_practical_td.innerHTML = full_marks_practical;
	    pass_marks_practical_td.innerHTML = pass_marks_practical;

	    starting_date_theory_td.innerHTML = starting_date_theory;
	    ending_date_theory_td.innerHTML = ending_date_theory;

	    starting_date_practical_td.innerHTML = starting_date_practical;
	    ending_date_practical_td.innerHTML = ending_date_practical;
	    
	    parenttbl.appendChild(tableRow);

	    tableRow.appendChild(theory_subject_td);
	    tableRow.appendChild(full_marks_theory_td);
	    tableRow.appendChild(pass_marks_theory_td);
	    tableRow.appendChild(starting_date_theory_td);
	    tableRow.appendChild(ending_date_theory_td);

	    tableRow.appendChild(practical_subject_td);
	    tableRow.appendChild(full_marks_practical_td);
	    tableRow.appendChild(pass_marks_practical_td);
	    tableRow.appendChild(starting_date_practical_td);
	    tableRow.appendChild(ending_date_practical_td);
	    if (result_detail != null) {
	    	var editResult_button = document.createElement('a');
	    	editResult_button.setAttribute('class','btn btn-primary');
	    	editResult_button.setAttribute('href',edit_result_url);
	    	editResult_button.innerHTML = 'Edit Result'
	    	tableRow.appendChild(actionButton).appendChild(editResult_button);
	    }else{
	    	var addResult_button = document.createElement('a');
	    	addResult_button.setAttribute('class','btn btn-primary');
	    	addResult_button.setAttribute('href',add_result_url);
	    	addResult_button.innerHTML = 'Add Result'
	    	tableRow.appendChild(actionButton).appendChild(addResult_button);
	    }
	}
}

function displayExamDetailPopup(params){
	var dialogId = params[0];
	document.getElementsByClassName("popup")[0].id = dialogId;
}

/*Payment*/
function displayPaymentPopup(params){
	var dialogId = params[0];
	var dayscholar_fees_amount = params[1];
	var dayborders_fees_amount = params[2];
	var hosteller_fees_amount = params[3];
	if (params[4] == '') {
		var transportation_cost = '-';
	}else{
		var transportation_cost = 'Rs. '+ params[4] +' /-';
	}
	//var transportation_cost = params[4];

	var fees_id = params[5];

	var fine_amount = params[6];
	var fine_description = params[7];
	var total_amount = params[8];
	var net_total = params[9];
	var paid_amount = params[10];
	var fee_requested_date = params[11]
	document.getElementsByClassName("popup")[0].id = dialogId;
	console.log(params);
	document.getElementById("dayscholar_fees_amount").innerHTML = 'Rs. '+ dayscholar_fees_amount +' /-';
	document.getElementById("dayborders_fees_amount").innerHTML = 'Rs. '+ dayborders_fees_amount +' /-';
	document.getElementById("hosteller_fees_amount").innerHTML = 'Rs. '+ hosteller_fees_amount +' /-';
	document.getElementById("hosteller_fees_amount").innerHTML = 'Rs. '+ hosteller_fees_amount +' /-';
	document.getElementById("transportation_cost").innerHTML = transportation_cost ;

	document.getElementById("fine_amount").innerHTML = 'Rs. '+ fine_amount +' /-';
	document.getElementById("total_amount").innerHTML = 'Rs. '+ total_amount +' /-';
	document.getElementById("paid_amount").innerHTML = 'Rs. '+ paid_amount +' /-';

}

function displayPaymentPopup(params){
	var dialogId = params[0];
	var dayscholar_fees_amount = params[1];
	var dayborders_fees_amount = params[2];
	var hosteller_fees_amount = params[3];
	var fees_id = params[4];

	var fine_amount = params[5];
	var fine_description = params[6];
	var total_amount = params[7];
	var net_total = params[8];
	var paid_amount = params[9];
	var fee_requested_date = params[10];
	document.getElementsByClassName("popupFeesDetail")[0].id = dialogId;
	console.log(params);
	console.log(dayscholar_fees_amount);
	document.getElementById("dayscholar_fees_amount").innerHTML = 'Rs. '+ dayscholar_fees_amount +' /-';
	document.getElementById("dayborders_fees_amount").innerHTML = 'Rs. '+ dayborders_fees_amount +' /-';
	document.getElementById("hosteller_fees_amount").innerHTML = 'Rs. '+ hosteller_fees_amount +' /-';
	console.log(total_amount.length);
	document.getElementById("fine_amount").innerHTML = 'Rs. '+ fine_amount +' /-';
	
	if (paid_amount.length == 0) {
		var total = parseInt(dayscholar_fees_amount) + parseInt(dayborders_fees_amount) + parseInt(hosteller_fees_amount) + parseInt(fine_amount);
		console.log(total);
		document.getElementById("total_amount").innerHTML = 'Rs. '+ total +' /-';
		document.getElementById("paid_amount").innerHTML = '<span class="label label-danger">Unpaid</span>';
	}else{
		document.getElementById("total_amount").innerHTML = 'Rs. '+ total_amount +' /-';
		document.getElementById("paid_amount").innerHTML = 'Rs. '+ paid_amount +' /-';
	}
}

/*Membership*/
function assignStudentMembershipPopup(params){

	var dialogId = params[0];
	
	var admission_no = params[2];
	var student_name = params[3];
	var act = params[4];
	var library_card_no = params[5];

	var title = 'Library Membership for';
	console.log(params);
	document.getElementsByClassName("membershipPopup")[0].id = dialogId;
	

	document.getElementById("admission_no").value = admission_no;
	if (library_card_no != null) {
		document.getElementById("library_card_no").value = library_card_no;
	}else{
		document.getElementById("library_card_no").value = null;
	}
	
	//document.getElementById("student").innerHTML = '  ';
	document.getElementById("student").innerHTML = act + ' ' + title + ' ' + student_name;
	if (act == 'Edit') {
		document.getElementById("submit").value = 'library-member-update';
		document.getElementById("submit").innerHTML = 'Edit Membership';

	}else{
		document.getElementById("submit").value = 'library-member-add';
		document.getElementById("submit").innerHTML = 'Assign Membership';

	}
	
}

function assignStaffMembershipPopup(params){

	var dialogId = params[0];
	var user_id = params[1];
	var staff_name = params[2];
	var act = params[3]
	var library_card_no = params[4];

	var title = 'Library Membership for';
	console.log(params);
	document.getElementsByClassName("staffMembershipPopup")[0].id = dialogId;

	if (library_card_no != null) {
		document.getElementById("library_card_no").value = library_card_no;
	}else{
		document.getElementById("library_card_no").value = null;
	}
	document.getElementById("xyz").value = user_id;
	
	document.getElementById("staff").innerHTML = act + ' ' + title + ' ' + staff_name;
	if (act == 'Edit') {
		document.getElementById("submit").value = 'library-staff-member-update';
		document.getElementById("submit").innerHTML = 'Edit Membership';

	}else{
		document.getElementById("submit").value = 'library-staff-member-add';
		document.getElementById("submit").innerHTML = 'Assign Membership';

	}
	
}

/*Timeline*/
function viewTimelinePopup(params){
	var dialogId = params[0];
	//console.log(params);
	document.getElementsByClassName("popup")[0].id = dialogId;
	console.log(params[2]);
	
	if (params[2] != null) {
		for (var i = 0; i<params[2].length; i++) {
			console.log(params[2][i].timeline_id);
			var mydate = new Date(params[2][i].event_date);
			var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][mydate.getMonth()];
			var month_b = ["1", "2", "3", "4", "5", "6","7", "8", "9", "10", "11", "12"][mydate.getMonth()];

			timeline_id = params[2][i].timeline_id;
			user_id = params[2][i].user_id;
			event_title = params[2][i].event_title;
			event_description = params[2][i].event_description;
			console.log(params[2][i].event_date);
			event_date = mydate.getFullYear() +'-'+ month +'-' + mydate.getDate();
			added_date = params[2][i].added_date;
			updated_date = params[2][i].updated_date;
			student_name = params[3];

			console.log(params[2][i]);
				//edit_timeline_url = params[2][i].edit_timeline_url;
				
				delete mydate;
				document.getElementById("exampleModalLabel").innerHTML = 'Student Timeline of ' + student_name;
				var parenttbl = document.getElementById("myTableBody");

				var tableRow = document.createElement('tr');

				var event_title_td = document.createElement('td');
				var event_description_td = document.createElement('td');
				var event_date_td = document.createElement('td');
				var actionButton = document.createElement('td');
				console.log(event_date_td);
				event_title_td.innerHTML = event_title;
				event_description_td.innerHTML = event_description;
				event_date_td.innerHTML = event_date;

				parenttbl.appendChild(tableRow);
				tableRow.appendChild(event_title_td);
				tableRow.appendChild(event_description_td);
				tableRow.appendChild(event_date_td);


				if (params[2] != null) {
					edit_url = "editTimelinePopup(['"+params[2][i].edit_timeline_url+"','"+timeline_id*1.675+"','"+event_title+"','"+event_description+"','"+event_date+"','"+added_date+"','"+updated_date+"'])";
					//del_url = "return confirm("+"'Are you sure you want to delete this timeline?'"+")";
					del_url = "onDeleteBtnClick(this,'"+ "process/timeline"+params[2][i].del_timeline_url + "','are you sure you want to delete this timeline?');";
					console.log(del_url);
					var editTimeline_button = document.createElement('button');
					var deleteTimeline_button = document.createElement('a');

					editTimeline_button.setAttribute('class','btn btn-primary btn-sm pull-right checkbox-toggle');
					editTimeline_button.setAttribute('data-toggle','modal');
					editTimeline_button.setAttribute('data-target','#'+params[2][i].edit_timeline_url);
					editTimeline_button.setAttribute('onClick',edit_url);
					editTimeline_button.innerHTML = 'Edit Timeline';

					deleteTimeline_button.setAttribute('type','button');
					deleteTimeline_button.setAttribute('class','btn btn-danger btn-sm pull-right checkbox-toggle');
					deleteTimeline_button.setAttribute('onClick',del_url);
					deleteTimeline_button.setAttribute('data-toggle','modal');
					deleteTimeline_button.setAttribute('data-target','#checkPasswordModal');
					deleteTimeline_button.innerHTML = 'Delete';
					console.log('deleteTimeline_button>>',deleteTimeline_button);
					//deleteTimeline_button.setAttribute('href','process/timeline'+params[2][i].del_timeline_url);

					tableRow.appendChild(actionButton).appendChild(editTimeline_button);
					tableRow.appendChild(actionButton).appendChild(deleteTimeline_button);

				}
			}
		}else{
			student_name = params[3];
			document.getElementById("exampleModalLabel").innerHTML = 'Student Timeline of ' + student_name;

		}

	}

	function viewTimelinePopupForLibrarian(params){
		var dialogId = params[0];
	//console.log(params);
	document.getElementsByClassName("popup")[0].id = dialogId;
	console.log(params);
	
	if (params[2] != null) {
		for (var i = 0; i<params[2].length; i++) {
			console.log(params[2][i].timeline_id);
			var mydate = new Date(params[2][i].event_date);
			var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][mydate.getMonth()];
			var month_b = ["1", "2", "3", "4", "5", "6","7", "8", "9", "10", "11", "12"][mydate.getMonth()];

			timeline_id = params[2][i].timeline_id;
			user_id = params[2][i].user_id;
			event_title = params[2][i].event_title;
			event_description = params[2][i].event_description;
			console.log(params[2][i].event_date);
			event_date = mydate.getFullYear() +'-'+ month +'-' + mydate.getDate();
			added_date = params[2][i].added_date;
			updated_date = params[2][i].updated_date;
			student_name = params[3];
				//edit_timeline_url = params[2][i].edit_timeline_url;
				edit_url = "editTimelinePopup(['"+params[2][i].edit_timeline_url+"','"+timeline_id*1.675+"','"+event_title+"','"+event_description+"','"+event_date+"','"+added_date+"','"+updated_date+"'])";
				del_url = "return confirm("+"'Are you sure you want to delete this timeline?'"+")";
				delete mydate;
				document.getElementById("exampleModalLabel").innerHTML = 'Student Timeline of ' + student_name;
				var parenttbl = document.getElementById("myTableBody");

				var tableRow = document.createElement('tr');
				var event_title_td = document.createElement('td');
				var event_description_td = document.createElement('td');
				var event_date_td = document.createElement('td');

				event_title_td.innerHTML = event_title;
				event_description_td.innerHTML = event_description;
				event_date_td.innerHTML = event_date;

				parenttbl.appendChild(tableRow);
				tableRow.appendChild(event_title_td);
				tableRow.appendChild(event_description_td);
				tableRow.appendChild(event_date_td);

			}
		}else{
			student_name = params[3];
			document.getElementById("exampleModalLabel").innerHTML = 'Student Timeline of ' + student_name;

		}
	}

	function addAchievementPopup(params){
		var dialogId = params[0];
		console.log(params);
		document.getElementsByClassName("popupAdd")[0].id = dialogId;
		sessionStorage.setItem("userIdForAddingTimeline", params[1]);
		$(document).ready(function(){
			$('#xyz').attr('value',params[1]);
			$('.xyz').attr('value',params[1]);

		});	
		console.log(userIdForAddingTimeline);
	}

	function editAchievementPopup(params){
		var dialogId = params[0];
		console.log(params);
		document.getElementsByClassName("popupedit")[0].id = dialogId;

		var mydate = new Date(params[4]);
		//var month = ["01", "02", "03", "04", "05", "06","07", "08", "09", "10", "11", "12"][mydate.getMonth()];
		timeline_id = params[1]/1.675;
		event_title = params[2];
		event_description = params[3];

		var month = ((mydate.getMonth()+1)>=10)? (mydate.getMonth()+1) : '0' + (mydate.getMonth()+1);  
		var date = ((mydate.getDate())>=10)? (mydate.getDate()) : '0' + (mydate.getDate());

		event_date_form = mydate.getFullYear() +'-'+ month +'-' + date ;
		added_date = params[5];
		updated_date = params[6];
		console.log('timeline_id>>',timeline_id);
		console.log('event_date_form>>',event_date_form);

		document.getElementById("event_title").value = event_title;
		document.getElementById("event_description").innerHTML = event_description;
		document.getElementById("event_date").value = event_date_form;
		document.getElementById("xyz").value = timeline_id*1.675;

	}
	/*Marksheet Management*/
	function changeExamPercentageForFirstTerm($value){
		$('#firstTermExamPercentage').val(100 - $value) ;
		console.log(100 - $value);
	}

	function changeUnitTestExamPercentageForFirstTerm($value){
		$('#firstTermUnitTestExamPercentage').val(100 - $value) ;
		console.log(100 - $value);
	}

	function changeExamPercentageForSecondTerm($value){
		$('#secondTermExamPercentage').val(100 - $value) ;
		console.log(100 - $value);
	}

	function changeUnitTestExamPercentageForSecondTerm($value){
		$('#secondTermUnitTestExamPercentage').val(100 - $value) ;
		console.log(100 - $value);
	}

	function changeExamPercentageForFinalTerm($value){
		$('#finalTermExamPercentage').val(100 - $value) ;
		console.log(100 - $value);
	}

	function changeUnitTestExamPercentageForFinalTerm($value){
		$('#finalTermUnitTestExamPercentage').val(100 - $value) ;
		console.log(100 - $value);
	}

	function onFirstTermExamPercentageForFinalTermChange($value){
		$('#finalTermExamPercentageForFinalTerm').val(100 - $('#secondTermExamPercentageForFinalTerm').val() - $value) ;
		$('#secondTermExamPercentageForFinalTerm').val(100 - $('#finalTermExamPercentageForFinalTerm').val() - $value) ;

		console.log(100 - $value);
	}

	function onSecondTermExamPercentageForFinalTermChange($value){
		$('#finalTermExamPercentageForFinalTerm').val(100 - $('#firstTermExamPercentageForFinalTerm').val() - $value) ;
		$('#firstTermExamPercentageForFinalTerm').val(100 - $('#finalTermExamPercentageForFinalTerm').val() - $value) ;

		console.log(100 - $value);
	}

	function onFinalTermExamPercentageForFinalTermChange($value){
		$('#secondTermExamPercentageForFinalTerm').val(100 - $('#firstTermExamPercentageForFinalTerm').val() - $value) ;
		$('#firstTermExamPercentageForFinalTerm').val(100 - $('#secondTermExamPercentageForFinalTerm').val() - $value) ;

		console.log(100 - $value);
	}

	function onFullMarksThChange($value){
		$('#full_marks_pr').val(100 - $value) ;

		console.log(100 - $value);
	}

/*function onPassMarksThChange($value){
    $('#full_marks_pr').val(100 - $value) ;
   	console.log(100 - $value);
   }*/

   function onFullMarksPrChange($value){
   	$('#full_marks_th').val(100 - $value) ;

   	console.log(100 - $value);
   }

/*function onPassMarksPrChange($value){
    $('#full_marks_pr').val(100 - $value) ;

   	console.log(100 - $value);
   }*/
   function onDeleteBtnClick(el,url,info){
   	console.log('url>>',url);
   	console.log('info>>',info);
   	console.log('this>>',el);   	
   	var myformSubmitBtnId = $('#myformSubmitBtnId');

   	var tagname = myformSubmitBtnId.prop("tagName");
   	if (myformSubmitBtnId && tagname == 'A') {
   		window.location.href = url;
   	}
   	/*$('#checkPasswordModal div div div h3')[0].innerHTML = info;*/
   }

   function onAnchorTagClick(el,url,info){
   	console.log('url>>',url);
   	console.log('info>>',info);
   	console.log('this>>',el);   	
   	var myformSubmitBtnId = $('#myformSubmitBtnId');

   	var tagname = myformSubmitBtnId.prop("tagName");
   	if (myformSubmitBtnId && tagname == 'A') {
   		window.location.href = url;
   	}
   	/*$('#checkPasswordModal div div div h3')[0].innerHTML = info;*/
   }

   function onAddBtnClick(el,url,info){
   	console.log('url>>',url);
   	console.log('info>>',info);
   	console.log('this>>',el);   	
   	var myformSubmitBtnId = $('#myformSubmitBtnId');

   	var tagname = myformSubmitBtnId.prop("tagName");
   	if (myformSubmitBtnId && tagname == 'A') {
   		window.location.href = url;
   	}
   }

   function onEditBtnClick(el,url,info){
   	console.log('url>>',url);
   	console.log('info>>',info);
   	console.log('this>>',el);   	
   	var myformSubmitBtnId = $('#myformSubmitBtnId');

   	var tagname = myformSubmitBtnId.prop("tagName");
   	if (myformSubmitBtnId && tagname == 'A') {
   		window.location.href = url;
   	}
   }

   function getAllClassName(){
   	$(document).ready(function () {
   		
   		readRecords();
   	});

   	// $(document).ready(function () {
   	// 	$('#section').change(function(){ 

   	// 		$('#manage-class-form').click();

   	// 	});
   	// });
   	function readRecords(){

   		var readrecords = "readrecords";
   		$.ajax({
   			url:"./process/class.php",
   			type:"POST",
   			data:{postType:'getAllClassName'},
   			success:function(data,status){
   				console.log('AllCLassName>>',data);
   				//console.log(excludingClassIdJsonArray);
   				if(data){
   					dataObj = JSON.parse(data);
   					console.log('dataObj>>',dataObj.length);
   					classesOptgroup = $('#classes optgroup');
   					batchOptgroup = $('#batch optgroup');

   					classesOptgroup.empty();
   					batchOptgroup.empty();


   					if(dataObj.length != 0){

   						classNameArray = unique(arrayColumn(dataObj,'class_name'));
   						batchArray = unique(arrayColumn(dataObj,'batch'));

   						console.log('classNameArray',classNameArray);
   						console.log('batchArray',batchArray);


   						classNameArray.forEach(function(item,index){

   							var option = $("<option></option>");
   							option.val(item);
   							option.html(item);
   							classesOptgroup.append(option);

   						});

   						batchArray.forEach(function(item,index){

   							var option = $("<option></option>");
   							option.val(item);
   							option.html(item);
   							batchOptgroup.append(option);

   						});
   					}
   					

   					$('#classes').selectpicker('default');
   					$('#classes').selectpicker('refresh');

   				}
   			},

   		});
   	}
   }

   function getClassNameByBatch(){
   	$(document).ready(function () {
   		
   		if ($('#batch').val()) {
   			var batch_name = $('#batch').val();
   			readRecords(batch_name); 

   		}
   		
   		$('#batch').change(function(){ 
   			var batch_name = $(this).val();
   			console.log('batchName>>',batch_name);

   			readRecords(batch_name); 

   		});
   	});

   	// $(document).ready(function () {
   	// 	$('#section').change(function(){ 

   	// 		$('#manage-class-form').click();

   	// 	});
   	// });
   	function readRecords(batch_name){

   		var readrecords = "readrecords";
   		$.ajax({
   			url:"./process/class.php",
   			type:"POST",
   			data:{batchName:batch_name},
   			success:function(data,status){
   				//console.log('ClassNameByBatch>>',data);
   				//console.log(excludingClassIdJsonArray);
   				var i = 0;
   				
   				dataObj = JSON.parse(data);

   				
   				//var obj = JSON.parse(excludingClassIdJsonArray);
   				//console.log(obj);
   				//var result = Object.keys(obj).map(function(key) {
   					//return obj[key];
   				//});
   				console.log('ClassNameByBatch>>',dataObj);
   				className = unique(arrayColumn(dataObj,'class_name'));
   				console.log(className);

   				var classOpt = $('#classOpt');
   				classOpt.empty();

   				if (dataObj) {
   					for ( var i = 0; i < className.length; i++) {
	   					//console.log("The URL of this page is: " + window.location.href);
	   					var option = document.createElement('option');

	   					var sPath = window.location.pathname;
	   					var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
	   					console.log('className>>',className[i]);

	   					option.setAttribute("value",className[i]);
	   					option.setAttribute("onclick","getClassData(this);");
	   					option.innerHTML = className[i];

	   					classOpt.append(option);

	   				}

	   				$('.selectpicker').selectpicker('default');
	   				$('.selectpicker').selectpicker('refresh');
	   			}

	   			
	   		},

	   	});
   	}
   }

   function getBatchByClassNameAndStartingMonth(){
   	$(document).ready(function () {
   		
   		if ($('#startingMonth').val()) {
   			var startingMonth = $('#startingMonth').val();

   			className = $('#classes');
   			if(className.val()){
   				class_name = className.val();
   			}else{
   				class_name = null ;

   			}
   			console.log('className>>',class_name);
   			console.log('startingMonth>>',startingMonth);

   			readRecords(class_name,starting_month); 

   		}
   		
   		$('#startingMonth').change(function(){ 
   			var startingMonth = $(this).val();

   			className = $('#classes');
   			if(className.val()){
   				class_name = className.val();
   			}else{
   				class_name = null ;

   			}
   			console.log('className>>',class_name);
   			console.log('startingMonth>>',startingMonth);

   			readRecords(class_name,starting_month); 

   		});
   	});

   	// $(document).ready(function () {
   	// 	$('#section').change(function(){ 

   	// 		$('#manage-class-form').click();

   	// 	});
   	// });
   	function readRecords(class_name,startingMonth = null){

   		var readrecords = "readrecords";
   		$.ajax({
   			url:"./process/class.php",
   			type:"POST",
   			data:{className:class_name,startingMonth:startingMonth,postType:'getBatchByClassNameAndStartingMonth'},
   			success:function(data,status){
   				//console.log('ClassNameByBatch>>',data);
   				//console.log(excludingClassIdJsonArray);
   				var i = 0;
   				
   				dataObj = JSON.parse(data);

   				
   				//var obj = JSON.parse(excludingClassIdJsonArray);
   				//console.log(obj);
   				//var result = Object.keys(obj).map(function(key) {
   					//return obj[key];
   				//});
   				console.log('batchByClassName>>',dataObj);
   				batch = unique(arrayColumn(dataObj,'batch'));
   				// console.log(batch);

   				var batchOpt = $('#batchOpt');
   				// var classOpt = $('#classOpt');
   				
   				// classOpt.empty();

   				batchOpt.empty();

   				if (dataObj) {
   					for ( var i = 0; i < batch.length; i++) {
	   					//console.log("The URL of this page is: " + window.location.href);
	   					var option = document.createElement('option');

	   					var sPath = window.location.pathname;
	   					var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
	   					console.log('batch>>',batch[i]);

	   					option.setAttribute("value",batch[i]);
	   					option.setAttribute("onclick","getClassData(this);");
	   					option.innerHTML = batch[i];

	   					batchOpt.append(option);

	   				}

	   				$('.selectpicker').selectpicker('default');
	   				$('.selectpicker').selectpicker('refresh');
	   			}

	   			
	   		},

	   	});
   	}
   }

   function getBatchAndClassNameByStartingMonth(){
   	$(document).ready(function () {

   		if ($('#startingMonth').val()) {
   			var startingMonth = $('#startingMonth').val();

   			
   			readRecords(startingMonth); 

   		}else{
   			startingMonth = null;
   			readRecords(startingMonth); 

   		}

   		$("#startingMonth").bind('keyup', function (e) {   
   			startingMonth = null ;
   			readRecords(startingMonth); 

   		});

   		$('#startingMonth').change(function(){ 
   			var startingMonth = $(this).val();
   			
   			if(startingMonth){
   				readRecords(startingMonth); 

   			}else{
   				startingMonth = null ;
   				readRecords(startingMonth); 

   			}


   		});
   	});

   	// $(document).ready(function () {
   	// 	$('#section').change(function(){ 

   	// 		$('#manage-class-form').click();

   	// 	});
   	// });
   	function readRecords(startingMonth){

   		var readrecords = "readrecords";
   		$.ajax({
   			url:"./process/class.php",
   			type:"POST",
   			data:{startingMonth:startingMonth,postType:'getBatchAndClassNameByStartingMonth'},
   			success:function(data,status){
   				//console.log('ClassNameByBatch>>',data);
   				//console.log(excludingClassIdJsonArray);
   				var i = 0;
   				if(data){
   					dataObj = JSON.parse(data);
   					console.log('dataObj>>',dataObj.length);
   					classesOptgroup = $('#classes optgroup');
   					batchOptgroup = $('#batch optgroup');

   					classesOptgroup.empty();
   					batchOptgroup.empty();


   					if(dataObj.length != 0){
   						changableBatch = dataObj[0].batch;
   						console.log('changableBatch>>',changableBatch);
   						classNameArray = unique(arrayColumn(dataObj,'class_name'));
   						batchArray = unique(arrayColumn(dataObj,'batch'));

   						// dataObj.forEach(function(item,index){
   						// 	console.log('changableClassName>>',item.class_name);
   						// 	var classNameOption = $("<option></option>");
   						// 	classNameOption.val(item.class_name);
   						// 	classNameOption.html(item.class_name);
   						// 	classesOptgroup.append(classNameOption);

   						// });

   						// var batchOption = $("<option></option>");
   						// batchOption.val(dataObj[0].batch);
   						// batchOption.html(dataObj[0].batch);
   						// batchOptgroup.append(batchOption);
   						console.log('classNameArray',classNameArray);
   						console.log('batchArray',batchArray);


   						classNameArray.forEach(function(item,index){

   							var option = $("<option></option>");
   							option.val(item);
   							option.html(item);
   							classesOptgroup.append(option);

   						});

   						batchArray.forEach(function(item,index){

   							var option = $("<option></option>");
   							option.val(item);
   							option.html(item);
   							batchOptgroup.append(option);

   						});

   					}


   					

   					$('.selectpicker').selectpicker('default');
   					$('.selectpicker').selectpicker('refresh');

   				}

   			},

   		});
   	}
   }

   function navigateExamRoutine(id){
   	console.log('id>>',id);
   	var class_name = $(this).val();
   	batchid = '#batch-'+id;
   	classOptid = '#classOpt-'+id;
   	option = '#classOpt-'+id;

   	var batch = $(batchid).val();
   	var classOpt = $(classOptid);
   	var option = $()
   	if (!batch) {
   		batch = false;
   	}
   	console.log('batch>>',batch);
   	console.log('className>>',className);


   			// console.log('class_name>>',class_name);
   			// readRecords(class_name,batch,false,false); 


   		}
   		/*manage-class js*/
   		function manageClassJs(examCategoryNo = false,excludingClassIdJsonArray = false){
   			$(document).ready(function () {
   				$('#classes').change(function(){ 
   					var class_name = $(this).val();
   					var batch = $('#batch').val();
   					var examRoutineClassesId = $('#examRoutineClassesId').val();

   					if (!batch) {
   						batch = false;
   					}
   					console.log('batch>>',batch);

   					console.log('class_name>>',class_name);
   					postType = 'readRecordsForExamList';

   					readRecords(class_name,batch,postType,examRoutineClassesId); 

   				});

   				$('#classesForStudentUpgrade').change(function(){ 
   					var class_name = $(this).val();
   					var batch = $('#batch').val();
   					optgroupId = $(this).find('optgroup').attr('id');
   					console.log('optgroupId>>',optgroupId);
   					if (optgroupId == 'upgrade') {
   						postType = 'classesForStudentUpgrade';
   					}else if(optgroupId == 'downgrade'){
   						postType = 'classesForStudentDowngrade';

   					}
   					readRecords(class_name,batch,postType,false); 

   				});

   				$('#classesForStudentDowngrade').change(function(){ 
   					var class_name = $(this).val();
   					var batch = $('#batch').val();
   					optgroupId = $(this).find('optgroup').attr('id');
   					console.log('optgroupId>>',optgroupId);
   					if (optgroupId == 'upgrade') {
   						postType = 'classesForStudentUpgrade';
   					}else if(optgroupId == 'downgrade'){
   						postType = 'classesForStudentDowngrade';

   					}
   					readRecords(class_name,batch,postType,false); 

   				});
   			});

   			$(document).ready(function () {
   				$('#section').change(function(){ 

   					$('#manage-class-form').click();

   				});
   			});
   			function readRecords(class_name,batch = false,postType = false,excludingClassIdJsonArray = false){

   				var readrecords = "readrecords";
   				$.ajax({
   					url:"./process/class.php",
   					type:"POST",
   					data:{className:class_name,batch:batch},
   					success:function(data,status){
   						console.log(data);
   				//console.log(excludingClassIdJsonArray);
   				var i = 0;
   				
   				dataObj = JSON.parse(data);
   				console.log('postType>>',postType);

   				if (postType == 'classesForStudentUpgrade') {
   					$('#sectionOptForUpgrade').empty();
   				}else if(postType == 'classesForStudentDowngrade'){

   					$('#sectionOptForDowngrade').empty();

   				}else{

   					$('#sectionOpt').empty();
   					
   				}
   				if (postType != 'readRecordsForExamList') {
   					if(excludingClassIdJsonArray){

   						var obj = JSON.parse(excludingClassIdJsonArray);
   						//console.log(obj);
   						var result = Object.keys(obj).map(function(key) {
   							return obj[key];
   						});
   						//console.log(result);
   					}

   				}

   				for ( var i = 0; i < dataObj.length; i++) {
   					//console.log("The URL of this page is: " + window.location.href);
   					var sPath = window.location.pathname;
   					var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
   					console.log('currentPage>>',currentPage);
   					if (Array.isArray(result) && result.length) {
   						if (result.includes(dataObj[i].class_id)) {

   							var option = document.createElement('option');
   							if (currentPage == 'add_result') {
   								option.setAttribute("value",dataObj[i].subject_id*1.675);
   								option.setAttribute("onclick","getSubjectExamListJs();");

   							}else{
   								option.setAttribute("value",dataObj[i].class_id*1.675);
   								option.setAttribute("onclick","postData();");

   							}

   							option.innerHTML = dataObj[i].class_section;
   							//console.log(i);
   							if (postType == 'classesForStudentUpgrade') {
   								$('#sectionOptForUpgrade').append(option);
   							}else if(postType == 'classesForStudentDowngrade'){

   								$('#sectionOptForDowngrade').append(option);

   							}else{
   								$('#sectionOpt').append(option);
   							}
   						}
   					}else{
   						var option = document.createElement('option');
   						if (currentPage == 'add_result') {
   							option.setAttribute("value",dataObj[i].subject_id*1.675);
   							option.setAttribute("onclick","getSubjectExamListJs();");
   							option.innerHTML = dataObj[i].class_section;

   						}else if(currentPage == 'exam_routine'){
   							if(postType == 'readRecordsForExamList'){
   								console.log('here');

   								console.log('excludingClassIdJsonArray1>>',excludingClassIdJsonArray);
   								var examRoutineClassesId =  excludingClassIdJsonArray.replace(/'/g, '"');
   								
   								var examRoutineClassesIdObj = JSON.parse(examRoutineClassesId);
   								console.log('examRoutineClassesIdObj>>',examRoutineClassesIdObj);

   								$.each(examRoutineClassesIdObj, function(index, value) {
   									console.log('examRoutineClassesIdObjValue>>',value);
   									if(value == dataObj[i].class_id){
   										option.setAttribute("value",dataObj[i].class_id.toString()*1.675);
   										option.innerHTML = dataObj[i].class_section;
   									}
   								});
   							}

   						}else{
   							var a = parseInt(dataObj[i].class_id);
   							console.log(a*1.675);
   							console.log(typeof(dataObj[i].class_id));

   							console.log(Math.floor(a));


   							option.setAttribute("value",parseInt(dataObj[i].class_id)*1.675);
   							//option.setAttribute("onclick","postData();");
   							option.innerHTML = dataObj[i].class_section;

   						}


   						if (postType == 'classesForStudentUpgrade') {

   							$('#sectionOptForUpgrade').append(option);
   						}else if(postType == 'classesForStudentDowngrade'){
   							$('#sectionOptForDowngrade').append(option);

   						}else{
   							if(postType == 'readRecordsForExamList'){
   								if(option.value){
   									$('#sectionOpt').append(option);
   								}
   							}else{
   								$('#sectionOpt').append(option);
   							}
   						}
   					}
   				}
   				$('.selectpicker').selectpicker('default');
   				$('.selectpicker').selectpicker('refresh');

   				/*$('#sectionOpt').append*/
   			},

   		});
}
}

function manageSubjectJs(){
	$(document).ready(function () {
		$('#class_id').change(function(){ 
			var classId = $(this).val();
			var batch = $('#batch').val();

			if (!batch) {
				batch = false;
			}
			console.log('batch>>',batch);
			var postType = 'manageSubjectJs';
			console.log('classId>>',classId);
			readRecords(classId,batch,postType); 

		});
	});
	function readRecords(classId,batch,postType){

		var readrecords = "readrecords";
		$.ajax({
			url:"./process/subject.php",
			type:"POST",
			data:{classId:classId,batch:batch,postType:postType},
			success:function(data,status){
   				//console.log(data);
   				
   				var i = 0;
   				
   				dataObj = JSON.parse(data);
   				console.log('postType>>',dataObj);
   				if (postType == 'manageSubjectJs') {
   					$('#subjectOpt').empty();
   				}

   				for ( var i = 0; i < dataObj.length; i++) {
   					//console.log("The URL of this page is: " + window.location.href);
   					var sPath = window.location.pathname;
   					var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);

   					var option = document.createElement('option');
   					if (currentPage == 'add_assignment' || currentPage == 'add_homework') {
   						option.setAttribute("value",dataObj[i].subject_id*1.675);
   						option.innerHTML = dataObj[i].subject_name;
   						$('#subjectOpt').append(option);
   					}
   				}
   				$('.selectpicker').selectpicker('default');
   				$('.selectpicker').selectpicker('refresh');
   			},

   		});
	}
}

function getClassByBatchAndClassName(){
	$(document).ready(function () {
		$('#batch').change(function(){ 
			var batch = $(this).val();
			var className = $('#classes').val();

			if (!batch) {
				batch = false;
			}
			console.log('batch>>',batch);
			console.log('className>>',className);

			var postType = 'getSectionByBatchAndClassName';

			readRecords(className,batch,postType); 

		});
	});
	function readRecords(className,batch,postType){

		var readrecords = "readrecords";
		$.ajax({
			url:"./process/class.php",
			type:"POST",
			data:{className:className,batch:batch,postType:postType},
			success:function(data,status){
   				//console.log(data);
   				
   				var i = 0;
   				
   				dataObj = JSON.parse(data);
   				console.log('postType>>',dataObj);

   				$('#sectionOpt').empty();

   				classSection = unique(arrayColumn(dataObj,'class_section'));

   				for ( var i = 0; i < dataObj.length; i++) {

   					var sPath = window.location.pathname;
   					var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);

   					var option = document.createElement('option');
   					option.setAttribute("value",dataObj[i].class_id*1.675);
   					option.innerHTML = dataObj[i].class_section;
   					$('#sectionOpt').append(option);
   				}
   				$('.selectpicker').selectpicker('default');
   				$('.selectpicker').selectpicker('refresh');
   			},

   		});
	}
}



function getClassStudentJs(examTypeNo = null,postType = 'readClassStudentRecords',excludingClassIdJsonArray = false){
	$(document).ready(function () {
   		$('#section').change(function(){ //when value is changed in class field by user
   			var class_id = $(this).val();
   			console.log('class_id',class_id);
   			
   			readClassStudentRecords(class_id,postType,excludingClassIdJsonArray); 

   			

   		});

   		console.log($('#section').val());
   		if($('#section').val()){ //when there is already value in class
   			var class_id = $('#section').val();
   			console.log('here');
   			readClassStudentRecords(class_id,postType,excludingClassIdJsonArray); 

   			
   		}
   	});

	function readClassStudentRecords(class_id,postType = 'readClassStudentRecords'){

		var readrecords = "readrecords";
		$.ajax({
			url:"./process/class.php",
			type:"POST",
			data:{classID:class_id,postType:postType},
			success:function(data,status){
				console.log(data);
		   				//console.log(excludingClassIdJsonArray);
		   				var i = 0;
		   				var	studentIdArray = [] ;
		   				
		   				dataObj = JSON.parse(data);
		   				console.log(dataObj);
		   				$('#studentID').empty();


		   				for ( var i = 0; i < dataObj.length; i++) {
		   					var sPath = window.location.pathname;
							//var sPage = sPath.substring(sPath.lastIndexOf('\\') + 1);
							var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);

							

							if(currentPage == 'student-individual-attendance-report'){
								if(!studentIdArray.includes(dataObj[i].student_id)){
									console.log(studentIdArray)
									console.log('currentPage>>',currentPage);
									var option = document.createElement('option');
									option.setAttribute("value",dataObj[i].student_id*1.675);

									studentIdArray.push(dataObj[i].student_id);

									option.innerHTML = '#'+ dataObj[i].student_id + ' ' + dataObj[i].student_name;

								}
							}


							console.log(i);
							$('#studentID').append(option);

						}
						//$('.selectpicker').selectpicker('default');
						//$('.selectpicker').selectpicker('refresh');

						/*$('#sectionOpt').append*/
					},

				});
	}
}

function getClassSubjectJs(examTypeNo = null,postType = 'readClassSubjectRecords',excludingClassIdJsonArray = false){
	$(document).ready(function () {
   		$('#classes').change(function(){ //when value is changed in class field by user
   			var class_name = $(this).val();
   			var batch = $('#batch').val();

   			if (!batch) {
   				batch = false;
   			}
   			console.log('class_name',class_name);
   			console.log('batch>>',batch);

   			if (examTypeNo) {
   				readClassSubjectByExamTypeNoRecords(class_name,examTypeNo,postType,excludingClassIdJsonArray); 

   			}else{
   				if (batch) {
   					readClassSubjectRecords(class_name,batch,postType,excludingClassIdJsonArray); 

   				}else{
   					readClassSubjectRecords(class_name,batch,postType,excludingClassIdJsonArray); 

   				}

   			}

   		});

   		console.log($('#classes').val());
   		if($('#classes').val()){ //when there is already value in class
   			var class_name = $('#classes').val();
   			var batch = $('#batch').val();

   			if (!batch) {
   				batch = false;
   			}
   			console.log('class_name',class_name);
   			if (examTypeNo) {
   				readClassSubjectByExamTypeNoRecords(class_name,examTypeNo,postType,excludingClassIdJsonArray); 

   			}else{
   				readClassSubjectRecords(class_name,batch,postType,excludingClassIdJsonArray); 

   			}
   		}
   	});
}
function readClassSubjectRecords(class_name,batch = false,postType = 'readClassSubjectRecords',excludingClassIdJsonArray = false){

	var readrecords = "readrecords";
	$.ajax({
		url:"./process/class.php",
		type:"POST",
		data:{className:class_name,postType:postType,batch:batch},
		success:function(data,status){
		   				//console.log(excludingClassIdJsonArray);
		   				console.log(data);

		   				if (data) {

		   					var i = 0;
		   					var	subjectIdArray = [] ;

		   					dataObj = JSON.parse(data);
		   					console.log(dataObj);
		   					$('#sectionOpt').empty();
		   					var obj = JSON.parse(excludingClassIdJsonArray);
		   					console.log(obj);
		   					var result = Object.keys(obj).map(function(key) {
		   						return obj[key];
		   					});
			   				//console.log(result);
			   				var subject;
			   				var sPath = window.location.pathname;
							//var sPage = sPath.substring(sPath.lastIndexOf('\\') + 1);
							var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
							for ( var i = 0; i < dataObj.length; i++) {
								if (Array.isArray(result) && result.length) {
									if (result.includes(dataObj[i].subject_id)) {

										if(dataObj[i].result_id == null && currentPage == 'add_result'){
											if(dataObj[i].subject_id != null && !subjectIdArray.includes(dataObj[i].subject_id)){
												console.log('subjectIdArray>>',subjectIdArray);
												console.log('currentPage>>',currentPage);
												var option = document.createElement('option');
												if (currentPage == 'add_result') {
													console.log(currentPage);

													option.setAttribute("value",dataObj[i].subject_id*1.675);
													option.setAttribute("onclick","getSubjectExamListJs();");
												}else{
													option.setAttribute("value",dataObj[i].class_id*1.675);
													option.setAttribute("onclick","postData();");
												}
												subjectIdArray.push(dataObj[i].subject_id);

												option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')';

											}
									}/*else{
										if(!subjectIdArray.includes(dataObj[i].subject_id)){
											console.log(subjectIdArray)
											console.log('currentPage>>',currentPage);
											var option = document.createElement('option');
											if (currentPage == 'edit_result') {
												console.log(currentPage);

												option.setAttribute("value",dataObj[i].subject_id*1.675);
												option.setAttribute("onclick","getSubjectExamListJs();");
											}else{
												option.setAttribute("value",dataObj[i].class_id*1.675);
												option.setAttribute("onclick","postData();");
											}
											subjectIdArray.push(dataObj[i].subject_id);

											option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')';

										}
										

									}*/
									

									console.log('option>>',option);
									$('#sectionOpt').append(option);
								}
							}else{


								if(dataObj[i].result_id == null){
									if(dataObj[i].subject_id != null && !subjectIdArray.includes(dataObj[i].subject_id)){
										console.log(subjectIdArray)

										var option = document.createElement('option');
										if (currentPage == 'add_result') {
											console.log(currentPage);

											option.setAttribute("value",dataObj[i].subject_id*1.675);
											option.setAttribute("onclick","getSubjectExamListJs();");
										}else{
											option.setAttribute("value",dataObj[i].class_id*1.675);
											option.setAttribute("onclick","postData();");
										}
										subjectIdArray.push(dataObj[i].subject_id);

										option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')';

									}
								}else{

									if(dataObj[i].subject_id != null && !subjectIdArray.includes(dataObj[i].subject_id)){

										var option = document.createElement('option');
										if (currentPage == 'edit_result') {
											console.log(currentPage);

											option.setAttribute("value",dataObj[i].subject_id*1.675);
											option.setAttribute("onclick","getSubjectExamListJs();");

										}else{
											option.setAttribute("value",dataObj[i].class_id*1.675);
											option.setAttribute("onclick","postData();");

										}
										subjectIdArray.push(dataObj[i].subject_id);
										

										option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')*';


									}

								}


								$('#sectionOpt').append(option);
							}
						}
						$('.selectpicker').selectpicker('default');
						$('.selectpicker').selectpicker('refresh');

						/*$('#sectionOpt').append*/
					}

				},

			});
}

function readRecordsForExamList(class_name,batch = false,sn,examRoutineClassesId){

	var readrecords = "readrecords";
	$.ajax({
		url:"./process/class.php",
		type:"POST",
		data:{className:class_name,batch:batch},
		success:function(data,status){
			console.log(data);
   				//console.log(excludingClassIdJsonArray);
   				var i = 0;
   				
   				dataObj = JSON.parse(data);

   				
   				$('#sectionOpt-'+sn).empty();
   				var examRoutineClassesIdObj = JSON.parse(examRoutineClassesId);
   				console.log(examRoutineClassesIdObj);
   				console.log('examRoutineClassesIdObj>>',examRoutineClassesIdObj);

   				var option = document.createElement('option');
   				option.innerHTML = 'Select section';
   				$('#sectionOpt-'+sn).append(option);

   				for ( var i = 0; i < dataObj.length; i++) {
   					//console.log("The URL of this page is: " + window.location.href);
   					var sPath = window.location.pathname;
   					var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);

   					
   					$.each(examRoutineClassesIdObj, function(index, value) {
   						console.log('examRoutineClassesIdObjValue>>',value);
   						if(value == dataObj[i].class_id){
   							var option = document.createElement('option');
   							console.log('non removable');

   							option.setAttribute("value",dataObj[i].class_id*1.675);
   							option.innerHTML = dataObj[i].class_section;
   							$('#sectionOpt-'+sn).append(option);

   						}
   					});
   					//console.log('option>>',option);

   					// if(option.value){

   					// 	$('#sectionOpt-'+sn).append(option);

   					// }
   					
   					
   				}
   				$('.selectpicker').selectpicker('default');
   				$('.selectpicker').selectpicker('refresh');

   				/*$('#sectionOpt').append*/
   			},

   		});
}
function readClassSubjectByExamTypeNoRecords(class_name,examTypeNo,postType = 'readClassSubjectRecords',excludingClassIdJsonArray = false){

	var readrecords = "readrecords";
	$.ajax({
		url:"./process/class.php",
		type:"POST",
		data:{className:class_name,examTypeNo:examTypeNo,postType:postType},
		success:function(data,status){
			console.log(data);
		   				//console.log(excludingClassIdJsonArray);
		   				var i = 0;
		   				var	subjectIdArray = [] ;
		   				var subjectIdWithResult = [];
		   				var subjectIdWithoutResult = [];

		   				dataObj = JSON.parse(data);
		   				console.log(dataObj);
		   				$('#sectionOpt').empty();
		   				var obj = JSON.parse(excludingClassIdJsonArray);
		   				console.log(obj);
		   				var result = Object.keys(obj).map(function(key) {
		   					return obj[key];
		   				});
		   				//console.log(result);
		   				for ( var i = 0; i < dataObj.length; i++) {
		   					if(dataObj[i].result_id == null){
		   						subjectIdWithoutResult.push(dataObj[i].subject_id);

		   					}else{
		   						subjectIdWithResult.push(dataObj[i].subject_id);

		   					}
		   				}
		   				for ( var i = 0; i < dataObj.length; i++) {
		   					var sPath = window.location.pathname;
							//var sPage = sPath.substring(sPath.lastIndexOf('\\') + 1);
							var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
							console.log(subjectIdArray);
							if (Array.isArray(result) && result.length) {
								if (result.includes(dataObj[i].subject_id)) {
									console.log('subjectIdArray>>',subjectIdArray)
									if(dataObj[i].result_id == null){

										if(!subjectIdArray.includes(dataObj[i].subject_id) && currentPage != 'edit_result'){
											console.log(subjectIdArray)

											var option = document.createElement('option');
											if (currentPage == 'add_result' || currentPage == 'edit_result') {
												console.log(currentPage);

												option.setAttribute("value",dataObj[i].subject_id*1.675);
												option.setAttribute("onclick","getSubjectExamListJs();");
											}else{
												option.setAttribute("value",dataObj[i].subject_id*1.675);
												option.setAttribute("onclick","postData();");
											}
											subjectIdArray.push(dataObj[i].subject_id);

											option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')';

										}
									}else{

										if(!subjectIdArray.includes(dataObj[i].subject_id)){

											var option = document.createElement('option');
											if (currentPage == 'add_result') {
												console.log(currentPage);

												option.setAttribute("value",dataObj[i].subject_id*1.675);
												option.setAttribute("onclick","getSubjectExamListJs();");

											}else{
												option.setAttribute("value",dataObj[i].subject_id*1.675);
												option.setAttribute("onclick","postData();");

											}
											subjectIdArray.push(dataObj[i].subject_id);

											option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')*';


										}

									}

									console.log(i);
									$('#sectionOpt').append(option);
								}
							}else{
								if(dataObj[i].result_id == null){

									if(!subjectIdArray.includes(dataObj[i].subject_id) && currentPage != 'edit_result'){
										console.log(subjectIdArray)

										var option = document.createElement('option');
										if (currentPage == 'add_result') {
											console.log(currentPage);

											option.setAttribute("value",dataObj[i].subject_id*1.675);
											option.setAttribute("onclick","getSubjectExamListJs();");
										}else{
											option.setAttribute("value",dataObj[i].class_id*1.675);
											option.setAttribute("onclick","postData();");
										}
										subjectIdArray.push(dataObj[i].subject_id);

										option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')';

									}
								}else{

									if(!subjectIdArray.includes(dataObj[i].subject_id)){

										var option = document.createElement('option');
										if (currentPage == 'add_result' || currentPage == 'edit_result') {
											console.log(currentPage);

											option.setAttribute("value",dataObj[i].subject_id*1.675);
											option.setAttribute("onclick","getSubjectExamListJs();");

										}else{
											option.setAttribute("value",dataObj[i].class_id*1.675);
											option.setAttribute("onclick","postData();");

										}
										subjectIdArray.push(dataObj[i].subject_id);
										console.log('subjectIdWithResult>>',subjectIdWithResult)
										console.log('subjectIdWithoutResult>>',subjectIdWithoutResult)
										if(subjectIdWithResult.includes(dataObj[i].subject_id)){ //subject has result
											
											if(subjectIdWithoutResult.includes(dataObj[i].subject_id)){ //subject doesn't have result
												option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')*';

										}else{
											option.innerHTML = dataObj[i].subject_name + ' ' + dataObj[i].class_name + ' (' + dataObj[i].class_section + ')**';

										}
									}


								}

							}


		   						//console.log(i);

		   						

		   						$('#sectionOpt').append(option);
		   					}
		   				}
		   				$('.selectpicker').selectpicker('default');
		   				$('.selectpicker').selectpicker('refresh');

		   				/*$('#sectionOpt').append*/
		   			},

		   		});
}

function getSubjectExamListJs(examTypeNo = null ,postType = 'readSubjectExamListRecords',excludingClassIdJsonArray = false){
	$(document).ready(function () {
		$('#section').change(function(){ 
			var subject_id = $(this).val();
			console.log('this is subject_id:',subject_id);
			if (examTypeNo) {
				readSubjectExamListRecordsByExamTypeNo(examTypeNo,subject_id,postType,excludingClassIdJsonArray); 


			}else{
				readSubjectExamListRecords(subject_id,postType,excludingClassIdJsonArray); 

			}

		});
	});

	function readSubjectExamListRecords(subject_id,postType = 'readSubjectExamListRecords',excludingClassIdJsonArray = false){

		var readrecords = "readrecords";
		$.ajax({
			url:"./process/exam.php",
			type:"POST",
			data:{subjectId:subject_id,postType:postType},
			success:function(data,status){
				console.log(data);
	   				//console.log(excludingClassIdJsonArray);
	   				var i = 0;
	   				
	   				dataObj = JSON.parse(data);
	   				console.log(dataObj);
	   				$('#subjectStDateOpt').empty();
	   				var obj = JSON.parse(excludingClassIdJsonArray);
	   				console.log(obj);
	   				var result = Object.keys(obj).map(function(key) {
	   					return obj[key];
	   				});
	   				console.log(result);
	   				for ( var i = 0; i < dataObj.length; i++) {
	   					var sPath = window.location.pathname;
						//var sPage = sPath.substring(sPath.lastIndexOf('\\') + 1);
						var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
						console.log(currentPage);
						if (Array.isArray(result) && result.length) {
							if (result.includes(dataObj[i].subject_id)) {
								var option = document.createElement('option');
								option.setAttribute("value",dataObj[i].exam_id*1.675);

								option.innerHTML = dataObj[i].starting_date + ' (' + dataObj[i].exam_type_no + ')';
								console.log(i);
								$('#subjectStDateOpt').append(option);
							}
						}else{
							console.log(dataObj[i]);

							var option = document.createElement('option');

							option.setAttribute("value",dataObj[i].exam_id*1.675);
							
	   						//console.log(i);
	   						var startingDate = formatDate(dataObj[i].starting_date);
	   						option.innerHTML = startingDate + ' (' + dataObj[i].exam_type_no + ') - '+ dataObj[i].exam_category;
	   						$('#subjectStDateOpt').append(option);
	   					}
	   				}
	   				$('.selectpicker').selectpicker('default');
	   				$('.selectpicker').selectpicker('refresh');

	   				/*$('#sectionOpt').append*/
	   			},

	   		});
	}

	function readSubjectExamListRecordsByExamTypeNo(examTypeNo,subject_id,postType = 'readSubjectExamListRecords',excludingClassIdJsonArray = false){

		var readrecords = "readrecords";
		$.ajax({
			url:"./process/exam.php",
			type:"POST",
			data:{subjectId:subject_id,examTypeNo:examTypeNo,postType:postType},
			success:function(data,status){
				console.log(data);
	   				//console.log(excludingClassIdJsonArray);
	   				var i = 0;
	   				
	   				dataObj = JSON.parse(data);
	   				console.log(dataObj);
	   				$('#subjectStDateOpt').empty();
	   				var obj = JSON.parse(excludingClassIdJsonArray);
	   				console.log(obj);
	   				var result = Object.keys(obj).map(function(key) {
	   					return obj[key];
	   				});
	   				console.log(result);
	   				for ( var i = 0; i < dataObj.length; i++) {
	   					var sPath = window.location.pathname;
						//var sPage = sPath.substring(sPath.lastIndexOf('\\') + 1);
						var currentPage = sPath.substring(sPath.lastIndexOf('/') + 1);
						console.log(currentPage);
						if (Array.isArray(result) && result.length) {
							if (result.includes(dataObj[i].subject_id)) {
								var option = document.createElement('option');
								option.setAttribute("value",dataObj[i].exam_id*1.675);

								option.innerHTML = dataObj[i].starting_date + ' (' + dataObj[i].exam_type_no + ')';
								console.log(i);
								$('#subjectStDateOpt').append(option);
							}
						}else{
							console.log(dataObj[i]);

							var option = document.createElement('option');

							option.setAttribute("value",dataObj[i].exam_id*1.675);
							
	   						//console.log(i);
	   						var startingDate = formatDate(dataObj[i].starting_date);
	   						option.innerHTML = startingDate + ' (' + dataObj[i].exam_type_no + ') - '+ dataObj[i].exam_category;
	   						$('#subjectStDateOpt').append(option);
	   					}
	   				}
	   				$('.selectpicker').selectpicker('default');
	   				$('.selectpicker').selectpicker('refresh');

	   				/*$('#sectionOpt').append*/
	   			},

	   		});
	}
}

function checkIfUserExists(userId,pwd){
	$(document).ready(function () {
		verifyLoggedInUser(userId,pwd); 

	});
}

function verifyLoggedInUser(userId,pwd){
	$.ajax({
		url:"./process/login.php",
		type:"POST",
		data:{userId:userId,pwd:pwd},
		success:function(data,status){

			if (data) {
				dataObj = JSON.parse(data);
				var isLoggedInUser = dataObj[0];
				if(isLoggedInUser){
					console.log('isLoggedInUser>>',isLoggedInUser);

					var myformSubmitBtnId = $('#myformSubmitBtnId');
					console.log('myformSubmitBtnIdInAjax>>',myformSubmitBtnId);
					myformSubmitBtnId.removeAttr('data-toggle');
					var tagname = myformSubmitBtnId.prop("tagName");
					console.log(typeof(tagname));
					console.log('tagname>>',tagname);

					if (tagname == 'A') {

						console.log('myformSubmitBtnIdA>>',myformSubmitBtnId[0]);
						$('#myformSubmitBtnId').click();
   						//window.location.href = window.location.href+'https://www.w3docs.com';

   					}else{
   						myformSubmitBtnId.attr('type','submit');

   						console.log('myformSubmitBtnId>>',myformSubmitBtnId[0]);
   						myformSubmitBtnId.click();

   					}
					//return isLoggedInUser;
				}else{
					$('#infoAlert').html('<div class="alert alert-danger alert-xs" role="alert" style="padding: 4px;">Incorrect Password</div>');
				}
			}


		}
	});
}

function getIssuedBooks(bookId = null,memberId = null,bookName = null,publisher = null,author = null,expired = null){
	console.log('Hello getIssuedBooks');
	if (bookId) {
		memberId = null;
		bookName = null;
		publisher = null;
		author = null;
		expired = null;
	}else{
		if (!(bookName || publisher || author || memberId || expired)) {
			$('#book_id').attr('disabled',false);

		}
	}
	console.log(bookId,memberId,bookName,publisher,author,expired);

   		//$('#setup_issue_book_id').click();
   		var readrecords = "readrecords";

   		$.ajax({
   			url:"./process/issueBook.php",
   			type:"POST",
   			data:{bookId:bookId,memberId:memberId,bookName:bookName,publisher:publisher,author:author,expired:expired},
   			success:function(data,status){
   				console.log(data);
   				var i = 0;

   				dataObj = JSON.parse(data);


   				console.log(dataObj);
   				$('#tbodyBookList').empty();
   				dataObj.forEach(function(item,index){
   					console.log('item>>',item);
   					bookBtnId = (index+1);
   					tableRow = document.createElement('tr');

   					snTd = document.createElement('td');
   					bookIdTd = document.createElement('td');
   					bookIssueIdTd = document.createElement('td');
   					bookNameTd = document.createElement('td');
   					bookAuthorTd = document.createElement('td');
   					bookPublisherTd = document.createElement('td');
   					bookISBNTd = document.createElement('td');
   					bookIssuedDateTd = document.createElement('td');
   					bookReturnDateTd = document.createElement('td');
   					bookReturnedDateTd = document.createElement('td');
   					bookFineTd = document.createElement('td');
   					actionTd = document.createElement('td');

   					bookIssuedToTdAnchorTag = document.createElement('a');

   					bookIssuedToTdAnchorTag.setAttribute('href','./library_member_profile?memberId='+parseInt(item.member_id)*1.675);
   					bookIssuedToTdAnchorTag.setAttribute('class','btn btn default');
   					bookIssuedToTdAnchorTag.setAttribute('target','_blank');

   					bookIssuedToTdAnchorTag.innerHTML = item.member_id;
   					bookIssuedToTdAnchorTag.setAttribute('title',item.name);

   					console.log(bookIssuedToTdAnchorTag);

   					snTd.innerHTML = '#'+(index+1);
   					bookIdTd.innerHTML = (item.book_id);
   					bookIssueIdTd.innerHTML = item.issue_id;
   					bookNameTd.innerHTML = item.book_name;
   					bookAuthorTd.innerHTML = item.book_author;
   					bookPublisherTd.innerHTML = item.book_publisher;
   					bookISBNTd.innerHTML = item.book_isbn;
   					bookIssuedDateTd.innerHTML = item.issued_date;
   					var issuedDate = addDate(item.issued_date,0);
   					var addedDays = parseInt(item.return_timeframe_days);
   					var return_date = addDate(issuedDate,addedDays);
   					//console.log(formatDate(issuedDate));
   					//console.log(formatDate(return_date));


   					console.log(addDate(issuedDate,addedDays));
   					bookReturnDateTd.innerHTML = formatDate(return_date);
   					renewButton = document.createElement('a');
   					url = ''
   					renewButton.setAttribute('id','renewBtn-' + item.member_id + '-' + item.issue_id);
	   				//renewButton.setAttribute('onclick','return confirm(\'Are you sure you want to renew this book?\')');
	   				//renewButton.setAttribute('href','./process/issueBook'+url);
	   				renewButton.setAttribute('class','btn btn-xs btn-primary');
	   				renewButton.setAttribute('data-toggle','modal');
	   				renewButton.setAttribute('data-target','#inputRenewPopup');
	   				renewButton.innerHTML = 'Renew Book'


	   				returnButton = document.createElement('a');
	   				returnButton.setAttribute('id','returnBtn-' + item.member_id + '-' + item.issue_id);
	   				returnButton.setAttribute('class','btn btn-xs btn-primary');
	   				returnButton.setAttribute('data-toggle','modal');
	   				returnButton.setAttribute('data-target','#inputReturnPopup');
	   				returnButton.innerHTML = 'Return Book';

	   				//$issued_date = $memberAndBookDataIssueIdAndReturnStatus[0]->issued_date;
	   				//const date1 = new Date(issuedDate);
	   				//const date2 = new Date(return_date);
	   				
					//$dateDiff = strtotime($return_date) - strtotime($issued_date);
					//debugger($dateDiff);
					if (item.returned_date) {
						var returnedDate = addDate(item.returned_date,0);
						const diffTime = Math.abs(returnedDate - issuedDate);
						console.log('diffTime>>',diffTime);
						no_of_notReturnedDays = Math.round(diffTime / (1000 * 60 * 60 * 24));

					}else{
						var currentDate = new Date();
						const diffTime = Math.abs(currentDate - issuedDate);
						no_of_notReturnedDays = Math.round(diffTime / (1000 * 60 * 60 * 24));

					}
					//debugger($return_date,true);
					//$data['fine_amount'] = $memberAndBookDataIssueIdAndReturnStatus[0]->fine_per_day * $no_of_notReturnedDays;
					console.log(typeof(issuedDate));
					console.log(typeof(return_date));

					console.log('issuedDate>>',issuedDate);
					console.log('returnDate>>',return_date);
					console.log('returnedDate>>',returnedDate);

					console.log('finePerDay>>',item.fine_per_day);
					//console.log('issuedDate>>',date1);
					//console.log('returnDate>>',date2);
	   				//return_date = date('Y-m-d h:i:s', strtotime($return_date.' + '.$memberAndBookDataIssueIdAndReturnStatus[0]->return_timeframe_days.' days'));
	   				//date_diff = 
	   				console.log('addedDays>>',addedDays);
	   				console.log('no_of_notReturnedDays>>',no_of_notReturnedDays);
	   				console.log('item>>',item);
	   				if (no_of_notReturnedDays > addedDays) {
	   					console.log('It Works!');
	   				}
	   				if (item.fine_per_day != null && item.member_id != null && item.user_type == 'Student' && item.has_returned == 0 && item.fine_id == null && (no_of_notReturnedDays > addedDays)) {
	   					fineButton = document.createElement('a');
	   					console.log('usertypeA>>',item.user_type);

	   					url = ''
	   					fineButton.setAttribute('id','fineBtn-' + item.member_id + '-' + item.issue_id);
	   					fineButton.setAttribute('class','btn btn-xs btn-primary');
	   					fineButton.setAttribute('data-toggle','modal');
	   					fineButton.setAttribute('data-target','#inputFinePopup');
	   					fineButton.setAttribute("style","margin-right: 5px;margin-top: 5px;margin-bottom: 5px;");
	   					fineButton.innerHTML = 'Add Fine';
	   					
	   					bookFineTd.innerHTML = '<span class="label label-primary">Active</span>';
	   					
	   					renewButton.removeAttribute('id');
	   					renewButton.removeAttribute('data-toggle');
	   					renewButton.removeAttribute('data-target');
	   					renewButton.setAttribute("onClick","return alert('You need to add fine before book renew')");

	   					returnButton.removeAttribute('id');
	   					returnButton.removeAttribute('data-toggle');
	   					returnButton.removeAttribute('data-target');
	   					returnButton.setAttribute('onClick',"return alert('You need to add fine before book return')");


	   					bookFineTd.append(fineButton);

	   				}else{
	   					console.log('usertypeB>>',item.user_type);
	   					console.log('bookFineTd>>',bookFineTd);
	   					

	   					bookFineTd.innerHTML = '<span class="label label-danger">Inactive</span>';

	   				}

	   				if (item.returned_date) {
	   					bookReturnedDateTd.innerHTML = formatDate(item.returned_date);
	   					actionTd.innerHTML = '-';
	   				}else{
	   					bookReturnedDateTd.innerHTML = '-';
	   					actionTd.append(renewButton,returnButton);

	   				}

	   				tableRow.append(snTd,bookIdTd,bookIssueIdTd,bookNameTd,bookAuthorTd,bookPublisherTd,bookISBNTd,bookIssuedToTdAnchorTag,bookIssuedDateTd,bookReturnDateTd,bookReturnedDateTd,bookFineTd,actionTd);
	   				$('#tbodyBookList').append(tableRow);
	   			});

},
});
}

function getBooksToIssue(bookSubject = null,bookPublisher = null,bookAuthor = null,bookType = null,bookID = null){
	console.log('Hello getBooksToIssue');
	console.log(bookID);
	if (bookID) {
		bookSubject = null;
		bookPublisher = null;
		bookAuthor = null;
		bookType = null
	}else{
		if (!(bookSubject || bookPublisher || bookAuthor || bookType)) {
			$('#book_id').attr('disabled',false);
		}
	}
   	//$('#setup_issue_book_id').click();
   	var readrecords = "readrecords";
   	$.ajax({
   		url:"./process/book.php",
   		type:"POST",
   		data:{bookSubject:bookSubject,bookPublisher:bookPublisher,bookAuthor:bookAuthor,bookType:bookType,bookID:bookID},
   		success:function(data,status){
   			console.log(data);
   			var i = 0;

   			dataObj = JSON.parse(data);

   			console.log(dataObj);
   			$('#tbodyBookList').empty();
   			dataObj.forEach(function(item,index){
   				console.log('item>>',item);
   				bookBtnId = (index+1);
   				tableRow = document.createElement('tr');

   				snTd = document.createElement('td');
   				bookNameTd = document.createElement('td');
   				bookAuthorTd = document.createElement('td');
   				bookPublisherTd = document.createElement('td');
   				bookISBNTd = document.createElement('td');
   				bookQuantityTd = document.createElement('td');
   				bookRackNoTd = document.createElement('td');
   				bookDescriptionTd = document.createElement('td');
   				bookAddedDateTd = document.createElement('td');
   				bookUpdatedDateTd = document.createElement('td');
   				actionTd = document.createElement('td');

   				snTd.innerHTML = (item.book_id);
   				bookNameTd.innerHTML = item.book_name;
   				bookAuthorTd.innerHTML = item.book_author;
   				bookPublisherTd.innerHTML = item.book_publisher;
   				bookISBNTd.innerHTML = item.book_isbn;
   				bookQuantityTd.innerHTML = item.book_qty;
   				bookRackNoTd.innerHTML = item.rack_number;
   				bookDescriptionTd.innerHTML = item.book_description;
   				bookAddedDateTd.innerHTML = item.added_date;
   				if (item.updated_date != null) {
   					bookUpdatedDateTd.innerHTML = item.updated_date;

   				}else{
   					bookUpdatedDateTd.innerHTML = '-';

   				}

   				button = document.createElement('button');
   				

   				if (item.book_qty <= 0) {
   					
   					button.setAttribute('type','button');
   					button.setAttribute('class','btn btn-primary');
   					button.setAttribute('disabled',true);

   					button.innerHTML = 'Issue Book';
   				}else{
   					button.setAttribute('id','bookBtn-' + item.book_id);
   					button.setAttribute('type','button');
   					button.setAttribute('class','btn btn-primary');
   					button.setAttribute('data-toggle','modal');
   					button.setAttribute('data-target','#inputMemberIdPopup');
   					button.removeAttribute('disabled');

   					button.innerHTML = 'Issue Book';

   				}

   				actionTd.append(button);
   				tableRow.append(snTd,bookNameTd,bookAuthorTd,bookPublisherTd,bookISBNTd,bookQuantityTd,bookRackNoTd,bookDescriptionTd,bookAddedDateTd,bookUpdatedDateTd,actionTd);
   				$('#tbodyBookList').append(tableRow);
   			});
   			
   		},
   	});

   }



   function getUserDataByStaffId(staffId){
   	console.log('here>>',staffId);
   	$.ajax({
   		url:"./process/book.php",
   		type:"POST",
   		data:{staffId:staffId},
   		success:function(data,status){
   			var i = 0;
   			//console.log('MemberAndBookDataToIssueBook>>',data);

   			


   			userName = $('#userName');
   			userEmailAddress = $('#userEmailAddress');
   			userType = $('#userType');
   			userContactNo = $('#userContactNo');
   			userAddress = $('#userAddress');
   			fileUpload = $('#file-upload');
   			image = $('#image');

   			console.log(data);
   			dataObj = JSON.parse(data);

   			if (dataObj) {

   				console.log('UserDataByUserIdToAddMembership>>',dataObj[0]);

   				if (dataObj[0]) {
   					if (dataObj[0].full_name) {
   						userName.val(dataObj[0].full_name);
   					}

   					if (dataObj[0].email_address) {
   						userEmailAddress.val(dataObj[0].email_address);
   					}

		   			if (dataObj[0].user_type == 9) { //staff
		   				userType.val('Staff');

		   				if (dataObj[0].contact_no) {
		   					userContactNo.val(dataObj[0].contact_no);
		   				}

		   				if (dataObj[0].permanent_address) {
		   					userAddress.val(dataObj[0].permanent_address);

		   				}else if(dataObj[0].temporary_address){
		   					userAddress.val(dataObj[0].temporary_address);

		   				}

		   				if (dataObj[0].staff_image) {
		   					image.attr('src','../upload/StaffDirectory/'+dataObj[0].staff_image);
		   				}
		   			}else if(dataObj[0].user_type == 3){ //student
		   				userType.val('Student');

		   				if (dataObj[0].student_contact_no) {
		   					userContactNo.val(dataObj[0].student_contact_no);

		   				}else if(dataObj[0].parent_guardian_contact_no){
		   					userContactNo.val(dataObj[0].parent_guardian_contact_no);

		   				}

		   				if (dataObj[0].student_current_address) {
		   					userAddress.val(dataObj[0].student_current_address);

		   				}

		   				if(dataObj[0].student_image){
		   					image.attr('src','../upload/studentImage/100x100/'+dataObj[0].student_image);
		   				}
		   			}else if (dataObj[0].user_type == 2) { //admin

		   			}else if (dataObj[0].user_type == 1) { //superadmin

		   			}else{
		   				image.attr('src','../upload/noImageAvailable.png');
		   			}
		   		}else{
			    	/*userName.val('');
			        userName.attr('readonly','false');

			        userEmailAddress.val('');
			        userEmailAddress.attr('readonly','false');

			        userType.val('');
			        userType.attr('readonly','false');

			        userContactNo.val('');
			        userContactNo.attr('readonly','false');

			        userAddress.val('');
			        userAddress.attr('readonly','false');

			        image.attr('src','../upload/noImageAvailable.png');*/
			    }

		   			/*console.log($('#modalID'));
		   			$('#modalID').empty();
		   			br1 = document.createElement('br');
		   			br2 = document.createElement('br');

		   			fullNameLabel = document.createElement('label');
		   			fullNameLabel.innerHTML = 'Full Name : '+dataObj[0].full_name;
		   			emailAddressLabel = document.createElement('label');
		   			emailAddressLabel.innerHTML = 'Email Address : '+dataObj[0].email_address;
		   			userTypeLabel = document.createElement('label');
		   			userTypeLabel.innerHTML = 'User Type : '+dataObj[0].user_type;

		   			$('#modalID').append(fullNameLabel,br1,emailAddressLabel,br2,userTypeLabel);*/
		   		}

		   	},
		   });
   }

   function getUserDataByStudentId(studentId){
   	$.ajax({
   		url:"./process/book.php",
   		type:"POST",
   		data:{studentId:studentId},
   		success:function(data,status){
   			var i = 0;
   			
   			userName = $('#userName');
   			userEmailAddress = $('#userEmailAddress');
   			userType = $('#userType');
   			userContactNo = $('#userContactNo');
   			userAddress = $('#userAddress');
   			fileUpload = $('#file-upload');
   			image = $('#image');

   			console.log(data);
   			dataObj = JSON.parse(data);

   			if (dataObj) {

   				console.log('UserDataByUserIdToAddMembership>>',dataObj[0]);

   				if (dataObj[0]) {
   					if (dataObj[0].student_name) {
   						userName.val(dataObj[0].student_name);
   					}

   					if (dataObj[0].student_email) {
   						userEmailAddress.val(dataObj[0].student_email);
   					}

   					userType.val('Student');

   					if (dataObj[0].student_contact_no) {
   						userContactNo.val(dataObj[0].student_contact_no);

   					}else if(dataObj[0].parent_guardian_contact_no){
   						userContactNo.val(dataObj[0].parent_guardian_contact_no);

   					}

   					if (dataObj[0].student_current_address) {
   						userAddress.val(dataObj[0].student_current_address);

   					}

   					if(dataObj[0].student_image){
   						image.attr('src','../upload/studentImage/100x100/'+dataObj[0].student_image);
   					}else{
   						image.attr('src','../upload/noImageAvailable.png');

   					}
   				}else{
   				}
   			}

   		},
   	});
   }

   function getMemberAndBookDataToIssueBook(libraryMemberID){
   	$.ajax({
   		url:"./process/book.php",
   		type:"POST",
   		data:{libraryMemberID:libraryMemberID},
   		success:function(data,status){
   			var i = 0;
   			//console.log('MemberAndBookDataToIssueBook>>',data);

   			dataObj = JSON.parse(data);

   			/*$('.table').DataTable( {
   				destroy: true,
   			} );*/

   			console.log('MemberAndBookDataToIssueBook>>',dataObj[0]);
   			console.log($('#modalID'));
   			$('#modalID').empty();
   			br1 = document.createElement('br');
   			br2 = document.createElement('br');
   			br3 = document.createElement('br');
   			br4 = document.createElement('br');
   			br5 = document.createElement('br');

   			fullNameLabel = document.createElement('label');
   			fullNameLabel.innerHTML = 'Full Name : '+dataObj[0].name;

   			emailAddressLabel = document.createElement('label');
   			emailAddressLabel.innerHTML = 'Email Address : '+dataObj[0].user_email;

   			userTypeLabel = document.createElement('label');
   			userTypeLabel.innerHTML = 'User Type : '+dataObj[0].user_type;

   			userIdLabel = document.createElement('label');
   			userIdLabel.innerHTML = (dataObj[0].user_id) ? 'User Id : ' + dataObj[0].user_id : 'User Id : -' ;

   			contactNoLabel = document.createElement('label');
   			contactNoLabel.innerHTML = 'Contact No : '+dataObj[0].contact_no;

   			userStatusLabel = document.createElement('label');
   			if (dataObj[0].is_member == 1) {
   				status = 'Active';
   				$('#setup_issue_book_id').removeAttr('disabled');

   			}else{
   				status = 'Inactive';
   				$('#setup_issue_book_id').prop('disabled',true);


   			}
   			userStatusLabel.innerHTML = 'Status : '+status;
   			$('#modalID').append(fullNameLabel,br1,emailAddressLabel,br2,userTypeLabel,br3,userIdLabel,br4,contactNoLabel,br5,userStatusLabel);
   		},
   	});
   }

   function getMemberAndBookDataToRenewBook(libraryMemberID){
   	$.ajax({
   		url:"./process/book.php",
   		type:"POST",
   		data:{libraryMemberID:libraryMemberID},
   		success:function(data,status){
   			var i = 0;
   			//console.log('MemberAndBookDataToIssueBook>>',data);

   			dataObj = JSON.parse(data);

   			/*$('.table').DataTable( {
   				destroy: true,
   			} );*/

   			console.log('MemberAndBookDataToIssueBook>>',dataObj[0]);
   			console.log($('#modalIDRenew'));
   			$('#modalIDRenew').empty();
   			br1 = document.createElement('br');
   			br2 = document.createElement('br');
   			br3 = document.createElement('br');
   			br4 = document.createElement('br');
   			br5 = document.createElement('br');

   			fullNameLabel = document.createElement('label');
   			fullNameLabel.innerHTML = 'Full Name : '+dataObj[0].name;

   			emailAddressLabel = document.createElement('label');
   			emailAddressLabel.innerHTML = 'Email Address : '+dataObj[0].user_email;

   			userTypeLabel = document.createElement('label');
   			userTypeLabel.innerHTML = 'User Type : '+dataObj[0].user_type;

   			userIdLabel = document.createElement('label');
   			userIdLabel.innerHTML = (dataObj[0].user_id) ? 'User Id : ' + dataObj[0].user_id : 'User Id : -' ;

   			contactNoLabel = document.createElement('label');
   			contactNoLabel.innerHTML = 'Contact No : '+dataObj[0].contact_no;

   			userStatusLabel = document.createElement('label');
   			if (dataObj[0].is_member == 1) {
   				status = 'Active';
   				$('#setup_issue_book_id').removeAttr('disabled');

   			}else{
   				status = 'Inactive';
   				$('#setup_issue_book_id').prop('disabled',true);


   			}
   			userStatusLabel.innerHTML = 'Status : '+status;
   			$('#modalIDRenew').append(fullNameLabel,br1,emailAddressLabel,br2,userTypeLabel,br3,userIdLabel,br4,contactNoLabel,br5,userStatusLabel);
   		},
   	});
   }

   function getMemberAndBookDataToIssueFine(issueId){
   	$.ajax({
   		url:"./process/issueBook.php",
   		type:"POST",
   		data:{issueId:issueId},
   		success:function(data,status){

   			if (data) {
   				console.log('data>>',data);
   				dataObj = JSON.parse(data);
   				console.log(dataObj);
   				var issuedDate = new Date(dataObj[0].issued_date);

   				var returnDate = new Date(dataObj[0].issued_date);
   				var currentDate = new Date();
   				returnDate.setDate(parseInt(returnDate.getDate()) + parseInt(dataObj[0].return_timeframe_days));
   				var Difference_In_Time = currentDate.getTime() - returnDate.getTime(); 
   				var no_of_notReturnedDays = parseInt(Difference_In_Time / (1000 * 3600 * 24)); 
   				console.log('no_of_notReturnedDays>>',no_of_notReturnedDays);

   				fineAmount = dataObj[0].fine_per_day * no_of_notReturnedDays;
   				//console.log('fineAmount>>',fineAmount);
   				$('#fine_amount').val(fineAmount);

   			}

   			
   		}
   	});
   }


   function checkGetMethod(name){
   	if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
   		return decodeURIComponent(name[1]);
   }
   function pad(d) {
   	return (d < 10) ? '0' + d.toString() : d.toString();
   }

   function inArray(needle, haystack) {
   	var length = haystack.length;
   	for(var i = 0; i < length; i++) {
   		if(haystack[i] == needle) return true;
   	}
   	return false;
   }

   function removeArrayElement(array,elemToRemove){

   	if(inArray(elemToRemove,array)){
   		const index = array.indexOf(elemToRemove);
   		if (index > -1) {
   			array.splice(index, 1);
   		}
   	}
   	
   	console.log(array); 

   	return array;
		// array = [2, 9]
	}

	function formatDate(date,addedDay = null,addedMonth = null,addedYear = null){
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

		if (month.length < 2){ 

			month = '0' + month;
		}
		console.log('addedDay>>',addedDay);
		if (day.length < 2){ 

			day = '0' + day;
		}

		if (year.length < 2){

			year = '0' + year;
		}

		if(addedMonth){
			month = month + addedMonth;
		}

		if(addedDay){
			day = day + addedDay;
		}
		if(addedYear){
			year = year + addedYear;
		} 
		return [year, month, day].join('-');
	}

	function addDate(date,addedDay,addedMonth,addedYear){
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

		seconds = d.getSeconds();
		minutes = d.getMinutes();
		hour = d.getHours();

		if(addedDay){
			day = parseInt(day) + addedDay;
		}

		if(addedMonth){
			month = parseInt(month) + addedMonth;
		}

		if(addedYear){
			year = parseInt(year) + addedYear;
		}

		if (month.length < 2){ 

			month = '0' + month;
		}
		if (day.length < 2){ 

			day = '0' + day;
		}

		if (year.length < 2){

			year = '0' + year;
		} 
		d.setDate(day);
		return d;

   	//hrMinSec = [hour,minutes,seconds].join(':');
   	//return [year, month, day].join('-')+' '+hrMinSec;
   }

   function getSelectedSections(){
  //console.log("selectPicker>>",$("#selectsection"));

  $("#selectsection").change(function() {


  	var selectedTargets = [];
  	var unselectedTargets = [];


  	$.each($("#selectsection option:selected"), function(){
  		selectedTargets.push($(this).val());
  		currentSection = $(this).val();

  		sectionTrClass = '.tableRow-'+currentSection+' td input';

  		checkbox = $(sectionTrClass);
  		if (checkbox) {
  			checkbox.prop('checked', true);
  		}


  	});

  	$.each($("#selectsection option:not(:selected)"), function(){
  		unselectedTargets.push($(this).val());
  		currentSection = $(this).val();

  		sectionTrClass = '.tableRow-'+currentSection+' td input';

  		checkbox = $(sectionTrClass);
  		if (checkbox) {
  			checkbox.prop('checked', false); 
  		}


  	});

  	selectedSubjects = selectedTargets.join(", ");
  	console.log("You have selected the targets - " + selectedSubjects);
  	unselectedSubjects = unselectedTargets.join(", ");
  	console.log("You have unselected the targets - " + unselectedSubjects);

  	//$(classId)

  });




}

function getAddExamSubjectListSelectedValue(id){
	subjectId = '#'+id;
	currentId = id.split('_')[1];
	prevId = currentId - 1;
	examCategoryId = '#examCategory_'+currentId ;

	if (prevId != 0) {
		do {
			console.log(currentId);
			prevSelectedSubjectValueArray = $(subjectId).val() ;
			prevSelectedExamCategoryValueArray = $(examCategoryId).val();

			prevId = prevId - 1;
		} while (prevId > 0);

		var selectedExamCategoryAndSubjectArray = new Array(prevSelectedSubjectValueArray, prevSelectedExamCategoryValueArray);
		console.log(selectedExamCategoryAndSubjectArray);
	}

}

function removeRow(rowId){
	var tableRowId = '#tableRow-'+rowId;
	$(tableRowId).remove();
}
function readURL(input) {
	inputEl = $(input);

	allChildEl = inputEl.parent().parent().children();
	
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {

			$(allChildEl).each(function( index ) {
				tdEl = $(this);
				tdInnerChildEl = tdEl.children();

				if ( tdInnerChildEl.is('img') ) {
					//console.log('tdInnerChildEl>>',tdInnerChildEl[0]);
					
					tdInnerChildEl.attr('src', e.target.result);

				}

			});
		}

		reader.readAsDataURL(input.files[0]);
	}
}
function arraysEqual(_arr1, _arr2) {

	if (!Array.isArray(_arr1) || ! Array.isArray(_arr2) || _arr1.length !== _arr2.length)
		return false;

	var arr1 = _arr1.concat().sort();
	var arr2 = _arr2.concat().sort();

	for (var i = 0; i < arr1.length; i++) {

		if (arr1[i] !== arr2[i])
			return false;

	}

	return true;

}

function loadCustomImageDropdown(id){
	$(document).ready(function(e) {
		$(id).msDropdown({visibleRows:4});
  			$("#tech").msDropdown().data("dd");//{animStyle:'none'} /{animStyle:'slideDown'} {animStyle:'show'}   
  			//no use
  			try {
  				var pages = $("#pages").msDropdown({on:{change:function(data, ui) {
  					var val = data.value;
  					if(val!="")
  						window.location = val;
  				}}}).data("dd");

  				var pagename = document.location.pathname.toString();
  				pagename = pagename.split("/");
  				pages.setIndexByValue(pagename[pagename.length-1]);
  				$("#ver").html(msBeautify.version.msDropdown);
  			} catch(e) {
    	//console.log(e); 
    }
    $('.fnone').attr('style','height:45px;width:45px;');
    $("#ver").html(msBeautify.version.msDropdown);
});
}

function replaceLast(what, replacement) {
	return this.split(' ').reverse().join(' ').replace(new RegExp(what), replacement).split(' ').reverse().join(' ');
};





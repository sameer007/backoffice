$(document).ready(function() {
    //console.log('thisEl>>');
    function appendInputTypeHiddenCanHide(inputTypeEl,inputTypeElArr = null){

      if(inputTypeElArr){
        inputTypeElParent = inputTypeEl.parent().parent().parent().parent();
        inputTypeElNameAttr = inputTypeEl.attr('name')

        newInputTypeElNameAttr = inputTypeElNameAttr.replace("-url", "");
        
        canHideInputTypeNameAttr = newInputTypeElNameAttr + '-canHide';
      }else{
        inputTypeElParent = inputTypeEl.parent();
        inputTypeElNameAttr = inputTypeEl.attr('name')
        canHideInputTypeNameAttr = inputTypeElNameAttr + '-canHide';
      }
      

      $('<input>').attr({
        type: 'hidden',
        name: canHideInputTypeNameAttr,
        value: 1
      }).appendTo(inputTypeElParent);
    }

    function removeInputTypeHiddenCanHide(inputTypeEl,inputTypeElArr = null){
      if(inputTypeElArr){
        inputTypeElParent = inputTypeEl.parent().parent().parent().parent();
        inputTypeElNameAttr = inputTypeEl.attr('name')

        newInputTypeElNameAttr = inputTypeElNameAttr.replace("-url", "");
        
        canHideInputTypeNameAttr = newInputTypeElNameAttr + '-canHide';
      }else{
        inputTypeElParent = inputTypeEl.parent();
        inputTypeElNameAttr = inputTypeEl.attr('name')
        canHideInputTypeNameAttr = inputTypeElNameAttr + '-canHide';
      }
      console.log('inputTypeElParent>>',inputTypeElParent[0]);

      inputTypeElParent.children().each(function() {

        currentEl = $(this);
            console.log('currentEl[0]>>',currentEl[0]);
           // "this" is the current element in the loop
          inputType = currentEl.attr('type');
          if(inputType == 'hidden'){
            currentEl.remove();
          }
        });
    }
    $(document).on("click", "#currentPage", function () {
      //console.log('this>>',this);
      pageName = findGetParameter('pageName');
      pageName = pageName + '.php';
      //console.log('#pageName');

      $('#pageName').attr('value',pageName);
      $('#pageName').attr('readonly','readonly');

      $('#saveAsBtn').html('Save As : Current Page');

    });

    $(document).on("click", "#newPage", function () {
      //console.log('this>>',this);
      //pageName = findGetParameter('pageName');
      //pageName = 'index12';
      pageName =  $('#webpageData').attr('value');
      console.log('#pageName>>',pageName);

      doesEndWithNumber = endsWithNumber(pageName);
      if(doesEndWithNumber){
        currentNum = parseInt(pageName.match(/\d+$/)[0], 10);
        currentNum = currentNum + 1 ;
        pageName = 'index'+currentNum+'.php';
      } else{
        pageName = pageName+'1'+'.php';
      }
      //console.log(pageName);
      $('#pageName').attr('value',pageName);

      //$('#pageName').prop('value',pageName);
      $('#pageName').removeAttr('readonly');
      
      
      $('#saveAsBtn').html('Save As : New Page');

    });

    $(document).on("change", "#pageName", function () {
      //console.log('this>>',this);
      var pageNameEl = $(this);
      pageName = pageNameEl.val();

      if(pageName.endsWith('.php')){
        pageNameEl.attr('value',pageName);
        pageNameEl.prop('value',pageName);

      }else{
        pageName = pageName.replace(/\.[^/.]+$/, "");
        pageName = pageName + '.php';
        pageNameEl.attr('value',pageName);
        pageNameEl.prop('value',pageName);

      }


    });

    $(document).on("change", ".imageFileInput", function () {
      //console.log('this>>',this);
      readURL(this);
    });
    $(document).on('click', ".isCanHide", function() {
      thisEl = $(this);
      childEl = thisEl.parent().parent();

      if(childEl.is("tr")){
        
        inputTypeElArr = childEl.children().children().children().children().children();
        //inputTypeEl.attr('disabled', 'disabled');
        //thisEl.attr('style','display: none');
        inputTypeElArr.each(function (key) {
          inputTypeTdEl = $(this);
          console.log('inputTypeTdEl>>',inputTypeTdEl[0]);
          if(inputTypeTdEl.children().is("input")){
            inputTypeEl = inputTypeTdEl.children();
            inputTypeEl.attr('disabled', 'disabled');
            thisEl.attr('style','display: none');
              
          }

        });
        parentOfInputTypeUrl = inputTypeElArr.parent();
        console.log('parentOfInputTypeUrl>>',parentOfInputTypeUrl);
            appendInputTypeHiddenCanHide(inputTypeEl,inputTypeElArr);

        thisEl.parent().children().each(function (key) {
          thisBtn = $(this);

          if(thisBtn.hasClass('isCanShow')){
            thisBtn.attr('style','');
          }
        });
      }else if(childEl.is("div")){

        inputTypeEl = childEl.children().children().children().first();
        inputTypeEl.attr('disabled', 'disabled');

        thisEl.attr('style','display: none');


        thisEl.parent().children().each(function (key) {
          thisBtn = $(this);
          if(thisBtn.hasClass('isCanShow')){
            thisBtn.attr('style','');
          }
        });
        appendInputTypeHiddenCanHide(inputTypeEl);
      }

    });

    $(document).on('click', ".isCanShow", function() {
      thisEl = $(this);
      childEl = thisEl.parent().parent();


      if(childEl.is("tr")){
        inputTypeElArr = childEl.children().children().children().children().children();
        //inputTypeEl.attr('disabled', 'disabled');
        //thisEl.attr('style','display: none');
        inputTypeElArr.each(function (key) {
          inputTypeTdEl = $(this);
          console.log('inputTypeTdEl>>',inputTypeTdEl[0]);
          if(inputTypeTdEl.children().is("input")){
            inputTypeEl = inputTypeTdEl.children();
            inputTypeEl.prop("disabled", false);
            thisEl.attr('style','display: none');
              
          }
        });
      //thisEl.parent().children().first().attr('style','');

      thisEl.parent().children().each(function (key) {
        thisBtn = $(this);
        if(thisBtn.hasClass('isCanHide')){
          thisBtn.attr('style','');
        }
      });
            removeInputTypeHiddenCanHide(inputTypeEl,inputTypeElArr);


    }else if(childEl.is("div")){
      inputTypeEl = childEl.children().children().children().first();
      inputTypeEl.prop("disabled", false);
      thisEl.attr('style','display: none');
        //console.log(thisEl.parent().children());


        thisEl.parent().children().each(function (key) {
          thisBtn = $(this);
          if(thisBtn.hasClass('isCanHide')){
            thisBtn.attr('style','');
          }
        });
        removeInputTypeHiddenCanHide(inputTypeEl);
      }
    });

    $(document).on('click', ".isMultiple", function() {

      thisEl = $(this);
      parentEl = thisEl.parent().parent();
      tbodyEl = thisEl.parent().parent().parent();

      lastParentEl = thisEl.parent().parent().parent().children().last();
      clonedParentEl = parentEl.clone();

      previousId = lastParentEl.children().first().html();


      clonedParentEl.children().each(function (clonedParentElKey) {
        var childThisEl = $(this);

        if(clonedParentElKey == 0){
          childThisEl.html(tbodyEl.children().length + 1);
        }
        if(clonedParentElKey == 2){
          console.log('childThisEl>>',childThisEl.children().is("img"));
          if(childThisEl.children().is("img")){
            img = childThisEl.children();
            img.attr('src','../../images/noImageAvailable.jpg');
          }else{
            replacedHeadingName = childThisEl.html().replace(/[0-9]/g, tbodyEl.children().length + 1);
            //console.log('replaced>>',replaced);
            childThisEl.html(replacedHeadingName);
          }
          
          formChildEl = childThisEl.parent().children().eq(1).children();

          if(formChildEl.is("table")){
            formTbodyEL = formChildEl.children();

            var i = 0 ;
            formTbodyELChild = formTbodyEL.children();


            formTbodyELChild.each(function (key) {
              formTrEL = $(this);

              formEl = formTrEL.children().children().first() ;

              attrName = formEl.attr('name');
              const attrNamePropsArray = attrName.split("-");

              attrNameContentType = attrNamePropsArray[1];
              console.log('attrNamePropsArray>>',attrNamePropsArray);

              attrNameContentTypeArray = attrNameContentType.split('_');

              i = i + 1 ;
              newAttrNameContentTypeArray = attrNameContentTypeArray;
              nameAttrNumber = parseInt(formTbodyEL.parent().parent().parent().children().first().html());
              //console.log('attrNameContentTypeArray>>>>>',attrNameContentTypeArray);
              newAttrNameContentTypeArray[attrNameContentTypeArray.length - 1] = nameAttrNumber;
              newAttrNameContentType = newAttrNameContentTypeArray.join("_");

              console.log('attrNameContentType>>',attrNameContentType);

              console.log('newAttrNameContentType>>',newAttrNameContentType);
              

              newAttrName = attrName.replace(attrNameContentType, newAttrNameContentType);
              console.log('newAttrName>>',newAttrName);

              formEl.attr('name',newAttrName);


            });


          }else{

            attrName = formChildEl.attr('name');

            const attrNamePropsArray = attrName.split("-");

            attrNameContentType = attrNamePropsArray[1];
            console.log('attrNamePropsArray>>',attrNamePropsArray);
            attrNameContentTypeArray = attrNameContentType.split('_');

            newAttrNameContentTypeArray = attrNameContentTypeArray;
            newAttrNameContentType = newAttrNameContentTypeArray.join("_");

            newAttrNameContentTypeArray[attrNameContentTypeArray.length - 1] = tbodyEl.children().length + 1;

            newAttrNameContentType = newAttrNameContentTypeArray.join('_');

            newAttrName = attrName.replace(attrNameContentType, newAttrNameContentType);

            console.log('newAttrName>>>>>',newAttrName);

            formChildEl.attr('name',newAttrName);

          }
        }
        if(clonedParentElKey == 3){

        }
        if(clonedParentElKey == 3){

          childThisEl.children().each(function (childThisElKey) {
            var actionItem = $(this);
            actionItemClass = actionItem.attr('class');

            if(actionItem.hasClass('isCanHide')){
              actionItem.remove();
            }
            if(actionItem.hasClass('isCanShow')){
              actionItem.remove();

            }
            if(actionItem.hasClass('isMultiple')){
              actionItem.remove();

            }
            if(actionItem.hasClass('isRemoved')){
              actionItem.attr('style','display: block');

            }

          });
        }
      });
      parentEl.parent().append(clonedParentEl);
    });
$(document).on('click', ".isRemoved", function() {
  thisEl = $(this);

  tbodyEl = thisEl.parent().parent().parent();

  removableRow = thisEl.parent().parent();
  removableRow.remove();

  var id = 0 ;

  tbodyElChild = tbodyEl.children();

      // accessing id of each rows after removing row to change id
      tbodyElChild.each(function (key) {
        var childThisEl = $(this);
        id = id + 1;
        childThisEl.children().each(function (tbodyElChildChildrenKey) {
          currentIdDataEl = $(this);
          
          //code to change id after removing a row in table
          
          if(tbodyElChildChildrenKey == 0){

            currentIdDataEl.html(id);
          }
          if(tbodyElChildChildrenKey == 1){
            //console.log('currentIdDataEl>>',currentIdDataEl[0]);
            formChildEl = currentIdDataEl.children();
            //console.log('formChildEl>>',formChildEl[0]);

            if(formChildEl.is("table")){
              formTbodyEL = formChildEl.children();

              var i = 0 ;
              formTbodyELChild = formTbodyEL.children();


              formTbodyELChild.each(function (key) {
                formTrEL = $(this);

                formEl = formTrEL.children().children().first() ;

                attrName = formEl.attr('name');
                const attrNamePropsArray = attrName.split("-");

                attrNameContentType = attrNamePropsArray[1];

                attrNameContentTypeArray = attrNameContentType.split('_');

                i = i + 1 ;
                newAttrNameContentTypeArray = attrNameContentTypeArray;
                nameAttrNumber = parseInt(formTbodyEL.parent().parent().parent().children().first().html());

                newAttrNameContentTypeArray[attrNameContentTypeArray.length - 1] = nameAttrNumber;
                newAttrNameContentType = newAttrNameContentTypeArray.join("_");

                newAttrName = attrName.replace(attrNameContentType, newAttrNameContentType);

                formEl.attr('name',newAttrName);


              });


            }else{

              attrName = formChildEl.attr('name');

              const attrNamePropsArray = attrName.split("-");

              attrNameContentType = attrNamePropsArray[1];
              console.log('attrNamePropsArray>>',attrNamePropsArray);
              attrNameContentTypeArray = attrNameContentType.split('_');

              newAttrNameContentTypeArray = attrNameContentTypeArray;
              newAttrNameContentType = newAttrNameContentTypeArray.join("_");

              newAttrNameContentTypeArray[attrNameContentTypeArray.length - 1] = id;

              newAttrNameContentType = newAttrNameContentTypeArray.join('_');

              newAttrName = attrName.replace(attrNameContentType, newAttrNameContentType);

              console.log('newAttrName>>>>>',newAttrName);

              formChildEl.attr('name',newAttrName);

            }
            //childThisEl.html(replacedHeadingName);
            //currentIdDataEl.html(id);
          }
          if(tbodyElChildChildrenKey == 2){

            //currentIdDataEl.html(id);
            if(currentIdDataEl.children().is("img")){
            }else{
              replacedHeadingName = currentIdDataEl.html().replace(/[0-9]/g, id);
              //console.log('replacedHeadingName>>',replacedHeadingName);
              currentIdDataEl.html(replacedHeadingName);
            }
            
          }

        });
      });
    });
});
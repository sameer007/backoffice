<?php 
require '../config/config.php';


require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';

$role = new Role();
$user = new User();

//debugger($_POST,true);

if(isset($_POST) && !empty($_POST)){
	if (isset($_POST['add-role']) && !empty($_POST['add-role']) && $_POST['add-role'] == 'submit') {

		debugger($_POST);
		
		$data = array();
		$data['role_title'] = sanitize($_POST['role-title']);


		$data['domain_setting_permission'] = sanitizeCheckbox($_POST['domain-setting-permission']);
		$data['assignRolesAndPermission_permission'] = sanitizeCheckbox($_POST['assign-roles-and-permission-permission']);
		$data['enable_maintainance_mode_permission'] = sanitizeCheckbox($_POST['enable-maintainance-mode-permission']);
		$data['create_new_website_permission'] = sanitizeCheckbox($_POST['create-new-website-permission']);
		$data['manage_website_permission'] = sanitizeCheckbox($_POST['manage-website-permission']);
		$data['create_new_role_permission'] = sanitizeCheckbox($_POST['create-new-role-permission']);
		$data['newsletter_management_permission'] = sanitizeCheckbox($_POST['newsletter-management-permission']);


		//debugger($data,true);

		$hasSameRoleTitle = $role->getRoleByRoleTitle($data['role_title']);

		$roleData = $role->addRole($data);

			//debugger($roleData,true);
		/*$password = sha1($username.$_POST['password']);*/
		if(isset($hasSameRoleTitle) && !empty($hasSameRoleTitle)){
			if (isset($roleData) && !empty($roleData)) {
				$userRole = $data['role_title'];
				redirect("../user-management/manage-roles/","success","User role '$userRole' added successfully");

			}else{
				redirect('../user-management/manage-roles/','error','Sorry! Something went wrong while adding user role');

			}
		}else{
			redirect('../user-management/manage-roles/','error','Cannot add new user role with same name');

		}
	}elseif(isset($_POST['update-role']) && !empty($_POST['update-role']) && $_POST['update-role'] == 'submit') {

		$roleInfo = $role->getRoleById($_SESSION['roleId']);

		$updatableData = array();
		$updatableData['role_title'] = sanitize($_POST['role-title']);

		$updatableData['domain_setting_permission'] = sanitizeCheckbox($_POST['domain-setting-permission']);
		$updatableData['assignRolesAndPermission_permission'] = sanitizeCheckbox($_POST['assign-roles-and-permission-permission']);
		$updatableData['enable_maintainance_mode_permission'] = sanitizeCheckbox($_POST['enable-maintainance-mode-permission']);
		$updatableData['create_new_website_permission'] = sanitizeCheckbox($_POST['create-new-website-permission']);
		$updatableData['manage_website_permission'] = sanitizeCheckbox($_POST['manage-website-permission']);
		$updatableData['create_new_role_permission'] = sanitizeCheckbox($_POST['create-new-role-permission']);
		$updatableData['newsletter_management_permission'] = sanitizeCheckbox($_POST['newsletter-management-permission']);

		if (isset($roleInfo) && !empty($roleInfo)) {
			unset($_SESSION['roleId']);
			$is_updated = $role->updateRole($updatableData,(int)$roleInfo[0]->role_id);
			//debugger($is_updated,true);
			if($is_updated == true){
				$role_title = $roleInfo[0]->role_title;
				redirect("../user-management/manage-roles/","success","User role '$role_title' updated successfully");

			}else{

				redirect('../user-management/manage-roles/','error','Sorry! Something went wrong while updating user role');

			}
		}else{
			redirect('../user-management/manage-roles/','error','User info not found in database for role update');

		}

	}else{
		redirect('../404');

	}

}elseif(isset($_GET) && !empty($_GET)){
	$updatableData = array();

	if (isset($_GET['roleId']) && !empty($_GET['roleId'])) {

		if ($_GET['act'] == substr(md5('delete-role-'.$_GET['roleId'].'-'.$_SESSION['token']), 5, 15)) {
			$deletedData = $role->getRoleById($_GET['roleId']);
			//debugger($_GET,true);
			$assignedUserRole = $user->getUserByRoleId($_GET['roleId']);
			if(!isset($assignedUserRole) || empty($assignedUserRole)){
				$isdeleted = $role->deleteRole($_GET['roleId']);
			}else{
				redirect('../user-management/manage-roles','error','Role  \''.$deletedData[0]->role_title.'\' cant be deleted when assigned to user');

			}

			if ($isdeleted == true) {
				redirect('../user-management/manage-roles','success', 'User role \''.$deletedData[0]->role_title.'\' deleted successfully!.');
			}else{
				redirect('../user-management/manage-roles','error','Sorry! Something went wrong while deleting user role');
			}
		}else{
			redirect('../404');
		}
	}else{
		redirect('../404');
	}

} else {
	redirect('../', 'error','Unauthorized access');
}
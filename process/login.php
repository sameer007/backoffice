<?php 

require '../config/config.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';

$user = new User();
$role = new Role();

//debugger($user,true);

//debugger($_POST,true);

if(isset($_POST) && !empty($_POST)){
	//debugger($_POST);
	if (isset($_POST['register']) && !empty($_POST['register']) && $_POST['register'] == 'submit') {

		$register_info  = array();

		$register_info['full_name'] = sanitize(html_entity_decode($_POST['full-name']));
		$register_info['email_address'] = filter_var(sanitize($_POST['email']) , FILTER_VALIDATE_EMAIL);
		//$register_info['role'] = sanitize(html_entity_decode($_POST['role']));

		//$password  = sanitize($_POST['password']);
		//$rePassword = sanitize($_POST['re-password']);
		$pwd = explode('@', $register_info['email_address'])[0] ;
		$password = $pwd.'-123';
		$rePassword = $pwd.'-123';

		$userRoleId = sanitize(html_entity_decode($_POST['role'])); 

		$roleData = $role->getRoleById($userRoleId);

		if(isset($roleData) && !empty($roleData)){
			$register_info['role_id'] = $roleData[0]->role_id; 
			$register_info['status'] = 1; 

			if ($password === $rePassword) {
			//$password  = sanitize($_POST['password']);
				debugger($password);

				$hashFormat = "$2y$10$";
				$salt = "backofficecrazystring22";
				$hash_and_salt = $hashFormat . $salt ;


				$register_info['password'] = crypt($password,$hash_and_salt);
				//debugger($register_info['password'],true);


				$data = $user->getUserByUserEmail($register_info['email_address']);
				/*$data = $user->getDestinationByDestinationCategoryAndDestinationRanke($destinationCategory,5);*/

			}

			if (!$data) {
				/*email address is not used*/
			//debugger($_FILES);
				if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){

					$register_info['image'] = uploadSingleFile($_FILES['image'], 'user');
				}
			//debugger($register_info,true);

				$user->addUser($register_info,true);

			//debugger(1,true);
				/*$password = sha1($username.$_POST['password']);*/
				redirect('../user-management','success','User Registration Completed Successfully.');

			}else{
				/*Email address is already used*/
				redirect('../','info','Please Register using different email.');
			}
		}else{
			redirect('../user-management','error','User role needs to be added before adding new user');

		}
		
		
	}elseif(isset($_POST['change-role']) && !empty($_POST['change-role']) && $_POST['change-role'] == 'submit') {

		$userId = sanitize($_POST['userId']);
		$roleId = sanitize($_POST['role-id']);

		$userInfo = $user->getUserByUserId($userId);

		$updatableData = array();
		$updatableData['role_id'] = sanitize($_POST['role-id']);

		$roleData = $role->getRoleById($roleId);

		if (isset($roleData) && !empty($roleData)) {
			$updatableData['role_id'] = $roleData[0]->role_id;

			if (isset($userInfo) && !empty($userInfo)) {
			//debugger($updatableData);
			//debugger($userInfo[0]->id,true);

				$is_updated = $user->updateUser($updatableData,(int)$userInfo[0]->id);
				//debugger($is_updated,true);
				if($is_updated == true){
					$full_name = $userInfo[0]->full_name;
					redirect("../user-management","success","User role of '$full_name' updated successfully");

				}else{

					redirect('../user-management','error','Sorry! Something went wrong while updating user role');

				}
			}else{
				redirect('../user-management','error','User info not found in database for role update');

			}
		}else{
			redirect('../user-management','error','Role data not found in database for role update');

		}
		

	}elseif(isset($_POST['switch-role']) && !empty($_POST['switch-role']) && $_POST['switch-role'] == 'submit') {
		//debugger($_POST,true);
		$updatebleData = array();
		$updatebleData['currentSuperadminUserId'] = sanitize($_POST['current-superadmin-user-id']);
		$updatebleData['currentSuperadminNewRoleId'] = sanitize($_POST['current-superadmin-new-role']);
		$updatebleData['newSuperadminUserId'] = sanitize($_POST['new-superadmin-user-id']);


		$roleData = $role->getRoleById($updatebleData['currentSuperadminNewRoleId']);

		if (isset($roleData) && !empty($roleData)) {
			$newSuperadminRole = $roleData[0]->role_title;
		}else{
			redirect('../user-management','error','Invalid data received');

		}
		// if($updatebleData['currentSuperadminNewRole'] == '2'){
		// 	$newSuperadminRole = 'Admin';
		// }elseif($updatebleData['currentSuperadminNewRole'] == '3'){
		// 	$newSuperadminRole = 'User';
		// }else{
		// 	redirect('../user-management','error','Invalid data received');
		// }
		$currentSuperadminInfo = $user->getUserByUserId($updatebleData['currentSuperadminUserId']);

		if(isset($currentSuperadminInfo) && !empty($currentSuperadminInfo)){
			$newSuperadminInfo = $user->getUserByUserId($updatebleData['newSuperadminUserId']);
			if(isset($newSuperadminInfo) && !empty($newSuperadminInfo)){
				//switch roles
				debugger($updatebleData);
				$roleIsSwitched = $user->updateUserRole($updatebleData);
				$oldSuperadmin = $currentSuperadminInfo[0]->full_name;
				$newSuperadmin = $newSuperadminInfo[0]->full_name;

				if($roleIsSwitched == true){
					$_SESSION['info'] = "User role of '$oldSuperadmin' has been changed to $newSuperadminRole";
					redirect("../logout","success","User role of '$newSuperadmin' has been changed to superadmin");

				}else{
					redirect('../user-management','error','Sorry! Something went wrong while switching user roles');

				}
			}else{
				redirect('../user-management','error','Sorry! Cant find user data to switch user roles');

			}
		}else{
			redirect('../user-management','error','Sorry! Cant find superadmin data to switch user roles');

		}

	}elseif(isset($_POST['update-status']) && !empty($_POST['update-status']) && $_POST['update-status'] == 'submit') {

		$userId = sanitize($_POST['update-status-user-id']);

		$userInfo = $user->getUserByUserId($userId);

		$updatableData = array();
		$updatableData['status'] = sanitize($_POST['user-status']);

		//debugger($updatableData,true);
		if (isset($userInfo) && !empty($userInfo)) {
			//debugger($updatableData);
			//debugger($userInfo[0]->id,true);

			$is_updated = $user->updateUser($updatableData,(int)$userInfo[0]->id);
			//debugger($is_updated,true);
			if($is_updated == true){
				$full_name = $userInfo[0]->full_name;
				redirect("../user-management","success","User status of '$full_name' updated successfully");

			}else{

				redirect('../user-management','error','Sorry! Something went wrong while updating user status');

			}
		}else{
			redirect('../user-management','error','User info not found in database for status update');

		}

	}else{
		
		//debugger($_POST,true);

		$user_email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
		//debugger($user_email,true);

		$password  = sanitize($_POST['password']);
		$hashFormat = "$2y$10$";
		$salt = "backofficecrazystring22";
		$hash_and_salt = $hashFormat . $salt ;

		

		$enc_password = crypt($password,$hash_and_salt);

		$user_info = $user->getUserByUserEmail($user_email);

		//debugger($enc_password,true);
		//debugger($user_info,true);
		//debugger($_POST);
		if(!$user_info){
			redirect('../','error','User not found.');
		} else {
			if($user_info[0]->password == $enc_password){
				function checkUserSession($user_info){
					if (isset($_SESSION['token']) && !empty($_SESSION['token'])) {
						debugger($_SESSION);
						//debugger($user_info,true);
						if ($_SESSION['user_id'] == $user_info[0]->id && $_SESSION['email_address'] == $user_info[0]->email_address) {
							redirect('../dashboard','success','Welcome '.$_SESSION['full_name'].'! You are already logged in to superadmin panel of Back Office.');
						}else{
							redirect('../index');
						}
					}
				}
				if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']) && isset($_SESSION['email_address']) && !empty($_SESSION['email_address'])) {
					//debugger(1,true);
					checkUserSession($user_info);

				}else{

					$_SESSION['user_id'] = $user_info[0]->id;
					$_SESSION['full_name'] = $user_info[0]->full_name;
					$_SESSION['email_address'] = $user_info[0]->email_address;
					$_SESSION['lastLoginDate'] = (!isset($_SESSION['lastLoginDate']) ? time() : $_SESSION['lastLoginDate']) ;
					$_SESSION['token'] = generateRandomString(30);

					//unsetOtherUsers($_SESSION['superadmin_token']);

					redirect('../dashboard','success','Welcome '.$user_info[0]->full_name.'! You have been successfully logged in to Back Office.');
				}
			} else {

				redirect('../','error','Password does not match.');
			}
		}
	}
}elseif(isset($_GET) && !empty($_GET)){
	$updatableData = array();

	if (isset($_GET['userId']) && !empty($_GET['userId'])) {
		if ($_GET['act'] == substr(md5('del-user-'.$_GET['userId'].'-'.$_SESSION['token']), 5, 15)) {
			$deletedData = $user->getUserByUserId($_GET['userId']);
			
			$isdeleted = $user->deleteUser($_GET['userId']);
			if ($isdeleted == true) {
				if($deletedData[0]->image != NULL && file_exists(UPLOAD_DIR.'/user/'.$deletedData[0]->image)){
					unlink(UPLOAD_DIR.'/user/'.$deletedData[0]->image);

				}

				redirect('../user-management','success', 'User \''.$deletedData[0]->full_name.'\' deleted successfully!.');
			}else{
				redirect('../user-management','error','Sorry! Something went wrong while deleting user');
			}
		}else{
			redirect('../404');
		}
	}else{
		redirect('../404');
	}

} else {
	redirect('../', 'error','Unauthorized access');
}
<?php 
require '../config/config.php';


require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';

$theme = new Theme();
$user = new User();

//debugger(1,true);

if(isset($_POST) && !empty($_POST)){
		//debugger($_POST,true);

	if (isset($_POST['add-theme']) && !empty($_POST['add-theme']) && $_POST['add-theme'] == 'submit') {
		//echo class_exists('ZipArchive');
		
		$data = array();
		$data['theme_name'] = sanitize($_POST['theme-name']);
		$data['category'] = implode(',', sanitize($_POST['category'])) ;
		$data['description'] = sanitize($_POST['description']);
		$data['current_version'] = sanitize($_POST['current-version']);
		$data['creator'] = sanitize($_POST['creator']);
		$data['licence'] = sanitize($_POST['licence']);

		//debugger($data,true);
		if(isset($_FILES['themeZipFIle']) && $_FILES['themeZipFIle']['error'] == 0){
			//debugger($_FILES);

			if($_FILES['themeZipFIle']['type'] == 'application/x-zip-compressed'){
				// $data['folderName'] = uploadSingleFile($_FILES['themeZipFIle'], 'user');
				//debugger(PATHINFO_DIRNAME);

				// assuming file.zip is in the same directory as the executing script.
				$zipfile = $_FILES['themeZipFIle']['name'] ;
				//$flag = (file_exists($zipfile))? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE;

				// get the absolute path to $file
				//$path = pathinfo(realpath($zipfile), PATHINFO_DIRNAME);
				//debugger($path);
				//debugger(THEME_UPLOAD_DIR);
				$zip = new ZipArchive();
				$res = $zip->open($_FILES['themeZipFIle']['tmp_name']);
				if ($res === TRUE) {
			  		// extract it to the path we determined above
					$themeName = $data['theme_name'];
					$path = THEME_UPLOAD_DIR.$themeName.'/';
					$zip->extractTo($path);
					$zip->close();

					//debugger($data,true);

					$hasSamethemeTitle = $theme->getThemeByThemeTitle($data['theme_name']);

					

					//debugger($themeData,true);
					/*$password = sha1($username.$_POST['password']);*/
					if(!isset($hasSamethemeTitle) || empty($hasSamethemeTitle)){
						$themeData = $theme->addTheme($data);
						if (isset($themeData) && !empty($themeData)) {
							$themeTitle = $data['theme_name'];
							redirect("../theme/","success","Theme '$themeTitle' added successfully");

						}else{
							redirect("../theme/new_theme","error","Sorry! Something went wrong while adding theme '$themeTitle'");

						}
					}else{
						redirect('../theme/new_theme','error','Cannot add new theme with same name');

					}
					//echo "WOOT! $zipfile extracted to $path";
				} else {
					//echo "Doh! I couldn't open $zipfile";
					redirect('../theme/new_theme','error','Something went wrong while extracting theme zip file');

				}
			}else{
				redirect('../theme/new_theme','error','Please upload theme package file of zip format');

			}
			
		}else{
			redirect('../theme/new_theme','error','Please upload theme package file');

		}
	}elseif(isset($_POST['edit-theme']) && !empty($_POST['edit-theme']) && $_POST['edit-theme'] == 'submit'){
		$data = array();
		$data['theme_name'] = sanitize($_POST['theme-name']);
		$data['category'] = implode(',', sanitize($_POST['category'])) ;
		$data['description'] = sanitize($_POST['description']);
		$data['current_version'] = sanitize($_POST['current-version']);
		$data['creator'] = sanitize($_POST['creator']);
		$data['licence'] = sanitize($_POST['licence']);

		//debugger($_SESSION);
		$themeData = $theme->getThemeById($_SESSION['theme_id']);
		//debugger($data,true);

		if (isset($themeData) && !empty($themeData)) {
			$hasSamethemeTitle = $theme->getThemeByThemeTitle($data['theme_name']);

			//debugger($hasSamethemeTitle,true);
			/*does not have same theme name as existing theme in database*/
			if(!isset($hasSamethemeTitle) || empty($hasSamethemeTitle)){
				$data['theme_name'] = sanitize($_POST['theme-name']);
			}else{
				$data['theme_name'] = $themeData[0]->theme_name;

			}

			$theme_id = $theme->updateTheme($data,$_SESSION['theme_id']);


			$themeId = $_SESSION['theme_id'];

			if (isset($theme_id) && !empty($theme_id)) {

				if(isset($_FILES['themeZipFIle']) && $_FILES['themeZipFIle']['error'] == 0){
					//debugger($_FILES);
					$oldThemeName = $themeData[0]->theme_name;
					$newThemeName = $data['theme_name'];

					if ($oldThemeName == $newThemeName) {
						deleteDirectory(THEME_UPLOAD_DIR.$oldThemeName.'/');
					}

					$isUploaded = uploadAndExtractZip($_FILES['themeZipFIle'],THEME_UPLOAD_DIR.$newThemeName);

					if($isUploaded){

						if ($oldThemeName != $newThemeName) {
							deleteDirectory(THEME_UPLOAD_DIR.$oldThemeName.'/');

						}
					}

				}else{
					rename(THEME_UPLOAD_DIR.$themeData[0]->theme_name, THEME_UPLOAD_DIR.$data['theme_name']);
				}
				$updatedData = $theme->getThemeById($themeId);

				if (isset($updatedData) && !empty($updatedData)) {

					$url = '?themeId='.$updatedData[0]->id.'&act='.substr(md5('edit-theme-'.$updatedData[0]->id.'-'.$_SESSION['token']), 5, 15);
						//debugger($url,true);
					redirect('../theme/theme_details'.$url,'success','Theme details of "'.$updatedData[0]->theme_name.'" updated succesfully.');
				}else{
					redirect('../theme/','warning', 'Cannot find updated theme data');
				}
			}else{
					redirect('../theme/','error', 'Sorry! Something went wrong while updating theme info.');

			}
		}else{
			redirect('../theme/','warning', 'Cannot find theme info in database');

		}
	}else{
		redirect('../404');

	}

}elseif(isset($_GET) && !empty($_GET)){
	$updatableData = array();

	if (isset($_GET['themeId']) && !empty($_GET['themeId'])) {

		if ($_GET['act'] == substr(md5('delete-theme-'.$_GET['themeId'].'-'.$_SESSION['token']), 5, 15)) {
			// $deletedData = $theme->getthemeById($_GET['themeId']);
			// //debugger($_GET,true);
			// $assignedUsertheme = $user->getUserBythemeId($_GET['themeId']);
			// if(!isset($assignedUsertheme) || empty($assignedUsertheme)){
			// 	$isdeleted = $theme->deletetheme($_GET['themeId']);
			// }else{
			// 	redirect('../user-management/manage-themes','error','theme  \''.$deletedData[0]->theme_title.'\' cant be deleted when assigned to user');

			// }

			// if ($isdeleted == true) {
			// 	redirect('../user-management/manage-themes','success', 'User theme \''.$deletedData[0]->theme_title.'\' deleted successfully!.');
			// }else{
			// 	redirect('../user-management/manage-themes','error','Sorry! Something went wrong while deleting user theme');
			// }
		}else{
			redirect('../404');
		}
	}else{
		redirect('../404');
	}

} else {
	redirect('../', 'error','Unauthorized access');
}
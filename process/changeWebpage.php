<?php 
require '../config/config.php';


require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/manageWebsiteFunctions.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';

$website = new Website();
$theme = new Theme();

$user = new User();



if(isset($_POST) && !empty($_POST)){

	if (isset($_POST['change-webpage']) && !empty($_POST['change-webpage']) && $_POST['change-webpage'] == 'submit') {
		$contentTypeNameArray = getContentTypeNames();
        //debugger(sanitize($_POST['current-page-url']));
		$websiteData = $website->getAllWebsiteDataById($_SESSION['website_id']);

		$currentPageUrl  = sanitize($_POST['current-page-url']);
		//debugger($currentPageUrl);
		$xml = loadHtmlToXml($currentPageUrl);
		$sectionElArray = $xml->body;
		//debugger($sectionElArray);
		foreach ($sectionElArray as $key => $sectionElArrayValue) {
			foreach ($sectionElArrayValue as $sectionElArrayValueKey => $sectionElArrayValueItems) { 

				$isSection = false;

				/*check if class attribute of section exists*/
				/*proccessing to get section name*/
				if(isset($sectionElArrayValueItems->attributes()['class'][0]) && !empty($sectionElArrayValueItems->attributes()['class'][0])){
					$attrValue = (string)$sectionElArrayValueItems->attributes()['class'][0];
					$sectionClassArr = explode(' ', $attrValue);

					if(isset(explode('-',$sectionClassArr[0])[1]) && !empty(explode('-',$sectionClassArr[0])[1])){

						$themeNameFromUIFromSectionClassArr = explode('-',$sectionClassArr[0])[1];

						if($websiteData[0]->theme_name == $themeNameFromUIFromSectionClassArr){
							$isSection = true;
							$sectionName = explode('-',$sectionClassArr[0])[0];
						}
					}
				}

				/*if there is section*/
				if($isSection && isset($sectionName) && !empty($sectionName)){

					/*loop through section elements*/
					foreach ($sectionElArrayValueItems as $sectionElArrayValueItemsKey => $sectionElArrayValueItemsContentTypeEl) {

						$isContentType = false;
						$isSubContentType = false;
						$isContentTypeOfSubContentType = false;

						$subContentTypeArray = array();

						if(isset($sectionElArrayValueItemsContentTypeEl->attributes()['class'][0]) && !empty($sectionElArrayValueItemsContentTypeEl->attributes()['class'][0])){
							$contentTypeAttrValue = (string)$sectionElArrayValueItemsContentTypeEl->attributes()['class'][0];
							$contentTypeClassArr = explode(' ', $contentTypeAttrValue);

                                                      //debugger($contentTypeClassArr);
							/*if there are multiple columns inside row in section*/
							if(in_array('row', $contentTypeClassArr)){
								foreach ($sectionElArrayValueItemsContentTypeEl as $sectionElArrayValueItemsContentTypeElKey => $sectionElArrayValueItemsContentTypeElValue) {

									$subContentTypeClassAttribute = $sectionElArrayValueItemsContentTypeElValue->attributes()['class'][0];

									$subContentTypeClassAttributeArr = explode(' ', $subContentTypeClassAttribute);

									$isSubContentType = true;
									$subContentTypeName = explode('-',$subContentTypeClassAttributeArr[0])[0];

									/*check content type inside columns and display it*/
									foreach ($sectionElArrayValueItemsContentTypeElValue as $contentTypeAttributesKey => $contentTypeAttributesValue) {

										$contentTypeClassAttribute = $contentTypeAttributesValue->attributes()['class'][0];
										$contentTypeClassAttributeArr = explode(' ', $contentTypeClassAttribute);

										$contentTypeNameOfSubContentTypeArr = array();

										/*loop through to check and display content types*/
										foreach ($contentTypeClassAttributeArr as $key => $contentTypeClassAttributeArrValue) {
											$themeNameFromUIFromContentTypeClassArr = explode('-',$contentTypeClassAttributeArrValue);

											if(in_array($websiteData[0]->theme_name, $themeNameFromUIFromContentTypeClassArr)){

												$isContentTypeOfSubContentType = true;
												$contentTypeNameOfSubContentType = $themeNameFromUIFromContentTypeClassArr[0];
												if(in_array('Multiple', $themeNameFromUIFromContentTypeClassArr)){
													$contentTypeNameOfSubContentType = $contentTypeNameOfSubContentType.'-Multiple';
												}
												/*initializing content types*/
                                                                //debugger($contentTypeNameOfSubContentType);
												$contentTypeAttributesValueAllClassName = $contentTypeAttributesValue->attributes()['class'][0]; 
												$contentTypeAttributesValueClassNameArr = explode(' ', $contentTypeAttributesValueAllClassName);
												if (strpos($contentTypeAttributesValueClassNameArr[0], 'Content') !== false) {
												//debugger($contentTypeAttributesValue);

													foreach ($contentTypeAttributesValue as $contentTypeAttributesValueKey => $contentTypeAttributesValueItems) {
														//$subContentTypeName
														//debugger($contentTypeAttributesValueClassNameArr);
														$colContentTypeDetailsArr = explode('-', $contentTypeAttributesValueClassNameArr[0]);
														$columnName = $colContentTypeDetailsArr[1];

														$subContentTypeArray[$subContentTypeName][$columnName]['contentTypeContents'][] = $contentTypeNameOfSubContentType;
														$subContentTypeArray[$subContentTypeName][$columnName]['contentTypeElValue'] = $contentTypeAttributesValue;
													}
												}else{
													$subContentTypeArray[$subContentTypeName]['contentTypeContents'][] = $contentTypeNameOfSubContentType;
													$subContentTypeArray[$subContentTypeName]['contentTypeElValue'] = $contentTypeAttributesValue;
												}
												
												//$subContentTypeArray[$subContentTypeName]['contentTypeContents'][] = $contentTypeNameOfSubContentType;
												//$subContentTypeArray[$subContentTypeName]['contentTypeElValue'] = $contentTypeAttributesValue;
											}
										}

									}

								}
							}else{
                                //debugger('here');

								/*if there are no multiple colums inside rows in section*/
								if(isset(explode('-',$contentTypeClassArr[0])[1]) && !empty(explode('-',$contentTypeClassArr[0])[1])){

									$themeNameFromUIFromContentTypeClassArr = explode('-',$contentTypeClassArr[0])[1];
									if($websiteData[0]->theme_name == $themeNameFromUIFromContentTypeClassArr){
										$isContentType = true;
										$contentTypeName = explode('-',$contentTypeClassArr[0])[0];

									}
								}

							}

						}
						$contentCount = 0 ;
						/*to display sub content types of section having columns*/
						if($isSubContentType && isset($subContentTypeArray) && !empty($subContentTypeArray)){
							//debugger($subContentTypeArray);
							foreach ($subContentTypeArray as $subContentTypeColumnName => $subContentTypeNameArr) { 
								$contentCount = 0 ;
								//debugger($subContentTypeNameArr);

								foreach ($subContentTypeNameArr as $columnName => $columnElItems) {
									# code...
									processNodeForBackend([],$columnElItems['contentTypeElValue'],$output,$contentTypeNameArray,$websiteData[0]->theme_name);
									
									foreach ($columnElItems['contentTypeContents'] as $subContentTypeNameArrKey => $subContentTypePropsData) {


                                    //debugger($subContentTypeName);
										$subContentTypePropsArray = explode('-',$subContentTypePropsData);
										$subContentTypeName = $subContentTypePropsArray[0];

										$subContentTypeNameToDisplay = $subContentTypeName ;
										if($subContentTypeName == 'Content'){
											$contentCount ++ ;
											$subContentTypeNameToDisplay = $subContentTypeNameToDisplay.'-'.$contentCount ;
										}

									}
									$arrangedRenderFormData = createArrangedRenderFormData($contentTypeProperties,$subContentTypeName,$subContentTypeNameToDisplay);

									//debugger($contentTypeProperties);

											//debugger($arrangedRenderFormData);
									foreach ($arrangedRenderFormData as $arrangedRenderFormDataKey => $arrangedRenderFormDataList) {
											//debugger($arrangedRenderFormDataKey);
										if(isset($arrangedRenderFormDataList['tableData']) && !empty($arrangedRenderFormDataList['tableData'])){
											//debugger($arrangedRenderFormDataList);
											foreach ($arrangedRenderFormDataList['tableData'] as $tableDataKey => $tableDataItems) {
											//loop through in table 

												$inputType = $arrangedRenderFormDataList['tableData'][0]['inputType'];
												$label = explode('-', $arrangedRenderFormDataList['tableData'][0]['label'])[0];
												$nameAttr = $tableDataItems['nameAttr'];

												if($inputType == 'file'){
												//debugger($nameAttr);
													if(isset($_FILES[$nameAttr]) && !empty($_FILES[$nameAttr])){
												//
														if($_FILES[$nameAttr]['error'] == 0){

															$destination = SITE_DIR.$websiteData[0]->website_id.'/uploads/images';
															$allowedExtention = array('apng','jpg','jpeg','avif','gif','jfif','pjpeg','png','svg','webp');
															$uploaded_file = uploadSingleThemeFile($_FILES[$nameAttr],$destination,$allowedExtention);

															if(isset($uploaded_file) && !empty($uploaded_file)){

																$uploadedFileUrl = getUploadedFileUrlForWeb($websiteData,$uploaded_file,'images');
																$tableDataItems['imageName'] = $uploaded_file;
																$tableDataItems['src'] = $uploadedFileUrl;


																alterWebpage($contentTypeProperties,'Content',$sectionElArray,$tableDataItems,$websiteData);

															}

														}
													}
												}else{
													if($inputType == 'text' || $inputType == 'textarea' || $inputType == 'url'){

														if($inputType == 'url'){
															if((isset($_POST[$nameAttr.'-text']) && !empty($_POST[$nameAttr.'-text'])) || (isset($_POST[$nameAttr.'-checkbox']) && !empty($_POST[$nameAttr.'-checkbox'])) || (isset($_POST[$nameAttr.'-url']) && !empty($_POST[$nameAttr.'-url']))){
																alterWebpage($contentTypeProperties,'Content',$sectionElArray,$tableDataItems,$websiteData);

															}else{
																if(isset($_POST[$nameAttr]) && !empty($_POST[$nameAttr])){
																	alterWebpage($contentTypeProperties,'Content',$sectionElArray,$tableDataItems,$websiteData);
																}
															}

														}else{
															if(isset($_POST[$nameAttr]) && !empty($_POST[$nameAttr])){
															//debugger($contentTypeProperties);

																alterWebpage($contentTypeProperties,'Content',$sectionElArray,$tableDataItems,$websiteData);

															}
														}
														
													}
												}
											}

										}else{
										//loop through not in table
											//debugger('$arrangedRenderFormDataList');

											//debugger($arrangedRenderFormDataList);
											foreach ($arrangedRenderFormDataList as $arrangedRenderFormDataListkey => $arrangedRenderFormDataListItems) {
										//debugger($arrangedRenderFormDataListItems);
												$inputType = $arrangedRenderFormDataListItems['inputType'];
												$nameAttr = $arrangedRenderFormDataListItems['nameAttr'];
											//debugger($arrangedRenderFormDataListItems);
												$subContentType = $arrangedRenderFormDataListItems['subContentType'];

												$subContentType = str_replace(' ', '', $subContentType)	;
												if($inputType == 'file'){
												//debugger($nameAttr);
													if(isset($_FILES[$nameAttr]) && !empty($_FILES[$nameAttr])){

														if($_FILES[$nameAttr]['error'] == 0){

															$destination = SITE_DIR.$websiteData[0]->website_id.'/uploads/images';
															$allowedExtention = array('apng','jpg','jpeg','avif','gif','jfif','pjpeg','png','svg','webp');
															$uploaded_file = uploadSingleThemeFile($_FILES[$nameAttr],$destination,$allowedExtention);

															if(isset($uploaded_file) && !empty($uploaded_file)){

																$uploadedFileUrl = getUploadedFileUrlForWeb($websiteData,$uploaded_file,'images');
																$arrangedRenderFormDataListItems['imageName'] = $uploaded_file;
																$arrangedRenderFormDataListItems['src'] = $uploadedFileUrl;

																alterWebpage($contentTypeProperties,'Content',$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);

															}

														}
													}	
												}else{
													if($inputType == 'text' || $inputType == 'textarea' || $inputType == 'url'){
													//debugger($_POST[$nameAttr]);
														//debugger('$arrangedRenderFormDataListItems');

														if($inputType == 'url'){

															debugger('$arrangedRenderFormDataListItems');

														//debugger($arrangedRenderFormDataListItems);
															if((isset($_POST[$nameAttr.'-text']) && !empty($_POST[$nameAttr.'-text'])) || (isset($_POST[$nameAttr.'-checkbox']) && !empty($_POST[$nameAttr.'-checkbox'])) || (isset($_POST[$nameAttr.'-url']) && !empty($_POST[$nameAttr.'-url']))){


																alterWebpage($contentTypeProperties,'Content',$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);

															}else{
																if(isset($_POST[$nameAttr]) && !empty($_POST[$nameAttr])){
																	alterWebpage($contentTypeProperties,'Content',$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);

																}
															}

														}else{
															if(isset($_POST[$nameAttr]) && !empty($_POST[$nameAttr])){

																/*passing 'Content' as content type for content type form inside columns '*/

																alterWebpage($contentTypeProperties,'Content',$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);

															}
														}
														
													}
												}

											}
										//debugger($arrangedRenderFormDataList);
										}

									}
									unset($contentTypeProperties);

								}


							}
						}
						/*to display content types of section which doesn't have columns*/
						if($isContentType && isset($contentTypeName) && !empty($contentTypeName)){

							processNodeForBackend([],$sectionElArrayValueItems,$output,$contentTypeNameArray,$websiteData[0]->theme_name);
							
							$arrangedRenderFormData = createArrangedRenderFormData($contentTypeProperties,$contentTypeName,$contentTypeName);
							//debugger($arrangedRenderFormData);
							foreach ($arrangedRenderFormData as $arrangedRenderFormDataKey => $arrangedRenderFormDataList) {
								//debugger($arrangedRenderFormDataKey);
								if(isset($arrangedRenderFormDataList['tableData']) && !empty($arrangedRenderFormDataList['tableData'])){

									foreach ($arrangedRenderFormDataList['tableData'] as $tableDataKey => $tableDataItems) {
										//loop through in table 

										$inputType = $arrangedRenderFormDataList['tableData'][0]['inputType'];
										$label = explode('-', $arrangedRenderFormDataList['tableData'][0]['label'])[0];
										$nameAttr = $tableDataItems['nameAttr'];

										if($inputType == 'file'){
											//debugger($nameAttr);
											
											if(isset($_FILES[$nameAttr]) && !empty($_FILES[$nameAttr])){
												//debugger($_FILES[$nameAttr]);
												if($_FILES[$nameAttr]['error'] == 0){

													$destination = SITE_DIR.$websiteData[0]->website_id.'/uploads/images';
													$allowedExtention = array('apng','jpg','jpeg','avif','gif','jfif','pjpeg','png','svg','webp');
													$uploaded_file = uploadSingleThemeFile($_FILES[$nameAttr],$destination,$allowedExtention);

													if(isset($uploaded_file) && !empty($uploaded_file)){

														$uploadedFileUrl = getUploadedFileUrlForWeb($websiteData,$uploaded_file,'images');
														$tableDataItems['imageName'] = $uploaded_file;
														$tableDataItems['src'] = $uploadedFileUrl;

														alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$tableDataItems,$websiteData);

													}

												}else{
													alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$tableDataItems,$websiteData);

												}
											}else{
												//debugger('xxx');

											}

										}else{

											if($inputType == 'text' || $inputType == 'textarea'){
												//debugger($_POST[$nameAttr]);
													//debugger($contentTypeName);
												//
												//
												if(isset($_POST[$nameAttr]) && !empty($_POST[$nameAttr])){
													alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$tableDataItems,$websiteData);

												}else{
													processingToHideDomElement($arrangedRenderFormDataListItems,$nameAttr,$sectionElArray);

												}
											}elseif($inputType == 'url'){

												alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$tableDataItems,$websiteData);
											}
										}
									}
								}else{
											//loop through not in table
									$i = 0 ;

									//debugger($contentTypeProperties[$contentTypeName]);
													//debugger($arrangedRenderFormDataList);

									foreach ($arrangedRenderFormDataList as $arrangedRenderFormDataListkey => $arrangedRenderFormDataListItems) {
										$inputType = $arrangedRenderFormDataListItems['inputType'];
										$nameAttr = $arrangedRenderFormDataListItems['nameAttr'];
										$subContentType = $arrangedRenderFormDataListItems['subContentType'];
										
										$subContentType = str_replace(' ', '', $subContentType)	;		
										//debugger($subContentType);
										//debugger($nameAttr);
										//debugger($arrangedRenderFormDataListItems);
										
										if($inputType == 'file'){
											
											if(isset($_FILES[$nameAttr]) && !empty($_FILES[$nameAttr])){

												if($_FILES[$nameAttr]['error'] == 0){

													$destination = SITE_DIR.$websiteData[0]->website_id.'/uploads/images';
													$allowedExtention = array('apng','jpg','jpeg','avif','gif','jfif','pjpeg','png','svg','webp');
													$uploaded_file = uploadSingleThemeFile($_FILES[$nameAttr],$destination,$allowedExtention);

													if(isset($uploaded_file) && !empty($uploaded_file)){

														$uploadedFileUrl = getUploadedFileUrlForWeb($websiteData,$uploaded_file,'images');
														$arrangedRenderFormDataListItems['imageName'] = $uploaded_file;
														$arrangedRenderFormDataListItems['src'] = $uploadedFileUrl;

														alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);

													}

												}
											}else{
												/*processing for hiding dom element feature*/
												processingToHideDomElement($arrangedRenderFormDataListItems,$nameAttr,$sectionElArray);
											}
										}else{
													//debugger($arrangedRenderFormDataListItems);

											if($inputType == 'text' || $inputType == 'textarea'){

												if(isset($_POST[$nameAttr]) && !empty($_POST[$nameAttr])){
													//debugger($nameAttr);
													//debugger($arrangedRenderFormDataListkey);
													//debugger($subContentType);

													alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);
													
												}else{	

													/*processing for hiding dom element feature*/
													
													debugger($arrangedRenderFormDataListItems);
													processingToHideDomElement($arrangedRenderFormDataListItems,$nameAttr,$sectionElArray);
													

													//debugger($currentXml);

												}
											}elseif($inputType == 'url'){

												alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$arrangedRenderFormDataListItems,$websiteData);

											}
										}

									}

								}

							}
							//debugger($parentXML);

								// not in table (file,text,url,textarea)
							unset($contentTypeProperties);
						}
					} 
				}
			}
		}


		if(isset($_POST['current-page-url']) && !empty($_POST['current-page-url'])){
			$currentPageUrl = sanitize($_POST['current-page-url']);
			$currentPageUrlArr = explode('/', $currentPageUrl);
			$currentPage = end($currentPageUrlArr);
			$page = explode('.php', $currentPage)[0];
			//debugger($page);

			//debugger($page);
			$editPageUrl = '../manage-website/home/edit-page?websiteId='.$websiteData[0]->website_id.'&pageName='.$page.'&act='.substr(md5('edit-page-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);

			if(isset($sectionElArray) && !empty($sectionElArray)){
				$pageName = 'index1.php';

					//debugger($websiteData);
				//debugger($sectionElArray,true);
				if(isset($_POST['page-name']) && !empty($_POST['page-name'])){
					$pageName = sanitize($_POST['page-name']);
				//$pageName = 'index1.php';
				$pathFileIsWritten = writeToPage($sectionElArray,$websiteData,$pageName);
					//debugger('$pathFileIsWritten');
					if(isset($pathFileIsWritten) && !empty($pathFileIsWritten)){
						if(file_exists($pathFileIsWritten)){
							//debugger('12121');
							//debugger($pathFileIsWritten);
							redirect($editPageUrl,'success','New Webpage Created successfully : '.$pageName);

						}
					}


				}else{
					redirect($editPageUrl,'error','Page name not found to make changes');

				}
			}else{
				redirect($editPageUrl,'error','SimpleExmElement object not found to make changes');
			}
		}else{
			redirect($editPageUrl,'error','Current page URL not found to make changes');

		}


		//debugger($editPageUrl);
		
        //$searchedEl = $sectionElArray->xpath("//*[@name='s1-Logo_Heading']");
        //debugger($searchedEl);
        //
		//debugger($_POST);

		//debugger($sectionElArray);
		//debugger($_POST);

		debugger(1,TRUE);







	}else{
		redirect('../404');

	}

}elseif(isset($_GET) && !empty($_GET)){
	$updatableData = array();

	if (isset($_GET['websiteId']) && !empty($_GET['websiteId'])) {
		//debugger($_GET,TRUE);

		if ($_GET['act'] == substr(md5('delete-website-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {

			
		}elseif ($_GET['act'] == substr(md5('duplicate-website-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {

		}else{
			redirect('../404');
		}
	}else{
		redirect('../404');
	}

} else {
	redirect('../', 'error','Unauthorized access');
}
<?php 
require '../config/config.php';


require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';

$website = new Website();
$theme = new Theme();

$user = new User();

	//debugger($_POST,true);

if(isset($_POST) && !empty($_POST)){
	//debugger($_POST);
	if (isset($_POST['add-new-page']) && !empty($_POST['add-new-page']) && $_POST['add-new-page'] == 'submit') {
		
		$websiteData = $website->getAllWebsiteDataById($_SESSION['website_id']);

		if(isset($websiteData) && !empty($websiteData)){
			unset($_SESSION['website_id']);
			$newPageName = explode('.',sanitize($_POST['new-page-name']))[0];

			$execeptionFilesAndFolders = array() ;
			$execeptionFilesAndFolders['folders'] = array();
			$execeptionFilesAndFolders['files'] = array('maintainance.php');

			// creating new page by copying blank.php and renaming it as desired newPageName 
			custom_copy(SITE_DIR.$websiteData[0]->website_id.'/system/', SITE_DIR.$websiteData[0]->website_id,$execeptionFilesAndFolders);
			rename(SITE_DIR.$websiteData[0]->website_id.'/blank.php' ,SITE_DIR.$websiteData[0]->website_id.'/'.$newPageName.'.php');

			//redirect
			$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
			redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","New Page  created successfully");
		}else{
			redirect("../manage-website","error","Website not found to add new page");
		}
	}elseif (isset($_POST['duplicate-page']) && !empty($_POST['duplicate-page']) && $_POST['duplicate-page'] == 'submit') {
		
		$websiteData = $website->getAllWebsiteDataById($_SESSION['website_id']);

		if(isset($websiteData) && !empty($websiteData)){
			unset($_SESSION['website_id']);
			$newPageName = explode('.',sanitize($_POST['new-page-name']))[0];

			$execeptionFilesAndFolders = array() ;
			$execeptionFilesAndFolders['folders'] = array();
			$execeptionFilesAndFolders['files'] = array('maintainance.php');

			// creating new page by copying blank.php and renaming it as desired newPageName 
			custom_copy(SITE_DIR.$websiteData[0]->website_id.'/system/', SITE_DIR.$websiteData[0]->website_id,$execeptionFilesAndFolders);
			rename(SITE_DIR.$websiteData[0]->website_id.'/blank.php' ,SITE_DIR.$websiteData[0]->website_id.'/'.$newPageName.'.php');

			//redirect
			$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
			redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","New Page  created successfully");
		}else{
			redirect("../manage-website","error","Website not found to add new page");
		}
	}elseif (isset($_POST['rename-page']) && !empty($_POST['rename-page']) && $_POST['rename-page'] == 'submit') {
		
		$websiteData = $website->getAllWebsiteDataById($_SESSION['website_id']);
		if(isset($websiteData) && !empty($websiteData)){
			//unset($_SESSION['website_id']);
			$newPageName = explode('.',sanitize($_POST['new-page-name']))[0];
			$currentPageName = explode('.',sanitize($_POST['current-page-name']))[0];

			$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
			$pageIsThere = false ;
			$newPageIsThere = false ;
			if(is_dir(SITE_DIR.$websiteData[0]->website_id.'/')){
				debugger('A');
				$dir = opendir(SITE_DIR.$websiteData[0]->website_id.'/');
				$pagesArray = array();
				$dirArray = array();
				while ($file = readdir($dir)) {

					if(!is_dir(SITE_DIR.$websiteData[0]->website_id.'/'.$file)){
								//debugger($file); 
						array_push($pagesArray, $file);
						if($currentPageName.'.php' == $file){
							$pageIsThere = true ;
						}
						if($newPageName.'.php' == $file){
							$newPageIsThere = true ;
						}


					}else{
						array_push($dirArray, $file);

					}

				}
			}


			if($pageIsThere){
				if(!$newPageIsThere){

					$execeptionFilesAndFolders = array() ;
					$execeptionFilesAndFolders['folders'] = array();
					$execeptionFilesAndFolders['files'] = array('maintainance.php');

					// creating new page by copying blank.php and renaming it as desired newPageName 
					//custom_copy(SITE_DIR.$websiteData[0]->website_id.'/system/', SITE_DIR.$websiteData[0]->website_id,$execeptionFilesAndFolders);
					rename(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName.'.php' ,SITE_DIR.$websiteData[0]->website_id.'/'.$newPageName.'.php');
					//debugger($websiteData,true);

					redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","New Page  created successfully");
				}else{
					redirect("../manage-website/home/".$manageWebsiteHomeURL,"error","Please use different page name to rename");

				}
			}else{
				redirect("../manage-website/home/".$manageWebsiteHomeURL,"error","Page not found to rename");

			}
			
		}else{
			redirect("../manage-website","error","Website not found to add new page");
		}
	}else{
		redirect("../manage-website/create-new-site","error","Theme not found to create website");

	}

}elseif(isset($_GET) && !empty($_GET)){
	$updatableData = array();
	//debugger($_GET,TRUE);

	if (isset($_GET['websiteId']) && !empty($_GET['websiteId'])) {
		if ($_GET['act'] == substr(md5('duplicate-page-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {
			$pageIsThere = false ;
			if (isset($_GET['pageName']) && !empty($_GET['pageName'])) {

				$websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);
				//debugger(1);

				//debugger($websiteData,true);
				//debugger(SITE_DIR.$websiteData[0]->website_id.'/'.$_GET['pageName'].'.php');
				if(isset($websiteData) && !empty($websiteData)){
					if(is_dir(SITE_DIR.$websiteData[0]->website_id.'/')){
						debugger('A');
						$dir = opendir(SITE_DIR.$websiteData[0]->website_id.'/');
						$pagesArray = array();
						$dirArray = array();
						while ($file = readdir($dir)) {

							if(!is_dir(SITE_DIR.$websiteData[0]->website_id.'/'.$file)){
								//debugger($file); 
								array_push($pagesArray, $file);
								if($_GET['pageName'].'.php' == $file){
									$pageIsThere = true ;
								}

							}else{
								array_push($dirArray, $file);

							}
							
						}
					}
					$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
					if($pageIsThere){

						$pagesException = $pagesArray ;
						$folderException = $dirArray ;
						//array_search(needle, haystack)
						if (($pagesKey = array_search($_GET['pageName'].'.php', $pagesException)) !== false) {
							unset($pagesException[$pagesKey]);
						}
						//debugger($pagesException);
						$execeptionFilesAndFolders = array() ;
						$execeptionFilesAndFolders['folders'] = $folderException;
						$execeptionFilesAndFolders['files'] = $pagesException;

						// creating new page by copying blank.php and renaming it as desired newPageName 
						custom_copy(SITE_DIR.$websiteData[0]->website_id, SITE_DIR.$websiteData[0]->website_id.'/system/',$execeptionFilesAndFolders);
						

						$i = 0 ;
						do {
							$i++ ;
							$newPageName = $_GET['pageName'].'.php';

							preg_match('/(\d+)\D*$/', $newPageName, $m);
							if(isset($m[1]) && !empty($m[1])){
								$lastnum = $m[1];
                //debugger($lastnum);
								$newPageNum = $lastnum + 1 ;
								$newPage = 'index'.$newPageNum ;
							}else{
								$newPage = 'index1' ;
							}
							$newPageName = $newPage.'.php' ;

						} while (in_array($newPageName, $pagesArray));

						$newPage = $newPageName;
						rename(SITE_DIR.$websiteData[0]->website_id.'/system/'.$_GET['pageName'].'.php' ,SITE_DIR.$websiteData[0]->website_id.'/'.$newPage);
						//debugger('x',true);
						
						$currentPageName = $_GET['pageName'].'.php';
						redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","Webpage '$currentPageName' duplicated successfully as '$newPage'");

					}else{
						redirect("../manage-website/home/".$manageWebsiteHomeURL,"error","Webpage not found for duplication");

					}
				}else{
					redirect("../manage-website/","error","Website data not found in database for page duplication");

				}
			}else{
				redirect("../manage-website/","error","Page name not provided");

			}
		}elseif ($_GET['act'] == substr(md5('set-as-homepage-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {
			$pageIsThere = false ;
			if (isset($_GET['pageName']) && !empty($_GET['pageName'])) {

				$websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);
				//debugger($websiteData);
				//debugger(SITE_DIR.$websiteData[0]->website_id.'/'.$_GET['pageName'].'.php');
				if(isset($websiteData) && !empty($websiteData)){
					if(is_dir(SITE_DIR.$websiteData[0]->website_id.'/')){
						debugger('A');
						$dir = opendir(SITE_DIR.$websiteData[0]->website_id.'/');
						$pagesArray = array();
						$dirArray = array();
						while ($file = readdir($dir)) {

							if(!is_dir(SITE_DIR.$websiteData[0]->website_id.'/'.$file)){
								//debugger($file); 
								array_push($pagesArray, $file);
								if($_GET['pageName'].'.php' == $file){
									$pageIsThere = true ;
								}

							}else{
								array_push($dirArray, $file);

							}
							
						}
					}
					$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
					if($pageIsThere){

						$pagesException = $pagesArray ;
						$folderException = $dirArray ;
						//array_search(needle, haystack)
						if (($pagesKey = array_search('index.php', $pagesException)) !== false) {
							unset($pagesException[$pagesKey]);
						}
						//debugger($pagesException);
						$execeptionFilesAndFolders = array() ;
						$execeptionFilesAndFolders['folders'] = $folderException;
						$execeptionFilesAndFolders['files'] = $pagesException;

						$indexFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/index.php');
						$pageFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/'.$_GET['pageName'].'.php');

						$currentPageName = $_GET['pageName'].'.php';
						if($indexFileContents && $pageFileContents){

							//debugger(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName,true);
							unlink(SITE_DIR.$websiteData[0]->website_id.'/index.php');
							unlink(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName);
							fopen(SITE_DIR.$websiteData[0]->website_id.'/index.php','r');
							fopen(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName,'r');

							file_put_contents(SITE_DIR.$websiteData[0]->website_id.'/index.php', $pageFileContents);
							file_put_contents(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName, $indexFileContents);
							//debugger('true');
							redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","Webpage '$currentPageName' set as 'index.php' homepage");
						}else{
							redirect("../manage-website/home/".$manageWebsiteHomeURL,"error","Cannot read page contents to switch homepage");

						}

					}else{
						redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","Webpage not found for setting up homepage");

					}
				}else{
					redirect("../manage-website/","error","Website data not found in database for changing homepage");

				}
			}else{
				redirect("../manage-website/","error","Page name not provided");

			}
		}elseif ($_GET['act'] == substr(md5('delete-page-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {
			$websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);

			$websiteName = $websiteName = 'sampleWebsiteDomain - '.$websiteData[0]->website_id;
			$pageIsThere = false ;
			if(isset($websiteData) && !empty($websiteData)){
				if(is_dir(SITE_DIR.$websiteData[0]->website_id.'/')){
					$dir = opendir(SITE_DIR.$websiteData[0]->website_id.'/');
					$pagesArray = array();
					$dirArray = array();
					while ($file = readdir($dir)) {

						if(!is_dir(SITE_DIR.$websiteData[0]->website_id.'/'.$file)){
							debugger($file); 
							array_push($pagesArray, $file);
							if($_GET['pageName'].'.php' == $file){
								$pageIsThere = true ;
							}

						}else{
							array_push($dirArray, $file);

						}

					}
				}
			}
					//debugger('A',true);

			$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);


			if($pageIsThere){
				$currentPageName = $_GET['pageName'].'.php';

				$pagesException = $pagesArray ;
				$folderException = $dirArray ;
						//array_search(needle, haystack)
				if (($pagesKey = array_search($currentPageName, $pagesException)) !== false) {
					unset($pagesException[$pagesKey]);
				}
						//debugger($pagesException);
				$execeptionFilesAndFolders = array() ;
				$execeptionFilesAndFolders['folders'] = $folderException;
				$execeptionFilesAndFolders['files'] = $pagesException;

				unlink(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName);

				$indexFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/index.php');
				$pageFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName);
				
				redirect("../manage-website/home/".$manageWebsiteHomeURL,'success', 'Page \''.$currentPageName.'\' deleted successfully!.');

			}else{
				redirect("../manage-website/home/".$manageWebsiteHomeURL,"error","Webpage not found");

			}

			
		}elseif ($_GET['act'] == substr(md5('rename-page-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {
			$websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);

			$websiteName = $websiteName = 'sampleWebsiteDomain - '.$websiteData[0]->website_id;
			$pageIsThere = false ;
			if(isset($websiteData) && !empty($websiteData)){
				if(is_dir(SITE_DIR.$websiteData[0]->website_id.'/')){
					$dir = opendir(SITE_DIR.$websiteData[0]->website_id.'/');
					$pagesArray = array();
					$dirArray = array();
					while ($file = readdir($dir)) {

						if(!is_dir(SITE_DIR.$websiteData[0]->website_id.'/'.$file)){
							debugger($file); 
							array_push($pagesArray, $file);
							if($_GET['pageName'].'.php' == $file){
								$pageIsThere = true ;
							}

						}else{
							array_push($dirArray, $file);

						}

					}
				}
			}
					//debugger('A',true);

			$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);


			if($pageIsThere){
				$currentPageName = $_GET['pageName'].'.php';

				$pagesException = $pagesArray ;
				$folderException = $dirArray ;
						//array_search(needle, haystack)
				if (($pagesKey = array_search($currentPageName, $pagesException)) !== false) {
					unset($pagesException[$pagesKey]);
				}
						//debugger($pagesException);
				$execeptionFilesAndFolders = array() ;
				$execeptionFilesAndFolders['folders'] = $folderException;
				$execeptionFilesAndFolders['files'] = $pagesException;

				//unlink(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName);

				$indexFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/index.php');
				$pageFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/'.$currentPageName);
				
				redirect("../manage-website/home/".$manageWebsiteHomeURL,'success', 'Page \''.$currentPageName.'\' deleted successfully!.');

			}else{
				redirect("../manage-website/home/".$manageWebsiteHomeURL,"error","Webpage not found");

			}

			
		}else{
			redirect("../manage-website/","error","Theme not found");

		}
	}else{
		redirect('../404');
	}

} else {
	redirect('../', 'error','Unauthorized access');
}
<?php 
require '../config/config.php';


require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';

$website = new Website();
$theme = new Theme();

$user = new User();


if(isset($_POST) && !empty($_POST)){
	//debugger($_POST,TRUE);

	if (isset($_POST['create-new-site']) && !empty($_POST['create-new-site']) && $_POST['create-new-site'] == 'submit') {
		
		$data['category'] = sanitize($_POST['category']);
		$themeId = sanitize($_POST['theme']);

		$themeData = $theme->getThemeById($themeId);

		if (isset($themeData) && !empty($themeData)) {
			$data['theme_id'] = $themeData[0]->id;

			$addedWebsiteId = $website->addWebsite($data);

			//debugger($addedWebsiteId);
			if (isset($addedWebsiteId) && !empty($addedWebsiteId)) {

				$websiteData = $website->getAllWebsiteDataById($addedWebsiteId);

				$updatableData = array();
				$updatableData['times_installed'] = $websiteData[0]->theme_times_installed + 1;


				/*copy theme file as website*/
				mkdir(SITE_DIR.$websiteData[0]->website_id);
				$execeptionFilesAndFolders = array() ;
				$execeptionFilesAndFolders['folders'] = array();
				$execeptionFilesAndFolders['files'] = array();

				custom_copy(THEME_UPLOAD_DIR.$themeData[0]->theme_name, SITE_DIR.$websiteData[0]->website_id,$execeptionFilesAndFolders);
				//debugger(1,true);
				
				$is_updated = $theme->updateTheme($updatableData,$themeData[0]->id);

				if (!isset($is_updated) || empty($is_updated)) {
					$_SESSION['warning'] = 'something went wrong while updating times_installed flag';
				}

				$websiteSettingsData = array();
				$websiteSettingsData['site_title'] = 'project'.$websiteData[0]->website_id;
				
				$manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
				redirect("../manage-website/home/".$manageWebsiteHomeURL,"success","New website created successfully");

				//redirect("../manage-website/create-new-site","success","New website created successfully");

			}else{
				redirect("../manage-website/create-new-site","error","Something went wrong while creating new website");

			}

		}else{
			redirect("../manage-website/create-new-site","error","Theme not found to create website");

		}

	}elseif (isset($_POST['change-setting']) && !empty($_POST['change-setting']) && $_POST['change-setting'] == 'submit') {
		
		$updatableSettingData = array();
		$updatableSettingData['website_domain'] = sanitize($_POST['site-domain']);

		if (isset($_POST['site-title']) && !empty($_POST['site-title'])) {
			$updatableSettingData['site_title'] = sanitize($_POST['site-title']);
		}
		if (isset($_POST['copyright-notice']) && !empty($_POST['copyright-notice'])) {
			$updatableSettingData['copyright_notice'] = sanitize($_POST['copyright-notice']);
		}
		if (isset($_POST['email-address']) && !empty($_POST['email-address'])) {
			$updatableSettingData['website_email_address'] = filter_var(sanitize($_POST['email-address']) , FILTER_VALIDATE_EMAIL);
		}
		if (isset($_POST['site-domain']) && !empty($_POST['site-domain'])) {
			$updatableSettingData['website_domain'] = sanitize($_POST['site-domain']);
			# code...
		}
		$websiteData = $website->getAllWebsiteDataById((int)$_SESSION['website_id']);


		if (isset($_POST['enable-maintenance-mode']) && !empty($_POST['enable-maintenance-mode'])) {
			$updatableSettingData['maintenance_mode'] = 1 ;
			$maintenanceFiledestination = SITE_DIR.$websiteData[0]->website_id.'/system/';
			//debugger($destination);
			if(isset($_FILES['maintenance-file']) && $_FILES['maintenance-file']['error'] == 0){
				$allowedExtention = array('php');
				$updatableSettingData['maintenance_file'] = uploadSingleThemeFile($_FILES['maintenance-file'], $maintenanceFiledestination,$allowedExtention);
			}
		}else{
			$updatableSettingData['maintenance_mode'] = 0 ;

		}

		if(isset($_FILES['favicon-file']) && $_FILES['favicon-file']['error'] == 0){
			$allowedExtention = array('ico');
			$faviconFileDestination = SITE_DIR.$websiteData[0]->website_id.'/image/system/';
			$updatableSettingData['favicon_img'] = uploadSingleThemeFile($_FILES['favicon-file'], $faviconFileDestination,$allowedExtention);
		}
		
		//debugger($websiteData,true);

		$website_is_updated = $website->updateWebsite($updatableSettingData,$websiteData[0]->website_id);
		unset($_SESSION['website_id']);
		
		if ($website_is_updated) {

			if($websiteData[0]->maintenance_file != NULL && file_exists($faviconFileDestination.'/'.$websiteData[0]->maintenance_file)){
				if((isset($updatableSettingData['maintenance_file']) && !empty($updatableSettingData['maintenance_file'])) && $updatableSettingData['maintenance_file'] != $websiteData[0]->maintenance_file){
					unlink($faviconFileDestination.'/'.$websiteData[0]->maintenance_file);

				}
			}

			if($websiteData[0]->favicon_img != NULL && file_exists($faviconFileDestination.'/'.$websiteData[0]->favicon_img)){
				if((isset($updatableSettingData['favicon_img']) && !empty($updatableSettingData['favicon_img'])) && $updatableSettingData['favicon_img'] != $websiteData[0]->favicon_img){
					unlink($faviconFileDestination.'/'.$websiteData[0]->favicon_img);

				}
			}
			//$updatableData = $website->getAllWebsiteDataById($websiteData[0]->website_id);
			
			$websiteName = 'sampleWebsiteDomain - '.$websiteData[0]->website_id;


			$websiteSettingsData = array();
			$websiteSettingsData['site_title'] = 'project'.$websiteData[0]->website_id;

			redirect("../manage-website/","success","Website '$websiteName' settings changed successfully");

		}else{
			redirect("../manage-website/","error","Something went wrong while changing website settings");

		}

	}elseif (isset($_POST['edit-site']) && !empty($_POST['edit-site']) && $_POST['edit-site'] == 'submit') {
		$updatableData = array();
		$updatableData['category'] = sanitize($_POST['category']);
		$themeId = sanitize($_POST['theme']);

		$themeData = $theme->getThemeById($themeId);

		if (isset($themeData) && !empty($themeData)) {
			$updatableData['theme_id'] = $themeData[0]->id;

			//debugger($updatableData);
			$website_is_updated = $website->updateWebsite($updatableData,(int)$_SESSION['website_id']);

			//debugger($_SESSION);
			//debugger($website_is_updated,true);
			if ($website_is_updated) {

				$websiteData = $website->getAllWebsiteDataById((int)$_SESSION['website_id']);

				$websiteName = 'sampleWebsiteDomain - '.$websiteData[0]->website_id;

				unset($_SESSION['website_id']);
				$updatableData = array();
				$updatableData['times_installed'] = $websiteData[0]->theme_times_installed + 1;

				deleteDirectory(SITE_DIR.$websiteData[0]->website_id);
				mkdir(SITE_DIR.$websiteData[0]->website_id);
				/*copy theme file as website*/
				$execeptionFilesAndFolders = array() ;
				$execeptionFilesAndFolders['folders'] = array();
				$execeptionFilesAndFolders['files'] = array('about.php');
				
				custom_copy(THEME_UPLOAD_DIR.$themeData[0]->theme_name, SITE_DIR.$websiteData[0]->website_id,$execeptionFilesAndFolders);
				//debugger(1,true);
				
				$theme_is_updated = $theme->updateTheme($updatableData,$themeData[0]->id);

				if (!$theme_is_updated) {
					$_SESSION['warning'] = 'something went wrong while updating times_installed flag';
				}

				$websiteSettingsData = array();
				$websiteSettingsData['site_title'] = 'project'.$websiteData[0]->website_id;
				
				redirect("../manage-website/","success","Website '$websiteName' updated successfully");

			}else{
				redirect("../manage-website/","error","Something went wrong while updating website");

			}

		}else{
			redirect("../manage-website/","error","Theme not found to edit website");

		}

	}else{
		redirect('../404');

	}

}elseif(isset($_GET) && !empty($_GET)){
	$updatableData = array();

	if (isset($_GET['websiteId']) && !empty($_GET['websiteId'])) {
		//debugger($_GET,TRUE);

		if ($_GET['act'] == substr(md5('delete-website-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {
			$deletedData = $website->getWebsiteById($_GET['websiteId']);

			$websiteName = $websiteName = 'sampleWebsiteDomain - '.$deletedData[0]->id;

			//debugger($_GET,true);
			if(isset($deletedData) && !empty($deletedData)){
				$isdeleted = $website->deleteWebsite($_GET['websiteId']);

				if ($isdeleted == true) {
					deleteDirectory(SITE_DIR.$deletedData[0]->id);
					redirect('../manage-website','success', 'Website \''.$websiteName.'\' deleted successfully!.');
				}else{
					redirect('../manage-website','error','Sorry! Something went wrong while deleting website');
				}
			}else{
				redirect('../manage-website','error','Can\'t find website info in database');

			}

			
		}elseif ($_GET['act'] == substr(md5('duplicate-website-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)) {

			$websiteData = $website->getWebsiteById($_GET['websiteId']);

			$data = array();
			$data['theme_id'] = $websiteData[0]->theme_id;
			$data['category'] = $websiteData[0]->category;

			$themeData = $theme->getThemeById($websiteData[0]->theme_id);
			$addedWebsiteId = $website->addWebsite($data);
			if (isset($themeData) && !empty($themeData)) {
				if (isset($addedWebsiteId) && !empty($addedWebsiteId)) {
					$duplicatedWebsiteData = $website->getAllWebsiteDataById($addedWebsiteId);

					$updatableData = array();
					$updatableData['times_installed'] = $duplicatedWebsiteData[0]->theme_times_installed + 1;

					mkdir(SITE_DIR.$duplicatedWebsiteData[0]->website_id);
					$execeptionFilesAndFolders = array() ;
					$execeptionFilesAndFolders['folders'] = array();
					$execeptionFilesAndFolders['files'] = array('about.php');

					custom_copy(SITE_DIR.$websiteData[0]->id, SITE_DIR.$duplicatedWebsiteData[0]->website_id,$execeptionFilesAndFolders);

					$is_updated = $theme->updateTheme($updatableData,$themeData[0]->id);

					if (!isset($is_updated) || empty($is_updated)) {
						$_SESSION['warning'] = 'something went wrong while updating times_installed flag';
					}

					$websiteSettingsData = array();
					$websiteSettingsData['site_title'] = 'project'.$duplicatedWebsiteData[0]->website_id;

					redirect("../manage-website/","success","New website created successfully with duplication");
				}else{
					redirect("../manage-website/","error","Something went wrong while creating new website with duplication");

				}
			}else{
				redirect("../manage-website/","error","Theme not found to duplicate website");

			}
		}else{
			redirect('../404');
		}
	}else{
		redirect('../404');
	}

} else {
	redirect('../', 'error','Unauthorized access');
}
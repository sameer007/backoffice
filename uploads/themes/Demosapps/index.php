<?php include "inc/header.php"; ?>

<section class="S1-Demosapps site-header" id="header">
    <nav class="Navbar-Demosapps navbar navbar-expand-lg transparent-bg- static-nav">
        <a href="#" class="d-inline-block sidemenu_btn " id="sidemenu_toggle" />
        <!-- <i class="icon-svg-bars svg-icon txt-grey"></i> -->
        <img src="./assets/image/hamburger.svg" class="icon-svg-bars ham-icon" />

        <div class="container-fluid">
            <a href="./" class="navbar-brand navbar-brand-name">
                <img src="assets\image\Logo\Demosapp.png"
                    class="Image-it_file_S1*Navbar*Logo_Image-CanHide-Demosapps logo-default" />
                <!-- <h3 class="NavbarTitle-it_text_S1*Navbar*Logo_Heading-CanHide-Demosapps logo-title"> Demosapp</h3> -->
            </a>
            <div class="collapse navbar-collapse" id="navbarToggleExternalContent">
                <ul class="NavbarItem-Demosapps single-encapsulation navbar-nav ml-auto">
                    <li
                        class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_1-Multiple-Demosapps nav-item svg-icon svg-bg'>
                        <a href='./' class='nav-link'>Home</a>
                    </li>

                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_2-Multiple-Demosapps nav-item svg-icon svg-bg'
                        onclick="scrollto('services');"><a href='#' class='nav-link'>Services</a></li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_3-Multiple-Demosapps nav-item svg-icon svg-bg'
                        onclick="scrollto('projects');"><a href='#' class='nav-link'>Project</a></li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_4-Multiple-Demosapps nav-item svg-icon svg-bg'
                        onclick="scrollto('teams');"><a href='#' class='nav-link'>Team</a></li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_5-Multiple-Demosapps nav-item svg-icon svg-bg'
                        onclick="scrollto('blog');"><a href='#' class='nav-link'>Blog</a></li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_6-Multiple-Demosapps nav-item svg-icon svg-bg'
                        onclick="scrollto('contacts');"><a href='#' class='nav-link transition'>Contact</a></li>
                </ul>
            </div>
        </div>

        <!--side menu open button-->
        <div class="language_btn d-block" id="sidemenu_toggle">
        </div>
    </nav>

    <!-- Side Menu -->
    <div class="side-menu">
        <div class="overlay"></div>
        <div class="inner-wrapper">
            <span class="btn-close" id="btn_sideNavClose">
              <img src="./assets/image/Icon/Side Panel/close.svg" width="34px" />
              <!-- <i class="fa fa-times h1 font-weight-bold"></i> -->
            </span>
            <nav class="side-nav w-100">
                <ul class="NavbarItem-Cloned-Demosapp single-encapsulation navbar-nav ul-disc">
                    <li class="NavbarItem-it_url_S1*Navbar_Heading_1-Multiple-Cloned-Demosapp nav-item li-disc">
                        <a href="./" class="nav-link"> Home</a>
                    </li>
                    <li class="NavbarItem-it_url_S1*Navbar_Heading_2-Multiple-Cloned-Demosapp nav-item li-disc"
                    onclick="scrollto('services');">
                        <a href="#AboutDemosapp" class="nav-link"> Services</a>
                    </li>
                    <li class="NavbarItem-it_url_S1*Navbar_Heading_3-Multiple-Cloned-Demosapp nav-item li-disc"
                    onclick="scrollto('projects');">
                        <a href="#" class="nav-link"> Project</a>
                    </li>
                    <li class="NavbarItem-it_url_S1*Navbar_Heading_4-Multiple-Cloned-Demosapp nav-item li-disc"
                    onclick="scrollto('teams');">
                        <a href="#" class="nav-link"> Team</a>
                    </li>
                    <li class="NavbarItem-it_url_S1*Navbar_Heading_5-Multiple-Cloned-Demosapp nav-item li-disc"
                    onclick="scrollto('blog');">
                        <a href="#" class="nav-link"> Blog</a>
                    </li>
                    <li class="NavbarItem-it_url_S1*Navbar_Heading_6-Multiple-Cloned-Demosapp nav-item li-disc"
                    onclick="scrollto('contacts');">
                        <a href="#" class="nav-link"> Contact</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>

<section class="S2-Demosapps started">
    <div class="row mr-0">
        <div class="ColumnA-Demosapps col-lg-6 order-lg-12 order-2 pr-0">
            <div class="Content-c1-Demosapps inner-div">
                <div class="details">
                    <h1 class="Heading-it_text_S2*ColumnA*c1*Started_TitleHeading_1-Demosapps text-uppercase">
                        DEDICATED<br />DEVELOPMENT
                        <span>TEAM</span>
                    </h1>
                    <div
                        class="ParagraphGroup-it_textarea_S2*ColumnA*c1*Started_Paragraph_1-Demosapps single-encapsulation">
                        <p class="mt-4">Hire top tech talent and quickly scale your delivery capacity. Our engineers
                            have the necessary niche skills, deep expertise, and are highly enthusiastic about their
                            assigned work.</p>
                    </div>
                    <button class="btn">GET STARTED</button>
                </div>
            </div>
        </div>
        <div class="ColumnB-Demosapps col-lg-6 order-lg-12 order-1 pr-0 pl-0">
            <div class="Content-c1-Demosapps">
                <img src="assets\image\Images\DesktopView\Homepage\1_Dedicated_development_team.png"
                    alt="Dedicated_development_team"
                    class="Image-it_file_S2*ColumnB*c1*Image_1-Demosapps" />
            </div>
        </div>
    </div>
</section>

<section class="S3-Demosapps offer">
    <div class="row mr-0">
        <div class="ColumnA-Demosapps col-lg-4 pr-0 inner-div">
            <div class="Content-c1-Demosapps">
                <div class="details">
                    <h2 class="Heading-it_text_S3*ColumnA*c1*Heading_1-Demosapps text-uppercase title">WHAT WE
                        <br /><span>OFFER</span>
                    </h2>
                    <div class="ParagraphGroup-it_textarea_S3*ColumnA*c1*Paragraph_1-Demosapps single-encapsulation">
                        <p class="mt-4 title-desc">Our offers have a wide range of services available and ready for our
                            client. No matter if it is a small to big scale project, we are more than ready and excited
                            to work with our client for any kinds of projects.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ColumnB-Demosapps col-lg-8 pr-0 pl-0 offertype-bg">
            <div class="Content-c1-Demosapps row offer-types offer-type-inner-div">
                <div class="col-lg-6">
                    <div class="offer-type-details">
                        <img src="assets\image\Icon\Homepage\Development.svg" alt="Web_Development" />
                        <h5 class="Heading-it_text_S3*ColumnB*c1*SubHeading_1-Demosapps font-weight-bold">Development
                        </h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c1*SubParagraph_1-Demosapps single-encapsulation">
                            <p>We had a range of development services such as Mobile Applications, Websites, Domain
                                and Hosting and so on. Most applications are compatible with all operating systems
                                depending on project requirements.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="offer-type-details">
                        <img src="assets\image\Icon\Homepage\Graphic Design.svg" alt="Graphic_Design" />
                        <h5 class="Heading-it_text_S3*ColumnB*c1*SubHeading_2-Demosapps font-weight-bold">Graphic Design
                        </h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c1*SubParagraph_2-Demosapps single-encapsulation">
                            <p>From logo designs, re-branding strategies and marketing materials, we offer a wide
                                range of branding and graphic design services to improve user’s overall visual
                                experiences.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="offer-type-details">
                        <img src="assets\image\Icon\Homepage\Digital Marketing.svg" alt="Digital_Marketing" />
                        <h5 class="Heading-it_text_S3*ColumnB*c1*SubHeading_3-Demosapps font-weight-bold">Digital
                            Marketing
                        </h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c1*SubParagraph_3-Demosapps single-encapsulation">
                            <p>We make use of our specialists skills to help businesses to increase sales as well as
                                building leads, and expand market share.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="offer-type-details">
                        <img src="assets\image\Icon\Homepage\Business Venture.svg" alt="Business Venture" />
                        <h5 class="Heading-it_text_S3*ColumnB*c1*SubHeading_4-Demosapps font-weight-bold">Business
                            Venture
                        </h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c1*SubParagraph_4-Demosapps single-encapsulation">
                            <p>We help to build up businesses starting from the beginning until the end. From the
                                financial standpoint or building up the fame, we help to discover it all together
                                with our client.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- </div> -->
        </div>
    </div>
</section>

<section class="S4-Demosapps benefits" id="services">
    <div class="row text-center">
        <div class="ColumnA-Demosapps col-lg-12">
            <div class="Content-c1-Demosapps">
                <h2
                    class="Heading-it_text_S4*ColumnA*c1*Benefits_TitleHeading_1-Demosapps text-uppercase text-center title">
                    WHY OUR
                    <span>SERVICE</span>
                    ?
                </h2>
            </div>
        </div>
    </div>
    <div class="row outer-div mr-0 position-relative overflow-hidden bgimage1">
        <div class="ColumnB-Demosapps col-lg-7 order-lg-12 order-2 pr-0">
            <div class="Content-c1-Demosapps inner-div details">
                <h2 class="Heading-it_text_S4*ColumnB*c1*SubHeading_1-Demosapps font-weight-bold">We are a team of
                    professional
                </h2>
                <div class="ParagraphGroup-it_textarea_S4*ColumnB*c1*SubParagraph_1-Demosapps single-encapsulation">
                    <p class="mt-4">We value each of everyone in our team members, with beliefs solely in reliable
                        approach to our job and interesting passions and life goals. We are a professional team in
                        which everyone specializes in only delivering the perfect results. </p>
                </div>
            </div>
        </div>
        <div class="ColumnC-Demosapps col-lg-5 order-lg-12 order-1 pr-0 pl-0">
            <div class="Content-c1-Demosapps">
                <img class="Image-it_file_S4*ColumnC*c1*Image_1-Demosapps"
                    src="assets\image\Images\DesktopView\Homepage\2_we_are_ateam_of_professional.png"
                    alt="We_are_a_team_of_professional" width="100%" />
            </div>
        </div>
    </div>
    <div class="row outer-div mr-0 mt-5">
        <div class="ColumnD-Demosapps col-lg-5 order-lg-12 order-1 pr-0">
            <div class="Content-c1-Demosapps">
                <img class="Image-it_file_S4*ColumnD*c1*Image_2-Demosapps"
                    src="assets\image\Images\DesktopView\Homepage\3_we_provide_one_stop_solution.png"
                    alt="We_provide_one_stop_solution" width="100%" />
            </div>
        </div>
        <div class="ColumnE-Demosapps col-lg-7 order-lg-12 order-2 pr-0 pl-0">
            <div class="Content-c1-Demosapps inner-div details position-right">
                <h2 class="Heading-it_text_S2*ColumnE*c1*SubHeading_2-Demosapps font-weight-bold">We provide one stop
                    solution
                </h2>
                <div class="ParagraphGroup-it_textarea_S4*ColumnE*c1*SubParagraph_2-Demosapps single-encapsulation">
                    <p class="mt-4">No matter what industry you are in, whether you are in the agriculture field
                        or even in the kitchen as a chef, as long as you are in need of IT related service, we
                        are more than ready to serve and deliver. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row outer-div mr-0 position-relative overflow-hidden bgimage1">
        <div class="ColumnF-Demosapps col-lg-7 order-lg-12 order-2 pr-0">
            <div class="Content-c1-Demosapps inner-div details">
                <h2 class="Heading-it_text_S2*ColumnF*c1*SubHeading_3-Demosapps font-weight-bold">We simplify complex
                    structure
                    into understandable terms</h2>
                <div class="ParagraphGroup-it_textarea_S4*ColumnF*c1*SubParagraph_3-Demosapps single-encapsulation">
                    <p class="mt-4">IT may be hard to understand, complicated to work with. But with over 10 years
                        in providing IT solutions we are confident that you will enjoy using our service with our
                        process being simple and easy to understand. </p>
                </div>
            </div>
        </div>
        <div class="ColumnG-Demosapps col-lg-5 order-lg-12 order-1 pr-0 pl-0">
            <div class="Content-c1-Demosapps">
                <img class="Image-it_file_S4*ColumnG*c1*Image_3-Demosapps"
                    src="assets\image\Images\DesktopView\Homepage\4_We_simplify_complex_structure_into _understandable_terms.png"
                    alt="We_simplify_complex_structure_into _understandable_terms" width="100%" />
            </div>
        </div>
    </div>
    <div class="row outer-div mr-0 mt-5">
        <div class="ColumnH-Demosapps col-lg-5 order-lg-12 order-1 pr-0">
            <div class="Content-c1-Demosapps">
                <img class="Image-it_file_S4*ColumnG*c1*Image_4-Demosapps"
                    src="assets\image\Images\DesktopView\Homepage\5_We_believe_in_efficiency_and_effectiveness_communication.png"
                    alt="We_simplify_complex_structure_into _understandable_terms" width="100%" />
            </div>
        </div>
        <div class="ColumnI-Demosapps col-lg-7 order-lg-12 order-2 pr-0 pl-0">
            <div class="Content-c1-Demosapps inner-div details position-right">
                <h2 class="Heading-it_text_S4*ColumnI*c1*SubHeading_4-Demosapps font-weight-bold">We believe in
                    efficiency
                    and
                    effectiveness communication</h2>
                <div class="ParagraphGroup-it_textarea_S4*ColumnI*c1*SubParagraph_4-Demosapps single-encapsulation">
                    <p class="mt-4">We believe in using only the best practice processes methodologies to
                        develop your product in a structured and methodical way. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="S5-Demosapps deliver mt-4 mb-5" id="projects">
    <div class="row text-center">
        <div class="ColumnA-Demosapps col-lg-12">
            <div class="Content-c1-Demosapps">
                <h2 class="Heading-it_text_S5*ColumnA*c1*TitleHeading_1-Demosapps text-uppercase text-center title">WE
                    DELIVER ONLY THE
                    <span>BEST</span> AND NEVER FAIL TO <span>IMPRESS</span>
                </h2>
            </div>
        </div>
    </div>

    <div class="row mr-0 outer-div position-relative">
        <div class="ColumnB-Demosapps col-lg-5 order-lg-12 order-1">
            <div class="Content-c1-Demosapps">
                <img src="assets\image\Images\DesktopView\Homepage\6_project_a.png"
                    class="Image-it_file_S5*ColumnB*c1*Image_1-Demosapps" alt="Project_a" width="100%" />
            </div>
        </div>
        <div class="ColumnC-Demosapps col-lg-7 order-lg-12 order-2">
            <div class="Content-c1-Demosapps inner-div details position-right">
                <h3 class="Heading-it_text_S5*ColumnC*c1*Heading_1-Demosapps font-weight-bold">Project A</h3>
                <div class="ParagraphGroup-it_textarea_S5*ColumnC*c1*Paragraph_1-Demosapps single-encapsulation">
                    <p class="mt-4">Redefine a brand new look for brand awareness, achieve the impossible in
                        terms of limited resources, yet able deliver only the best maximized results. </p>
                </div>
                <div class="text-center pb-5 mobile-btn d-none">
                    <button class="btn">PREVIEW</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mr-0 mt-5 outer-div position-relative">
        <div class="ColumnD-Demosapps col-lg-7 order-lg-12 order-2">
            <div class="Content-c1-Demosapps inner-div details">
                <h3 class="Heading-it_text_S5*ColumnD*c1*Heading_2-Demosapps font-weight-bold">Project B</h3>
                <div class="ParagraphGroup-it_textarea_S5*ColumnD*c1*Paragraph_2-Demosapps single-encapsulation">
                    <p class="mt-4">A community platform caters for needs of knowledge seeking and knowledge
                        delivering. Rewarding participants with generous rewards, mini games, and many more. The
                        real true All-in-one platform. </p>
                </div>
                <div class="text-center pb-5 mobile-btn d-none">
                    <button class="btn">PREVIEW</button>
                </div>
            </div>
        </div>
        <div class="ColumnE-Demosapps col-lg-5 order-lg-12 order-1">
            <div class="Content-c1-Demosapps">
                <img src="assets\image\Images\DesktopView\Homepage\7_project_b.png"
                    class="Image-it_file_S5*ColumnE*c1*Image_2-Demosapps" alt="Project_b" width="100%" />
            </div>
        </div>
    </div>

    <div class="text-center py-5 desktop-btn">
        <button class="btn">VIEW MORE</button>
    </div>
</section>

<section class="S6-Demosapps slider">
    <div class="Carousel-Demosapps carousel slide animated fadeInLeft" data-interval="4000" data-ride="carousel"
        id="carouselIndicators">
        <ol class="carousel-indicators">
            <li data-slide-to="0" class="active" data-target="#carouselIndicators"></li>
            <li data-slide-to="1" class="" data-target="#carouselIndicators"></li>
            <li data-slide-to="2" class="" data-target="#carouselIndicators"></li>
        </ol>

        <div class="carousel-inner ImageSlider-Boostrap">
            <div class="carousel-item active animated fadeIn">
                <div class="details">
                    <h2 class="Heading-it_text_S6*Carousel*Heading_1-Demosapps">
                        <img src="assets\image\inverted comma.png" alt="" />
                        DemosApp is basically a One Stop Solution without hassle, from pre-marketing to roll-out. Even
                        from the financial side they are a huge help to assist my project with them. Everything they
                        have done so far has been very professional which has achieved above my expectation, I will
                        definitely recommend them to all great mind project enthusiastic people!
                    </h2>
                    <h5 class="Heading-it_text_S6*Carousel*SubHeading_1-Demosapps">Project Owner Of Project B, Joshua
                        Ooi
                    </h5>
                </div>
            </div>
            <div class="carousel-item  animated fadeIn">
                <div class="details">
                    <h2 class="Heading-it_text_S6*Carousel*Heading_2-Demosapps">
                        <img src="assets\image\inverted comma.png" alt="" />
                        DemosApp is basically a One Stop Solution without hassle, from pre-marketing to roll-out. Even
                        from the financial side they are a huge help to assist my project with them. Everything they
                        have done so far has been very professional which has achieved above my expectation, I will
                        definitely recommend them to all great mind project enthusiastic people!
                    </h2>
                    <h5 class="Heading-it_text_S6*Carousel*SubHeading_2-Demosapps">Project Owner Of Project B, Joshua
                        Ooi
                    </h5>
                </div>
            </div>
            <div class="carousel-item  animated fadeIn">
                <div class="details">
                    <h2 class="Heading-it_text_S6*Carousel*Heading_3-Demosapps">
                        <img src="assets\image\inverted comma.png" alt="" />
                        DemosApp is basically a One Stop Solution without hassle, from pre-marketing to roll-out. Even
                        from the financial side they are a huge help to assist my project with them. Everything they
                        have done so far has been very professional which has achieved above my expectation, I will
                        definitely recommend them to all great mind project enthusiastic people!
                    </h2>
                    <h5 class="Heading-it_text_S6*Carousel*SubHeading_3-Demosapps">Project Owner Of Project B, Joshua
                        Ooi
                    </h5>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" data-slide="prev" href="#carouselIndicators" role="button"><span
                class="sr-only">Previous</span> </a> <a class="carousel-control-next" data-slide="next"
            href="#carouselIndicators" role="button"> <span class="sr-only">Next</span> </a>
    </div>
</section>

<section class="S7-Demosapps team position-relative bgimage7" id="teams">
    <div class="Heading-Demosapps">
        <h2 class="TitleHeading-it_text_S7*Team*Benefit_TitleHeading_1-Demosapps text-uppercase text-center title">
            CREATIVE
            <span>TEAM</span>
        </h2>
    </div>
    <div class="Carousel-Demosapps outer-div mx-5">
        <div class="owl-carousel1 owl-carousel owl-theme">
            <div class="item col position-relative">
                <div class="inner-div" onclick="window.location='#'">
                    <div class="outerborder">
                        <img src="assets\image\Images\DesktopView\Homepage\9_Alvin.png"
                            class="Image-it_file_S7*Carousel*Image_1-Demosapps img-responsive" alt="Alvin"
                            width="100%" />
                    </div>
                    <div class="details">
                        <h5 class="Heading-it_text_S7*Carousel*Heading_1-Demosapps name">Alvin</h5>
                        <span>Business Analyst</span>
                        <div class="Socialbar-Demosapps mt-3">
                            <a href="http://twitter.com"
                                class="SocialbarItem-it_url_S7*Carousel*First_SocialbarItem_1-Demosapps"><i
                                    class="fab fa-twitter fa-fw"> </i></a>
                            <a href="http://facebook.com"
                                class="SocialbarItem-it_url_S7*Carousel*First_SocialbarItem_2-Demosapps"><i
                                    class="fab fa-facebook-f fa-fw"> </i></a>
                            <a href="http://linkedin.com"
                                class="SocialbarItem-it_url_S7*Carousel*First_SocialbarItem_3-Demosapps"><i
                                    class="fab fa-linkedin-in fa-fw"> </i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item col position-relative">
                <div class="inner-div" onclick="window.location='#'">
                    <div class="outerborder">
                        <img src="assets\image\Images\DesktopView\Homepage\10_Tim.png"
                            class="Image-it_file_S7*Carousel*Image_2-Demosapps img-responsive" alt="Tim" width="100%" />
                    </div>
                    <div class="details">
                        <h5 class="Heading-it_text_S7*Carousel*Heading_2-Demosapps name">Tim</h5>
                        <span>Graphic Design</span>
                        <div class="Socialbar-Demosapps mt-3">
                            <a href="http://twitter.com"
                                class="SocialbarItem-it_url_S7*Carousel*Second_SocialbarItem_1-Demosapps"><i
                                    class="fab fa-twitter fa-fw"> </i></a>
                            <a href="http://facebook.com"
                                class="SocialbarItem-it_url_S7*Carousel*Second_SocialbarItem_2-Demosapps"><i
                                    class="fab fa-facebook-f fa-fw"> </i></a>
                            <a href="http://linkedin.com"
                                class="SocialbarItem-it_url_S7*Carousel*Second_SocialbarItem_3-Demosapps"><i
                                    class="fab fa-linkedin-in fa-fw"> </i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item col position-relative">
                <div class="inner-div" onclick="window.location='#'">
                    <div class="outerborder">
                        <img src="assets\image\Images\DesktopView\Homepage\8_Roger.png"
                            class="Image-it_file_S7*Carousel*Image_3-Demosapps img-responsive" alt="Roger"
                            width="100%" />
                    </div>
                    <div class="details">
                        <h5 class="Heading-it_text_S7*Carousel*Heading_3-Demosapps name">Roger</h5>
                        <span>Digital Marketer</span>
                        <div class="Socialbar-Demosapps mt-3">
                            <a href="http://twitter.com"
                                class="SocialbarItem-it_url_S7*Carousel*Third_SocialbarItem_1-Demosapps"><i
                                    class="fab fa-twitter fa-fw"> </i></a>
                            <a href="http://facebook.com"
                                class="SocialbarItem-it_url_S7*Carousel*Third_SocialbarItem_2-Demosapps"><i
                                    class="fab fa-facebook-f fa-fw"> </i></a>
                            <a href="http://linkedin.com"
                                class="SocialbarItem-it_url_S7*Carousel*Third_SocialbarItem_3-Demosapps"><i
                                    class="fab fa-linkedin-in fa-fw"> </i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="S8-Demosapps team position-relative previous-post" id="blog">
    <div class="row text-center">
        <div class="ColumnA-Demosapps col-lg-12">
            <div class="Content-c1-Demosapps">
                <h2 class="Heading-it_text_S8*ColumnA*c1*TitleHeading_1-Demosapps text-uppercase text-center title">BLOG
                    POST</h2>

            </div>
        </div>
    </div>

    <div class="row mr-0 align-items-start outer-div">
        <div class="ColumnB-Demosapps col-xl-4 col-lg-6 pl-0 pr-0">
            <div class="Content-c1-Demosapps inner-div details inner-details blog-teaser">
                <img class="Image-it_file_S8*ColumnB*c1*Image_1-Demosapps"
                    src="assets\image\Images\DesktopView\Blog\3_Mobile_Design.png"
                    alt="Case_Study_for_Brand_New_Logo_Design" width="100%" />
                <h6 class="Heading-it_text_S8*ColumnB*c1*SubHeading_1-Demosapps text-uppercase blog-tag">UI / UX</h6>
                <h4 class="Heading-it_text_S8*ColumnB*c1*Heading_1-Demosapps">Mobile Design: 14 Stylish and User
                    Friendly App Design Concepts</h4>
                <div class="ParagraphGroup-it_textarea_S8*ColumnB*c1*Paragraph_1-Demosapps single-encapsulation">
                    <p class="mt-3">A big bunch of practical UI design examples, this time on mobile user
                        experience: check the app concepts designed for the diversity.. </p>
                </div>
                <a href="./blog_individual_page">
                    <button class="btn transition">
                        Read Article
                        <i class="fas fa-angle-right"> </i>
                    </button></a>
            </div>
        </div>
        <div class="ColumnC-Demosapps col-xl-4 col-lg-6 pl-0 pr-0">
            <div class="Content-c1-Demosapps inner-div details blog-teaser">
                <img class="Image-it_file_S8*ColumnC*c1*Image_2-Demosapps"
                    src="assets\image\Images\DesktopView\Blog\4_Brand_New_Future_Furniture_App _Launching Soon.png"
                    alt="New Furniture Apps" width="100%" />
                <div class="inner-details">
                    <h6 class="Heading-it_text_S8*ColumnC*c1*SubHeading_2-Demosapps text-uppercase blog-tag">UI / UX
                    </h6>
                    <h4 class="Heading-it_text_S8*ColumnC*c1*Heading_2-Demosapps">Brand New Future Furniture App
                        Launching
                        Soon</h4>
                    <div class="ParagraphGroup-it_textarea_S8*ColumnC*c1*Paragraph_2-Demosapps single-encapsulation">
                        <p class="mt-3">Selecting furniture and décor for your home can prove to be a perplexing
                            task especially when you have to move stores to stores to..</p>
                    </div>
                    <a href="./blog_individual_page">
                        <button class="btn transition">
                            Read Article
                            <i class="fas fa-angle-right"> </i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnD-Demosapps col-xl-4 col-lg-6 pl-0 pr-0">
            <div class="Content-c1-Demosapps inner-div details blog-teaser">
                <img class="Image-it_file_S8*ColumnD*c1*Image_3-Demosapps"
                    src="assets\image\Images\DesktopView\Blog\5_Case_Study_for_Brand_New_Logo_Design .png"
                    alt="Case_Study_for_Brand_New_Logo_Design" width="100%" />
                <div class="inner-details">
                    <h6 class="Heading-it_text_S8*ColumnD*c1*SubHeading_3-Demosapps text-uppercase blog-tag">Branding
                    </h6>
                    <h4 class="Heading-it_text_S8*ColumnD*c1*Heading_3-Demosapps">Case Study for Brand New Logo Design
                        and more
                    </h4>
                    <div class="ParagraphGroup-it_textarea_S8*ColumnD*c1*Paragraph_3-Demosapps single-encapsulation">
                        <p class="mt-3">Check the creative process behind the bright brand identity design for
                            Uplyfe, the health app that employs the power of technology and more..</p>
                    </div>
                    <a href="./blog_individual_page">
                        <button class="btn transition">
                            Read Article
                            <i class="fas fa-angle-right"> </i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnE-Demosapps col-xl-4 col-lg-6 pl-0 pr-0">
            <div class="Content-c1-Demosapps inner-div details blog-teaser">
                <div class="inner-details">
                    <img class="Image-it_file_S8*ColumnE*c1*Image_4-Demosapps"
                        src="assets\image\Images\DesktopView\Blog\3_Mobile_Design.png"
                        alt="Case_Study_for_Brand_New_Logo_Design" width="100%" />
                    <h6 class="Heading-it_text_S8*ColumnE*c1*SubHeading_4-Demosapps text-uppercase blog-tag">UI / UX
                    </h6>
                    <h4 class="Heading-it_text_S8*ColumnE*c1*Heading_4-Demosapps">Mobile Design: 14 Stylish and User
                        Friendly App Design Concepts</h4>
                    <div class="ParagraphGroup-it_textarea_S8*ColumnE*c1*Paragraph_4-Demosapps single-encapsulation">
                        <p class="mt-3">A big bunch of practical UI design examples, this time on mobile user
                            experience: check the app concepts designed for the diversity.. </p>
                    </div>
                    <a href="./blog_individual_page">
                        <button class="btn transition">
                            Read Article
                            <i class="fas fa-angle-right"> </i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnF-Demosapps col-xl-4 col-lg-6 pl-0 pr-0">
            <div class="Content-c1-Demosapps inner-div details blog-teaser">
                <img class="Image-it_file_S8*ColumnF*c1*Image_5-Demosapps"
                    src="assets\image\Images\DesktopView\Blog\4_Brand_New_Future_Furniture_App _Launching Soon.png"
                    alt="New Furniture Apps" width="100%" />
                <div class="inner-details">
                    <h6 class="Heading-it_text_S8*ColumnF*c1*SubHeading_5-Demosapps text-uppercase blog-tag">UI / UX
                    </h6>
                    <h4 class="Heading-it_text_S8*ColumnF*c1*Heading_5-Demosapps">Brand New Future Furniture App
                        Launching
                        Soon</h4>
                    <div class="ParagraphGroup-it_textarea_S8*ColumnF*c1*Paragraph_5-Demosapps single-encapsulation">
                        <p class="mt-3">Selecting furniture and décor for your home can prove to be a perplexing
                            task especially when you have to move stores to stores to..</p>
                    </div>
                    <a href="./blog_individual_page">
                        <button class="btn transition">
                            Read Article
                            <i class="fas fa-angle-right"> </i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnG-Demosapps col-xl-4 col-lg-6 pl-0 pr-0">
            <div class="Content-c1-Demosapps inner-div details blog-teaser">
                <img class="Image-it_file_S8*ColumnG*c1*Image_6-Demosapps"
                    src="assets\image\Images\DesktopView\Blog\5_Case_Study_for_Brand_New_Logo_Design .png"
                    alt="Case_Study_for_Brand_New_Logo_Design" width="100%" />
                <div class="inner-details">
                    <h6 class="Heading-it_text_S8*ColumnG*c1*SubHeading_6-Demosapps text-uppercase blog-tag">Branding
                    </h6>
                    <h4 class="Heading-it_text_S8*ColumnG*c1*Heading_6-Demosapps">Case Study for Brand New Logo Design
                    </h4>
                    <div class="ParagraphGroup-it_textarea_S3*ColumnG*c1*Paragraph_6-Demosapps single-encapsulation">

                        <p class="mt-3">Check the creative process behind the bright brand identity design for
                            Uplyfe, the health app that employs the power of..</p>
                    </div>
                    <a href="./blog_individual_page">
                        <button class="btn transition">
                            Read Article
                            <i class="fas fa-angle-right"> </i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="text-center py-5 view-more outer-div">
        <a href="#"><button class="btn">VIEW MORE</button></a>
    </div>
</section>


<section class="S9-Demosapps package mt-4">
    <div class="Heading-Demosapps">
        <h2 class="TitleHeading-it_text_S9*Package_TitleHeading_1-Demosapps text-uppercase text-center title">OUR
            <span>PACKAGES</span>
        </h2>
        <h5 class="TitleHeading-it_text_S9*Package_TitleHeading_2-Demosapps text-center title-desc">Our prices maybe
            varied depending
            on the
            project</h5>
    </div>
    <div class="Carousel-Demosapps outer-div">
        <div class="row owl-carousel2 owl-carousel owl-theme">
            <div class="col item">
                <div class="details transition" onclick="window.location='#'">
                    <div class="inner-div position-relative">
                        <h4 class="Heading-it_text_S9*Carousel*SubHeading_1-Demosapps mb-0">Standard</h4>
                        <h1 class="Heading-it_text_S9*Carousel*Heading_1-Demosapps d-inline-block">RM 1,499</h1>
                        <span class="slash">/</span>
                        <h5 class="Heading-it_text_S9*Carousel*SmallHeading_1-Demosapps d-inline-block">per<br />month
                        </h5>
                        <ul class="single-encapsulation">
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_1-Multiple-Demosapps">One-Page Website
                            </li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_2-Multiple-Demosapps">Basic Contact Form
                            </li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_3-Multiple-Demosapps">Marketing tools
                                Setup</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_4-Multiple-Demosapps">Google Business
                                Setup</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_5-Multiple-Demosapps">Get FREE Basic SEO
                                and Backlink</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_6-Multiple-Demosapps">FREE 1 year domain
                                and hosting</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_7-Multiple-Demosapps">FREE SSL
                                certificate</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_8-Multiple-Demosapps">FREE Onsite /
                                Remote Training</li>
                        </ul>

                        <div class="text-center pb-2 started-btn">
                            <button class="btn">Get Started</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col item">
                <div class="details transition" onclick="window.location='#'">
                    <div class="inner-div position-relative">
                        <h4 class="Heading-it_text_S9*Carousel*SubHeading_2-Demosapps mb-0">Business</h4>
                        <h1 class="Heading-it_text_S9*Carousel*Heading_2-Demosapps d-inline-block">RM 2,499</h1>
                        <span class="slash">/</span>
                        <h5 class="Heading-it_text_S9*Carousel*SmallHeading_2-Demosapps d-inline-block">per<br />month
                        </h5>
                        <ul class="single-encapsulation">
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_9-Multiple-Demosapps">5 Page Website</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_10-Multiple-Demosapps">Advance Contact
                                Form</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_11-Multiple-Demosapps">Marketing tools
                                Setup</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_12-Multiple-Demosapps">Google Business
                                Setup</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_13-Multiple-Demosapps">Get FREE Basic SEO
                                and Backlink</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_14-Multiple-Demosapps">FREE 1 year domain
                                and hosting</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_15-Multiple-Demosapps">FREE SSL
                                certificate </li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_16-Multiple-Demosapps">FREE Onsite /
                                Remote Training</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_17-Multiple-Demosapps">Email Marketing
                                Setup</li>
                        </ul>
                        <div class="text-center pb-2 started-btn">
                            <button class="btn">Get Started</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col item">
                <div class="details transition" onclick="window.location='#'">
                    <div class="inner-div position-relative">
                        <h4 class="Heading-it_text_S9*Carousel*SubHeading_3-Demosapps mb-0">Enterprise</h4>
                        <h1 class="Heading-it_text_S9*Carousel*Heading_3-Demosapps d-inline-block">RM 4,499</h1>
                        <span class="slash">/</span>
                        <h5 class="Heading-it_text_S9*Carousel*SmallHeading_3-Demosapps d-inline-block">per<br />month
                        </h5>
                        <ul class="single-encapsulation">
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_18-Multiple-Demosapps">10 Page Website
                            </li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_19-Multiple-Demosapps">Advance Contact
                                Form</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_20-Multiple-Demosapps">Marketing tools
                                Setup</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_21-Multiple-Demosapps">Google Business
                                Setup</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_22-Multiple-Demosapps">Get FREE Basic SEO
                                and Backlink</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_23-Multiple-Demosapps">FREE 2 year domain
                                and hosting</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_24-Multiple-Demosapps">FREE SSL
                                certificate </li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_25-Multiple-Demosapps">FREE Onsite /
                                Remote Training</li>
                            <li class="Paragraph-it_text_S9*Carousel*Paragraph_26-Multiple-Demosapps">Email Marketing
                                Setup</li>
                        </ul>

                        <div class="text-center pb-2 started-btn">
                            <button class="btn">Get Started</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="S10-Demosapps contactus" id="contacts">
    <div class="row position-relative outer-div bgimage10">
        <div class="ColumnA-Demosapps col-lg-12 position-absolute details">
            <div class="Content-c1-Demosapps">
                <h1 class="Heading-it_text_Footer*ColumnA*C1*TitleHeading_1-Demosapps text-center title">Let’s
                    build something
                    great together
                </h1>

                <h5 class="Heading-it_text_Footer*ColumnA*C1*TitleHeading_2-Demosapps text-center">Make your idea
                    a reality -
                    contact us for a
                    free consultation today.</h5>
            </div>

            <div class="contact_us_icons text-center">
                <img src="assets\image\Icon\Homepage\Contact Us-Chat.svg" alt="contact_us_chat" width="70" />
                <img src="assets\image\Icon\Homepage\Contact Us-Whatsapp.svg" alt="contact_us_whatsapp" width="70" />
                <img src="assets\image\Icon\Homepage\Contact Us-Mail.svg" alt="contact_us_mail" width="70" />
            </div>

            <div class="text-center">
                <button class="btn">CONTACT US</button>
            </div>
        </div>
    </div>
</section>

<section class="Footer-Demosapps footer">
    <div class="row">
        <div class="ColumnA-Demosapps col-xl-4 col-12">
            <div class="Content-c1-Demosapps">
                <h5 class="Heading-it_text_Footer*ColumnA*C1*Heading_1-Demosapps text-white mt-4">ABOUT DEMOSAPP</h5>
                <div class="ParagraphGroup-it_textarea_Footer*ColumnA*c1*Paragraph_1-Demosapps single-encapsulation">
                    <p>Demosapp is an IT-driven company who provide IT solutions towards any businesses with innovative
                        technology across any platforms.</p>
                </div>
                <div class="Socialbar-Demosapps single-encapsulation slinks d-block d-lg-none">
                    <a href="http://instagram.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_1-Demosapps fab fa-instagram fa-fw">
                    </a>
                    <a href="http://facebook.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_2-Demosapps fab fa-facebook-f fa-fw">
                    </a>
                    <a href="http://linkedin.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_3-Demosapps fab fa-linkedin-in fa-fw">
                    </a>
                    <a href="http://twitter.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_4-Demosapps fab fa-twitter fa-fw">
                    </a>
                </div>
            </div>

        </div>
        <div class="ColumnB col-xl col-lg-3 col-6">
            <div class="Content-c1-Demosapps">
                <h5 class="Heading-it_text_Footer*ColumnB*C1*SubHeading_1-Demosapps text-white mt-4">QUICK LINK</h5>
                <div class="Socialbar-Demosapps single-encapsulation footerList">
                    <a href="#BlogDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnB*c1*FooterFirst_QuickLink_1-Demosapps">Blog
                    </a>
                    <a href="#ProjectsDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnB*c1*FooterFirst_QuickLink_2-Demosapps">
                        Project
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnB*c1*FooterFirst_QuickLink_3-Demosapps">Contact Us

                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnC col-xl col-lg-3 col-6">
            <div class="Content-c1-Demosapps">
                <h5 class="Heading-it_text_Footer*ColumnC*C1*SubHeading_2-Demosapps text-white mt-4">COMPANY</h5>
                <div class="Socialbar-Demosapps single-encapsulation footerList">
                    <a href="#AboutDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnC*c1*FooterSecond_QuickLink_1-Demosapps">About
                    </a>
                    <a href="#ServicesDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnC*c1*FooterSecond_QuickLink_2-Demosapps">Why
                        Demosapp
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnD col-xl col-lg-3 col-6">
            <div class="Content-c1-Demosapps">
                <h5 class="Heading-it_text_Footer*ColumnD*C1*SubHeading_3-Demosapps text-white mt-4">SERVICES</h5>
                <div class="Socialbar-Demosapps single-encapsulation  footerList">
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterThird_QuickLink_1-Demosapps">Web Development
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterThird_QuickLink_2-Demosapps">Graphic
                        Design
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterThird_QuickLink_3-Demosapps">Digital
                        Marketing
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnE col-xl col-lg-3 col-6">
            <div class="Content-c1-Demosapps">
                <h5 class="Heading-it_text_Footer*ColumnE*C1*Heading_1-Demosapps text-white mt-4">CAREER</h5>
                <div class="Socialbar-Demosapps single-encapsulation  footerList">
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterForth_QuickLink_1-Demosapps">UI/UX
                        Designer
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterForth_QuickLink_2-Demosapps">Graphic
                        Designer
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterForth_QuickLink_3-Demosapps">Digital
                        Marketer
                    </a>
                </div>
            </div>
        </div>
    </div>

    <hr />

    <div class="row bottom-footer">
        <div class="ColumnF-Demosapps col-lg-5 d-none d-lg-block">
            <div class="Content-c1-Demosapps">
                <div class="Socialbar-Demosapps">
                    <a href="http://instagram.com"
                        class="SocialbarItem-it_url_Footer*ColumnF*c1*BottomFooter_SocialbarItem_1-Demosapps fab fa-instagram fa-fw">
                    </a>
                    <a href="http://facebook.com"
                        class="SocialbarItem-it_url_Footer*ColumnF*c1*BottomFooter_SocialbarItem_2-Demosapps fab fa-facebook-f fa-fw">
                    </a>
                    <a href="http://linkedin.com"
                        class="SocialbarItem-it_url_Footer*ColumnF*c1*BottomFooter_SocialbarItem_3-Demosapps fab fa-linkedin-in fa-fw">
                    </a>
                    <a href="http://twitter.com"
                        class="SocialbarItem-it_url_Footer*ColumnF*c1*BottomFooter_SocialbarItem_4-Demosapps fab fa-twitter fa-fw">
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnG-Demosapps col-lg-7">
            <div class="Content-c1-Demosapps">
                <div class="text-center text-lg-right copyright">
                    <h6 class="Heading-it_text_Footer*ColumnG*c1*BottomFooter_TitleHeading_1-Demosapps">Copyright @
                        2021 All Rights Reserved by Demosapp</h6>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "inc/footer.php"; ?>
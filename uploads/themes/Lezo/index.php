<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Theme 1</title>

    <!-- css  -->
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/services.css">
    <link rel="stylesheet" href="css/portfolio.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/footer.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200&display=swap" rel="stylesheet">
</head>
<body>
    <div class="main_div">
        <!-- header start -->
        <section id="header">
            <div class="nav">
                <div class="leftnav">
                    <h1>Theme 1</h1>
                </div>
                <div class="rightnav">
                    <ul>
                        <li>
                            <a href="#home">Home</a>
                        </li>
                        <li>
                            <a href="#about">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Portfolio</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- header end  -->

        <!-- home start  -->
        <section id="home">
            <div class="home">
                <h1>Theme One</h1>
                <h5>Lorem ipsum dolor sit amet.</h5>
            </div>
        </section>
        <!-- home end  -->

        <!-- about start  -->
        <section id="about">
            <div class="about">
                <div class="about_content">
                    <h1>About Us</h1>
                    <p class="paragraph1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore modi eos totam similique ut perspiciatis eaque sint ad blanditiis omnis perspiciatis eaque sint ad blanditiis omnis perspiciatis eaque sint ad blanditiis omnis?</p>
                    <p class="paragraph2">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, amet doloremque asperiores rerum cum accusamus, inventore beatae officia autem aspernatur cumque, commodi facere culpa fuga eveniet! Sunt ipsum sapiente ea?</p>  
                </div>    
                <div class="about_img">
                    <img src="image/aboutus.png" alt="about us">
                </div>
            </div>
        </section>
        <!-- about end  -->

        <!-- services start -->
        <section id="services">
            <div class="services">
                <h1>Our Services</h1>
                <p class="services_subheading">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellendus earum, harum ad minus doloribus <br> veritatis voluptates aspernatur eos odio corrupti, consequatur temporibus ut  debitis quis fuga <br> nulla accusantium eaque hic facilis eveniet unde molestias? Corporis?
                </p>
                <div class="services_card">
                    <div class="services_carditem">
                        <h4 class="services_cardheading">Card 1</h4>
                        <p class="services_carddescription">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores beatae minima aperiam consectetur quas doloremque eligendi quisquam deserunt debitis ipsam.
                        </p>
                    </div>
                    <div class="services_carditem">
                        <h4 class="services_cardheading">Card 2</h4>
                        <p class="services_carddescription">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores beatae minima aperiam consectetur quas doloremque eligendi quisquam deserunt debitis ipsam.
                        </p>
                    </div>
                    <div class="services_carditem">
                        <h4 class="services_cardheading">Card 3</h4>
                        <p class="services_carddescription">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores beatae minima aperiam consectetur quas doloremque eligendi quisquam deserunt debitis ipsam.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- services end -->

        <!-- portfolio start  -->
        <section id="portfolio">
            <div class="portfolio">
                <h1>Our portfolio</h1>
                <p class="portfolio_subheading">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellendus earum, harum ad minus doloribus <br> veritatis voluptates aspernatur eos odio corrupti, consequatur temporibus ut  debitis quis fuga <br> nulla accusantium eaque hic facilis eveniet unde molestias? Corporis?
                </p>
                <div class="portfolio_card">
                    <div class="portfolio_carditem">
                        <h4 class="portfolio_cardheading">Card 1</h4>
                        <p class="portfolio_carddescription">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores beatae minima aperiam consectetur quas doloremque eligendi quisquam deserunt debitis ipsam.
                        </p>
                    </div>
                    <div class="portfolio_carditem">
                        <h4 class="portfolio_cardheading">Card 2</h4>
                        <p class="portfolio_carddescription">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores beatae minima aperiam consectetur quas doloremque eligendi quisquam deserunt debitis ipsam.
                        </p>
                    </div>
                    <div class="portfolio_carditem">
                        <h4 class="portfolio_cardheading">Card 3</h4>
                        <p class="portfolio_carddescription">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores beatae minima aperiam consectetur quas doloremque eligendi quisquam deserunt debitis ipsam.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- portfolio end  -->

        <!-- contact start  -->
        <section id="contact">
            <div class="contact">
                <h1>Contact Us</h1>
                <p class="contact_description">Kathmandu, Nepal</p>
                <p class="contact_description">1234567890</p>
            </div>
        </section>
        <!-- contact end  -->

        <!-- footer start  -->
        <footer class="footer">
            <h3>copyright @ 2021</h3>
        </footer>
        <!-- footer end  -->

    </div>
    
</body>
</html>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
    <link rel="stylesheet" href="css\bootstrap.css">
    <title>Hello, world!</title>
</head>

<body>
    <section class="NavBar">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Bootstrap</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  active" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  active" href="#">Gallery</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  active" href="#">Portfolio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  active" href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </section>
    <section class="ImageSlider">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="https://images.unsplash.com/photo-1587061949409-02df41d5e562?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://images.unsplash.com/photo-1623849716515-ef59cc26d817?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1074&q=80" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://images.unsplash.com/flagged/photo-1550929708-6f5da120f9ba?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <section class="About">
        <div class="row AboutRow">
            <div class="col-lg-8 c1">
                <h1 class="mb-4">Lorem ipsum dolor sit amet consectetur.</h1>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio voluptatum exercitationem quis, sint aut laudantium dolore quos nihil corrupti repellat porro nemo, voluptas vel similique. Voluptatibus voluptates consequatur voluptate quidem impedit a, perferendis eum debitis ipsum nulla sint consequuntur possimus doloremque, labore ad tempore aliquid qui mollitia corrupti! Omnis quaerat quod, natus doloribus alias quas ex cum cumque tempore id fugiat similique vero adipisci, exercitationem nihil quis expedita. Ullam id corporis quas at praesentium eaque delectus modi et illo ex suscipit nobis porro beatae velit tenetur quidem perferendis enim, eos autem! Delectus, saepe perferendis repudiandae corporis distinctio nemo veniam recusandae voluptate vel iure repellendus facere molestiae, maiores eum velit. Saepe, culpa aperiam at pariatur ipsam esse corrupti unde natus dolore voluptas nihil similique, temporibus illo quod voluptates nostrum laborum recusandae et distinctio? Veritatis consequatur odio magnam possimus autem, harum tenetur accusamus commodi ipsam corporis placeat, blanditiis magni obcaecati eligendi eos molestiae? Libero temporibus possimus quidem ex laborum architecto voluptatem itaque earum quasi quibusdam nemo sapiente quod molestiae porro cupiditate et, odit mollitia nesciunt consequatur ut? Voluptatum autem magni ipsa blanditiis tenetur beatae, earum maxime eveniet aliquid illo iure repellat fugit laborum quae. Excepturi in qui recusandae aspernatur odio iusto soluta eos magni omnis temporibus repellat aliquid mollitia architecto corrupti minima iure numquam ad, delectus saepe quis! Autem ipsa quasi ad, molestias, incidunt perspiciatis pariatur quis accusamus unde ducimus temporibus iste. Architecto tempora odit culpa mollitia voluptate laborum quibusdam alias cupiditate laboriosam ducimus. </p>
            </div>
            <div class="col-lg-4 c2">
                <h1 class="mb-4">Lorem, ipsum dolor.</h1>
                <p>Lorem ipsum dolor sit amet consectetur.</p>
                <p>Lorem ipsum dolor sit amet consectetur.</p>
                <p>Lorem ipsum dolor sit amet consectetur.</p>
                <p>Lorem ipsum dolor sit amet consectetur.</p>
                <p>Lorem ipsum dolor sit amet consectetur.</p>
            </div>
        </div>
    </section>
    <section class="Gallery">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="card">
                    <div class="card-img">
                        <img src="https://images.unsplash.com/photo-1633114073758-c4be9aeb15ac?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80"
                            class="card-img-top" alt="...">
                    </div>

                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="card">
                    <div class="card-img">
                        <img src="https://images.unsplash.com/photo-1636714560735-9f84a7c06d16?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1074&q=80"
                            class="card-img-top" alt="...">
                    </div>

                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="card">
                    <div class="card-img">
                        <img src="https://images.unsplash.com/photo-1589642314445-999ac13b0075?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1173&q=80"
                            class="card-img-top" alt="...">
                    </div>

                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="Footer">
        <div class="footer">
            <h3>copyright @ 2021</h3>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        })
    </script>
</body>

</html>
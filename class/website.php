<?php 

class Website extends Model{
	public function __construct(){
		Model::__construct();
		$this->table('websites');
	}

	public function getWebsiteById($WebsiteId,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('id'=>$WebsiteId));

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getAllWebsiteDataById($WebsiteId,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array(
			'fields' => array('websites.id AS website_id','themes.id AS theme_id','themes.theme_name AS theme_name','themes.category AS theme_category','themes.description AS theme_description','times_installed AS theme_times_installed','themes.current_version AS theme_current_version','themes.creator AS theme_creator','themes.licence AS theme_licence','themes.added_date AS theme_added_date','themes.updated_date AS theme_updated_date','websites.category AS website_category','websites.site_title','websites.copyright_notice','websites.website_email_address','websites.website_domain','websites.maintenance_mode','websites.maintenance_file','websites.favicon_img','websites.added_date AS website_added_date','websites.updated_date AS website_updated_date'),
			'where' => array('websites.id'=>$WebsiteId),
			'join'=> array('LEFT JOIN themes ON websites.theme_id = themes.id')
			);

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getAllWebsites($is_die = false){
		/*$sql = "SELECT * FROM users*/

		$args = array();

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function addWebsite($data,$is_die = false) {
		return $this->insert($data,$is_die);
	}

	public function updateWebsite($data, $id, $is_die = false){
		$attr = array('where' => array('id' => $id));
		return $this->update($data, $attr, $is_die);
	}

	public function deleteWebsite($id){
		$attr = array(
			'where' => array(
				'id'=>$id
			)
		);
		return $this->delete($attr);
	}
}
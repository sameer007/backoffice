<?php 


abstract class Model{
	public $conn = null;
	private $stmt = null;

	private $sql = null;
	private $table = null;

	function __construct(){
		try{
			$this->conn = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';', DB_USER, DB_PASS);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->stmt =  $this->conn->prepare('SET NAMES utf8');
			$this->stmt->execute();
		} catch(PDOException $e){
			error_log('PDO, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].ROOT.'/error/error.log');
			print_r(1);
			print_r('PDO, ['.date('Y-m-d H:i:s').']: '.$e->getMessage());
			//break;
			return false;
		} catch(Exception $e){
			error_log('General, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].ROOT.'/error/error.log');
			print_r(2);

			print_r('General, ['.date('Y-m-d H:i:s').']: '.$e->getMessage());
			//break;
			return false;
		}
	}
	
	final protected function table($_table){
		$this->table = $_table;
	}

	final protected function select($args = array(), $is_die = false){
		try {
			/*
				SELECT * FROM table
				join statements
				WHERE condition
				GROUP BY condition
				ORDER BY condition 
				LIMIT start, count
			*/	

			$this->sql = "SELECT ";

			if(isset($args['fields'])) {
				if(is_array($args['fields'])){
					$this->sql .= implode(", ",$args['fields']);
				} else {
					$this->sql .= $args['fields'];
				}
			} else {
				$this->sql .= " * ";
			}

			$this->sql .= " FROM ";

			
			if(!isset($this->table) || $this->table == NULL){
				throw new Exception("Table Not set");
			}

			$this->sql .= $this->table;
			
			if(isset($args['join']) && !empty($args['join'])){
				
				$this->sql .= " ".$args['join'][0];
				
			}
			
			if(isset($args['where']) && !empty($args['where'])){
				
				if(is_array($args['where'])){
					$temp = array();
					foreach($args['where'] as $column_name => $value){
						//$tempColumnName = str_replace('.', '_', $column_name);
						$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

						$str = $column_name." = :".$tempColumnName;
						$temp[] = $str;
					}

					// foreach($args['where'] as $column_name => $value){
				 //        $str = $column_name."= ";
				 //        if(is_string($value)){
				 //            $str .= " '".$value."' ";
				 //        } else {
				 //            $str .= $value;
				 //        }
				 //        $temp[] = $str;
				 //    }
					$this->sql .= " WHERE ".implode(' AND ', $temp);
					
				} else {
					$this->sql .= " WHERE ".$args['where'];
				}
			}

			if(isset($args['groupby']) && !empty($args['groupby'])){
				foreach ($args['groupby'] as $index => $value) {
					$this->sql .= " GROUP BY ".$value;
				}
			}

			if(isset($args['orderby']) && !empty($args['orderby'])){

				if(is_array($args['orderby'])){
					foreach ($args['orderby'] as $index => $value) {
						if ($index == 0) {
							$this->sql .= " ORDER BY ".$value;
						}else{
							$this->sql .= ",".$value;
						}
					}
				}else{
					$this->sql .= " ORDER BY ".$args['orderby'];
				}
			}

			if(isset($args['sql']) && !empty($args['sql'])){
				$this->sql = null;
				$this->sql .= " ".$args['sql'][0];
				
			}
			if($is_die){
				debugger($args);
				debugger($this->sql, true);
			}
			
			$this->stmt = $this->conn->prepare($this->sql);
			/*Value Bind*/

			if(isset($args['where']) && is_array($args['where']) && !empty($args['where'])){
				foreach($args['where'] as $column_name => $value){
					
					if(is_null($value)){
						$param = PDO::PARAM_NULL;
					} else if(is_bool($value)){
						$param = PDO::PARAM_BOOL;
					} else if(is_int($value)){
						$param = PDO::PARAM_INT;
					} else {
						$param = PDO::PARAM_STR;
					}
					//$tempColumnName = str_replace('.', '_', $column_name);
					$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);
					//debugger($tempColumnName);
					//debugger($value);
					if($param){
						$this->stmt->bindValue(":".$tempColumnName, $value, $param);
					}
				}
			}
			//debugger($this->stmt);
			$this->stmt->execute();

			$data = $this->stmt->fetchAll(PDO::FETCH_OBJ);
			
			return $data;
		} catch(PDOException $e){
			error_log('PDO, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		} catch(Exception $e){
			error_log('General, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		}
	}


	final protected function insert($data = array(), $is_die = false){
		try{
			/*
					INSERT INTO table SET 
						column_name_1 = value_1,
						column_name_2 = value_2, 
						.....
						column_name_n = value_n

					INSERT INTO table SET 
						column_name_1 = :key_1,
						column_name_2 = :key_2,
						... ,
						column_name_n = :key_n

					$stmt => 
					$stmt->bindValue(':key_1', value_1, data type)
					$stmt->bindValue(':key_2', value_2, data type)
					....
					$stmt->bindValue(':key_n', value_n, data type)

			*/

			$this->sql = "INSERT INTO ";

			if(!isset($this->table) || $this->table == NULL){
				throw new Exception("Table Not set");
			}

			$this->sql .= $this->table." SET ";


			if(isset($data) && !empty($data)){
				if(is_array($data)){
					$temp = array();
					foreach($data as $column_name => $value){
						$dataType = gettype($value);
						//$tempColumnName = str_replace('.', '_', $column_name);
						$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

						$str = $column_name." = :".$tempColumnName;

						$temp[] = $str;
					}

					// foreach($data as $column_name => $value){
				 //        $str = $column_name." = ";
				 //        if(is_string($value)){
				 //            $str .= " '".$value."' ";
				 //        } else {
				 //            $str .= $value;
				 //        }
				 //        $temp[] = $str;
				 //    }
					$this->sql .= implode(', ', $temp);
				} else {
					$this->sql .= $data;
				}
			} else {
				throw new Exception("Data not set.");
			}
			if($is_die){
				debugger($this->sql);

				debugger($data);
			}
			$this->stmt = $this->conn->prepare($this->sql);


			if(is_array($data)){
				foreach($data as $column_name => $value){
					if(is_null($value)){
						$param = PDO::PARAM_NULL;
					} else if(is_int($value)){
						$param = PDO::PARAM_INT;
					} else if(is_bool($value)){
						$param = PDO::PARAM_BOOL;
					} else {
						$param = PDO::PARAM_STR;
					}
					//$tempColumnName = str_replace('.', '_', $column_name);
					$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

					if(isset($param)){
						$this->stmt->bindValue(':'.$tempColumnName, $value, $param);
					}
				}
			}

			$success = $this->stmt->execute();

			if($success){
				return $this->conn->lastInsertId();
			} else {
				return false;
			}
		} catch(PDOException $e){
			error_log('PDO, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		} catch(Exception $e){
			error_log('General, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		}
	}

	final protected function delete($args = array(), $is_die = false){
		try {
			/*
				DELETE FROM table
				WHERE condition
			*/	

			$this->sql = "DELETE FROM ";

			if(!isset($this->table) || $this->table == NULL){
				throw new Exception("Table Not set");
			}

			$this->sql .= $this->table;


			if(isset($args['where']) && !empty($args['where'])){
				if(is_array($args['where'])){
					$temp = array();
					foreach($args['where'] as $column_name => $value){
						//$tempColumnName = str_replace('.', '_', $column_name);
						$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

						$str = $column_name." = :".$tempColumnName;
						$temp[] = $str;
					}
					// foreach($args['where'] as $column_name => $value){
				 //        $str = $column_name." = ";
				 //        if(is_string($value)){
				 //            $str .= " '".$value."' ";
				 //        } else {
				 //            $str .= $value;
				 //        }
				 //        $temp[] = $str;
				 //    }
					$this->sql .= " WHERE ".implode(' AND ', $temp);

				} else {
					$this->sql = " WHERE ".$this->args;
				}
			}



			if($is_die){
				debugger($this->sql, true);
			}

			$this->stmt = $this->conn->prepare($this->sql);
			/*Value Bind*/
			if(isset($args['where']) && is_array($args['where']) && !empty($args['where'])){
				foreach($args['where'] as $column_name => $value){
					if(is_null($value)){
						$param = PDO::PARAM_NULL;
					} else if(is_bool($value)){
						$param = PDO::PARAM_BOOL;
					} else if(is_int($value)){
						$param = PDO::PARAM_INT;
					} else {
						$param = PDO::PARAM_STR;
					}
					$tempColumnName = str_replace('.', '_', $column_name);

					if($param){
						$this->stmt->bindValue(":".$tempColumnName, $value, $param);
					}
				}
			}

			$data = $this->stmt->execute(); //true or false

			return $data;
		} catch(PDOException $e){
			error_log('PDO, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		} catch(Exception $e){
			error_log('General, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		}
	}


	final protected function update($data = array(), $attr = NULL, $is_die = false){

		try{
			/*
					UPDATE table SET 
						column_name_1 = :key_1,
						column_name_2 = :key_2,
						... ,
						column_name_n = :key_n

					WHERE condition

					$stmt => 
					$stmt->bindValue(':key_1', value_1, data type)
					$stmt->bindValue(':key_2', value_2, data type)
					....
					$stmt->bindValue(':key_n', value_n, data type)

			*/

			$this->sql = "UPDATE  ";

			if(!isset($this->table) || $this->table == NULL){
				throw new Exception("Table Not set");
			}

			$this->sql .= $this->table." SET ";


			if(isset($data) && !empty($data)){
				if(is_array($data)){
					$temp = array();
					foreach($data as $column_name => $value){
						//$tempColumnName = str_replace('.', '_', $column_name);
						$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

						$str = $column_name." = :".$tempColumnName;
						$temp[] = $str;
					}
					// foreach($data as $column_name => $value){
				 //        $str = $column_name." = ";

				 //        if(is_string($value)){
				 //            $str .= " '".$value."' ";
				 //        } else {
				 //        	if (is_null($value)) {
				 //            	$str .= 'NULL';
				 //        	}else{
				 //            	$str .= $value;

				 //        	}

				 //        }
				 //        $temp[] = $str;
				 //    }
					$this->sql .= implode(', ', $temp);
				} else {
					$this->sql .= $data;
				}
			} else {
				throw new Exception("Data not set.");
			}

			if(isset($attr['where']) && !empty($attr['where'])){
				if(is_array($attr['where'])){
					$temp = array();
					foreach($attr['where'] as $column_name => $value){
						//$tempColumnName = str_replace('.', '_', $column_name);
						$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

						$str = $column_name." = :".$tempColumnName;
						$temp[] = $str;
					}
					// foreach($attr['where'] as $column_name => $value){
				 //        $str = $column_name." = ";

				 //        if(is_string($value)){
				 //            $str .= " '".$value."' ";
				 //        }else {

				 //            $str .= $value;
				 //        }
				 //        $temp[] = $str;
				 //    }
					$this->sql .= " WHERE ".implode(' AND ', $temp);

				} else {
					$this->sql .= " WHERE ".$attr['where'];
				}
			}

			if(isset($attr['sql']) && !empty($attr['sql'])){
				$this->sql = null;
				$this->sql .= " ".$attr['sql'][0];
				//$data = '$this->sql';
			}
			
			if($is_die){
				debugger($this->sql);
				debugger($data, true);
			}

			$this->stmt = $this->conn->prepare($this->sql);


			if(is_array($data)){
				foreach($data as $column_name => $value){
					if(is_null($value)){
						$param = PDO::PARAM_NULL;
					} else if(is_int($value)){
						$param = PDO::PARAM_INT;
					} else if(is_bool($value)){
						$param = PDO::PARAM_BOOL;
					} else {
						$param = PDO::PARAM_STR;
					}
					//$tempColumnName = str_replace('.', '_', $column_name);
					$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

					if(isset($param)){
						$this->stmt->bindValue(':'.$tempColumnName, $value, $param);
					}
				}
			}

			if(isset($attr['where']) && is_array($attr['where']) && !empty($attr['where'])){
				foreach($attr['where'] as $column_name => $value){
					if(is_null($value)){
						$param = PDO::PARAM_NULL;
					} else if(is_bool($value)){
						$param = PDO::PARAM_BOOL;
					} else if(is_int($value)){
						$param = PDO::PARAM_INT;
					} else {
						$param = PDO::PARAM_STR;
					}
					//$tempColumnName = str_replace('.', '_', $column_name);
					$tempColumnName = preg_replace("/[^a-zA-Z]/", "_", $column_name);

					if($param){
						$this->stmt->bindValue(":".$tempColumnName, $value, $param);
					}
				}
			}
			//debugger($this,true);
			$success = $this->stmt->execute();
			if($success){
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e){
			error_log('PDO, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		} catch(Exception $e){
			error_log('General, ['.date('Y-m-d H:i:s').']: '.$e->getMessage(), 3, $_SERVER['DOCUMENT_ROOT'].'error/error.log');
			return false;
		}
	}
}
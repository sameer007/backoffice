<?php 

class theme extends Model{
	public function __construct(){
		Model::__construct();
		$this->table('themes');
	}

	public function getThemeById($id,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('id'=>$id));

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getThemeByThemeTitle($themeTitle,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('theme_title'=>$themeTitle));

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getAllThemesByThemeCategory($themeCategory = array(),$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/
		$condition = '';
		foreach ($themeCategory as $key => $value) {
			$condition .= "category LIKE '%$value%'";
			
			if (sizeof($themeCategory) > ($key+1)) {
				$condition .= " AND " ;
			}
		}
		$args = array(
			'where' => $condition
			);

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getAllThemes($is_die = false){
		/*$sql = "SELECT * FROM users*/

		$args = array();

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function addTheme($data) {
		return $this->insert($data);
	}

	public function updateTheme($data, $id, $is_die = false){
		$attr = array('where' => array('id' => $id));
		return $this->update($data, $attr, $is_die);
	}

	public function deleteTheme($id){
		$attr = array(
			'where' => array(
				'id'=>$id
			)
		);
		return $this->delete($attr);
	}
}
<?php 

class User extends Model{
	public function __construct(){
		Model::__construct();
		$this->table('users');
	}

	public function getUserByUserEmail($userEmail,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('email_address'=>$userEmail));

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getUserByUserType($user_type){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('user_type'=>$user_type));

		$data = $this->select($args);

		return $data;
	}

	public function getUserByUserId($userId,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array(
			'fields' => array('users.id','role.role_id','users.full_name','role.role_title','users.email_address','users.image','users.status','users.added_date','users.updated_date'),
			'join'=> array('LEFT JOIN role ON users.role_id = role.role_id'),
			'where' => array('id'=>$userId)
		);

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getUserByRoleId($roleId,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array(
			'fields' => array('users.id','role.role_id','users.full_name','role.role_title','users.email_address','users.image','users.status','users.added_date','users.updated_date'),
			'join'=> array('LEFT JOIN role ON users.role_id = role.role_id'),
			'where' => array('role.role_id'=>$roleId)
		);

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getAllUsers($is_die = false){
		/*$sql = "SELECT * FROM users*/

		$args = array(
			'fields' => array('users.id','role.role_id','users.full_name','role.role_title','users.email_address','users.image','users.status','users.added_date','users.updated_date'),
			'join'=> array('LEFT JOIN role ON users.role_id = role.role_id')

			);

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getUserByUserTypeAndId($user_type,$id){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('user_type'=>$user_type,'id'=>$id));

		$data = $this->select($args);

		return $data;
	}

	public function addUser($data) {
		return $this->insert($data);
	}



	public function updateUserRole($data,$is_die = false){
		// $data['currentSuperadminUserId']
		// $data['currentSuperadminUserId']
		// $data['newSuperadminUserId']

		$attr = array(
			'sql'=> array("UPDATE users
				SET role_id = CASE
				WHEN id = ".$data['currentSuperadminUserId']." THEN ".$data['currentSuperadminNewRoleId']."
				WHEN id = ".$data['newSuperadminUserId']." THEN 4
				ELSE role_id
				END")
		);
		return $this->update($data, $attr, $is_die);
	}

	public function updateUser($data, $id, $is_die = false){
		$attr = array('where' => array('id' => $id));
		return $this->update($data, $attr, $is_die);
	}

	public function deleteUser($id){
		$attr = array(
			'where' => array(
				'id'=>$id
			)
		);
		return $this->delete($attr);
	}
}
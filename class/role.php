<?php 

class Role extends Model{
	public function __construct(){
		Model::__construct();
		$this->table('role');
	}

	public function getRoleById($roleId,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('role_id'=>$roleId));

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getRoleByRoleTitle($roleTitle,$is_die = false){
		/*$sql = "SELECT * FROM users WHERE email_address = :key";*/

		$args = array('where' => array('role_title'=>$roleTitle));

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function getAllRoles($is_die = false){
		/*$sql = "SELECT * FROM users*/

		$args = array();

		$data = $this->select($args,$is_die);

		return $data;
	}

	public function addRole($data) {
		return $this->insert($data);
	}

	public function updateRole($data, $id, $is_die = false){
		$attr = array('where' => array('role_id' => $id));
		return $this->update($data, $attr, $is_die);
	}

	public function deleteRole($id){
		$attr = array(
			'where' => array(
				'role_id'=>$id
			)
		);
		return $this->delete($attr);
	}
}
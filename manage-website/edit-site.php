<?php 
include '../inc/header.php';
include '../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';

$theme = new Theme();
$website = new Website();

//$allUsers = $users->getAllUsers();
$allThemes = $theme->getAllThemes();

// $allThemes2 = $allThemes;
// $allThemes = array_merge($allThemes,$allThemes2,$allThemes2);

if (isset($_GET) && !empty($_GET)) {
  if (isset($_GET['act']) && !empty($_GET['act'])) {
    //debugger($_GET['act'],true);
    if($_GET['act'] == substr(md5('edit-website-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)){
      //debugger('here',true);
      $websiteData = $website->getWebsiteById($_GET['websiteId']);


      //debugger($websiteData);
      if(isset($websiteData) && !empty($websiteData)){
        $_SESSION['website_id'] = $websiteData[0]->id;

      }else{
        redirect('./create-new-site','error','Something went wrong while retreiving website info');
      }
    }else{
      redirect('./404');
    }
  }else{
    redirect('./404');
  }
}else{
  redirect('./404');
}


$arrangedAllThemesData = array();
$noOfItemsInSet = 3;
$count = 1;
$page = 1;
foreach ($allThemes as $key => $value) {

  if($count == 5){
    //increase page
    $page++;
    $count = 1;
  }else{

  }
  $arrangedAllThemesData['page'.'-'.$page][$count] = $value;
  $count++;

}

?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Create New Site</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form id="msform" action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/website" method="post" id="msform">
          <div class="row">

            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Edit Website</h3>
                </div>
                <div class="card-body">
                  <!-- progressbar -->
                  <ul id="progressbar">
                    <li class="active" id="info"><strong>Info</strong></li>
                    <li id="theme"><strong>Theme</strong></li>
                    <li id="completed"><strong>Finalize</strong></li>
                  </ul>
                  <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                  </div> <br> <!-- fieldsets -->
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <fieldset>
                    <div class="row">
                      <div class="col-7">
                        <h2 class="fs-title">Info:</h2>
                      </div>
                      <div class="col-5">
                        <h2 class="steps">Step 1 - 3</h2>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label for="siteCategory">What kind of website do you want to create?</label>
                      <div class="select2-purple">
                        <?php 
                        $categoryArray = array('Sport','E-Commerce','Blog','Gaming','Entertainment','News','Portfolio');

                        ?>
                        <select class="select2" name="category" id="siteCategory" data-placeholder="Select website category" data-dropdown-css-class="select2-purple" style="width: 100%;" required>
                          <?php foreach ($categoryArray as $key => $value) { ?>
                            <?php if($websiteData[0]->category == $value){ ?>
                              <option value="<?php echo $value ?>" selected><?php echo $value ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo $value ?>"><?php echo $value ?></option>
                            <?php } ?>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <input type="button" name="next" class="next-progressbar action-button" value="Next" />
                  </fieldset>
                  <fieldset>
                    <div class="form-card">
                      <div class="row">
                        <div class="col-7">
                          <h2 class="fs-title">Pick a theme you like:</h2>
                        </div>
                        <div class="col-5">
                          <h2 class="steps">Step 2 - 3</h2>
                        </div>
                      </div> 
                      <?php if(isset($arrangedAllThemesData) && !empty($arrangedAllThemesData)){   
                        ?>

                        <div class="row">
                          <div class="col-lg-12" id="themes">
                            <?php foreach ($arrangedAllThemesData as $key => $value) {  

                              $page = explode('-', $key)[1];
                              $pageId = 'page-'.$page ;

                              ?>
                              <div class="row pageContent d-none" id="<?php echo $pageId ?>">
                                <?php foreach ($arrangedAllThemesData[$key] as $arrangedAllThemesDataKey => $arrangedAllThemesDataValue) {  ?>

                                  <div class="col-lg-3 divselect item">
                                    <input type="radio" name="theme" value="<?php echo $arrangedAllThemesDataValue->id ?>" class="d-none radio-theme" <?php echo ($websiteData[0]->theme_id == $arrangedAllThemesDataValue->id ) ? 'checked = "checked"' : '' ?> >

                                    <div class="small-box-custom p-2 bg-default d-block w-100">
                                      <div class="inner h-200">
                                      </div>
                                      <h3 class="p-1 m-1">
                                        <div class="title-left ">
                                          <?php echo $arrangedAllThemesDataValue->theme_name ?>  
                                        </div> 
                                        <div class="title-right"> 
                                          <?php 
                                          $themeDetailUrl = './theme_details?themeId='.$arrangedAllThemesDataValue->id.'&act='.substr(md5('edit-theme-'.$arrangedAllThemesDataValue->id.'-'.$_SESSION['token']), 5, 15);
                                          ?>
                                          <span class="btn btn-default btn-free checkTheme"><i class="fas fa-check"></i> </span>
                                        </div>
                                      </h3>
                                      <p class="p-1 m-1"><?php echo $arrangedAllThemesDataValue->description ?></p>
                                    </div>
                                  </div>
                                <?php } ?>
                              </div>

                            <?php } ?>
                          </div>
                        </div>
                      <?php } ?>
                      <div id="light-pagination" class="pagination"></div>

                    </div> 
                    <input type="button" name="next" class="next-progressbar action-button" value="Next" />
                    <input type="button" name="previous" class="previous-progressbar action-button-previous" value="Previous" />
                  </fieldset>
                  <fieldset>
                    <div class="form-card">
                      <div class="row">
                        <div class="col-7">
                          <h2 class="fs-title">You are doing great!!</h2>
                        </div>
                        <div class="col-5">
                          <h2 class="steps">Step 3 - 3</h2>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12">
                          <p>Click submit to finalize website creation...</p>
                        </div>
                      </div>
                    </div> 
                    <button type="submit" name="edit-site" value="submit" class="next-progressbar action-button" id="btn-submit">Submit</button>
                    <input type="button" name="previous" class="previous-progressbar action-button-previous" value="Previous" />
                  </fieldset>

                </div>

              </div>
            </div>
          </div>
        </form>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src="<?php echo ASSETS_URL ?>js/functions.js"></script>
  <?php 
  $scripts = '
  <script src="'.ASSETS_URL.'js/progressbar.js"></script>
  <script src="'.VENDOR_URL.'select2/js/select2.full.min.js"></script>
  <script src="'.VENDOR_URL.'simplePagination/js/jquery.simplePagination.js"></script>
  <script src="'.ASSETS_URL.'js/light-pagination.js"></script>
  <script src="'.ASSETS_URL.'js/create-new-site.js"></script>';
  include '../inc/footer.php';
  ?>
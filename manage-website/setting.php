<?php 
include '../inc/header.php';
include '../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';

$role = new Role();
$website = new Website();


$allUsers = $users->getAllUsers();
$allRole = $role->getAllRoles();
$allWebsites = $website->getAllWebsites();
//debugger($allWebsites);

if (isset($_GET) && !empty($_GET)) {
  if (isset($_GET['act']) && !empty($_GET['act'])) {
    //debugger($_GET['act'],true);
    if($_GET['act'] == substr(md5('website-setting'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)){
      //debugger('here',true);
      $websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);

      if (isset($websiteData) && !empty($websiteData)) {
        $_SESSION['website_id'] = $websiteData[0]->website_id;
      }else{
        redirect('./404');
      }
      //debugger($websiteData,true);
      
    }else{
      redirect('./404');
    }
  }else{
    redirect('./404');
  }
}else{
  redirect('./404');
}
?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Website</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <ul class="nav nav-tabs" id="navTabs">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">General</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Front-End</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Maintenance</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Favicon</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="row pl-2 pr-2">
              <div class="col-lg-6">
                <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/website" method="post" id="settingForm" enctype='multipart/form-data'>
                  <div class="swappableDivCollection pt-3">
                    <div class="row d-block swappableDivs" id="general">
                      <div class="card">
                        <div class="card-body">
                          <div class="form-group">
                            <label for="themeName">Site Title</label>
                            <input type="text" class="form-control" value="<?php echo (isset($websiteData[0]->site_title) && !empty($websiteData[0]->site_title) ? $websiteData[0]->site_title : '') ?>" name="site-title" placeholder="Site Title">
                          </div>
                          <div class="form-group">
                            <label for="themeName">Copyright Notice</label>
                            <input type="text" class="form-control" value="<?php echo (isset($websiteData[0]->copyright_notice) && !empty($websiteData[0]->copyright_notice) ? $websiteData[0]->copyright_notice : '') ?>" name="copyright-notice" placeholder="Copyright Notice">
                          </div>
                          <div class="form-group">
                            <label for="themeName">Email Address</label>
                            <input type="email" class="form-control" value="<?php echo (isset($websiteData[0]->website_email_address) && !empty($websiteData[0]->website_email_address) ? $websiteData[0]->website_email_address : '') ?>" name="email-address" placeholder="Site Email">
                          </div>
                          <div class="form-group">
                            <label for="themeName">Connect a Domain</label>
                            <input type="text" class="form-control" value="<?php echo (isset($websiteData[0]->website_domain) && !empty($websiteData[0]->website_domain) ? $websiteData[0]->website_domain : '') ?>" name="site-domain" placeholder="Site Domain">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row d-none swappableDivs" id="front-End">
                      <div class="row">
                        <div class="card">
                          <div class="card-body">
                            <div class="col-lg-6">
                              <img src="../assets/images/gallery.png">
                              <p class="pl-3">Preview</p>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="card">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-lg-12">
                                  <?php 
                                      //debugger($websiteData);

                                   ?>
                                  <p class="pb-0 mb-0">Current theme : <strong><?php echo $websiteData[0]->theme_name ?></strong></p>
                                  <p class="pb-0 mb-0">Created By : <?php echo $websiteData[0]->theme_creator ?></p>
                                  <div class="input-group-prepend">

                                  </div>
                                  <div class="btn btn-default">
                                    <input class="form-check-custom ml-0" name="theme-action"  type="radio" id="activateTheme">
                                    <label class="form-check-label ml-3"  for="activateTheme">
                                      Activate
                                    </label>
                                  </div>
                                  <div class="btn btn-default">
                                    <input class="form-check-custom ml-0" name="theme-action"  type="radio" id="manageTheme">
                                    <label class="form-check-label ml-3"  for="manageTheme">
                                      Manage
                                    </label>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-lg-12">
                                  <p class="pb-0 mb-0">Create New Blank Theme</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-lg-12">
                                  <p class="pb-0 mb-0">Find new themes</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>
                    <div class="row d-none swappableDivs" id="maintenance">


                      <div class="card">
                        <div class="card-body">
                          <div class="form-check">
                            <?php if($websiteData[0]->maintenance_mode == 1){ ?>
                              <input class="form-check-input" type="checkbox" name="enable-maintenance-mode" value="1" id="enablemaintenanceMode" checked>
                            <?php }else{ ?>
                              <input class="form-check-input" type="checkbox" name="enable-maintenance-mode" value="1" id="enablemaintenanceMode">
                            <?php } ?>

                            <label for="enablemaintenanceMode">
                              Enable maintenance Mode
                            </label>
                          </div>
                          <div class="form-group">
                            <label for="maintenanceFile">Upload a file to show when maintenance mode is activated</label>
                            <input type="file" class="form-control" name="maintenance-file" id="maintenanceFile" placeholder="Copyright Notice" disabled>
                            <p>* The file can be images or html files</p>
                            <p>* maintenance mode will display the maintenance page to visitors who are not signed in to the back-office area</p>
                          </div>
                        </div>
                      </div>


                    </div>
                    <div class="row d-none swappableDivs" id="favicon">

                      <div class="card">
                        <div class="card-body">

                          <div class="form-group">
                            <label for="faviconFile">A Favicon is a small icon to your site. Get a custom favicon to help visitors recognize your brand and to stand out in browser tabs</label>
                            <input type="file" class="form-control" name="favicon-file" id="faviconFile" placeholder="Copyright Notice">
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <button type="submit" name="change-setting" value="submit" class="btn btn-primary" id="btn-submit">Save</button>
                  <br>
                </form>


              </div>
            </div>
          </div>
        </div>

      </div>


    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<script type="text/javascript">
  $('#navTabs li a').on( "click", function() {
    navLink = $('.nav-link');
    var swappableDivs = $('.swappableDivs');

    $('#navTabs li a div').addClass('d-none');
    navLink.removeClass('active');
    $('.swappableDivs').addClass('d-none').removeClass('d-block');
    $(this).addClass('active');
    swappableDiv = '#'+camelize($(this).html())
    //console.log(camel);
    $(swappableDiv).addClass('d-block').removeClass('d-none');
  });

  $('#enablemaintenanceMode').on( "click", function() {
    if($(this).is(':checked')) {
      $('#maintenanceFile').attr('disabled',false)
    }else{
      $('#maintenanceFile').attr('disabled',true)

    }
  });

  if($('#enablemaintenanceMode').is(':checked')) {
    $('#maintenanceFile').attr('disabled',false)
  }else{
    $('#maintenanceFile').attr('disabled',true)

  }
</script>
<?php 
$scripts = '
';
include '../inc/footer.php';
?>
?>
<?php 
include '../inc/header.php';
include '../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';

$role = new Role();
$website = new Website();


$allUsers = $users->getAllUsers();
$allRole = $role->getAllRoles();
$allWebsites = $website->getAllWebsites();
//debugger($allWebsites);
?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Website</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <?php if(isset($allWebsites) && !empty($allWebsites)){ ?>
            <?php foreach ($allWebsites as $key => $value) { 

              $websiteUrl = './site/'.$value->id.'/';
              $editWebsiteUrl = './edit-site?websiteId='.$value->id.'&act='.substr(md5('edit-website-'.$value->id.'-'.$_SESSION['token']), 5, 15);
              $manageWebsiteHomeUrl = './home/?websiteId='.$value->id.'&act='.substr(md5('manage-website-home-'.$value->id.'-'.$_SESSION['token']), 5, 15);

              $duplicateWebsiteUrl = './../process/website?websiteId='.$value->id.'&act='.substr(md5('duplicate-website-'.$value->id.'-'.$_SESSION['token']), 5, 15);
              $deleteWebsiteUrl = './../process/website?websiteId='.$value->id.'&act='.substr(md5('delete-website-'.$value->id.'-'.$_SESSION['token']), 5, 15);
              $websiteSettingUrl = './setting?websiteId='.$value->id.'&act='.substr(md5('website-setting'.$value->id.'-'.$_SESSION['token']), 5, 15);
              ?>
              <div class="col-md-4 offset-md-1">
                <div class="card">
                  <div class="card-body">
                    <div class="tab-content p-0">
                      <!-- Morris chart - Sales -->
                      <div class="chart tab-pane tab-pane-fixed active">
                      </div>
                      <div class="chart tab-pane tab-pane-fixed">
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-header header-bottom">
                    <h3 class="card-title p-2">
                      <i class="fas fa-pen mr-1"></i>
                      <a href="<?php echo $manageWebsiteHomeUrl ?>">Manage Website</a>
                    </h3>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-sm-8">
                        <h3 class="card-title p-2">
                          <i class="fas fa-globe mr-1"></i> <a href="<?php echo $websiteUrl ?>"><?php echo (isset($value->website_domain) && !empty($value->website_domain)) ? $value->website_domain : 'sampleWebsiteDomain - '.$value->id.'.com'  ?></a>
                        </h3>
                      </div><!-- /.col -->
                      <div class="col-sm-4">
                        <ol class="float-sm-right">
                          <div class="dropdown">
                            <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="<?php echo $websiteSettingUrl ?>">Setting</a>
                              <a class="dropdown-item" href="<?php echo $editWebsiteUrl ?>">Edit Site</a>
                              <a class="dropdown-item" href="<?php echo $duplicateWebsiteUrl ?>">Duplicate Site</a>
                              <a class="dropdown-item" target="_blank" href="<?php echo $websiteUrl ?>">View Site</a>
                              <a class="dropdown-item text-danger" href="<?php echo $deleteWebsiteUrl ?>" onclick="return confirm(<?php echo '\'Are you sure you want to delete website '.'sampleWebsiteDomain - '.$value->id.'.com ?\''  ?> ); " >Delete Site</a>
                            </div>
                          </div>
                        </ol>
                      </div><!-- /.col -->
                    </div>
                  </div><!-- /.card-header -->
                </div>
              </div>
            <?php } ?>
          <?php } ?>
          <div class="col-md-4 offset-md-1">
            <div class="card">
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Morris chart - Sales -->
                  <a href="./create-new-site" style="color: #212529">
                    <p class="text-center">
                      <span class="fas fa-plus fa-10x center-fa" aria-hidden="true"></span>
                    </p>
                  </a>
                  
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>

      </div>


    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php 
$scripts = '
';
include '../inc/footer.php';
?>
?>
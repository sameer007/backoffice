<?php 
include '../../inc/header.php';
include '../../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';

$website = new Website();


$webpages = array();

if (isset($_GET) && !empty($_GET)) {
	if (isset($_GET['act']) && !empty($_GET['act'])) {
    	//debugger($_GET['act'],true);
		if($_GET['act'] == substr(md5('manage-website-home-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)){
      		//debugger('here',true);

			$websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);


      		//debugger($websiteData,true);
			if(isset($websiteData) && !empty($websiteData)){
				$websiteDIR = SITE_DIR.$websiteData[0]->website_id.'/';
				//debugger($websiteDIR);
				$dir = opendir($websiteDIR); 
				$file = readdir($dir);

				while( $file = readdir($dir) ) { 
					if(!is_dir($websiteDIR . '/' . $file)){
						array_push($webpages, $file);

					}
				}
				$_SESSION['website_id'] = $websiteData[0]->website_id;

			}else{
				redirect('./manage-website','error','Something went wrong while retreiving website info');
			}
		}else{
			redirect('./404');
		}
	}else{
		redirect('./404');
	}
}else{
	redirect('./404');
}


?>

<div class="wrapper">
	<?php include '../../inc/left-sidebar.php';?>
	<!-- Content Wrapper. Contains page content -->

	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header flash">
			<div class="container-fluid flash">
				<div class="row">
					<div class="col-auto">
						<?php flash(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-left">
							<div class="circle-back">
								<i class="far fa-arrow-alt-circle-left fa-lg"></i>
							</div>
							<?php  if(isset($routeArray) && !empty($routeArray)){
								displayRoutes($routeArray);
							}
							?>
						</ol>
					</div><!-- /.col -->
				</div><!-- /.row -->
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0">Website Management</h1>
					</div><!-- /.col -->
					<?php 
					$publishUrl = './../process/website?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('publish-website-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
					$addNewPageUrl = './../process/website?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('add-new-page-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);

					?>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<a type="button" class="btn btn-md btn-default mr-1" data-toggle="modal" data-target="#new-page" href="<?php echo $addNewPageUrl ?>"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New Page</a>
							<a type="button" class="btn btn-md btn-primary mr-1" onclick="return confirm(<?php echo '\'Are you sure you want to publish this website : '.'sampleWebsiteDomain - '.$websiteData[0]->website_id.'.com ?\''  ?> ); " href="<?php echo $publishUrl ?>">Publish</a>
						</ol>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<?php foreach ($webpages as $key => $value) { ?>
						<div class="col-lg-4 offset-1">
							<div class="card"> 
								<div class="card-header">
									<div class="row">
										<div class="col-lg-1"></div>
										<div class="col-lg-2">
											<i class="far fa-file fa-3x mt-1"></i>
										</div>
										<div class="col-lg-7">
											<div class="text-center">
												<p class="mt-3"><b><?php echo $value ?></b></p>
											</div>
										</div>
										<div class="col-lg-2">
											<ol class="float-sm-right">
												<div class="dropdown">
													<button class="btn p-0 mt-3" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="fas fa-ellipsis-v fa-lg"></i>
													</button>
													<?php 
													$page = explode('.', $value)[0]; 
													$viewPageUrl = './../site/'.$websiteData[0]->website_id.'/'.$page;
													$editPageUrl = './edit-page?websiteId='.$websiteData[0]->website_id.'&pageName='.$page.'&act='.substr(md5('edit-page-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
													$duplicatePageUrl = '../../process/manageWebsite?websiteId='.$websiteData[0]->website_id.'&pageName='.$page.'&act='.substr(md5('duplicate-page-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
													$deletePageUrl = '../../process/manageWebsite?websiteId='.$websiteData[0]->website_id.'&pageName='.$page.'&act='.substr(md5('delete-page-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
													$renamePageUrl = '../../process/manageWebsite?websiteId='.$websiteData[0]->website_id.'&pageName='.$page.'&act='.substr(md5('rename-page-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
													$setAsHomepageUrl = '../../process/manageWebsite?websiteId='.$websiteData[0]->website_id.'&pageName='.$page.'&act='.substr(md5('set-as-homepage-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);

													?>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="<?php echo $editPageUrl ?>">Edit</a>
														<a class="dropdown-item" target="_blank" href="<?php echo $viewPageUrl ?>">View Page</a>
														<a class="dropdown-item" href="<?php echo $duplicatePageUrl ?>">Duplicate Page</a>
														<?php if($value != 'idontknow.php'){ ?>
															<a class="dropdown-item" data-toggle="modal" data-target="#rename-page" id="<?php echo 'renamePage-'.$value ?>" href="<?php echo $renamePageUrl ?>">Rename Page</a>
															<a class="dropdown-item" onclick="return confirm(<?php echo '\'Are you sure you want to set : '.$value.' content as index.php?\''  ?>); " href="<?php echo $setAsHomepageUrl ?>">Set As Homepage</a>
															<a class="dropdown-item text-danger" href="<?php echo $deletePageUrl ?>" onclick="return confirm(<?php echo '\'Are you sure you want to delete : '.$value.' webpage?\''  ?>); " >Delete Page</a>
														<?php }else{ ?>
															<a class="dropdown-item disabled" href="<?php echo $renamePageUrl ?>" disabled>Rename Page</a>
															<a class="dropdown-item disabled" href="#" disabled>Set As Homepage</a>
															<a class="dropdown-item text-danger disabled" href="#" disabled>Delete Page</a>
														<?php } ?>
														
													</div>
												</div>
											</ol>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					<!-- <div class="col-lg-4 offset-1">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Users</h3>
								<div class="card-tools">
									
								</div>
							</div>
							<?php //debugger($allUsers); ?>
							<div class="card-body p-0">

							</div>
						</div>
					</div> -->
				</div>
				

			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
		<div class="modal fade" id="new-page" style="display: none;" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">New Page</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/manageWebsite" id="update-status-form" method="POST">

						<div class="modal-body">
							<div class="card-body">
								<div class="form-group" style="max-width: 100%" data-select2-id="55">
									<input type="text" class="form-control" id="update-status-user-id" placeholder="new page name"  name="new-page-name" accept="image/*" required>
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<div class="modal-footer justify-content-between">
							<button type="submit" name="add-new-page" onclick="return confirm('Are you sure you want to add new page?')" value="submit" class="btn btn-primary">Add New Page</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="rename-page" style="display: none;" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Rename Page</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/manageWebsite" id="rename-page-form" method="POST">

						<div class="modal-body">
							<div class="card-body">
								<div class="form-group" style="max-width: 100%" data-select2-id="55">
									<label>Current Name</label>
									<input type="text" class="form-control" id="currentPageName" placeholder="current name" name="current-page-name" readonly>
								</div>
								<div class="form-group" style="max-width: 100%" data-select2-id="55">
									<label>New Name</label>
									<input type="text" class="form-control" placeholder="new name"  name="new-page-name" required>
								</div>

							</div>
							<!-- /.card-body -->
						</div>
						<div class="modal-footer justify-content-between">
							<button type="submit" name="rename-page" onclick="return confirm('Are you sure you want to rename this page?')" value="submit" class="btn btn-primary">Rename Page</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="duplicate-page" style="display: none;" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Duplicate Page</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/manageWebsite" method="POST">

						<div class="modal-body">
							<div class="card-body">
								<div class="form-group" style="max-width: 100%" data-select2-id="55">
									<input type="text" class="form-control" placeholder="duplicate page name"  name="duplicate-page-name" required>
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<div class="modal-footer justify-content-between">
							<button type="submit" name="add-new-page" onclick="return confirm('Are you sure you want to add new page?')" value="submit" class="btn btn-primary">Duplicate Page</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</div>

	<script type="text/javascript">
		$('#rename-page').on('shown.bs.modal', function (e) {
			// $('#currentPageName').
			//console.log(this)
			var button = e.relatedTarget;
			renameBtnId = button.id
			currentPage = renameBtnId.split('-')[1] 
			console.log(renameBtnId.split('-')[1])
			$('#currentPageName').val(currentPage);
		})
	</script>
	<?php 
	$scripts = '
	';
	include '../../inc/footer.php';
	?>
	?>
<?php 
include '../../inc/header.php';
include '../../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/website.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/manageWebsiteFunctions.php';

$theme = new Theme();
$website = new Website();

//$allUsers = $users->getAllUsers();
$allThemes = $theme->getAllThemes();

// $allThemes2 = $allThemes;
// $allThemes = array_merge($allThemes,$allThemes2,$allThemes2);
$pageIsThere = false;

if (isset($_GET) && !empty($_GET)) {
  if (isset($_GET['act']) && !empty($_GET['act'])) {
    //debugger($_GET['act'],true);
    if($_GET['act'] == substr(md5('edit-page-'.$_GET['websiteId'].'-'.$_SESSION['token']), 5, 15)){
      if(isset($_GET['pageName']) && !empty($_GET['pageName'])){
        $websiteData = $website->getAllWebsiteDataById($_GET['websiteId']);
        if(isset($websiteData) && !empty($websiteData)){
          //debugger($websiteData);
          $_SESSION['website_id'] = $websiteData[0]->website_id;

          $webpages = array();

          $websiteDIR = SITE_DIR.$websiteData[0]->website_id.'/';

          $dir = opendir($websiteDIR); 
          $file = readdir($dir);

          while( $file = readdir($dir) ) { 
            if(!is_dir($websiteDIR . '/' . $file)){
              array_push($webpages, $file);

            }
          }
          //debugger($webpages);
          $manageWebsiteHomeURL = '?websiteId='.$websiteData[0]->website_id.'&act='.substr(md5('manage-website-home-'.$websiteData[0]->website_id.'-'.$_SESSION['token']), 5, 15);
          if(is_dir(SITE_DIR.$websiteData[0]->website_id.'/')){
          //debugger('A');
            $dir = opendir(SITE_DIR.$websiteData[0]->website_id.'/');
            $pagesArray = array();
            $dirArray = array();
            while ($file = readdir($dir)) {

              if(!is_dir(SITE_DIR.$websiteData[0]->website_id.'/'.$file)){
                //debugger($file); 
                array_push($pagesArray, $file);
                if($_GET['pageName'].'.php' == $file){
                  $pageIsThere = true ;
                }

              }else{
                array_push($dirArray, $file);

              }

            }
          }
          if(!$pageIsThere){
            redirect("../../manage-website/home/".$manageWebsiteHomeURL,'error','Page not found');
          }else{
            // getting html layout
            //$pageFileContents = file_get_contents(SITE_DIR.$websiteData[0]->website_id.'/'.$_GET['pageName']);
            $currentPageURL = SITE_URL.'manage-website/site/'.$websiteData[0]->website_id.'/'.$_GET['pageName'].'.php';
            //debugger($pageFileContents,true);
            //debugger($xml,true);
            $xml = loadHtmlToXml($currentPageURL);
            $sectionElArray = $xml->body;
            $output = [];

            $contentTypeNameArray = getContentTypeNames();

            //debugger($xml,true);
            //
            if (!isset($sectionElArray) || empty($sectionElArray)) {
              redirect("../../manage-website/home/".$manageWebsiteHomeURL,'error','Invalid html tag');
            }

            //debugger(1,true);
            //$ret[$d->documentElement->nodeName] = dom_to_array($d->documentElement);

            //$ret = json_encode($ret);
            //debugger($ret,true);

          }

        }else{
          redirect('./create-new-site','error','Something went wrong while retreiving website info');
        }
      }else{
        redirect('./404');

      }
      
    }else{
      redirect('./404');
    }
  }else{
    redirect('./404');
  }
}else{
  redirect('./404');
}

?>

<div class="wrapper">
  <?php include '../../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Page <?php echo $_GET['pageName'] ?></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <ul class="nav nav-tabs" id="navTabs">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Page</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Meta</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">SEO</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="row pl-2 pr-2">
              <div class="col-lg-12">
                <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/changeWebpage" method="POST" id="changeWebpageForm" enctype='multipart/form-data'>
                  <div class="swappableDivCollection pt-3">
                    <div class="row d-block swappableDivs" id="page">
                      <div class="card">
                        <div class="row">
                          <?php if(isset($sectionElArray) && !empty($sectionElArray)){ ?>
                            <div class="card-body">
                              <div class="card">
                                <div class="card-body">
                                  <div class="row">
                                   <div class="col-md-12">
                                    <ul id="tree1">
                                      <li>
                                        <i class="indicator fas fa-plus-circle"></i>
                                        <a href="#">HTML</a>
                                        <?php foreach ($sectionElArray as $key => $sectionElArrayValue) {  ?>
                                          <?php foreach ($sectionElArrayValue as $sectionElArrayValueKey => $sectionElArrayValueItems) { 

                                            $isSection = false;
                                            //processNode([],$xml,$output,$contentTypeNameArray);
                                            /*check if class attribute of section exists*/
                                            /*proccessing to get section name*/
                                            if(isset($sectionElArrayValueItems->attributes()['class'][0]) && !empty($sectionElArrayValueItems->attributes()['class'][0])){
                                              $attrValue = (string)$sectionElArrayValueItems->attributes()['class'][0];
                                              $sectionClassArr = explode(' ', $attrValue);
                                                //debugger($sectionClassArr);
                                              //debugger($sectionElArrayValueItems);
                                              if(isset(explode('-',$sectionClassArr[0])[1]) && !empty(explode('-',$sectionClassArr[0])[1])){

                                                $themeNameFromUIFromSectionClassArr = explode('-',$sectionClassArr[0])[1];

                                                if($websiteData[0]->theme_name == $themeNameFromUIFromSectionClassArr){
                                                  $isSection = true;
                                                  $sectionName = explode('-',$sectionClassArr[0])[0];
                                                }
                                              }
                                            }

                                            //debugger($sectionElArrayValueItems);

                                            ?>
                                            <!-- check if section exists -->
                                            <?php if($isSection && isset($sectionName) && !empty($sectionName)){ ?>
                                              <ul>
                                               <li>
                                                <i class="indicator fas fa-plus-circle"></i>
                                                <a href="#"><?php echo $sectionName; ?></a>
                                                <ul>
                                                  <?php foreach ($sectionElArrayValueItems as $sectionElArrayValueItemsKey => $sectionElArrayValueItemsContentTypeEl) {  
                                                    //debugger($sectionElArrayValueItemsKey);
                                                    //debugger($sectionElArrayValueItemsContentTypeEl->attributes()['class'][0]);
                                                    //debugger($sectionElArrayValueItemsContentTypeEl);

                                                    $isContentType = false;
                                                    $isSubContentType = false;
                                                    $isContentTypeOfSubContentType = false;

                                                    $subContentTypeArray = array();

                                                    if(isset($sectionElArrayValueItemsContentTypeEl->attributes()['class'][0]) && !empty($sectionElArrayValueItemsContentTypeEl->attributes()['class'][0])){
                                                      $contentTypeAttrValue = (string)$sectionElArrayValueItemsContentTypeEl->attributes()['class'][0];
                                                      $contentTypeClassArr = explode(' ', $contentTypeAttrValue);
                                                      
                                                      //debugger($contentTypeClassArr);
                                                      /*if there are multiple colums inside row in section*/
                                                      if(in_array('row', $contentTypeClassArr)){
                                                        foreach ($sectionElArrayValueItemsContentTypeEl as $sectionElArrayValueItemsContentTypeElKey => $sectionElArrayValueItemsContentTypeElValue) {

                                                          $subContentTypeClassAttribute = $sectionElArrayValueItemsContentTypeElValue->attributes()['class'][0];
                                                          
                                                          $subContentTypeClassAttributeArr = explode(' ', $subContentTypeClassAttribute);
                                                          
                                                          $isSubContentType = true;
                                                          $subContentTypeName = explode('-',$subContentTypeClassAttributeArr[0])[0];

                                                          /*check content type inside columns and display it*/
                                                          foreach ($sectionElArrayValueItemsContentTypeElValue as $contentTypeAttributesKey => $contentTypeAttributesValue) {
                                                           //debugger($contentTypeAttributesValue->attributes());
                                                            $contentTypeClassAttribute = $contentTypeAttributesValue->attributes()['class'][0];
                                                            $contentTypeClassAttributeArr = explode(' ', $contentTypeClassAttribute);

                                                            $contentTypeNameOfSubContentTypeArr = array();

                                                            /*loop through to check and display content types*/
                                                            foreach ($contentTypeClassAttributeArr as $key => $contentTypeClassAttributeArrValue) {
                                                              $themeNameFromUIFromContentTypeClassArr = explode('-',$contentTypeClassAttributeArrValue);

                                                              if(in_array($websiteData[0]->theme_name, $themeNameFromUIFromContentTypeClassArr)){

                                                                $isContentTypeOfSubContentType = true;
                                                                $contentTypeNameOfSubContentType = $themeNameFromUIFromContentTypeClassArr[0];
                                                                if(in_array('Multiple', $themeNameFromUIFromContentTypeClassArr)){
                                                                  $contentTypeNameOfSubContentType = $contentTypeNameOfSubContentType.'-Multiple';
                                                                }
                                                                /*initializing content types*/
                                                                //debugger($contentTypeNameOfSubContentType);

                                                                $subContentTypeArray[$subContentTypeName]['contentTypeContents'][] = $contentTypeNameOfSubContentType;
                                                                $subContentTypeArray[$subContentTypeName]['contentTypeElValue'][] = $contentTypeAttributesValue;
                                                              }
                                                            }

                                                          }

                                                        }
                                                      }else{
                                                        //debugger('here');

                                                        /*if there are no multiple colums inside rows in section*/
                                                        if(isset(explode('-',$contentTypeClassArr[0])[1]) && !empty(explode('-',$contentTypeClassArr[0])[1])){

                                                          $themeNameFromUIFromContentTypeClassArr = explode('-',$contentTypeClassArr[0])[1];
                                                          if($websiteData[0]->theme_name == $themeNameFromUIFromContentTypeClassArr){
                                                            $isContentType = true;
                                                            $contentTypeName = explode('-',$contentTypeClassArr[0])[0];

                                                          }
                                                        }

                                                      }
                                                      
                                                    }

                                                    $contentCount = 0 ;
                                                    ?>
                                                    
                                                    <!-- to display sub content types of section having columns -->
                                                    <?php if($isSubContentType && isset($subContentTypeArray) && !empty($subContentTypeArray)){  ?>
                                                      <?php 
                                                      //debugger($sectionClassArr);
                                                      //debugger($sectionElArrayValueItems);
                                                      //debugger($subContentTypeArray);
                                                      ?>
                                                      <?php foreach ($subContentTypeArray as $subContentTypeColumnName => $subContentTypeNameArr) { 

                                                         //debugger($subContentTypeNameArr['contentTypeContents']);
                                                        $contentCount = 0 ;

                                                        ?>
                                                        <li>
                                                          <i class="indicator fas fa-plus-circle"></i>
                                                          <a href="#"><?php echo $subContentTypeColumnName ?></a>

                                                          <ul>
                                                            <?php foreach ($subContentTypeNameArr['contentTypeContents'] as $subContentTypeNameArrKey => $subContentTypePropsData) {
                                                              //debugger($subContentTypePropsData);

                                                              //debugger($subContentTypeNameArr['contentTypeElValue']);

                                                              //debugger($subContentTypePropsArray);
                                                              $subContentTypePropsArray = explode('-',$subContentTypePropsData);
                                                              $subContentTypeName = $subContentTypePropsArray[0];
                                                              $subContentTypeNameToDisplay = $subContentTypeName ;

                                                              // if (strpos($subContentTypeName, 'Column') !== false) {
                                                              //   $nameAttributeOfCurrentContentType = $nameAttributeOfCurrentContentType.'_'.end($inputTypeDataArrayOfCurrentContentType);

                                                              // }
                                                              //debugger($subContentTypeNameToDisplay);
                                                              // 
                                                              if (strpos($subContentTypeName, 'Content') !== false) {
                                                                $contentCount ++ ;

                                                                preg_match_all("/[A-Z]+|\d+/", $subContentTypeNameToDisplay, $matches);
                                                                //debugger($matches[0]);

                                                                if(!isset($matches[0][1]) || empty($matches[0][1])){
                                                                  $subContentTypeNameToDisplay = $subContentTypeNameToDisplay.'-'.$contentCount ;

                                                                }else{
                                                                  $subContentTypeNameToDisplay = 'Content-'.$contentCount ;

                                                                }

                                                                
                                                              //debugger($)
                                                                //debugger($subContentTypeNameArr['contentTypeElValue'][$contentCount - 1]);
                                                                processNode([],$subContentTypeNameArr['contentTypeElValue'][$contentCount - 1],$output,$contentTypeNameArray,$websiteData[0]->theme_name);
                                                                
                                                              }
                                                              
                                                              ?>

                                                              <li>
                                                                <i class="indicator fas fa-plus-circle"></i>
                                                                <a href="#"><?php echo $subContentTypeNameToDisplay ?></a>
                                                                <ul>
                                                                  <li>
                                                                    <div class="card">
                                                                      <div class="card-body">
                                                                        <?php 
                                                                        //debugger($contentTypeProperties[$subContentTypeName]);
                                                                        //debugger($contentTypeProperties['Content']);
                                                                        //debugger($subContentTypeNameArr['contentTypeElValue'][$contentCount - 1]);

                                                                        renderFORMS($contentTypeProperties,$subContentTypeName,$webpages,$subContentTypeNameToDisplay);
                                                                        unset($contentTypeProperties);
                                                                        ?>
                                                                      </div>
                                                                    </div>
                                                                  </li>
                                                                </ul>
                                                              </li>
                                                            <?php } ?>
                                                          </ul>
                                                          <?php //unset($contentTypeProperties); ?>
                                                        </li>
                                                      <?php } ?>
                                                    <?php } ?>
                                                    <!-- to display content types of section which doesn't have columns -->
                                                    <?php if($isContentType && isset($contentTypeName) && !empty($contentTypeName)){ ?>
                                                      <?php 
                                                      //debugger($sectionClassArr);
                                                      //debugger($websiteData);

                                                      processNode([],$sectionElArrayValueItems,$output,$contentTypeNameArray,$websiteData[0]->theme_name);
                                                      //debugger($contentTypeProperties[$contentTypeName]);
                                                      $contentTypeNameToDisplay = $contentTypeName ;
                                                      if($contentTypeName == 'Content'){
                                                        $contentCount ++ ;
                                                        $contentTypeNameToDisplay = $contentTypeNameToDisplay.'-'.$contentCount ;
                                                      }
                                                        //debugger($contentTypeNameToDisplay);
                                                      
                                                      ?>

                                                      <li>
                                                        <i class="indicator fas fa-plus-circle"></i>
                                                        <a href="#"><?php echo $contentTypeName ?></a>
                                                        <ul class="textarea-forms">
                                                         <li>
                                                          <div class="row">
                                                            <div class="col-lg-12">
                                                              <div class="card">
                                                                <div class="card-body">

                                                                  <?php 
                                                                        //debugger($contentTypeProperties[$contentTypeName]);

                                                                  renderFORMS($contentTypeProperties,$contentTypeName,$webpages,$contentTypeName);

                                                                  
                                                                  unset($contentTypeProperties);
                                                                  ?>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </li>
                                                      </ul>
                                                      <?php //unset($contentTypeProperties); ?>
                                                    </li>

                                                  <?php } ?>
                                                <?php } ?>
                                              </ul>
                                            </li>
                                          </ul>
                                        <?php } ?>
                                      <?php } ?>
                                    <?php } ?>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  </div>

                </div>
              </div>
              <div class="row d-none swappableDivs" id="meta">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="themeName">Page Name</label>
                          <input type="text" class="form-control" name="site-title" placeholder="Page Name">
                        </div>
                        <div class="form-group">
                          <label for="themeName">Meta Name</label>
                          <input type="text" class="form-control" name="copyright-notice" placeholder="Meta Name">
                        </div>
                        <div class="form-group">
                          <label for="themeName">Meta Description</label>
                          <input type="email" class="form-control" name="email-address" placeholder="Meta Description">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row d-none swappableDivs" id="sEO">
                <div class="card">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="themeName">Preview on google</label>
                      <input type="text" class="form-control" name="site-title" placeholder="Preview on google">
                    </div>
                    <div class="form-group">
                      <label for="themeName">What's the last part(or slug) of the page URL?</label>
                      <input type="text" class="form-control" name="copyright-notice" placeholder="Slug">
                    </div>
                    <div class="form-group">
                      <label for="themeName">What's the page's title on search results and browser tabs</label>
                      <input type="text" class="form-control" name="copyright-notice" placeholder="Page Title">
                    </div>
                    <div class="form-group">
                      <label for="themeName">Meta Description</label>
                      <textarea class="form-control"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" name="current-page-url" value="<?php echo (isset($currentPageURL) && !empty($currentPageURL)) ? $currentPageURL : '' ?>">
            <div class="input-group mb-3">
              <div class="input-group-prepend">

                <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" id="saveAsBtn">Save As : <?php echo ($_GET['pageName'] != 'index') ? 'Current Page' : 'New Page' ?> </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#" id="currentPage">Current Page</a>
                  <a class="dropdown-item" href="#" id="newPage">New Page</a>
                  <div role="separator" class="dropdown-divider"></div>
                </div>
              </div>
              <?php 
              $lastWebpage = explode('.php',end($webpages))[0];
              //debugger($lastWebpage);
              preg_match('/(\d+)\D*$/', $lastWebpage, $m);
              if(isset($m[1]) && !empty($m[1])){
                $lastnum = $m[1];
                //debugger($lastnum);
                $newPageNum = $lastnum + 1 ;
                $newPage = 'index'.$newPageNum ;
              }else{
                $newPage = 'index1' ;
              }
              

              ?>
              <input type="hidden" name="webpages" id="webpageData" value="<?php echo explode('.php',end($webpages))[0]; ?>" disabled>

              <?php if($_GET['pageName'] != 'index'){ ?>
                <input type="text" class="form-control" aria-label="Text input with dropdown button" id="pageName" name="page-name" value="<?php echo $_GET['pageName'].'.php' ?>" readonly>

              <?php }else{ ?>
                <input type="text" class="form-control" aria-label="Text input with dropdown button" id="pageName" name="page-name" value="<?php echo $newPage.'.php' ?>">


              <?php } ?>
            </div>
            <button type="submit" name="change-webpage" value="submit" class="btn btn-primary" id="btn-submit">Save</button>
            <br>
          </form>


        </div>
      </div>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

<script src="<?php echo ASSETS_URL ?>js/functions.js"></script>
<?php   


?>
<script src="<?php echo ASSETS_URL ?>js/editPage.js"></script>
<script type="text/javascript">
  $('#navTabs li a').on( "click", function() {
    navLink = $('.nav-link');
    var swappableDivs = $('.swappableDivs');

    $('#navTabs li a div').addClass('d-none');
    navLink.removeClass('active');
    $('.swappableDivs').addClass('d-none').removeClass('d-block');
    $(this).addClass('active');
    swappableDiv = '#'+camelize($(this).html())
    //console.log(camelize($(this).html()));
    $(swappableDiv).addClass('d-block').removeClass('d-none');
  });

  $('#enablemaintenanceMode').on( "click", function() {
    if($(this).is(':checked')) {
      $('#maintenanceFile').attr('disabled',false)
    }else{
      $('#maintenanceFile').attr('disabled',true)

    }
  });

  if($('#enablemaintenanceMode').is(':checked')) {
    $('#maintenanceFile').attr('disabled',false)
  }else{
    $('#maintenanceFile').attr('disabled',true)

  }
</script>




<?php 
$scripts = '
<script src="'.ASSETS_URL.'js/progressbar.js"></script>
<script src="'.VENDOR_URL.'select2/js/select2.full.min.js"></script>
<script src="'.VENDOR_URL.'simplePagination/js/jquery.simplePagination.js"></script>
<script src="'.ASSETS_URL.'js/light-pagination.js"></script>
<script src="'.ASSETS_URL.'js/treeView.js"></script>';
include "../../inc/footer.php";
?>
<?php
include "inc/header.php";
?>
<section class="S1-Bootstrap NavBar">
    <nav class="Navbar-Bootstrap navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#home">
                <div class="logosection">
                    <div class="imglogosection">
                        <img src="https://cdn.dribbble.com/users/230290/screenshots/15128882/media/4175d17c66f179fea9b969bbf946820f.jpg?compress=1 &amp;resize=400x300"
                            alt="" class="Image-it_file_S1*Navbar*Logo_Image-CanHide-Bootstrap" />
                    </div>
                    <div class="logonamesection">
                        <h3 class="NavbarTitle-it_text_S1*Navbar*Logo_Heading-CanHide-Bootstrap"> Logo</h3>
                    </div>
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"> </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="single-encapsulation navbar-nav me-auto mb-2 mb-lg-0 ">
                    <li class="nav-item NavbarItem-it_url_S1*Navbar*Navbar_Heading_1-Multiple-Bootstrap">
                        <a class="nav-link active" href="#home">Home</a>
                    </li>
                    <li class="nav-item NavbarItem-it_url_S1*Navbar*Navbar_Heading_2-Multiple-Bootstrap">
                        <a class="nav-link" href="#about">About</a>
                    </li>
                    <li class="nav-item NavbarItem-it_url_S1*Navbar*Navbar_Heading_3-Multiple-Bootstrap">
                        <a class="nav-link" href="#gallery">Gallery</a>
                    </li>
                    <li class="nav-item NavbarItem-it_url_S1*Navbar*Navbar_Heading_4-Multiple-Bootstrap">
                        <a class="nav-link" href="#services">Services</a>
                    </li>
                    <li class="nav-item NavbarItem-it_url_S1*Navbar*Navbar_Heading_5-Multiple-Bootstrap">
                        <a class="nav-link" href="#testimonials">Testimonials</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
<section class="S2-Bootstrap SocialBar">
    <div class="Socialbar-Bootstrap single-encapsulation icon-bar">
        <a href="#" class="facebook SocialbarItem-it_url_S2*Socialbar*SocialbarItem_1-Bootstrap"><i
                class="fa fa-facebook"> </i> </a>
        <a href="#" class="twitter SocialbarItem-it_url_S2*Socialbar*SocialbarItem_2-Bootstrap"><i
                class="fa fa-twitter"> </i> </a>
        <a href="#" class="google SocialbarItem-it_url_S2*Socialbar*SocialbarItem_3-Bootstrap"><i class="fa fa-google">
            </i> </a>
        <a href="#" class="linkedin SocialbarItem-it_url_S2*Socialbar*SocialbarItem_4-Bootstrap"><i
                class="fa fa-linkedin"> </i> </a>
        <a href="#" class="youtube SocialbarItem-it_url_S2*Socialbar*SocialbarItem_5-Bootstrap"><i
                class="fa fa-youtube"> </i> </a>
    </div>
</section>
<section class="S3-Bootstrap ImageSlider" id="home">
    <div id="carouselExampleIndicators" class="Carousel-Bootstrap carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators ImageSlider-Ref-Bootstrap">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner ImageSlider-Bootstrap">
            <div class="carousel-item active">
                <img src="https://images.unsplash.com/photo-1587061949409-02df41d5e562?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1170&amp;q=80"
                    class="Image-it_file_S3*Carousel*ImageSlider_1-Bootstrap d-block w-100" alt="..." />
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/photo-1623849716515-ef59cc26d817?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1074&amp;q=80"
                    class="Image-it_file_S3*Carousel*ImageSlider_2-Bootstrap d-block w-100" alt="..." />
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/flagged/photo-1550929708-6f5da120f9ba?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1170&amp;q=80"
                    class="Image-it_file_S3*Carousel*ImageSlider_3-Bootstrap d-block w-100" alt="..." />
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</section>
<section class="S4-Bootstrap About" id="about">
    <div class="row text-center">
        <div class="ColumnA-Bootstrap col-lg-12">
            <div class="Content-c1-Bootstrap">
                <h1 class="Heading-it_text_S4*ColumnA*c1*Heading_1-Bootstrap sectionheading">About Us</h1>
            </div>
        </div>
    </div>
    <div class="row AboutRow">
        <div class="ColumnB-Bootstrap col-lg-8 c1">
            <div class="Content-c1-Bootstrap">
                <h1 class="Heading-it_text_S4*ColumnB*c1*Heading_1-Bootstrap mb-4">Lorem ipsum dolor sit amet
                    consectetur.</h1>
                <div class="ParagraphGroup-it_textarea_S4*ColumnB*c1*About_Paragraph-Bootstrap single-encapsulation">
                    <p>Lorem ipsum dolor sit, amet consectetur
                        adipisicing elit. Distinctio
                        voluptatum exercitationem quis, sint
                        aut laudantium dolore quos nihil corrupti repellat porro nemo, voluptas vel similique.
                        Voluptatibus
                        voluptates consequatur voluptate quidem impedit a, perferendis eum debitis ipsum nulla sint
                        consequuntur
                        possimus doloremque, labore ad tempore aliquid qui mollitia corrupti! Omnis quaerat quod, natus
                        doloribus alias quas ex cum cumque tempore id fugiat similique vero adipisci, exercitationem
                        nihil
                        quis
                        expedita. Ullam id corporis quas at praesentium eaque delectus modi et illo ex suscipit nobis
                        porro
                        beatae velit tenetur quidem perferendis enim, eos autem! Delectus, saepe perferendis repudiandae
                        corporis distinctio nemo veniam recusandae voluptate vel iure repellendus facere molestiae,
                        maiores
                        eum
                        velit. </p>
                    <p>
                        Saepe, culpa aperiam at pariatur ipsam esse corrupti unde natus dolore voluptas nihil
                        similique,
                        temporibus illo quod voluptates nostrum laborum recusandae et distinctio? Veritatis consequatur
                        odio
                        magnam possimus autem, harum tenetur accusamus commodi ipsam corporis placeat, blanditiis magni
                        obcaecati eligendi eos molestiae? Libero temporibus possimus quidem ex laborum architecto
                        voluptatem
                        itaque earum quasi quibusdam nemo sapiente quod molestiae porro cupiditate et, odit mollitia
                        nesciunt
                        consequatur ut? Voluptatum autem magni ipsa blanditiis tenetur beatae, earum maxime eveniet
                        aliquid
                        illo
                        iure repellat fugit laborum quae. Excepturi in qui recusandae aspernatur odio iusto soluta eos
                        magni
                        omnis temporibus repellat aliquid mollitia architecto corrupti minima iure numquam ad, delectus
                        saepe
                        quis! Autem ipsa quasi ad, molestias, incidunt perspiciatis pariatur quis accusamus unde ducimus
                        temporibus iste. Architecto tempora odit culpa mollitia voluptate laborum quibusdam alias
                        cupiditate
                        laboriosam ducimus.
                    </p>
                </div>
            </div>
        </div>
        <div class="ColumnC-Bootstrap col-lg-4 c2">
            <div class="Content-c1-it_textarea_S4*Paragraph-Multiple-Bootstrap">
                <h1 class="Heading-it_text_S4*ColumnC*c1*Heading_2-Bootstrap mb-4">Lorem, ipsum dolor.</h1>
                <div class="single-encapsulation">
                    <p class="Paragraph-it_textarea_S4*ColumnC*c1*Paragraph_1-Multiple-Bootstrap">Lorem ipsum dolor sit
                        amet
                        consectetur.</p>
                    <p class="Paragraph-it_textarea_S4*ColumnC*c1*Paragraph_2-Multiple-Bootstrap">Lorem ipsum dolor sit
                        amet
                        consectetur.</p>
                    <p class="Paragraph-it_textarea_S4*ColumnC*c1*Paragraph_3-Multiple-Bootstrap">Lorem ipsum dolor sit
                        amet
                        consectetur.</p>
                    <p class="Paragraph-it_textarea_S4*ColumnC*c1*Paragraph_4-Multiple-Bootstrap">Lorem ipsum dolor sit
                        amet
                        consectetur.</p>
                    <p class="Paragraph-it_textarea_S4*ColumnC*c1*Paragraph_5-Multiple-Bootstrap">Lorem ipsum dolor sit
                        amet
                        consectetur.</p>
                </div>
            </div>
        </div>
        <div class="ColumnD-Bootstrap col-lg-12 c2">
            <div class="Content-c1-Bootstrap text-center">
                <div class="Video-Bootstrap AboutVideo">
                    <iframe class="VideoUrl-it_url_S4*ColumnD*c1*Sample_Video_1-Bootstrap" width="500" height="350"
                        src="https://www.youtube.com/embed/tgbNymZ7vqY">
                    </iframe>
                </div>
            </div>
        </div>
    </div>

</section>
<section class="S5-Bootstrap Gallery" id="gallery">
    <div class="Heading-Bootstrap Sectionheading">
        <h1 class="TitleHeading-it_text_S5*Gallery*Heading_1-Bootstrap sectionheading">Gallery</h1>
    </div>
    <div class="Gallery-Bootstrap multiple-encapsulation gallery ">
        <div class="gallery_item">
            <img src="https://images.unsplash.com/photo-1598954384340-e500b48d88ed?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDl8fHxlbnwwfHx8fA%3D%3D&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                class="Image-it_file_S5*Gallery*Gallery_Image_1-Multiple-Bootstrap card-img-top" alt="..." />
            <!-- <div class="card-body">
            <p class="paragraph-Bootsrap card-text"></p>
        </div> -->
        </div>
        <div class="gallery_item">
            <img src="https://images.unsplash.com/photo-1562306956-798bfd2e3c16?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=735&amp;q=80"
                class="Image-it_file_S5*Gallery*Gallery_Image_2-Multiple-Bootstrap card-img-top" alt="..." />
            <!-- <div class="card-body">
            <p class="paragraph-Bootsrap card-text"></p>
        </div> -->
        </div>
        <div class="gallery_item">
            <img src="https://images.unsplash.com/photo-1598896824491-5c1def8525f8?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDV8fHxlbnwwfHx8fA%3D%3D&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                class="Image-it_file_S5*Gallery*Gallery_Image_3-Multiple-Bootstrap card-img-top" alt="..." />
            <!-- <div class="card-body">
            <p class="paragraph-Bootsrap card-text"></p>
        </div> -->
        </div>
        <div class="gallery_item">
            <img src="https://images.unsplash.com/photo-1636878540162-c815c775ad5d?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyfHx8ZW58MHx8fHw%3D&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                class="Image-it_file_S5*Gallery*Gallery_Image_4-Multiple-Bootstrap card-img-top" alt="..." />
            <!-- <div class="card-body">
            <p class="paragraph-Bootsrap card-text"></p>
        </div> -->
        </div>
        <div class="gallery_item">
            <img src="https://images.unsplash.com/photo-1623672655463-f25b22d30338?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDEwfHx8ZW58MHx8fHw%3D&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                class="Image-it_file_S5*Gallery*Gallery_Image_5-Multiple-Bootstrap card-img-top" alt="..." />
            <!-- <div class="card-body">
            <p class="paragraph-Bootsrap card-text"></p>
        </div> -->
        </div>
        <div class="gallery_item">
            <img src="https://images.unsplash.com/photo-1623672655530-2fd989eb9860?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE2fHx8ZW58MHx8fHw%3D&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                class="Image-it_file_S5*Gallery*Gallery_Image_6-Multiple-Bootstrap card-img-top" alt="..." />
            <!-- <div class="card-body">
            <p class="paragraph-Bootsrap card-text"></p>
        </div> -->
        </div>
    </div>
</section>
<section class="S6-Bootstrap Card" id="services">
    <div class="Heading-Bootstrap sectionheading">
        <h1 class="TitleHeading-it_text_S6*Services*Heading_1-Bootstrap sectionheading">Services</h1>
    </div>

    <div class="Carousel-Bootstrap owl-carousel owl-theme">
        <div class="item">
            <div class="card">
                <div class="card-img">
                    <img src="https://images.unsplash.com/photo-1633114073758-c4be9aeb15ac?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1170&amp;q=80"
                        class="Image-it_file_S6*Services*Carousel_Image_1-Bootstrap card-img-top" alt="..." />
                </div>
                <div class="card-body">
                    <h5 class="Heading-it_text_S6*Services*Carousel_Heading_1-Bootstrap card-title">Vitae perspiciatis
                    </h5>
                    <div class="single-encapsulation">
                        <p class="Paragraph-it_textarea_S6*Services*Carousel_Paragraph_1-Bootstrap card-text">Lorem
                            ipsum dolor
                            sit
                            amet consectetur adipisicing elit.
                            Optio quos rem, autem cum saepe sed impedit voluptatem in, ad minus doloribus veritatis
                            corporis
                            reprehenderit autem cum saepe sed impedit voluptatem in, ad minus doloribus veritatis
                            corporis
                            reprehenderit..</p>
                    </div>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <div class="card-img">
                    <img src="https://images.unsplash.com/photo-1636714560735-9f84a7c06d16?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1074&amp;q=80"
                        class="Image-it_file_S6*Services*Carousel_Image_2-Bootstrap card-img-top" alt="..." />
                </div>
                <div class="card-body">
                    <h5 class="Heading-it_text_S6*Services*Carousel_Heading_2-Bootstrap card-title">Ullam natus</h5>
                    <div class="single-encapsulation">
                        <p class="Paragraph-it_textarea_S6*Services*Carousel_Paragraph_2-Bootstrap card-text">Optio quos
                            rem,
                            autem
                            cum saepe sed impedit voluptatem in,
                            ad minus doloribus veritatis corporis reprehenderit, labore voluptas enim vitae perspiciatis
                            recusandae explicabo ullam natus suscipit nisi autem cum saepe sed impedit voluptatem in, ad
                            minus doloribus veritatis corporis reprehenderit.</p>
                    </div>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card">
                <div class="card-img">
                    <img src="https://images.unsplash.com/photo-1589642314445-999ac13b0075?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1173&amp;q=80"
                        class="Image-it_file_S6*Services*Carousel_Image_3-Bootstrap card-img-top" alt="..." />
                </div>

                <div class="card-body">
                    <h5 class="Heading-it_text_S6*Services*Carousel_Heading_3-Bootstrap card-title">Nemo veniam</h5>
                    <div class="single-encapsulation">
                        <p class="Paragraph-it_textarea_S6*Services*Carousel_Paragraph_3-Bootstrap card-text">Ratione,
                            quam fugit
                            doloribus deleniti, nemo veniam eius
                            error sint ipsam, tenetur autemautem cum saepe sed impedit voluptatem in, ad minus doloribus
                            veritatis corporis reprehenderit.</p>
                    </div>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="S7-Bootstrap Testimonials" id="testimonials">
    <div class="row text-center">
        <div class="ColumnA-Bootstrap col-lg-12">
            <div class="Content-c1-Bootstrap">
                <h1 class="TitleHeading-it_text_S7*ColumnA*c1*Heading_1-Bootstrap sectionheading">Testimonials</h1>
            </div>
        </div>
    </div>
    <div class="row testimonial-card mb-3">
        <div class="ColumnB-Bootstrap">
            <div class="Content-c1-Bootstrap row testimonial-card ">
                <div class="testimonial-img">
                    <img src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGVyc29ufGVufDB8fDB8fA%3D%3D&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                        class="Image-it_file_S7*ColumnB*c1*Testimonial_Image_1-Bootstrap " alt="..." />
                </div>
                <div
                    class="card-body Paragraph-it_textarea_S7*ColumnB*c1*Testimonial_Paragraph-Multiple-Bootstrap testimonial-card-body">
                    <h5 class="Heading-it_text_S7*ColumnB*c1*Testimonial_Heading_1-Bootstrap  mt-4">Matina Dongol</h5>
                    <div class="single-encapsulation">
                        <p class="Paragraph-it_textarea_S7*ColumnB*c1*Testimonial_Paragraph_1-Multiple-Bootstrap">Lorem
                            ipsum
                            dolor sit amet consectetur adipisicing
                            elit.
                            Maxime nisi animi culpa, deserunt sunt facilis quis fugit officiis dolorum laboriosam?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row testimonial-card mb-3">
        <div class="ColumnC-Bootstrap">
            <div class="Content-c1-Bootstrap row testimonial-card">
                <div class="testimonial-img">
                    <img src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDZ8fHxlbnwwfHx8fA%3D%3D&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60"
                        class="Image-it_file_S7*ColumnC*c1*Testimonial_Image-1-Bootstrap " alt="..." />
                </div>
                <div
                    class="card-bodyParagraph-it_textarea_S7*ColumnC*c1*Testimonial_Paragraph-Multiple-Bootstrap testimonial-card-body">
                    <h5 class="Heading-it_text_S7*ColumnC*c1*Testimonial_Heading_1-Bootstrap  mt-4">Luniva Maharjan</h5>
                    <div class="single-encapsulation">
                        <p class="Paragraph-it_textarea_S7*ColumnC*c1*Testimonial_Paragraph_1-Multiple-Bootstrap">Lorem
                            ipsum
                            dolor sit amet consectetur adipisicing
                            elit.
                            Maxime nisi animi culpa, deserunt sunt facilis quis fugit officiis dolorum laboriosam?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<section class="S8-Bootstrap ContactForm">
    <div class="sectionheading">
        <h1 class="Heading-it_text_S8*Contact*Heading_1-Bootstrap sectionheading">Contact Us</h1>
    </div>
    <div class="ContactForm-Bootstrap contactus">
        <form class="mb-4">
            <div class="mb-3">
                <label for="name" class="form-label LabelName-it_text_S8*Contact*Heading_1-Bootstrap">Name</label>
                <input type="text" required="true" class="form-control" id="name" aria-describedby="emailHelp" />
            </div>
            <div class="mb-3">
                <label for="contact_number" class="form-label LabelName-it_text_S8*Contact*Heading_2-Bootstrap">Contact
                    Number</label>
                <input type="text" required="true" class="form-control" id="contact_number"
                    aria-describedby="emailHelp" />
            </div>
            <div class="mb-3">
                <label for="email" class="form-label LabelName-it_text_S8*Contact*Heading_3-Bootstrap">Email
                    address</label>
                <input type="email" required="true" class="form-control" id="email" aria-describedby="emailHelp" />
                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            </div>
            <div class="mb-3">
                <label for="message" class="form-label LabelName-it_text_S8*Contact*Heading_4-Bootstrap">Message</label>
                <textarea class="form-control" required="true" id="message" rows="3">

                </textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</section>
<section class="S9-Bootstrap Footer">
    <div class="Footer-Bootstrap footer">
        <h3 class="FooterHeading-it_text_S9*Footer*Heading_1-Bootstrap">copyright @ 2021</h3>
    </div>
</section>
<?php
include "inc/footer.php";
?>
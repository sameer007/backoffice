<?php
include "inc/header.php";
?>
<section class="S1-Demosapp site-header" id="header">
    <nav class="Navbar-Demosapp navbar navbar-expand-lg transparent-bg- static-nav">
        <!--side menu open button-->
        <a href="#" class="d-inline-block sidemenu_btn " id="sidemenu_toggle">
            <!-- <i class="icon-svg-bars svg-icon txt-grey"></i> -->
            <img src="./assets/image/hamburger.svg" class="icon-svg-bars ham-icon" />
        </a>
        <div class="container-fluid">
            <a href="./" class="navbar-brand navbar-brand-name">
                <img src=".\assets\image\Logo\Demosapp.png"
                    class="Image-it_file_S1*Navbar*Logo_Image-CanHide-Demosapp logo-default" />
                <!--to hide logo add class "logo-hide" -->
                <!-- <h3 class="NavbarTitle-it_text_S1*Navbar*Logo_Heading-CanHide-Demosapp navbar_logoHeading"> Demosapp
                </h3> -->
            </a>
            <div class="collapse navbar-collapse" id="navbarToggleExternalContent">
                <ul class="NavbarItem-Demosapp single-encapsulation navbar-nav ml-auto">
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_1-Multiple-Demosapp nav-item svg-icon svg-bg'>
                        <a href='./' class='nav-link'>Home</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_2-Multiple-Demosapp nav-item svg-icon svg-bg'>
                        <a href='#AboutDemosapp' class='nav-link'>About</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_3-Multiple-Demosapp nav-item svg-icon svg-bg'>
                        <a href='#ServicesDemosapp' class='nav-link'>Services</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_4-Multiple-Demosapp nav-item svg-icon svg-bg'>
                        <a href='#ProjectsDemosapp' class='nav-link'>Project</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_5-Multiple-Demosapp nav-item svg-icon svg-bg'>
                        <a href='#BlogDemosapp' class='nav-link'>Blog</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_6-Multiple-Demosapp nav-item svg-icon svg-bg'>
                        <a href='#ContactDemosapp' class='nav-link'>Contact</a>
                    </li>
                </ul>
            </div>
        </div>
        <!--side menu open button-->
    </nav>
    <div class="side-menu opacity-0 w-99 ">
        <div class="overlay"></div>
        <div class="inner-wrapper">
            <span class="btn-close" id="btn_sideNavClose">
                <img src="assets\image\Icon\Side Panel\close.svg" width="34px" />
                <!-- <i class="fa fa-times h1 font-weight-bold"></i> -->
            </span>
            <nav class="side-nav w-100 ">
                <ul class="NavbarItem-Cloned-Demosapp single-encapsulation navbar-nav ul-disc">
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_1-Multiple-Cloned-Demosapp nav-item li-disc'>
                        <a href='./' class='nav-link'> Home</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_2-Multiple-Cloned-Demosapp nav-item li-disc'>
                        <a href='#AboutDemosapp' class='nav-link'> About</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_3-Multiple-Cloned-Demosapp nav-item li-disc'>
                        <a href='#ServicesDemosapp' class='nav-link'> Services</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_4-Multiple-Cloned-Demosapp nav-item li-disc'>
                        <a href='#ProjectsDemosapp' class='nav-link'> Project</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_5-Multiple-Cloned-Demosapp nav-item li-disc'>
                        <a href='#BlogDemosapp' class='nav-link'> Blog</a>
                    </li>
                    <li class='NavbarItem-it_url_S1*Navbar*Navbar_Heading_6-Multiple-Cloned-Demosapp nav-item li-disc'>
                        <a href='#ContactDemosapp' class='nav-link'> Contact</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>
<section class="S2-Demosapp started">
    <div class="row mx-2">
        <div class="ColumnA-Demosapp col-lg-6 order-lg-12 order-2 pr-0 pl-0">
            <div class="Content-c1-Demosapp inner-div px-1">
                <div class="details pl-3">
                    <h1 class="Heading-it_text_S2*ColumnA*c1*Home_Heading-Demosapp text-uppercase">
                        DEDICATED<br />DEVELOPMENT
                        <span>TEAM</span>
                    </h1>
                    <div class="ParagraphGroup-it_textarea_S2*ColumnA*c1*Home_Paragraph-Demosapp single-encapsulation">
                        <p class="mt-4">Hire top tech talent and quickly scale
                            your
                            delivery capacity. Our engineers have the necessary niche skills, deep expertise, and are
                            highly
                            enthusiastic about their assigned work.</p>
                    </div>
                    <button class="btn">GET STARTED</button>
                </div>
            </div>
        </div>
        <div class="ColumnB-Demosapp col-lg-6 order-lg-12 order-1 pr-0 pl-0">
            <div class="Content-c1-Demosapp started_img">
                <img src="assets\image\home1.png" alt="Dedicated_development_team"
                    class="Image-it_file_S2*ColumnB*c1*Started_Image_1-Demosapp" />
                <!-- <img src="assets\image\mobile\home1.png" alt="Dedicated_development_team"
                    class="Image-it_file_S2*ColumnB*c1*Started_Image_1-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
    </div>
</section>
<section class="S3-Demosapp offer" id="AboutDemosapp">
    <div class="row offerContent">
        <div class="ColumnA-Demosapp col-lg-4 pr-0 inner-div offerContentA">
            <div class="Content-c1-Demosapp ">
                <div class="details">
                    <h2 class="Heading-it_text_S3*ColumnA*c1*About_Heading_1-Demosapp text-uppercase title">WHAT WE
                        <br /><span>OFFER</span>
                    </h2>
                    <div class="ParagraphGroup-it_textarea_S3*ColumnA*c1*About_Paragraph-Demosapp single-encapsulation">
                        <p class="mt-4 title-desc">Our
                            offers
                            have a wide range of services available and ready for our
                            client. No matter if it is a small to big scale project, we are more than ready and
                            excited
                            to
                            work with our client for any kinds of projects.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ColumnB-Demosapp col-lg-8 pr-0 pl-0 offertype-bg">
            <div class="Content-c1-Demosapp offer-types offer-type-inner-div row mt-5 mr-5">
                <div class="col-lg-6 col-md-6">
                    <div class="offer-type-details">
                        <img src=".\assets\image\Icon\Homepage\Development.svg" alt="Web_Development" />
                        <h5 class="Heading-it_text_S3*ColumnB*c1*About_Sub_Heading_1-Demosapp font-weight-bold">
                            Development</h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c1*About_Sub_Paragraph_1-Demosapp single-encapsulation">
                            <p>We had a
                                range of development services such as Mobile Applications, Websites,
                                Domain &amp; Hosting and so on. Most applications are compatible with all
                                operating
                                systems depending on project requirements.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="offer-type-details">
                        <img src=".\assets\image\Icon\Homepage\Graphic Design.svg" alt="Graphic_Design" />
                        <h5 class="Heading-it_text_S3*ColumnB*c1*About_Sub_Heading_2-Demosapp font-weight-bold">
                            Graphic Design</h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c1*About_Sub_Paragraph_2-Demosapp single-encapsulation">
                            <p>From logo
                                designs, re-branding strategies and marketing materials, we offer a
                                wide range of branding and graphic design services to improve user’s overall
                                visual experiences.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="Content-c2-Demosapp offer-types offer-type-inner-div row mb-5 mr-5">
                <div class="col-lg-6 col-md-6">
                    <div class="offer-type-details">
                        <img src=".\assets\image\Icon\Homepage\Digital Marketing.svg" alt="Digital_Marketing" />
                        <h5 class="Heading-it_text_S3*ColumnB*c2*About_Sub_Heading_3-Demosapp font-weight-bold">
                            Digital Marketing</h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c2*About_Sub_Paragraph_3-Demosapp single-encapsulation">
                            <p>We make
                                use of our specialists skills to help businesses to increase sales as
                                well as building leads, and expand market share.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="offer-type-details">
                        <img src=".\assets\image\Icon\Homepage\Business Venture.svg" alt="Business Venture" />
                        <h5 class="Heading-it_text_S3*ColumnB*c2*About_Sub_Heading_4-Demosapp font-weight-bold">
                            Business Venture</h5>
                        <div
                            class="ParagraphGroup-it_textarea_S3*ColumnB*c2*About_Sub_Paragraph_4-Demosapp single-encapsulation">
                            <p>We help to
                                build up businesses starting from the beginning until the end. From
                                the financial standpoint or building up the fame, we help to discover it all
                                together with our client.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="S4-Demosapp benefits">
    <div class="row w-99">
        <div class="ColumnA-Demosapp">
            <div class="Content-c1-Demosapp">
                <h2
                    class="Heading-it_text_S4*ColumnA*c1*Benefit_TitleHeading_1-Demosapp text-uppercase text-center title">
                    WE EMPOWER
                    <span>CREATIVITY</span> WITH <span>CRAFTSMANSHIP</span>
                </h2>
                <div
                    class="ParagraphGroup-it_textarea_S4*ColumnA*c1*Benefit_TitleParagraph_1-Demosapp single-encapsulation">
                    <p class="text-center title-desc mb-2">We have created many
                        opportunities
                        with
                        digital solutions that appeal to people
                        with encouragement on action and fuel of growth. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row w-99 outer-div position-relative bgimage">
        <div class="ColumnB-Demosapp col-lg-7 order-lg-12 order-2 pr-0">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details">
                    <h3 class="Heading-it_text_S4*ColumnB*c1*Benefit_Heading_1-Demosapp font-weight-bold">Solutions with
                        a
                        positive
                        impact on every business.</h3>
                    <div
                        class="ParagraphGroup-it_textarea_S4*ColumnB*c1*Benefit_Paragraph_1-Demosapp single-encapsulation">
                        <p class="mt-4">
                            We employ over 10 specialists in the field of:</p>
                    </div>
                    <ul class="single-encapsulation">
                        <li class="Paragraph-it_text_S4*ColumnB*c1*Benefit_SubParagraph_1-Multiple-Demosapp">Brand
                            Strategy</li>
                        <li class="Paragraph-it_text_S4*ColumnB*c1*Benefit_SubParagraph_2-Multiple-Demosapp">Digital
                            Strategy</li>
                        <li class="Paragraph-it_text_S4*ColumnB*c1*Benefit_SubParagraph_3-Multiple-Demosapp">UI /UX
                        </li>
                        <li class="Paragraph-it_text_S4*ColumnB*c1*Benefit_SubParagraph_4-Multiple-Demosapp">Design
                        </li>
                        <li class="Paragraph-it_text_S4*ColumnB*c1*Benefit_SubParagraph_5-Multiple-Demosapp">Creation
                        </li>
                        <li class="Paragraph-it_text_S4*ColumnB*c1*Benefit_SubParagraph_6-Multiple-Demosapp">
                            Development</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="ColumnC-Demosapp col-lg-5 order-lg-12 order-1 pr-0 pl-0">
            <div class="Content-c1-Demosapp">
                <img src="assets\image\about1.png" alt="Solution_with_a_positive_impact_on_every_business" width="100%"
                    class="Image-it_file_S4*ColumnC*c1*About_Image_1-Demosapp" />
                <!-- <img src="assets\image\mobile\about1.png" alt="Dedicated_development_team"
                    class="Image-it_file_S4*ColumnC*c1*About_Image_1-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
    </div>

    <div class="row w-99 outer-div position-relative bgimage benefit_third">
        <div class="ColumnD-Demosapp col-lg-5 order-lg-12 order-1 px-0">
            <div class="Content-c1-Demosapp">
                <img src="assets\image\about2.png" alt="We_love_ideas_and_innovative" width="100%"
                    class="Image-it_file_S4*ColumnD*c1*About_Image_2-Demosapp" />
                <!-- <img src="assets\image\mobile\about2.png" alt="Dedicated_development_team"
                    class="Image-it_file_S4*ColumnD*c1*About_Image_2-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
        <div class="ColumnE-Demosapp col-lg-7 order-lg-12 order-2 px-0">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details position-right px-2">
                    <h3 class="Heading-it_text_S4*ColumnE*c1*Benefit_Heading_2-Demosapp font-weight-bold ">We love ideas
                        and
                        innovative</h3>
                    <div
                        class="ParagraphGroup-it_textarea_S4*ColumnE*c1*Benefit_Paragraph_2-Demosapp single-encapsulation">
                        <p class="mt-4">Our crafts
                            are from
                            solid
                            data gathering and input of requirements given by
                            our clients. We will make use of the internet and our networks to build the dream
                            into
                            reality. </p>
                    </div>
                    <div class="py-2">
                        <button class="btn">CONTACT US</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="S5-Demosapp benefits" id="ServicesDemosapp">
    <div class="row w-99 text-center">
        <div class="ColumnA-Demosapp col-lg-12">
            <div class="Content-c1-Demosapp">
                <h2 class="Heading-it_text_S5*ColumnA*c1*Services_TitleHeading_1-Demosapp text-uppercase title">WHY OUR
                    <span>SERVICE</span>
                    ?
                </h2>
            </div>
        </div>
    </div>
    <div class="row outer-div w-99 position-relative overflow-hidden bgimage">
        <div class="ColumnB-Demosapp col-lg-7 order-lg-12 order-2 px-0">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details">
                    <h2 class="Heading-it_text_S5*ColumnB*c1*Services_Heading_1-Demosapp font-weight-bold">We are a team
                        of
                        professional</h2>
                    <div
                        class="ParagraphGroup-it_textarea_S5*ColumnB*c1*Services_Paragraph_1-Demosapp single-encapsulation">
                        <p class="mt-4">We value each of everyone
                            in
                            our
                            team members, with beliefs solely in
                            reliable approach to our job and interesting passions and life goals. We are a
                            professional team in which everyone specializes in only delivering the perfect
                            results.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ColumnC-Demosapp col-lg-5 order-lg-12 order-1 px-0">
            <div class="Content-c1-Demosapp">
                <img src="assets\image\service1.png" alt="We_are_a_team_of_professional" width="100%"
                    class="Image-it_file_S5*ColumnC*c1*Services_Image_1-Demosapp" />
                <!-- <img src="assets\image\mobile\service1.png" alt="Dedicated_development_team"
                    class="Image-it_file_S5*ColumnC*c1*Services_Image_1-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
    </div>

    <div class="row outer-div w-99 mt-5 position-relative overflow-hidden bgimage">
        <div class="ColumnD-Demosapp col-lg-5 order-lg-12 order-1 px-0">
            <div class="Content-c1-Demosapp">
                <img src="assets\image\service2.png" alt="We_provide_one_stop_solution" width="100%"
                    class="Image-it_file_S5*ColumnD*c1*Services_Image_2-Demosapp" />
                <!-- <img src="assets\image\mobile\service2.png" alt="Dedicated_development_team"
                    class="Image-it_file_S5*ColumnD*c1*Services_Image_2-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
        <div class="ColumnE-Demosapp col-lg-7 order-lg-12 order-2 pr-0 pl-0">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details position-right">
                    <h2 class="Heading-it_text_S5*ColumnE*c1*Services_Heading_2-Demosapp font-weight-bold">We provide
                        one stop
                        solution</h2>
                    <div class="Paragraph-it_textarea_S5*ColumnE*c1*Services_Paragraph_2-Demosapp single-encapsulation">
                        <p class="mt-4">No matter what industry you
                            are
                            in, whether you are in the agriculture field
                            or even in the kitchen as a chef, as long as you are in need of IT related service,
                            we
                            are more than ready to serve and deliver. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row outer-div w-99 position-relative overflow-hidden bgimage">
        <div class="ColumnF-Demosapp col-lg-7 order-lg-12 order-2 px-0">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details">
                    <h2 class="Heading-it_text_S5*ColumnF*c1*Services_Heading_3-Demosapp font-weight-bold">We simplify
                        complex
                        structure into understandable terms</h2>
                    <div
                        class="ParagraphGroup-it_textarea_S5*ColumnF*c1*Services_Paragraph_3-Demosapp single-encapsulation">
                        <p class="mt-4">IT may be hard to
                            understand,
                            complicated to work with. But with over 10
                            years in providing IT solutions we are confident that you will enjoy using our
                            service
                            with our process being simple and easy to understand. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ColumnG-Demosapp col-lg-5 order-lg-12 order-1 pr-0 pl-0">
            <div class="Content-c1-Demosapp">
                <img src="assets\image\service3.png" alt="We_simplify_complex_structure_into _understandable_terms"
                    width="100%" class="Image-it_file_S5*ColumnG*c1*Services_Image_3-Demosapp" />
                <!-- <img src="assets\image\mobile\service3.png" alt="Dedicated_development_team"
                    class="Image-it_file_S5*ColumnG*c1*Services_Image_3-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
    </div>

    <div class="row outer-div w-99 mt-5 position-relative overflow-hidden bgimage">
        <div class="ColumnH-Demosapp col-lg-5 order-lg-12 order-1 pr-0">
            <div class="Content-c1-Demosapp">
                <img src="assets\image\service4.png" alt="We_believe_in_efficiency_&amp;_effectiveness_communication"
                    width="100%" class="Image-it_file_S5*ColumnH*c1*Services_Image_4-Demosapp" />
                <!-- <img src="assets\image\mobile\service4.png" alt="Dedicated_development_team"
                    class="Image-it_file_S5*ColumnH*c1*Services_Image_4-Cloned-Demosapp d-block d-md-none" /> -->
            </div>
        </div>
        <div class="ColumnI-Demosapp col-lg-7 order-lg-12 order-2 pr-0 pl-0">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details position-right">
                    <h2 class="Heading-it_text_S5*ColumnI*c1*Services_Heading_4-Demosapp font-weight-bold">We believe in
                        efficiency
                        and effectiveness communication</h2>
                    <div
                        class="ParagraphGroup-it_textarea_S5*ColumnI*c1*Services_Paragraph_4-Demosapp single-encapsulation">
                        <p class="mt-4">We believe in using only
                            the
                            best
                            practice processes methodologies to
                            develop your product in a structured and methodical way. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="S6-Demosapp other_projects w-99 pb-5" id="ProjectsDemosapp">
    <div class="Heading-Demosapp">
        <h2 class="TitleHeading-it_text_S6*Carousel*Projects_TitleHeading_1-Demosapp text-uppercase text-center title">
            PROJECTS</h2>
        <h5 class="TitleHeading-it_text_S6*Carousel*Projects_TitleHeading_2-Demosapp text-center title-desc">Discover
            our craft of the
            brands
            you
            love, digitally enabled by us for our past
            work.</h5>
    </div>
    <div class="Carousel-Demosapp owl-carousel1 w-99 owl-carousel owl-theme px-5">
        <div class="item col inner-div position-relative">
            <div class="details text-center">
                <img src="assets\image\project1.png" alt="PROJECT_A"
                    class="Image-it_file_S6*Carousel*Projects_Image_1-Demosapp" />
                <!-- <img src="assets\image\mobile\project1.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_1-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="inner-details">
                    <h4
                        class="Heading-it_text_S6*Carousel*Projects_Heading_1-Demosapp font-weight-bold text-center mt-4">
                        PROJECT A</h4>
                    <div
                        class="ParagraphGroup-it_textarea_S5*Carousel*Projects_Paragraph_1-Demosapp single-encapsulation">
                        <p class="mt-3">
                            Redefine a brand new look for brand awareness, achieve the impossible in
                            terms of limited resources, yet able deliver only the best maximized results. </p>
                    </div>
                    <a href="#"><button class="btn transition">Learn More <i class="fas fa-angle-right">
                            </i></button></a>
                </div>
            </div>
        </div>
        <div class="item col inner-div position-relative">
            <div class="details text-center">
                <img src="assets\image\project2.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_2-Demosapp" />
                <!-- <img src="assets\image\mobile\project2.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_2-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="inner-details">
                    <h4
                        class="Heading-it_text_S6*Carousel*Projects_Heading_2-Demosapp font-weight-bold text-center mt-4">
                        PROJECT B </h4>
                    <div
                        class="ParagraphGroup-it_textarea_S6*Carousel*Projects_Paragraph_2-Demosapp single-encapsulation">
                        <p class="mt-3">A
                            community platform caters for needs of knowledge seeking and knowledge
                            delivering. Rewarding participants with generous rewards, mini games, and many more.
                            The
                            real true All-in-one platform.</p>
                    </div>
                    <a href="#"><button class="btn transition">Learn More <i class="fas fa-angle-right">
                            </i></button></a>
                </div>
            </div>
        </div>
        <div class="item col inner-div position-relative">
            <div class="details text-center">
                <img src="assets\image\project1.png" alt="PROJECT_A"
                    class="Image-it_file_S6*Carousel*Projects_Image_3-Demosapp" />
                <!-- <img src="assets\image\mobile\project1.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_3-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="inner-details">
                    <h4
                        class="Heading-it_text_S6*Carousel*Projects_Heading_3-Demosapp font-weight-bold text-center mt-4">
                        PROJECT A</h4>
                    <div
                        class="ParagraphGroup-it_textarea_S6*Carousel*Projects_Paragraph_3-Demosapp single-encapsulation">
                        <p class="mt-3">
                            Redefine a brand new look for brand awareness, achieve the impossible in
                            terms of limited resources, yet able deliver only the best maximized results. </p>
                    </div>
                    <a href="./project_individual_page"><button class="btn transition">Learn More <i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
        </div>
        <div class="item col inner-div position-relative">
            <div class="details text-center">
                <img src="assets\image\project2.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_4-Demosapp" />
                <!-- <img src="assets\image\mobile\project2.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_4-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="inner-details">
                    <h4
                        class="Heading-it_text_S6*Carousel*Projects_Heading_4-Demosapp font-weight-bold text-center mt-4">
                        PROJECT B</h4>
                    <div
                        class="ParagraphGroup-it_textarea_S6*Carousel*Projects_Paragraph_4-Demosapp single-encapsulation">
                        <p class="mt-3">
                            A
                            community platform caters for needs of knowledge seeking and knowledge
                            delivering. Rewarding participants with generous rewards, mini games, and many
                            more. The
                            real true All-in-one platform.</p>
                    </div>
                    <a href="./project_individual_page"><button class="btn transition">Learn More <i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
        </div>
        <div class="item col inner-div position-relative">
            <div class="details text-center">
                <img src="assets\image\project1.png" alt="PROJECT_A"
                    class="Image-it_file_S6*Carousel*Projects_Image_5-Demosapp" />
                <!-- <img src="assets\image\mobile\project1.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_5-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="inner-details">
                    <h4
                        class="Heading-it_text_S6*Carousel*Projects_Heading_5-Demosapp font-weight-bold text-center mt-4">
                        PROJECT A</h4>
                    <div
                        class="ParagraphGroup-it_textarea_S6*Carousel*Projects_Paragraph_5-Demosapp single-encapsulation">
                        <p class="mt-3">
                            Redefine a brand new look for brand awareness, achieve the impossible in
                            terms of limited resources, yet able deliver only the best maximized results.
                        </p>
                    </div>
                    <a href="./project_individual_page"><button class="btn transition">Learn More <i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
        </div>
        <div class="item col inner-div position-relative">
            <div class="details text-center">
                <img src="assets\image\project2.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_6-Demosapp" />
                <!-- <img src="assets\image\mobile\project2.png" alt="PROJECT_B"
                    class="Image-it_file_S6*Carousel*Projects_Image_6-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="inner-details">
                    <h4
                        class="Heading-it_text_S6*Carousel*Projects_Heading_6-Demosapp font-weight-bold text-center mt-4">
                        PROJECT B</h4>
                    <div
                        class="ParagraphGroup-it_textarea_S6*Carousel*Projects_Paragraph_6-Demosapp single-encapsulation">
                        <p class="mt-3">
                            A
                            community platform caters for needs of knowledge seeking and knowledge
                            delivering. Rewarding participants with generous rewards, mini games, and many
                            more.
                            The
                            real true All-in-one platform.</p>
                    </div>
                    <a href="./project_individual_page"><button class="btn transition">Learn More <i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
        </div>
    </div>

</section>
<section class="S7-Demosapp current" id="BlogDemosapp">
    <div class="row text-center">
        <div class="ColumnA-Demosapp col-lg-12">
            <div class="Content-c1-Demosapp">
                <h2
                    class="Heading-it_text_S7*ColumnA*c1*Blog_TitleHeading_1-Demosapp text-uppercase text-center title mb-5">
                    OUR
                    BLOGS</h2>
            </div>
        </div>
    </div>
    <div class="row mr-0 outer-div px-2">
        <div class="ColumnB-Demosapp col-xl-6">
            <div class="Content-c1-Demosapp inner-div">
                <img src="assets\image\blog1.png" alt="New Furniture Apps" width="100%"
                    class="Image-it_file_S7*ColumnB*c1*Blog_Image_1-Demosapp" />
                <!-- <img src="assets\image\mobile\blog1.png" alt="New Furniture Apps" width="100%"
                    class="Image-it_file_S7*ColumnB*c1*Blog_Image_1-Cloned-Demosapp d-block d-md-none" /> -->
                <div class="details">
                    <h6 class="Heading-it_text_S7*ColumnB*c1*Blog_Heading_1-Demosapp text-uppercase">MOBILE
                        APPLICATION
                    </h6>
                    <h3 class="Heading-it_text_S7*ColumnB*c1*Blog_SubHeading_1-Demosapp">New Furniture Apps</h3>
                    <div
                        class="ParagraphGroup-it_textarea_S7*ColumnB*c1*Blog_Paragraph_1-Demosapp single-encapsulation">
                        <p class="mt-4">A big
                            bunch
                            of practical UI design examples, this time on mobile user
                            experience: check the app concepts designed for the diversity of goals. </p>
                    </div>
                    <a href="./blog_individual_page"><button class="btn transition">Read Article<i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
        </div>
        <div class="ColumnC-Demosapp col-xl-6 pr-0 right">
            <div class="Content-c1-Demosapp inner-div">
                <div class="details">
                    <h6 class="Heading-it_text_S7*ColumnC*c1*Blog_Heading_2-Demosapp text-uppercase">UI / UX</h6>
                    <h3 class="Heading-it_text_S7*ColumnC*c1*Blog_SubHeading_2-Demosapp">Color Theory: Brief Guide For
                        Designers</h3>
                    <div
                        class="ParagraphGroup-it_textarea_S7*ColumnC*c1*Blog_Paragraph_2-Demosapp single-encapsulation">
                        <p class="mt-4">The
                            article
                            focused on the basics of color theory and color combinations in
                            design: learn more about color wheel, RGB, CMYK and models of color harmony. </p>
                    </div>
                    <a href="./blog_individual_page"><button class="btn transition">Read Article<i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
            <div class="Content-c2-Demosapp inner-div">
                <div class="details">
                    <h6 class="Heading-it_text_S7*ColumnC*c2*Blog_Heading_3-Demosapp text-uppercase">UI / UX</h6>
                    <h3 class="Heading-it_text_S7*ColumnC*c2*Blog_SubHeading_3-Demosapp">Color Theory: Brief Guide For
                        Designers</h3>
                    <div
                        class="ParagraphGroup-it_textarea_S7*ColumnC*c2*Blog_Paragraph_3-Demosapp single-encapsulation">
                        <p class="mt-4">The
                            article
                            focused on the basics of color theory and color combinations in
                            design: learn more about color wheel, RGB, CMYK and models of color harmony. </p>
                    </div>
                    <a href="./blog_individual_page"><button class="btn transition">Read Article<i
                                class="fas fa-angle-right"> </i></button></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="S8-Demosapp enquiry position-relative" id="ContactDemosapp">
    <div class="row text-center w-99">
        <div class="ColumnA-Demosapp col-lg-12">
            <div class="Content-c1-Demosapp">
                <h2
                    class="Heading-it_text_S8*ColumnA*c1*Contact_TitleHeading_1-Demosapp text-uppercase text-center title">
                    WE WOULD
                    LOVE TO
                    HEAR
                    FROM
                    YOU
                </h2>

                <h5 class="Heading-it_text_S8*ColumnA*c1*Contact_TitleHeading_2-Demosapp text-center title-desc">Whether
                    you have a
                    question about
                    features,
                    trials, pricing, need a demo, or
                    anything else, our team is ready to answer all your questions. Just drop us your enquiry.
                </h5>
            </div>
        </div>
    </div>
    <div class="row outer-div mr-0 ml-0 align-items-center bgimage1">
        <div class="ColumnB-Demosapp col-xl-6 pl-0 pr-0">
            <div class="Content-c1-Demosapp inner-div contact-details ">
                <div class="details">
                    <div class="inner-details">
                        <h4 class="Heading-it_text_S8*ColumnB*c1*Contact_Heading_1-Demosapp">Company Address</h4>
                        <div class="d-flex align-items-center">
                            <div>
                                <img src=".\assets\image\Icon\Contact Us\Company Address.svg" alt="Company_Address"
                                 class="contactIcon" />
                            </div>
                            <div
                                class="ParagraphGroup-it_textarea_S8*ColumnB*c1*Contact_Paragraph_1-Demosapp single-encapsulation">
                                <p class="mt-3">
                                    Block A-7-9, Avenue K, Jalan Ampang,<br /> Kelana Jaya, 50400, Kuala
                                    Lumpur.</p>
                            </div>
                        </div>
                    </div>
                    <div class="inner-details">
                        <h4 class="Heading-it_text_S8*ColumnB*c1*Contact_Heading_2-Demosapp">Phone</h4>
                        <div class="d-flex align-items-center">
                            <div>
                                <img src=".\assets\image\Icon\Contact Us\Phone.svg" alt="Phone" class="contactIcon" />
                            </div>
                            <div
                                class="ParagraphGroup-it_textarea_S8*ColumnB*c1*Contact_Paragraph_2-Demosapp single-encapsulation">
                                <p class="mt-3">
                                    Malaysia (6012-3456789)</p>
                            </div>
                        </div>
                    </div>
                    <div class="inner-details">
                        <h4 class="Heading-it_text_S8*ColumnB*c1*Contact_Heading_3-Demosapp">Email</h4>
                        <div class="d-flex align-items-center">
                            <div>
                                <img src=".\assets\image\Icon\Contact Us\Email.svg" alt="Email" class="contactIcon" />
                            </div>
                            <div
                                class="ParagraphGroup-it_textarea_S8*ColumnB*c1*Contact_Paragraph_3-Demosapp single-encapsulation">
                                <p class="mt-3">
                                    info@demosapp.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ColumnC-Demosapp col-xl-6 pl-0 pr-0">
            <div class="inner-div form">
                <div class="Content-c1-Demosapp details">
                    <form id="enquiry-form" method="GET" action="#">
                        <div class="form-group">
                            <label for="name"
                                class="Label-it_text_S8*ColumnC*c1*Contact_Label_1-Demosapp col-form-label">NAME
                                *</label>
                            <input type="text" class="form-control form-control-lg" id="name" name="name"
                                placeholder="Johnson" required="true" />
                        </div>

                        <div class="form-group">
                            <label for="email"
                                class="Label-it_text_S8*ColumnC*c1*Contact_Label_2-Demosapp col-form-label">EMAIL
                                *</label>
                            <input type="email" class="form-control form-control-lg" id="email" name="email"
                                placeholder="example@example.com" required="true" />
                        </div>

                        <div class="form-group">
                            <label for="phone"
                                class="Label-it_text_S8*ColumnC*c1*Contact_Label_3-Demosapp col-form-label">PHONE
                                *</label>
                            <input type="text" class="form-control form-control-lg" id="phone" name="phone"
                                placeholder="0123456789" required="true" />
                        </div>

                        <div class="form-group">
                            <label for="phone"
                                class="Label-it_text_S8*ColumnC*c1*Contact_Label_4-Demosapp col-form-label">WHAT
                                DO YOU HAVE
                                IN MIND *</label>
                            <input type="text" class="form-control form-control-lg" id="phone" name="phone"
                                placeholder="Share a message" required="true" />
                        </div>

                        <div class="pb-4">
                            <button class="btn" type="submit">SUBMIT</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="Footer-Demosapp footer">
    <div class="row">
        <div class="ColumnA-Demosapp col-xl-4 col-12">
            <div class="Content-c1-Demosapp">
                <h5 class="Heading-it_text_Footer*ColumnA*c1*Footer_TitleHeading_1-Demosapp text-white mt-4">ABOUT
                    DEMOSAPP
                </h5>
                <h6 class="Heading-it_text_Footer*ColumnA*c1*Footer_SubHeading_1-Demosapp text-white footer_subheading">
                    Demosapp is an
                    IT-driven company who provide IT solutions towards any businesses with innovative
                    technology across any platforms.</h6>

                <div class="Socialbar-Demosapp single-encapsulation slinks d-block d-lg-none">
                    <a href="http://instagram.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_1-Demosapp fab fa-instagram fa-fw">
                    </a>
                    <a href="http://facebook.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_2-Demosapp fab fa-facebook-f fa-fw">
                    </a>
                    <a href="http://linkedin.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_3-Demosapp fab fa-linkedin-in fa-fw">
                    </a>
                    <a href="http://twitter.com"
                        class="SocialbarItem-it_url_Footer*ColumnA*c1*First_SocialbarItem_4-Demosapp fab fa-twitter fa-fw">
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnB-Demosapp col-xl col-lg-3 col-md-4 col-6">
            <div class="Content-c1-Demosapp">
                <h5 class="Heading-it_text_Footer*ColumnB*c1*Footer_SubHeading_1-Demosapp text-white mt-4">QUICK LINK
                </h5>

                <div class="Socialbar-Demosapp single-encapsulation footerList">
                    <a href="#BlogDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnB*c1*FooterFirst_QuickLink_1-Demosapp">Blog
                    </a>
                    <a href="#ProjectsDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnB*c1*FooterFirst_QuickLink_2-Demosapp">
                        Project
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnB*c1*FooterFirst_QuickLink_3-Demosapp">Contact Us

                    </a>
                </div>

            </div>
        </div>
        <div class="ColumnC-Demosapp col-xl col-lg-3 col-md-4 col-6">
            <div class="Content-c1-Demosapp">
                <h5 class="Heading-it_text_Footer*ColumnC*c1*Footer_SubHeading_2-Demosapp text-white mt-4">COMPANY</h5>
                <div class="Socialbar-Demosapp single-encapsulation footerList">
                    <a href="#AboutDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnC*c1*FooterSecond_QuickLink_1-Demosapp">About
                    </a>
                    <a href="#ServicesDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnC*c1*FooterSecond_QuickLink_2-Demosapp">Why
                        Demosapp
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnD-Demosapp col-xl col-lg-3 col-md-4 col-6">
            <div class="Content-c1-Demosapp">
                <h5 class="Heading-it_text_Footer*ColumnD*c1*Footer_SubHeading_3-Demosapp text-white mt-4">CAREER</h5>

                <div class="Socialbar-Demosapp single-encapsulation  footerList">
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterThird_QuickLink_1-Demosapp">UI/UX
                        Designer
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterThird_QuickLink_2-Demosapp">Graphic
                        Designer
                    </a>
                    <a href="#ContactDemosapp"
                        class="SocialbarItem-it_url_Footer*ColumnD*c1*FooterThird_QuickLink_3-Demosapp">Digital
                        Marketer
                    </a>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="row bottom-footer">
        <div class="ColumnE-Demosapp col-lg-5 d-none d-lg-block">
            <div class="Content-c1-Demosapp">
                <div class="Socialbar-Demosapp">
                    <a href="http://instagram.com"
                        class="SocialbarItem-it_url_Footer*ColumnE*c1*BottomFooter_SocialbarItem_1-Demosapp fab fa-instagram fa-fw bottomFooter_icon">
                    </a>
                    <a href="http://facebook.com"
                        class="SocialbarItem-it_url_Footer*ColumnE*c1*BottomFooter_SocialbarItem_2-Demosapp fab fa-facebook-f fa-fw bottomFooter_icon">
                    </a>
                    <a href="http://linkedin.com"
                        class="SocialbarItem-it_url_Footer*ColumnE*c1*BottomFooter_SocialbarItem_3-Demosapp fab fa-linkedin-in fa-fw bottomFooter_icon">
                    </a>
                    <a href="http://twitter.com"
                        class="SocialbarItem-it_url_Footer*ColumnE*c1*BottomFooter_SocialbarItem_4-Demosapp fab fa-twitter fa-fw bottomFooter_icon">
                    </a>
                </div>
            </div>
        </div>
        <div class="ColumnF-Demosapp col-lg-7">
            <div class="Content-c1-Demosapp">
                <div class="text-center text-lg-right copyright">
                    <h6 class="Heading-it_text_Footer*ColumnF*c1*BottomFooter_TitleHeading_1-Demosapp">Copyright @
                        2021
                        All
                        Rights
                        Reserved by
                        Demosapp</h6>
                </div>
            </div>
        </div>

    </div>
</section>
<?php
include "inc/footer.php";
?>
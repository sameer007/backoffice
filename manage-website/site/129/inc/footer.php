


<script src="./vendor/bootstrap-4.5.2/js/bootstrap.min.js"></script>

<script src="./assets/lib/owlcarousel/jquery.min.js"></script>
<script src="./assets/lib/owlcarousel/owl.carousel.min.js"></script>

<script type="text/javascript">

    $('.owl-carousel1').owlCarousel({
        loop:true,
        nav:true,
        center:true,
        margin:40,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        responsiveClass:true,
        dots:false,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots:true
            },
            500:{
                items:1,
                nav:false,
                margin:0,
                dots:true
            },
            768:{
                items:2,
                nav:false,
                center:false,
                dots:true
            },
            992:{
                items:2,
                nav:false,
                center:false,
                dots:true
            },
            1200:{
                items:3,
                nav:false
            },
        }
    });

    $('.owl-carousel2').owlCarousel({
        loop:true,
        nav:true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        responsiveClass:true,
        dots:false,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots:true
            },
            500:{
                items:1,
                nav:false,
                dots:true
            },
            768:{
                items:1,
                nav:false,
                dots:true
            },
            992:{
                items:2,
                nav:false,
                autoplay:true,
                dots:true
            },
            1270:{
                items:3,
                nav:false,
                autoplay:false,
            },
        }
    });

    $('.owl-carousel1').find('.owl-dots').removeClass('disabled');
    $('.owl-carousel1').on('changed.owl.carousel', function(event) {
        $(this).find('.owl-dots').removeClass('disabled');
    });

    $('.owl-carousel2').find('.owl-dots').removeClass('disabled');
    $('.owl-carousel2').on('changed.owl.carousel', function(event) {
        $(this).find('.owl-dots').removeClass('disabled');
    });
</script>

<!-- Loader -->
<script>
    $(window).on('load', function() {
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<script type="text/javascript">
function scrollto(div)
{
 $('html,body').animate(
 {
  scrollTop: $("#"+div).offset().top
 },'slow');
}
</script>

<!-- Side Menu -->
<script type="text/javascript">
        /*Menu Onclick*/
    let sideMenuToggle = $("a#sidemenu_toggle");
    let sideMenu = $(".side-menu");
    if (sideMenuToggle.length) {
        sideMenuToggle.on("click", function () {
            $("body").addClass("overflow-hidden");
            sideMenu.addClass("side-menu-active");
            $(function () {
                setTimeout(function () {
                    $("#close_side_menu").fadeIn(300);
                }, 300);
            });
        });
        $("#close_side_menu , #btn_sideNavClose , .side-nav .nav-item").on("click", function () {
            $("body").removeClass("overflow-hidden");
            sideMenu.removeClass("side-menu-active");
            $("#close_side_menu").fadeOut(200);
            $(function () {
                setTimeout(function () {
                    $('.sideNavPages').removeClass('show');
                    $('.fas').removeClass('rotate-180');
                }, 400);
            });
        });
        $(document).keyup(function (e) {
            if (e.keyCode === 27) { // escape key maps to keycode `27`
                if (sideMenu.hasClass("side-menu-active")) {
                    $("body").removeClass("overflow-hidden");
                    sideMenu.removeClass("side-menu-active");
                    $("#close_side_menu").fadeOut(200);
                    //$tooltip.tooltipster('close');
                    $(function () {
                        setTimeout(function () {
                            $('.sideNavPages').removeClass('show');
                            $('.fas').removeClass('rotate-180');
                        }, 400);
                    });
                }
            }
        });
    }
  </script>
</body>

</html>
<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="robots" content="index, follow"/>
  <meta name="keywords" content="Demoapp, Web Development, Graphic Design, Digital Marketing, Media"/>
  <meta property="og:title" content="Demoapps" />
  <meta property="og:description" content="Demosapp is an IT-driven company who provide IT solutions towards any businesses with innovative technology across any platforms." />
  <meta property="og:image" content="./assets/image/Logo/Demosapp.png"/>
  <title>Demosapp</title>
  <link rel="stylesheet preload prefetch" as="style" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800|Open+Sans:300,400,600,700&amp;display=swap" />
  <link rel="stylesheet preload prefetch" as="style" type="text/css" href="./assets/lib/fontawesome/5.12.0/css/all.css" />
  <link href="./assets/image/Logo/Demosapp.png" rel="icon" />
  <link rel="stylesheet" href="./vendor/bootstrap-4.5.2/css/bootstrap.min.css" />
  <link rel="stylesheet" href="./assets/lib/owlcarousel/owl.carousel.min.css" />
  <link rel="stylesheet" href="./assets/lib/owlcarousel/owl.theme.default.min.css" />
  <link rel="stylesheet" href="./assets/lib/animate/animate.min.css" />
  <link rel="stylesheet" href="./assets/css/style.css" />
  <script src="./assets/js/jquery/jquery-3.6.0.min.js"></script>
</head>
<body>
    
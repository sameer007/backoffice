<?php 
include '../../inc/header.php';
include '../../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';

$role = new Role();

$allRoles = $role->getAllRoles();
?>

<div class="wrapper">
  <?php include '../../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <a class="btn btn-md btn-primary mr-1" href="./new_user"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New User</a> -->
              <a class="btn btn-md btn-primary mr-1" href="./untitled_role"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New Role</a>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Roles</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped projects">
              <thead>
                <tr>

                </tr>
              </thead>
              <tbody>
                <?php if(isset($allRoles) && !empty($allRoles)){ ?>
                  <?php foreach ($allRoles as $key => $value) {  ?>
                    <tr>
                      <td><?php echo (isset($value->role_title) && !empty($value->role_title)) ? $value->role_title : '-' ?></td>
                      <td>
                        <?php 

                        $editRoleUrl = './role_details?roleId='.$value->role_id.'&act='.substr(md5('edit-role-'.$value->role_id.'-'.$_SESSION['token']), 5, 15); 
                        $deleteRoleUrl = CURRENT_PAGE_BACK_ROUTE.'process/role?roleId='.$value->role_id.'&act='.substr(md5('delete-role-'.$value->role_id.'-'.$_SESSION['token']), 5, 15); 

                        ?>
                        <a class="btn btn-md btn-primary mr-1" href="<?php echo $editRoleUrl ?>"><span><i class="fas fa-eye mr-2 fa-lg"></i></span>Details</a>
                        <?php if($value->role_title != 'Superadmin'){ ?>
                          <a class="btn btn-md btn-default mr-1" onclick="return confirm('Are you sure you want to delete this role?')" href="<?php echo $deleteRoleUrl ?>"><span style="color: red"><i class="fas fa-trash mr-2 fa-lg"></i></span>Delete</a>
                        <?php }else{ ?>
                          <button class="btn btn-md btn-default mr-1" disabled><span style="color: red"><i class="fas fa-trash mr-2 fa-lg"></i></span>Delete</a>
                        <?php } ?>
                      </td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php 
  $scripts = '
  <script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>';
  include '../../inc/footer.php';
  ?>
  ?>
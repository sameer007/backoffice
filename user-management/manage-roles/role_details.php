<?php 
include '../../inc/header.php';
include '../../inc/session.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';

$role = new Role();

if (isset($_GET) && !empty($_GET)) {
  if ((isset($_GET['roleId']) && !empty($_GET['roleId'])) && ($_GET['act'] == substr(md5('edit-role-'.$_GET['roleId'].'-'.$_SESSION['token']), 5, 15))) {

    $roleData = $role->getRoleById($_GET['roleId']);

    if(!isset($roleData) || empty($roleData)){
      redirect('./../manage-roles','error','Something went wrong while retreiving role details');

    }
    
    $_SESSION['roleId'] = $roleData[0]->role_id ;

    $act = 'Edit';
  }else{
    redirect('./404');
  }
}else{
  redirect('./404');
}
?>

<div class="wrapper">
  <?php include '../../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?php echo (isset($_SESSION['loggedInUserData'][0]->role_title) && !empty($_SESSION['loggedInUserData'][0]->role_title)) ? $_SESSION['loggedInUserData'][0]->role_title : '' ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <a class="btn btn-md btn-primary mr-1" href="./untitled_role"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New Role</a>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/role" method="post">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Roles Details</h3>
            </div>

            <?php //debugger($allUsers); ?>
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputFullName">Role Title</label>
                <input type="text" class="form-control" name="role-title" value="<?php echo (isset($roleData[0]->role_title) && !empty($roleData[0]->role_title)) ? $roleData[0]->role_title :  '-' ?>" id="exampleInputFullName" placeholder="Role Title">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Permission</h3>
            </div>

            <?php //debugger($allUsers); ?>
            <div class="card-body">
              <table class="table table-borderless">
                <tbody>
                  <tr>
                    <td>Domain Setting</td>
                    <td><input type="checkbox" name="domain-setting-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->domain_setting_permission)) ? 'checked'  : '' ?>/></td>
                  </tr>
                  <tr>
                    <td>Assign Roles And Permission</td>
                    <td><input type="checkbox" name="assign-roles-and-permission-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->assignRolesAndPermission_permission) && ($roleData[0]->assignRolesAndPermission_permission == 1)) ? 'checked'  : '' ?>/></td>
                  </tr>
                  <tr>
                    <td>Enable Maintainance Mode</td>
                    <td><input type="checkbox" name="enable-maintainance-mode-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->enable_maintainance_mode_permission) && ($roleData[0]->enable_maintainance_mode_permission == 1)) ? 'checked'  : '' ?>/></td>
                  </tr>
                  <tr>
                    <td>Create New Website</td>
                    <td><input type="checkbox" name="create-new-website-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->create_new_website_permission) && ($roleData[0]->create_new_website_permission == 1)) ? 'checked'  : '' ?>/></td>
                  </tr>
                  <tr>
                    <td>Manage Website</td>
                    <td><input type="checkbox" name="manage-website-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->manage_website_permission) && ($roleData[0]->manage_website_permission == 1)) ? 'checked'  : '' ?>/></td>
                  </tr>
                  <tr>
                    <td>Create New Role</td>
                    <td><input type="checkbox" name="create-new-role-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->create_new_role_permission) && ($roleData[0]->create_new_role_permission == 1)) ? 'checked'  : '' ?>/></td>
                  </tr>
                  <tr>
                    <td>Newsletter Management</td>
                    <td><input type="checkbox" name="newsletter-management-permission" class="checkbox-float-inherit" <?php echo (isset($roleData[0]->newsletter_management_permission) && ($roleData[0]->newsletter_management_permission == 1)) ? 'checked'  : '' ?>/></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->

            
          </div>
          <div class="card">
            <div class="card-footer">
              <?php if($roleData[0]->role_title != 'Superadmin'){ ?>
              <button type="submit" name="update-role" value="submit" class="btn btn-primary">Update</button>
            <?php }else{ ?>
              <button class="btn btn-primary" disabled>Update</button>
            <?php } ?>
            </div>
          </div>
        </form>


      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php 
  $scripts = '
  <script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>';
  include '../../inc/footer.php';
  ?>
  ?>
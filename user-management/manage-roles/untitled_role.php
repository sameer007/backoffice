<?php 
include '../../inc/header.php';
include '../../inc/session.php';


$allUsers = $users->getAllUsers();
?>

<div class="wrapper">
  <?php include '../../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Untitled Role</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/role" method="post">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Roles Details</h3>
            </div>

            <?php //debugger($allUsers); ?>
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputFullName">Role Title</label>
                <input type="text" class="form-control" name="role-title" id="exampleInputFullName" placeholder="Role Title">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Permission</h3>
            </div>

            <?php //debugger($allUsers); ?>
            <div class="card-body">
              <table class="table table-borderless">
                <tbody>
                  <tr>
                    <td>Domain Setting</td>
                    <td><input type="checkbox" name="domain-setting-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                  <tr>
                    <td>Assign Roles And Permission</td>
                    <td><input type="checkbox" name="assign-roles-and-permission-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                  <tr>
                    <td>Enable Maintainance Mode</td>
                    <td><input type="checkbox" name="enable-maintainance-mode-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                  <tr>
                    <td>Create New Website</td>
                    <td><input type="checkbox" name="create-new-website-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                  <tr>
                    <td>Manage Website</td>
                    <td><input type="checkbox" name="manage-website-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                  <tr>
                    <td>Create New Role</td>
                    <td><input type="checkbox" name="create-new-role-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                  <tr>
                    <td>Newsletter Management</td>
                    <td><input type="checkbox" name="newsletter-management-permission" class="checkbox-float-inherit"/></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->

            
          </div>
          <div class="card">
            <div class="card-footer">
              <button type="submit" name="add-role" value="submit" class="btn btn-primary">Submit</button>
              <button type="reset"  value="Reset" class="btn btn-default">Cancel</button>
            </div>
          </div>
        </form>


      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php 
  $scripts = '
  <script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>';
  include '../../inc/footer.php';
  ?>
  ?>
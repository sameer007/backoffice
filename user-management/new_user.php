<?php 
include '../inc/header.php';
include '../inc/session.php';


$allUsers = $users->getAllUsers(true);
?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Add New User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <button type="button" class="btn btn-md btn-default mr-1"><i class="fas fa-cog fa-lg mr-2"></i>Manage Roles</button>
              <button type="button" class="btn btn-md btn-primary mr-1"><i class="fas fa-plus fa-lg mr-2"></i>New User</button>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">New User</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          
          <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/login" method="post" enctype='multipart/form-data'>
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputFullName">Full Name</label>
                <input type="text" class="form-control" name="full-name" id="exampleInputFullName" placeholder="Full Name">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password (default : emailPrefix-123@emailDomain.com)" readonly>
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Image</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" name="image" class="custom-file-input form-control" id="exampleInputFile" placeholder="image">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                  </div>
                </div>
              </div>
              <div class="form-group" style="max-width: 100%" data-select2-id="55">
                <label for="exampleInputRole">Role</label>
                <select class="form-group form-control" name="role" id="exampleInputRole" aria-hidden="true">
                  <option value="1">Super Admin</option>
                  <option value="2">Admin</option>
                  <option value="3" selected>User</option>
                </select>
              </div>
              <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" name="register" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php 
  $scripts = '
  <script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>';
  include '../inc/footer.php';
  ?>
  ?>
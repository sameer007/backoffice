<?php 
include '../inc/header.php';
include '../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/role.php';

$role = new Role();

$allUsers = $users->getAllUsers();
$allRole = $role->getAllRoles();
//debugger($allRole,true);
?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">User Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <a class="btn btn-md btn-default mr-1" href="./manage-roles"><span><i class="fas fa-plus fa-lg mr-2"></i></span>Manage Roles</a>

              <!-- <a class="btn btn-md btn-primary mr-1" href="./new_user"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New User</a> -->
              <button type="button" class="btn btn-md btn-primary mr-1" data-toggle="modal" data-target="#add-new-user-modal"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New User</button>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Users</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <?php //debugger($allUsers); ?>
          <div class="card-body p-0">
            <table class="table table-striped projects">
              <thead>
                <tr>
                  <th>#</th>
                  <th><input type="checkbox" style="float:inherit;"/> Name </th>
                  <th>Role</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if(isset($allUsers) && !empty($allUsers)){ ?>
                  <?php foreach ($allUsers as $key => $value) { 
                    //debugger($value);
                    ?>
                    <tr>
                      <td><?php echo ($key + 1) ?></td>
                      <td><input type="checkbox" style="float:inherit;"/><?php echo (isset($value->full_name) && !empty($value->full_name)) ? $value->full_name : '-' ?></td>
                      <td><?php echo (isset($value->role_title) && !empty($value->role_title)) ? $value->role_title : '-' ?></td>
                      <td><?php echo (isset($value->email_address) && !empty($value->email_address)) ? $value->email_address : '-' ?></td>
                      <td id="<?php echo 'userActiveStatus-'.$value->id ?>"><?php echo (isset($value->status)) ? getStatusByStatusId($value->status) : '-' ?></td>
                      <td>
                        <?php 
                        $deleteUser_url = CURRENT_PAGE_BACK_ROUTE.'process/login?userId='.$value->id.'&act='.substr(md5('del-user-'.$value->id.'-'.$_SESSION['token']), 5, 15); 

                        ?>
                        <div class="dropdown">
                          <?php if($_SESSION['loggedInUserData'][0]->role_title == 'Superadmin' && $value->role_title != 'Superadmin'){ ?>

                            <i class="fas fa-ellipsis-h fa-lg" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                              <button type="button" id="userid-<?php echo $value->id.'-'.substr(md5('update-status'.$value->id.'-'.$_SESSION['token']), 5, 15); ?>" class="dropdown-item btn btn-default btn-md" data-toggle="modal" data-target="#update-status-modal"><span><i class="fas fa-sync-alt mr-2 fa-lg"></i></span>Update</button>
                              <button type="button" id="userid-<?php echo $value->id.'-'.substr(md5('change-role'.$value->id.'-'.$_SESSION['token']), 5, 15); ?>" class="dropdown-item btn btn-default btn-md" data-toggle="modal" data-target="#change-role-modal"><span><i class="fas fa-users mr-1 fa-lg"></i>Change Role</button>
                                <a class="dropdown-item" onclick="return confirm('Are you sure you want to delete this user?')" href="<?php echo $deleteUser_url ?>"><span style="color: red"><i class="fas fa-trash mr-2 fa-lg"></i></span>Delete</a>

                              </div>
                            <?php }else{ ?>
                              <i class="fas fa-ellipsis-h fa-lg fa-disabled" type="button"></i>

                            <?php } ?>

                          </div>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>

        </div><!-- /.container-fluid -->
      </section>
      <div class="modal fade" id="add-new-user-modal" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add New User</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/login" method="post" enctype='multipart/form-data'>

              <div class="modal-body">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputFullName">Full Name</label>
                    <input type="text" class="form-control" name="full-name" id="exampleInputFullName" placeholder="Full Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password (default : emailPrefix-123@emailDomain.com)" readonly>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input form-control" id="exampleInputFile" placeholder="image">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" style="max-width: 100%" data-select2-id="55">
                    <label for="inputRole">Role</label>
                    <select class="form-group form-control" name="role-id" id="inputRole" aria-hidden="true">
                      <?php if(isset($allRole) && !empty($allRole)){ ?>
                        <?php foreach ($allRole as $key => $value) { ?>
                          <?php if($value->role_title != 'Superadmin'){ ?>
                            <option value="<?php echo (isset($value->role_id) && !empty($value->role_id)) ? $value->role_id : '' ?>" selected><?php echo (isset($value->role_title) && !empty($value->role_title)) ? $value->role_title : '' ?></option>
                          <?php }else{ ?>
                            <option value="<?php echo (isset($value->role_id) && !empty($value->role_id)) ? $value->role_id : '' ?>" ><?php echo (isset($value->role_title) && !empty($value->role_title)) ? $value->role_title : '' ?></option>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <div class="modal-footer justify-content-between">
                <button type="submit" name="register" value="submit" class="btn btn-primary">Add User</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </form>

          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="change-role-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Role</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/login" id="change-role-form" method="POST">

              <div class="modal-body">
                <div class="card-body">

                  <div class="form-group" style="max-width: 100%" data-select2-id="55">
                    <label for="modalnputChangeRole">Change Role</label>
                    <input type="hidden" id="change-role-user-id" value="" name="userId">
                    <select class="form-group form-control" name="role-id" id="modalInputChangeRole" aria-hidden="true" required>
                      <option disabled selected>Choose Role</option>
                      <?php if(isset($allRole) && !empty($allRole)){ ?>
                        <?php foreach ($allRole as $key => $value) { ?>
                          <?php if($value->role_title == 'Superadmin'){ ?>
                            <option id="superadminOption">Super Admin</option>
                          <?php }else{ ?>
                            <option value="<?php echo $value->role_id ?>"><?php echo $value->role_title; ?></option>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <div class="modal-footer justify-content-between">
                <button type="submit" id="changeRoleSubmit" name="change-role" value="submit" class="btn btn-primary">Change Role</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="switch-role-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Switch Role</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/login" id="switch-role-form" method="POST">

              <div class="modal-body">
                <div class="card-body">
                  <div class="form-group" style="max-width: 100%" data-select2-id="55">
                    <input type="hidden" id="switch-roles-superadmin-user-id" name="current-superadmin-user-id" value="<?php echo $_SESSION['loggedInUserData'][0]->id ?>" required>
                    <div class="form-group">
                      <label for="currentSuperadmin">Current Superadmin</label>
                      <input type="text" class="form-control" value="<?php echo $_SESSION['loggedInUserData'][0]->full_name ?>" id="currentSuperadmin" placeholder="Full Name" readonly>
                    </div>
                    <div class="form-group" style="max-width: 100%" data-select2-id="55">
                      <label for="superadminNewRole">New Role For Current Superadmin</label>

                      <select class="form-group form-control" name="current-superadmin-new-role" id="superadminNewRole" aria-hidden="true" required>
                        <option disabled selected>Choose Role</option>
                        <?php if(isset($allRole) && !empty($allRole)){ ?>
                          <?php foreach ($allRole as $key => $value) { ?>
                            <?php if($value->role_title != 'Superadmin'){ ?>
                              <option value="<?php echo $value->role_id ?>"><?php echo $value->role_title; ?></option>
                            <?php } ?>
                          <?php } ?>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group" style="max-width: 100%" data-select2-id="55">
                    <label for="modalnputChangeRole">New Superadmin</label>
                    <input type="hidden" name="new-superadmin-user-id" id="modalInputNewSuperadmin">
                    <select class="form-group form-control" name="new-superadmin-user-id" id="modalInputChooseNewSuperadmin" aria-hidden="true" required>
                      <option disabled>New Superadmin</option>
                      <?php if(isset($allUsers) && !empty($allUsers)){ ?>
                        <?php foreach ($allUsers as $key => $value) {  ?>
                          <?php if($_SESSION['loggedInUserData'][0]->id == $value->id){ ?>
                            <option disabled><?php echo $value->full_name ?></option> 

                          <?php }else{ ?>
                            <option value="<?php echo $value->id ?>"><?php echo $value->full_name ?></option> 
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>

                </div>
                <!-- /.card-body -->
              </div>
              <div class="modal-footer justify-content-between">
                <button type="submit" name="switch-role" onclick="return confirm('Your user role will be changed and you will no longer be superadmin.You will be logged out. Are you sure you want to proceed further to switch user role?')" value="submit" class="btn btn-primary">Switch Role</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="update-status-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Status Update</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/login" id="update-status-form" method="POST">

              <div class="modal-body">
                <div class="card-body">
                  <div class="form-group" style="max-width: 100%" data-select2-id="55">
                    <input type="hidden" id="update-status-user-id" name="update-status-user-id" value="" required>
                    <div class="form-group" style="max-width: 100%" data-select2-id="55">
                      <label for="userStatus">Status</label>
                      <select class="form-group form-control" name="user-status" id="modalInputUserStatus" aria-hidden="true" required>
                        <option disabled >Choose Status</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <div class="modal-footer justify-content-between">
                <button type="submit" name="update-status" onclick="return confirm('Are you sure you want to update this user status?')" value="submit" class="btn btn-primary">Update Status</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.content -->
    </div>
    <script type="text/javascript">
      $( "#change-role-modal" ).on('shown.bs.modal', function(event){
        button = event.relatedTarget; // The clicked button
        btnId = button.id; 
        userId = btnId.split("-")[1];
        act = btnId.split("-")[2];

        //console.log(userId);
        //console.log(act);
        actionUrl = './../'+'process/login?userId='+userId+'&act='+act;
        //console.log(actionUrl);
        $('#change-role-user-id').attr('value',userId);
        $("#modalInputChooseNewSuperadmin").val(userId).change();

        
      });
      $('#modalInputChangeRole').on('change', function() {
        changeRoleSubmit = $('#changeRoleSubmit');

        if(this.value == 'Super Admin'){
          changeRoleSubmit.attr('disabled',true);
          console.log(this.value+ 'Swith modal showing');
          console.log(modalInputChangeRole[0]);

          $('#switch-role-modal').modal('show');
        }else{
          changeRoleSubmit.attr('disabled',false);

        }
      });
      $( "#update-status-modal" ).on('shown.bs.modal', function(event){
        button = event.relatedTarget; // The clicked button
        btnId = button.id; 
        userId = btnId.split("-")[1];
        act = btnId.split("-")[2];

        //console.log(userId);
        //console.log(act);
        actionUrl = './../'+'process/login?userId='+userId+'&act='+act;
        //console.log(actionUrl);
        userActiveStatusTdId = '#userActiveStatus-'+userId;
        userActiveStatus = $(userActiveStatusTdId).html();

        if (userActiveStatus == 'Active') {
          activeStatusValue = 1;
        }else if(userActiveStatus == 'Inactive'){
          activeStatusValue = 0;

        }
        $('#update-status-user-id').attr('value',userId);

        $("#modalInputUserStatus > option").each(function() {
          console.log(this.text + ' ' + this.value);
          currentOption = $(this);


          if(this.value != activeStatusValue){
            if (userActiveStatus != 'Choose Status') {
              $(this).attr('selected',true);

            }
          }else{
            $(this).attr('selected',false);

          }
        });

        // $('#change-role-user-id').attr('value',userId);
        // $("#modalInputChooseNewSuperadmin").val(userId).change();

        
      });
      $( "#change-role-modal").on('hide.bs.modal', function(event){
        $('#modalInputChangeRole')[0].selectedIndex = 0;
      });
      $( "#switch-role-modal").on('hide.bs.modal', function(event){
        $('#modalInputChangeRole')[0].selectedIndex = 0;
      });
      $( "#update-status-modal").on('hide.bs.modal', function(event){
        $("#modalInputUserStatus > option").each(function() {
          if (!this.value) {
            $(this).attr('selected',true);
          }else{
            $(this).attr('selected',false);
          }

        });

        //$('#modalInputUserStatus')[0].selectedIndex = 0;
      });

    </script>
    <?php 
    $scripts = '
    <script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>';
    include '../inc/footer.php';
    ?>
    ?>
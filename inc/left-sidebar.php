<?php 

//debugger($_SESSION,true);
if (!isset($_SESSION['loggedInUserData']) || empty($_SESSION['loggedInUserData'])) {
  redirect('../backoffice','error','Please login first.');
}
//debugger(ASSETS_URL,true);
?>


<!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?php echo ASSETS_URL ?>dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>dashboard" class="nav-link">Home</a>        
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>account" class="nav-link">Account</a>        
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>logout" onclick="return confirm('Are you sure you want to logout?');" class="nav-link">Logout</a>        
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="./dashboard" class="brand-link">
      <img src="<?php echo ASSETS_URL ?>dist/img/AdminLTELogo.png" alt="Back Office" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Back Office</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo (isset($_SESSION['loggedInUserData']) && !empty($_SESSION['loggedInUserData'])) ? UPLOAD_URL.'/user/'.$_SESSION['loggedInUserData'][0]->image : '' ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo (isset($_SESSION['loggedInUserData']) && !empty($_SESSION['loggedInUserData'])) ? $_SESSION['loggedInUserData'][0]->full_name : '' ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>manage-website" class="nav-link">
              <i class="nav-icon fa fa-globe"></i>
              <p>
                Manage Website
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>newsletter" class="nav-link">
              <i class="nav-icon far fa-newspaper"></i>
              <p>
                Newsletter
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>user-management" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                User Management
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>theme" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Theme
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>setting" class="nav-link">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo CURRENT_PAGE_BACK_ROUTE ?>bin" class="nav-link">
              <i class="nav-icon fas fa-trash"></i>
              <p>
                Bin
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
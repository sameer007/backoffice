<?php /**
 * @Author: Sameer Acharya
 * @Date:   2018-04-26 08:43:59
 */
require_once $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/model.php';
require_once $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/user.php';

$users = new User();
//debugger($users);
if((!isset($_SESSION, $_SESSION['token']) || empty($_SESSION['token']) || strlen($_SESSION['token']) != 30)){

	if(getCurrentpage() != 'index'){
		redirect('../backoffice','error','Please login first.');
	}

}else{
	if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){
  		//unset($_SESSION);
		$loggedInUserData = $users->getUserByUserId($_SESSION['user_id']);

		if(isset($loggedInUserData) && !empty($loggedInUserData)){

			$_SESSION['loggedInUserData'] = $loggedInUserData;
			if(isLoginPage()){
				redirect('../backoffice/dashboard');
			}
		}
	}
}

$data = explode('/',$_SERVER['PHP_SELF']);

$routeArray = array();
foreach ($data as $key => $value) {
	if($key != 0){
		$route = rtrim($value,'.php');
		array_push($routeArray, $route);
	}
}

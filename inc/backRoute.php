<?php  if(isset($routeArray) && !empty($routeArray)){
  $backRoute = CURRENT_PAGE_BACK_ROUTE;

  foreach ($routeArray as $key => $value) {
    $noOfDirBack = sizeof($routeArray) - $key - 1;

    for ($i=1; $i < $noOfDirBack ; $i++) { 
      $backRoute = $backRoute.'../';
    }
    if(getCurrentPage() == $value){
      echo "<li class=\"breadcrumb-item active\">$value</li>";

    }else{
      if($value != 'index'){
        echo "<li class=\"breadcrumb-item\"><a href=\"$backRoute$value\">$value</a></li>";

      }
    }
    $backRoute = CURRENT_PAGE_BACK_ROUTE;
  }
}
?>
<?php 
include 'inc/header.php';
include 'inc/session.php';

  // if(!isset($_SESSION, $_SESSION['superadmin_token']) || empty($_SESSION['superadmin_token']) || strlen($_SESSION['superadmin_token']) != 30){
  //   redirect('../','error','Please login first.');
  // }
?>

<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>404 Error Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">404 Error Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-warning"><i class="fas fa-wrench"></i></h2>

        <div class="error-content">
          <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Our Site is currently under maintainance.</h3>

          <p>
            We could not find the page you were looking for.
            We will be back soon.
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
<!-- <script type="text/javascript">
  var calendar = $('#calendar').fullCalendar('getCalendar');

calendar.on('dayClick', function(date, jsEvent, view) {
  console.log('clicked on ' + date.format());
})
</script> -->

<?php 
$scripts = '
<script src="'.ASSETS_URL.'dist/js/pages/dashboard.js"></script>
<script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>';
include 'inc/footer.php';
?>
<?php 
include '../inc/header.php';
include '../inc/session.php';


//$allUsers = $users->getAllUsers();
?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php  if(isset($routeArray) && !empty($routeArray)){
                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Add New Theme</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">New Theme</h3>
              </div>

              <form action="<?php echo CURRENT_PAGE_BACK_ROUTE ?>process/theme" method="post" enctype='multipart/form-data'>
                <div class="card-body">
                  <div class="form-group">
                    <label for="themeName">Theme Name</label>
                    <input type="text" class="form-control" name="theme-name" id="themeName" placeholder="Full Name" required>
                  </div>
                  <div class="form-group" data-select2-id="55">
                    <label for="themeCategory">Category</label>
                    <div class="select2-purple">
                      <select class="select2" name="category[]" id="themeCategory" data-placeholder="Select categories" data-dropdown-css-class="select2-purple" style="width: 100%;" required multiple>
                        <option value="Booking">Booking</option>
                        <option value="Events">Events</option>
                        <option value="Single Page">Single Page</option>
                        <option value="Portfolio Website">Portfolio Website</option>
                        <option value="Under Construction">Under Construction</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="themeDescription">Description</label>
                    <textarea name="description" class="form-control" rows="4" cols="50" placeholder="Enter description"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="themeDescription">Current Version</label>
                    <input type="number" class="form-control" step="any" name="current-version" id="currentVersion" required>
                  </div>
                  <div class="form-group">
                    <label for="themeDescription">Created By</label>
                    <input type="text" class="form-control" name="creator" id="createdBy" required>
                  </div>
                  <div class="form-group">
                    <label for="themeLicence">Licence</label>
                    <div class="select2-purple">
                      <select class="select2" name="licence" id="themeLicence" data-placeholder="Select licence" data-dropdown-css-class="select2-purple" style="width: 100%;" required>
                        <option value="Regular">Regular</option>
                        <option value="Extended">Extended</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="themeZipFile">Screenshots</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="screenshots" class="custom-file-input form-control" id="themeScreenshots" placeholder="file" multiple>
                        <label class="custom-file-label" for="themeScreenshots">Choose file/s</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="themeZipFile">Theme file (zip)</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="themeZipFIle" class="custom-file-input form-control" id="themeZipFile" placeholder="file" required>
                        <label class="custom-file-label" for="themeZipFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" name="add-theme" value="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php 
  $scripts = '
  <script src="'.VENDOR_URL.'chart.js/Chart.min.js"></script>
  <script src="'.VENDOR_URL.'select2/js/select2.full.min.js"></script>';
  include '../inc/footer.php';
  ?>

  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })
    });
      // DropzoneJS Demo Code End
    </script>
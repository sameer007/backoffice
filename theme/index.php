<?php 
include '../inc/header.php';
include '../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';

$theme = new Theme();

/*retreiving themes based on category */
if (isset($_GET) && !empty($_GET)) {
  if(isset($_GET['getByCategory']) && !empty($_GET['getByCategory'])){
    $themeCategory = array();
    if (isset($_GET['booking']) && !empty($_GET['booking'])) {
      array_push($themeCategory,'Booking');
    }
    if (isset($_GET['events']) && !empty($_GET['events'])) {
      array_push($themeCategory,'Events');
      
    }
    if (isset($_GET['singlePage']) && !empty($_GET['singlePage'])) {
      array_push($themeCategory,'Single Page');
      
    }
    if (isset($_GET['portfolioWebsite']) && !empty($_GET['portfolioWebsite'])) {
      array_push($themeCategory,'Portfolio Website');
      
    }
    if (isset($_GET['underConstruction']) && !empty($_GET['underConstruction'])) {
      array_push($themeCategory,'Under Construction');
      
    }

    if(isset($themeCategory) && !empty($themeCategory)){
      $allThemes = $theme->getAllThemesByThemeCategory($themeCategory);
    }else{
        redirect('../theme/');
    }
  }
}else{
    /*retreiving all themes*/
    $allThemes = $theme->getAllThemes();
      //debugger($allThemes);

}

/*sorting themes to show popular themes*/
/*sorting theme in descending order by times installed*/
if(isset($allThemes) && !empty($allThemes)){
  $allThemesSorted = $allThemes;
  $col = array_column( $allThemesSorted, "times_installed" );
  array_multisort( $col, SORT_DESC, $allThemesSorted );
}

?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6 mb-2">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php include '../inc/backRoute.php' ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Theme</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <a type="button" class="btn btn-md btn-primary mr-1" href="./new_theme"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New Theme</a>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12 col-6 mb-2">
            <!-- small box -->
            <div class="float-sm-right">
              <div class="inner">
                <div class="input-group">
                  <div class="form-outline">
                    <input type="search" id="form1" placeholder="Search" class="form-control btn-sm" />
                  </div>
                  <button type="button" class="btn btn-primary">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-6 mb-4">
            <ul class="list-group list-group-horizontal">
              <li class="list-group-item-custom border-left-0"><span class="btn <?php echo (isset($_GET) && !empty($_GET) ? 'btn-default' : 'btn-primary') ?>  btn-md all">All</span></li>
              <li class="list-group-item-custom"><span class="btn <?php echo (isset($_GET['booking']) && !empty($_GET['booking']) ? 'btn-primary' : 'btn-default') ?> btn-md booking themeSelection">Booking</span></li>
              <li class="list-group-item-custom"><span class="btn <?php echo (isset($_GET['events']) && !empty($_GET['events']) ? 'btn-primary' : 'btn-default') ?> btn-md events themeSelection">Events</span></li>
              <li class="list-group-item-custom"><span class="btn <?php echo (isset($_GET['singlePage']) && !empty($_GET['singlePage']) ? 'btn-primary' : 'btn-default') ?> btn-md singlePage themeSelection">Single Page</span></li>
              <li class="list-group-item-custom"><span class="btn <?php echo (isset($_GET['portfolioWebsite']) && !empty($_GET['portfolioWebsite']) ? 'btn-primary' : 'btn-default') ?> btn-md portfolioWebsite themeSelection">Portfolio Website</span></li>
              <li class="list-group-item-custom border-right-0"><span class="btn <?php echo (isset($_GET['underConstruction']) && !empty($_GET['underConstruction']) ? 'btn-primary' : 'btn-default') ?> btn-md underConstruction themeSelection">Under Construction</span></li>

            </ul>
          </div>
          
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">
              <div class="title-left">
                New Themes
              </div>
              <div class="title-right text-fade font20 mt-2">
                More...
              </div>
            </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-1 col-6"></div>
          <div class="col-lg-10 col-6">

            <!--beginning of carousel-->
            <!-- displaying themes -->
            <?php if(isset($allThemes) && !empty($allThemes)){ ?>

              <div id="carouselNewThemes" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <?php foreach ($allThemes as $key => $value) { 

                    // if($key == 0){
                    //   $currentKey = $key + 1;

                    // }else{
                    //   $currentKey = $currentKey + $key + 1;

                    // }
                      $currentKey = $key + 1;


                    //debugger($currentKey-1);
                    //debugger($currentKey);
                    ?>

                    <?php if((isset($allThemes[$currentKey-1]) && !empty($allThemes[$currentKey-1])) || (isset($allThemes[$currentKey]) && !empty($allThemes[$currentKey]))){ ?>
                      <div class="carousel-item <?php echo ($key == 0) ? 'active' : '' ?>" data-interval="10000">
                        <div class="row">
                          <div class="col-lg-1 col-6"></div>
                          <?php if(isset($allThemes[$currentKey-1]) && !empty($allThemes[$currentKey-1])){ ?>

                            <div class="col-lg-5 col-6">
                              <div class="small-box-custom p-2 bg-default d-block w-100">
                                <div class="inner h-200">
                                </div>
                                <h3 class="p-3 m-3">
                                  <div class="title-left ">
                                    <?php echo $allThemes[$currentKey-1]->theme_name ?>  
                                  </div> 
                                  <div class="title-right"> 
                                    <?php 
                                    $themeDetailUrl = './theme_details?themeId='.$allThemes[$currentKey-1]->id.'&act='.substr(md5('edit-theme-'.$allThemes[$currentKey-1]->id.'-'.$_SESSION['token']), 5, 15);
                                    ?>
                                    <a href="<?php echo $themeDetailUrl ?>"><span class="btn btn-default btn-free"> Free </span></a>
                                  </div>
                                </h3>
                                <p class="p-3 m-3"><?php echo $allThemes[$key]->description ?></p>
                              </div>
                            </div>
                          <?php } ?>
                          <?php if(isset($allThemes[$currentKey]) && !empty($allThemes[$currentKey])){ ?>
                            <div class="col-lg-5 col-6">
                              <div class="small-box-custom p-2 bg-default d-block w-100">
                                <div class="inner h-200">
                                </div>
                                <h3 class="p-3 m-3">
                                  <div class="title-left ">
                                    <?php echo $allThemes[$currentKey]->theme_name ?>  
                                  </div> 
                                  <div class="title-right"> 
                                    <?php 
                                    $themeDetailUrl = './theme_details?themeId='.$allThemes[$currentKey]->id.'&act='.substr(md5('edit-theme-'.$allThemes[$currentKey]->id.'-'.$_SESSION['token']), 5, 15);
                                    ?>
                                    <a href="<?php echo $themeDetailUrl ?>"><span class="btn btn-default btn-free"> Free </span></a>
                                  </div>
                                </h3>
                                <p class="p-3 m-3"><?php echo $allThemes[$currentKey]->description ?></p>
                              </div>
                            </div>
                          <?php } ?>
                          <div class="col-lg-1 col-6"></div>
                        </div>
                      </div>
                    <?php } ?>
                  <?php } ?>


                <!-- <div class="carousel-item" data-interval="2000">
                </div> -->
              </div>
              <a class="carousel-control-prev" href="#carouselNewThemes" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselNewThemes" role="button" data-slide="next">
                <span class="carousel-control-next-icon next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          <?php } ?>
          <!--end of carousel-->
        </div>
        <div class="col-lg-1 col-6"></div>

      </div>


      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0">
            <div class="title-left">
              Popular Themes
            </div>
            <div class="title-right text-fade font20 mt-2">
              More...
            </div>
          </h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <!-- Main row -->
      <div class="row">
        <div class="col-lg-1 col-6"></div>
        <div class="col-lg-10 col-6">

          <!--beginning of carousel-->
          <?php if(isset($allThemesSorted) && !empty($allThemesSorted)){ ?>
              <!-- displaying popular themes based on tiems installed -->
            <div id="carouselPopularThemes" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <?php foreach ($allThemesSorted as $key => $value) { 

                  if($key == 0){
                    $currentKey = $key + 1;

                  }else{
                    $currentKey = $currentKey + $key + 1;

                  }
                  ?>

                  <?php if((isset($allThemesSorted[$currentKey-1]) && !empty($allThemesSorted[$currentKey-1])) || (isset($allThemesSorted[$currentKey]) && !empty($allThemesSorted[$currentKey]))){ ?>
                    <div class="carousel-item <?php echo ($key == 0) ? 'active' : '' ?>" data-interval="10000">
                      <div class="row">
                        <div class="col-lg-1 col-6"></div>
                        <?php if(isset($allThemesSorted[$currentKey-1]) && !empty($allThemesSorted[$currentKey-1])){ ?>

                          <div class="col-lg-5 col-6">
                            <div class="small-box-custom p-2 bg-default d-block w-100">
                              <div class="inner h-200">
                              </div>
                              <h3 class="p-3 m-3">
                                <div class="title-left ">
                                  <?php echo $allThemesSorted[$currentKey-1]->theme_name ?>  
                                </div> 
                                <div class="title-right"> 
                                  <a href="./theme_details"><span class="btn btn-default btn-free"> Free </span></a>
                                </div>
                              </h3>
                              <p class="p-3 m-3"><?php echo $allThemesSorted[$key]->description ?></p>
                            </div>
                          </div>
                        <?php } ?>
                        <?php if(isset($allThemesSorted[$currentKey]) && !empty($allThemesSorted[$currentKey])){ ?>
                          <div class="col-lg-5 col-6">
                            <div class="small-box-custom p-2 bg-default d-block w-100">
                              <div class="inner h-200">
                              </div>
                              <h3 class="p-3 m-3">
                                <div class="title-left ">
                                  <?php echo $allThemesSorted[$currentKey]->theme_name ?>  
                                </div> 
                                <div class="title-right"> 
                                  <a href="./theme_details"><span class="btn btn-default btn-free"> Free </span></a>
                                </div>
                              </h3>
                              <p class="p-3 m-3"><?php echo $allThemesSorted[$currentKey]->description ?></p>
                            </div>
                          </div>
                        <?php } ?>
                        <div class="col-lg-1 col-6"></div>
                      </div>
                    </div>
                  <?php } ?>
                <?php } ?>

                <!-- <div class="carousel-item" data-interval="2000">
                </div> -->
              </div>
              <a class="carousel-control-prev" href="#carouselPopularThemes" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselPopularThemes" role="button" data-slide="next">
                <span class="carousel-control-next-icon next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          <?php } ?>

          <!--end of carousel-->
        </div>
        <div class="col-lg-1 col-6"></div>

      </div>


      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>


<?php 
$scripts = '
';
include '../inc/footer.php';
?>
<script type="text/javascript">
  console.log(window.location);
  urlParams = [];

  if(checkGetMethod('getByCategory')){
      currentPathname = window.location.pathname;
      var currentUrl = window.location.origin + currentPathname ;
      var baseUrl = currentUrl+"?getByCategory="+true;
      if(checkGetMethod('booking')){
          urlParams.push('&'+'booking'+'='+true);
      }
      if(checkGetMethod('events')){
          urlParams.push('&'+'events'+'='+true);
      }
      if(checkGetMethod('singlePage')){
          urlParams.push('&'+'singlePage'+'='+true);
      }
      if(checkGetMethod('portfolioWebsite')){
          urlParams.push('&'+'portfolioWebsite'+'='+true);
      }
      if(checkGetMethod('underConstruction')){
          urlParams.push('&'+'underConstruction'+'='+true);
      }
      //urlParams = [];

  } else {
      var currentUrl = window.location.href;
      var baseUrl = currentUrl+"?getByCategory="+true;
      urlParams = [];

  }
  console.log('baseUrl>>',baseUrl);
  $(".themeSelection").click ( function() {
    var url = baseUrl ;
    $('.all').addClass('btn-default').removeClass('btn-primary');
    console.log($(this)[0]);
    camelizedClassname = camelize($(this).html());
    categoryClassName = '.'+camelizedClassname ;
    currentCategoryDom = $(categoryClassName);

    if(currentCategoryDom.hasClass("btn-default")){
      currentCategoryDom.addClass('btn-primary').removeClass('btn-default');
      urlParams.push('&'+camelizedClassname+'='+true);
      //console.log(urlParams);

    }else if(currentCategoryDom.hasClass("btn-primary")){
      currentCategoryDom.addClass('btn-default').removeClass('btn-primary');

      //urlParams.push('&'+camelizedClassname+'='+true);
      urlParams = removeArrayElement(urlParams,'&'+camelizedClassname+'='+true)
      //myArr = url.split("&");

    }
    urlParams.forEach(function(value){
      //console.log('value>>',value);
      
      url = url + value;
      
    });
    console.log('urlParams>>',urlParams);

    console.log('url>>',url);
    window.location.replace(url); 
  });
  $(".all").click ( function() {
    baseUrl = window.location.origin + window.location.pathname
    var url = baseUrl ;

    $('.themeSelection').addClass('btn-default').removeClass('btn-primary');
    console.log($(this)[0]);
    camelizedClassname = camelize($(this).html());
    categoryClassName = '.'+camelizedClassname ;
    currentCategoryDom = $(categoryClassName);

    if(currentCategoryDom.hasClass("btn-default")){
      currentCategoryDom.addClass('btn-primary').removeClass('btn-default');

    }else if(currentCategoryDom.hasClass("btn-primary")){
      currentCategoryDom.addClass('btn-default').removeClass('btn-primary');
    }
    window.location.replace(url);
  });


</script>
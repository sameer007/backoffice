<?php 
include '../inc/header.php';
include '../inc/session.php';

require $_SERVER['DOCUMENT_ROOT'].ROOT.'/class/theme.php';

$theme = new Theme();

if (isset($_GET) && !empty($_GET)) {
  if (isset($_GET['act']) && !empty($_GET['act'])) {
    //debugger(substr(md5('edit-theme-'.$_GET['themeId'].'-'.$_SESSION['token']), 5, 15));
    //debugger($_GET['act'],true);
    if($_GET['act'] == substr(md5('edit-theme-'.$_GET['themeId'].'-'.$_SESSION['token']), 5, 15)){
      $themeData = $theme->getThemeById($_GET['themeId']);
      //debugger($themeData,true);
    }else{
      redirect('./404');
    }
  }else{
    redirect('./404');
  }
}else{
  redirect('./404');
}


?>

<div class="wrapper">
  <?php include '../inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6 mb-2">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>
              <?php include '../inc/backRoute.php' ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?php echo (isset($themeData) && !empty($themeData)) ? $themeData[0]->theme_name : 'Theme' ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <a type="button" class="btn btn-md btn-primary mr-1" href="./new_theme"><span><i class="fas fa-plus fa-lg mr-2"></i></span>New Theme</a> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- Main row -->
        <div class="row">
          <div class="col-lg-12 col-6 col-6 mb-4">
            <ul class="list-group list-group-horizontal">
              <li class="list-group-item-custom border-left-0">
                <a data-toggle="collapse" href="#collapseDetails" role="button" aria-expanded="false" aria-controls="collapseDetails"><span class="btn btn-primary btn-md" id="detailsCollapseControl">Details</span></a>
              </li>
              <li class="list-group-item-custom border-left-0">
                <a data-toggle="collapse" href="#collapseScreenshot" role="button" aria-expanded="false" aria-controls="collapseScreenshot"><span class="btn btn-default btn-md" id="screenshotCollapseControl">Screenshot</span></a>
              </li>
              <li class="list-group-item-custom border-left-0">
                <a data-toggle="collapse" href="#collapseDocumentation" role="button" aria-expanded="false" aria-controls="collapseExample"><span class="btn btn-default btn-md" id="documentationCollapseControl">Documentation</span></a>
              </li>
            </ul>
          </div>
        </div>        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div class="collapse show" id="collapseDetails">
        <div class="container-fluid">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12 col-6 col-6 mb-4">
                  <div class="span4"></div>
                  <div class="span4"><img data-src="holder.js/100px250" class="center-block"  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%221151%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%201151%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17aa46f1d3e%20text%20%7B%20fill%3A%23AAAAAA%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A58pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17aa46f1d3e%22%3E%3Crect%20width%3D%221151%22%20height%3D%22250%22%20fill%3D%22%23EEEEEE%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22405.6015625%22%20y%3D%22150.8%22%3E1151x250%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"></div>
                  <div class="span4"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <p>Adden on <b><?php echo (isset($themeData[0]->added_date) && !empty($themeData[0]->added_date)) ? date('m D,Y',strtotime($themeData[0]->added_date)) : '' ?></b></p>
                  <p>Current Version: <b><?php echo (isset($themeData[0]->current_version)) ? $themeData[0]->current_version : '' ?></b></p>
                  <p>Licence: <b><?php echo (isset($themeData[0]->licence) && !empty($themeData[0]->licence)) ? $themeData[0]->licence : '' ?></b></p>
                  <p>Created By: <b><?php echo (isset($themeData[0]->creator) && !empty($themeData[0]->creator)) ? $themeData[0]->creator : '' ?></b></p>

                </div>
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <h4><div class="title-right">
                    <?php 
                      $editThemeUrl = './edit_theme?themeId='.$themeData[0]->id.'&act='.substr(md5('edit-theme-'.$themeData[0]->id.'-'.$_SESSION['token']), 5, 15);

                     ?>
                    <a href="" type="button" class="btn btn-primary"><i class="fa fa-download pl-2 pr-2" aria-hidden="true"></i> Install</a>
                    <a href="<?php echo $editThemeUrl ?>" type="button" class="btn btn-primary"><i class="fa fa-edit pl-2 pr-2" aria-hidden="true"></i> Edit </a>

                  </div></h4>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h1 class="card-title"><b>Description</b></h1>
            </div>
            <div class="card-body">
              <div class="row mb-2">
                <div class="col-md-12">
                  <p><?php echo (isset($themeData[0]->description) && !empty($themeData[0]->description)) ? $themeData[0]->description : '' ?></p>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h1 class="card-title"><b>Main Features</b></h1>
            </div>
            <div class="card-body">
              <div class="row mb-2">
                <div class="col-md-12">
                  <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">- List group item heading</h5>
                      </div>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">- List group item heading</h5>
                      </div>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">- List group item heading</h5>
                      </div>
                    </a>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div>
          </div>
        </div>
      </div>
      <div class="collapse" id="collapseScreenshot">
        <div class="container-fluid">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <img src="<?php echo ASSETS_URL.'images/img1.webp' ?>">
                </div>
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <img src="<?php echo ASSETS_URL.'images/img1.webp' ?>">
                </div>
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <img src="<?php echo ASSETS_URL.'images/img1.webp' ?>">
                </div>
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <img src="<?php echo ASSETS_URL.'images/img1.webp' ?>">
                </div>
                <div class="col-lg-6 col-6 col-6 mb-4">
                  <img src="<?php echo ASSETS_URL.'images/img1.webp' ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="collapse" id="collapseDocumentation">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header">
              <h1 class="card-title"><b>Side Menu Bar</b></h1>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12 col-6 col-6 mb-4">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  <img data-src="holder.js/100px250" class="center-block"  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%221151%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%201151%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17aa46f1d3e%20text%20%7B%20fill%3A%23AAAAAA%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A58pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17aa46f1d3e%22%3E%3Crect%20width%3D%221151%22%20height%3D%22250%22%20fill%3D%22%23EEEEEE%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22405.6015625%22%20y%3D%22150.8%22%3E1151x250%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E">
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h1 class="card-title"><b>Carousel</b></h1>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12 col-6 col-6 mb-4">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  <img data-src="holder.js/100px250" class="center-block"  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%221151%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%201151%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17aa46f1d3e%20text%20%7B%20fill%3A%23AAAAAA%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A58pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17aa46f1d3e%22%3E%3Crect%20width%3D%221151%22%20height%3D%22250%22%20fill%3D%22%23EEEEEE%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22405.6015625%22%20y%3D%22150.8%22%3E1151x250%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E">
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- <script type="text/javascript">
  var calendar = $('#calendar').fullCalendar('getCalendar');

calendar.on('dayClick', function(date, jsEvent, view) {
  console.log('clicked on ' + date.format());
})
</script> -->
<?php 
$scripts = '
<script src="'.ASSETS_URL.'dist/js/pages/dashboard.js"></script>
<script src="'.VENDOR_URL.'/chart.js/Chart.min.js"></script>
<script src="'.ASSETS_URL.'js/theme-details.js"></script>';
include '../inc/footer.php';
?>
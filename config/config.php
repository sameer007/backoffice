<?php 
ob_start();
session_start();
header("Access-Control-Allow-Origin: *");
if($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == "::1"){
	define('ENVIRONMENT','DEVELOPMENT');
} else {
	define('ENVIRONMENT','PRODUCTION');
}
// echo 'Hello';
// exit;

if(ENVIRONMENT == "DEVELOPMENT"){
	error_reporting(E_ALL);
	define('ROOT','/backoffice');
	$siteUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].ROOT.'/';
	$siteUrlArray = array('http://localhost:80/backoffice/','http://127.0.0.1:80/backoffice/');
	if(in_array($siteUrl, $siteUrlArray)){
		define('SITE_URL',$siteUrl);
	}
	define('BASE_URL',SITE_URL.ROOT);
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_NAME', 'backoffice');
	define('DB_PASS', '');
} else {
	error_reporting(E_ALL);
	define('ROOT', '');
	$siteUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].ROOT.'/';
	$siteUrlArray = array('http://5.161.49.229:8002/','http://192.168.50.16:8002/');
	// if(in_array($siteUrl, $siteUrlArray)){
	// 	define('SITE_URL',$siteUrl);
	// }
	define('SITE_URL','http://5.161.49.229:8002/');
	define('BASE_URL',SITE_URL.ROOT);
	define('DB_HOST', '5.161.49.229:3306');
	define('DB_USER', 'sameer');
	define('DB_NAME', 'backoffice');
	define('DB_PASS', 'Techtronix123NP');
}

/*Admin*/
define('URL', SITE_URL.'');
define('ALL_SITE_URL',SITE_URL.'manage-website/site');
define('ASSETS_URL', URL.'assets/');
define('CSS_URL', ASSETS_URL.'css/');
define('JS_URL', ASSETS_URL.'js/');
define('IMAGES_URL', ASSETS_URL.'images/');
define('VENDOR_URL', ASSETS_URL.'plugins/');


define('ALLOWED_EXTENSION',array('jpg','jpeg','png','gif','bmp','docx','doc','txt','xls','xlsx','ppt','pptx','pdf'));
define('ALLOWED_THEME_PHP_EXTENSION',array('php'));


define('SITE_TITLE', 'Back Office');
define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'].ROOT.'/uploads/');
define('THEME_UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'].ROOT.'/uploads/themes/');
define('SITE_DIR', $_SERVER['DOCUMENT_ROOT'].ROOT.'/manage-website/site/');
define('PROCESS_DIR', $_SERVER['DOCUMENT_ROOT'].ROOT.'/manage-website/process/');


define('UPLOAD_URL', SITE_URL.'/uploads');

// if(!is_dir($_SERVER['DOCUMENT_ROOT'].'error')){
// 	mkdir($_SERVER['DOCUMENT_ROOT'].'error', '0777', true);
// }

/*Frontend*/
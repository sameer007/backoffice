<?php 
function calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray){
  if(isset($_POST) && !empty($_POST)){

    if($arrangedContentTypeDetail['inputType'] != 'file'){
      if($arrangedContentTypeDetail['inputType'] == 'url'){
        //debugger('$arrangedContentTypeDetail');

        //debugger($arrangedContentTypeDetail);
        $nameAttrForText = $arrangedContentTypeDetail['nameAttr'].'-text';
        $nameAttrForCheckbox = $arrangedContentTypeDetail['nameAttr'].'-checkbox';
        $nameAttrForUrl = $arrangedContentTypeDetail['nameAttr'].'-url';

        if(isset($_POST[$arrangedContentTypeDetail['nameAttr']]) && !empty($_POST[$arrangedContentTypeDetail['nameAttr']])){
          $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

        }else{
          if(isset($_POST[$nameAttrForText]) && !empty($_POST[$nameAttrForText])){
            $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

          }
          if(isset($_POST[$nameAttrForCheckbox]) && !empty($_POST[$nameAttrForCheckbox])){
            $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

          }
          if(isset($_POST[$nameAttrForUrl]) && !empty($_POST[$nameAttrForUrl])){
            $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

          }
        }
        
      }else{
        if(isset($_POST[$arrangedContentTypeDetail['nameAttr']]) && !empty($_POST[$arrangedContentTypeDetail['nameAttr']])){
          $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

        }else{
          if(isset($_POST[$arrangedContentTypeDetail['nameAttr'].'-canHide']) && !empty($_POST[$arrangedContentTypeDetail['nameAttr'].'-canHide'])){
            $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

          }
        }
      }

    }else{

      if(isset($_FILES[$arrangedContentTypeDetail['nameAttr']]) && !empty($_FILES[$arrangedContentTypeDetail['nameAttr']])){
        $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

      }else{
        if(isset($_POST[$arrangedContentTypeDetail['nameAttr'].'-canHide']) && !empty($_POST[$arrangedContentTypeDetail['nameAttr'].'-canHide'])){
          $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

        }
      }

    }
    return $arrangedRenderFormsData;
  }else{
    $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;
    return $arrangedRenderFormsData;

  }
}
function createArrangedRenderFormData($contentTypeProperties,$contentTypeName,$subContentTypeNameToDisplay = null){
  $arrangedRenderFormsData = array();
  //debugger($contentTypeName);
  // if (strpos($nameAttributeOfCurrentContentType, 'Content') !== false) {
  //   $contentTypeName = 'Content';
  // }
  //
      //debugger($contentTypeProperties);
  
  if(isset($contentTypeProperties[$contentTypeName]) && !empty($contentTypeProperties[$contentTypeName])){

    foreach ($contentTypeProperties[$contentTypeName] as $key => $contentTypePropertiesDetails) { 

      if(gettype($key) != 'integer'){
        if($key == 'props'){
          $currentMainContentTypeName = $contentTypePropertiesDetails[0];
        }
      }
      if(gettype($key) == 'integer'){
        foreach ($contentTypePropertiesDetails as $contentTypePropertiesDetailsKey => $contentTypePropertiesDetailsValue) {
          $currentSubContentType = ucwords(implode(' ',preg_split('/(?=[A-Z])/', $contentTypePropertiesDetailsKey)));
          $currentSubContentTypeProps = implode('-', $contentTypePropertiesDetailsValue['props']);
          $currentSubContentTypeBase = explode('-', $contentTypePropertiesDetailsValue['base']);

          $currentSubContentTypeChildNodes = $contentTypePropertiesDetailsValue['childNodes'];
        }

        //debugger($contentTypePropertiesDetailsKey);
        $contentTag = $currentSubContentTypeBase[array_key_last($currentSubContentTypeBase)];

        $currentSubContentTypePropsArray = explode('-', $currentSubContentTypeProps);
        $inputTypeDataOfCurrentContentType = $currentSubContentTypePropsArray[1];
        $inputTypeDataArrayOfCurrentContentType = explode('*', $inputTypeDataOfCurrentContentType);

        $currentContentTypeNamePropertyDataArray = explode('_', $inputTypeDataArrayOfCurrentContentType[0]);

        if(isset($currentContentTypeNamePropertyDataArray[1]) && !empty($currentContentTypeNamePropertyDataArray[1])){
          $inputTypeOfCurrentContentType = $currentContentTypeNamePropertyDataArray[1];
        }


        $labelOfCurrentContentType = str_replace('_', '-', end($inputTypeDataArrayOfCurrentContentType));

          //debugger($inputTypeDataArrayOfCurrentContentType);
        if(isset($inputTypeDataArrayOfCurrentContentType[1]) && !empty($inputTypeDataArrayOfCurrentContentType[1])){
          if(isset($currentContentTypeNamePropertyDataArray[2]) && !empty($currentContentTypeNamePropertyDataArray[2])){
            $nameAttributeOfCurrentContentType = $currentContentTypeNamePropertyDataArray[2].'-'.$inputTypeDataArrayOfCurrentContentType[1];
          }

          //$labelOfCurrentContentType = str_replace('_', ' ', $inputTypeDataArrayOfCurrentContentType[1]);
        }
        if(isset($inputTypeDataArrayOfCurrentContentType[2]) && !empty($inputTypeDataArrayOfCurrentContentType[2])){
          //debugger($inputTypeDataArrayOfCurrentContentType[2]);
          $inTable = true ;
          $currentKey = $key;

          //$labelOfCurrentContentType = $labelOfCurrentContentType.'-'.$inputTypeDataArrayOfCurrentContentType[2];
          $nameAttributeOfCurrentContentType = $nameAttributeOfCurrentContentType.'_'.$inputTypeDataArrayOfCurrentContentType[2];

          if (strpos($nameAttributeOfCurrentContentType, 'Column') !== false) {
            $nameAttributeOfCurrentContentType = $nameAttributeOfCurrentContentType.'_'.end($inputTypeDataArrayOfCurrentContentType);

          }
        }

        
        //debugger($nameAttributeOfCurrentContentType);
        if(!in_array('CanHide', $currentSubContentTypePropsArray) && !in_array('Multiple', $currentSubContentTypePropsArray)){
          //debugger('no inArray and multiple');
          $multiple = false ;
          $canHide = false ;
        }else{
          if(in_array('CanHide', $currentSubContentTypePropsArray)){
          // content type can be hidden

            $canHide = true ;
          }else{
            $canHide = false ;
          }

          if(in_array('Multiple', $currentSubContentTypePropsArray)){
          //content type can be multiple

            $multiple = true ;
          }else{
            $multiple = false ;
          }
        }

        $mainContentTypeDetail = explode('-', $labelOfCurrentContentType);

        $actionArray = array();

        $actionArray['canHide'] = $canHide;
        $actionArray['multiple'] = $multiple;

        $arrangedContentTypeData = array();
        $contentTypeName = $labelOfCurrentContentType;

        

        //debugger($currentSubContentTypeChildNodes);
        if($inputTypeOfCurrentContentType == 'file'){

          //$arr = $currentSubContentTypeChildNodes->xpath("//img/@src");

          //$imgKey = returnKeyOfXmlDoc($arr,$currentSubContentTypeProps);

            //debugger($arr);


          if(isset($currentSubContentTypeChildNodes) && !empty($currentSubContentTypeChildNodes)){

            $imageXmlEl = $currentSubContentTypeChildNodes->xpath("//*[contains(@class, '$currentSubContentTypeProps')]")[0];

            $src =  (string)$imageXmlEl->attributes()['src'];
            $allClassName =  (string)$imageXmlEl->attributes()['class'];
            //$src = (string)$currentSubContentTypeChildNodes->xpath("//img/@src")[$imgKey];
            //$src = (string)$currentSubContentTypeChildNodes->xpath("//img/@src")[$imgKey];

            //$allClassName = (string)$currentSubContentTypeChildNodes->xpath("//img/@src")[$imgKey];

            //debugger($src);

            $path = pathinfo($src);
            //debugger($path);
            if(isset($path['extension'])){
              $ext = $path['extension'];

            }
            //$ext = explode('?', $path['extension'])[0];

            $image_name = $path['filename'].'.'.$ext;

            $arrangedContentTypeDetail['imageName'] = $image_name;
            $arrangedContentTypeDetail['src'] = $src;

            

          }
            //debugger($arrangedContentTypeDetail);

        }elseif($inputTypeOfCurrentContentType == 'text' || $inputTypeOfCurrentContentType == 'textarea'){
          $arr = $currentSubContentTypeChildNodes->xpath("//@class");
          $textKey = returnKeyOfXmlDoc($arr,$currentSubContentTypeProps);
            //debugger($currentSubContentTypePropsArray[0]);

          if(isset($textKey) && !empty($textKey)){

            $allClassName = (string)$currentSubContentTypeChildNodes->xpath("//@class")[$textKey];


            if (isset($allClassName) && !empty($allClassName)) {
              $currentContentTypeXml = $currentSubContentTypeChildNodes->xpath("//*[contains(@class, '$allClassName')]")[0];

              //$textArray = array();
            //debugger($currentContentTypeXml);
              if (isset($currentContentTypeXml) && !empty($currentContentTypeXml)) {

                $textArray = simpleXMLElement_innerXML($currentContentTypeXml);
                //debugger('$textArray');

               // debugger($nameAttributeOfCurrentContentType);

                if (isset($textArray) && !empty($textArray)) {
                  $arrangedContentTypeDetail['textArray'] = $textArray ;

                  foreach ($textArray as $key => $textArrayItems) {
                    if(strlen(ltrim($textArrayItems))<=0){
                      $textArray[$key] = null ;
                    }else{
                      $textArray[$key] = ltrim(rtrim($textArrayItems)) ;
                    }
                  }
                //debugger($textArray);

                  $arrangedContentTypeDetail['textArray'] = $textArray ;

                  unset($textArray);
                }
              }
            }
          }



        }elseif($inputTypeOfCurrentContentType == 'url'){
          //debugger($currentSubContentTypeChildNodes);
          $nameAttributeOfCurrentContentTypeArr = explode('_', $nameAttributeOfCurrentContentType);

          //$lastNameAttributeIdArr = 
          $arr = $currentSubContentTypeChildNodes->xpath("//@class");

          //debugger($arr);
          $hrefKey = returnKeyOfXmlDoc($arr,$currentSubContentTypeProps);

          //debugger($hrefKey);
          $allClassName = (string)$currentSubContentTypeChildNodes->xpath("//@class")[$hrefKey];

          $currentContentTypeXml = $currentSubContentTypeChildNodes->xpath("//*[contains(@class, '$allClassName')]")[0];

          $subContentTypeName = explode('-', $currentSubContentTypeProps)[0];
          //debugger($subContentTypeName);

          if (isset($currentContentTypeXml) && !empty($currentContentTypeXml)) {



            $classNames = $currentContentTypeXml->attributes()['class'][0];
            //debugger($classNames);
            if($subContentTypeName == 'SocialbarItem' || $subContentTypeName == 'NavbarItem' || $subContentTypeName == 'VideoUrl'){
              $labelName = explode(' ', $classNames)[0];

              if($subContentTypeName == 'SocialbarItem' || $subContentTypeName == 'NavbarItem'){
                $currentContentTypeXmlChildren = $currentContentTypeXml->children();

                $currentContentTypeXmlDom = dom_import_simplexml($currentContentTypeXml);
                //debugger($currentContentTypeXmlDom);

                if($currentContentTypeXmlDom->tagName == 'a'){
                  $textArray = $currentContentTypeXmlDom->textContent;  
                  $href =  $currentContentTypeXmlDom->getAttribute('href');
                  $target =  $currentContentTypeXmlDom->getAttribute('target');
                }else{
                  $currentContentTypeXmlChildrenDom = dom_import_simplexml($currentContentTypeXmlChildren);

                  if($currentContentTypeXmlChildrenDom->tagName == 'a'){
                    $textArray = $currentContentTypeXmlChildrenDom->textContent;  
                    $href =  $currentContentTypeXmlChildrenDom->getAttribute('href');
                    $target =  $currentContentTypeXmlChildrenDom->getAttribute('target');
                  }
                }

                if($subContentTypeName == 'SocialbarItem'){
                  $arrangedContentTypeDetail['textArray'] = null ;
                }
              }
              //debugger($href);
              if(isset($href) && !empty($href)){
                $arrangedContentTypeDetail['href'] = $href ;

              }

              if(isset($textArray) && !empty($textArray)){

                if(strlen(trim($textArray))<=0){
                  $arrangedContentTypeDetail['textArray'] = null ;

                }else{
                  $arrangedContentTypeDetail['textArray'] = $textArray ;


                }

              }else{
                $arrangedContentTypeDetail['textArray'] = null ;

              }
              if(isset($target) && !empty($target) && $target == '_blank'){
                $arrangedContentTypeDetail['target'] = $target ;

              }else{
                $arrangedContentTypeDetail['target'] = null ;

              }
              if(isset($labelName) && !empty($labelName)){

                if(isset($arrangedContentTypeDetail['textArray'])){
                  //unset($arrangedContentTypeDetail['textArray']);
                }
                $arrangedContentTypeDetail['labelName'] = $labelName ;

              }

              if($subContentTypeName == 'NavbarItem'){
                $textArray = simpleXMLElement_innerXML($currentContentTypeXml->children());
              //debugger($textArray);
                $arrangedContentTypeDetail['textArray'] = $textArray ;
              }elseif($subContentTypeName == 'SocialbarItem'){
                $href = $currentContentTypeXml->attributes()['href'][0];

                if(isset($href) && !empty($href)){
                  if(isset($arrangedContentTypeDetail['textArray'])){
                    //unset($arrangedContentTypeDetail['textArray']);
                  }
                //debugger($arrangedContentTypeDetail);

                  $arrangedContentTypeDetail['href'] = $href ;

                }
              }elseif($subContentTypeName == 'VideoUrl'){
                $src = $currentContentTypeXml->attributes()['src'][0];
                //debugger($src);

                if(isset($src) && !empty($src)){
                  if(isset($arrangedContentTypeDetail['textArray'])){
                    //unset($arrangedContentTypeDetail['textArray']);
                  }
                  //debugger($arrangedContentTypeDetail);

                  $arrangedContentTypeDetail['src'] = $src ;

                }
              }

            }


          }

        }
        


        if(!$canHide && !$multiple){ 
          //debugger('outside table');
          /* creating data to render form outside table*/

          $arrangedContentTypeDetail['label'] = $labelOfCurrentContentType;
          $arrangedContentTypeDetail['inputType'] = $inputTypeOfCurrentContentType;
          $arrangedContentTypeDetail['subContentType'] = $currentSubContentType;
          $arrangedContentTypeDetail['nameAttr'] = $nameAttributeOfCurrentContentType;
          if (isset($allClassName) && !empty($allClassName)) {
            $arrangedContentTypeDetail['allClassName'] = $allClassName;

            $currentXmlWithIndex = $currentSubContentTypeChildNodes->xpath(".//*[contains(@class, '$allClassName')]");
          }


          $arrangedContentTypeDetail['action'] = $actionArray;
                  //if (strpos($subContentTypeNameToDisplay[1], 'Content') !== false) {
          //debugger($currentSubContentTypePropsArray);
          
          if(strpos($subContentTypeNameToDisplay, 'Content') !== false) {
                  //if (strpos($subContentTypeNameToDisplay[1], 'Column') !== false) {
            $arrangedRenderFormsData = calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray);
  //debugger('$arrangedRenderFormsData');

                      //debugger($arrangedRenderFormsData);
            //$arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

                  //}
          }else{

            if(isset($subContentTypeNameToDisplay) && !empty($subContentTypeNameToDisplay)){
                  //debugger($currentSubContentTypePropsArray);
                  //debugger($arrangedContentTypeDetail);

              if (strpos($currentSubContentTypePropsArray[1],$subContentTypeNameToDisplay) !== false) {

                $arrangedRenderFormsData = calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray);
                
                
                

              }
            }
            
          }
            //$arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

        }else{
          if($canHide && !$multiple){
            /* creating data to render form outside table*/
            //debugger('outside table 2');

            $arrangedContentTypeDetail['label'] = $labelOfCurrentContentType;
            $arrangedContentTypeDetail['inputType'] = $inputTypeOfCurrentContentType;
            $arrangedContentTypeDetail['subContentType'] = $currentSubContentType;
            $arrangedContentTypeDetail['nameAttr'] = $nameAttributeOfCurrentContentType;
            if (isset($allClassName) && !empty($allClassName)) {
              $arrangedContentTypeDetail['allClassName'] = $allClassName;
            }


            $arrangedContentTypeDetail['action'] = $actionArray;
            if(explode('-', $subContentTypeNameToDisplay)[0] == 'Content'){
                  //if (strpos($currentSubContentTypePropsArray[1], 'Column') !== false) {
                  //debugger(1)
              $arrangedRenderFormsData = calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray);
              
              //debugger($arrangedContentTypeDetail);

                  //}
            }else{
              if(isset($subContentTypeNameToDisplay) && !empty($subContentTypeNameToDisplay)){

                if (strpos($currentSubContentTypePropsArray[1], (string)$subContentTypeNameToDisplay) !== false) {
                  $arrangedRenderFormsData = calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray);

                }
              }
            }
                        //$arrangedRenderFormsData[$currentSubContentTypePropsArray[0]][] = $arrangedContentTypeDetail;

          }else{

            /* creating data to render form inside table*/
                        //debugger('inside table');

            $arrangedContentTypeDetail['label'] = $labelOfCurrentContentType;
            $arrangedContentTypeDetail['inputType'] = $inputTypeOfCurrentContentType;
            $arrangedContentTypeDetail['subContentType'] = $currentSubContentType;
            $arrangedContentTypeDetail['nameAttr'] = $nameAttributeOfCurrentContentType;
            if (isset($allClassName) && !empty($allClassName)) {
              $arrangedContentTypeDetail['allClassName'] = $allClassName;
            }


            $arrangedContentTypeDetail['action'] = $actionArray;
                        //debugger($mainContentTypeDetail);

                        //if(isset(end($mainContentTypeDetail)) && !empty(end($mainContentTypeDetail))){

            if(end($mainContentTypeDetail) == 1){
              $arrangedTableContentTypeArray = array();
            }
            if(isset($arrangedTableContentTypeArray)){

              //debugger($arrangedTableContentTypeArray);
              $arrangedTableContentTypeArray['tableData'][] = $arrangedContentTypeDetail;

              if($subContentTypeNameToDisplay == explode('-', $contentTypeName)[0]){
                $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]] = $arrangedTableContentTypeArray;

              }


              if(explode('-', $subContentTypeNameToDisplay)[0] == 'Content'){
                if (strpos($currentSubContentTypePropsArray[1], 'Column') !== false) {
                        //debugger(1)

                  $arrangedRenderFormsData = calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray);

                  $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]] = $arrangedTableContentTypeArray;

                }
              }else{
                if(isset($subContentTypeNameToDisplay) && !empty($subContentTypeNameToDisplay)){

                  if (strpos($currentSubContentTypePropsArray[1], (string)$subContentTypeNameToDisplay) !== false) {
                    $arrangedRenderFormsData = calibratePostDataForArrangedRenderFormData($arrangedRenderFormsData,$arrangedContentTypeDetail,$currentSubContentTypePropsArray);
                    
                    $arrangedRenderFormsData[$currentSubContentTypePropsArray[0]] = $arrangedTableContentTypeArray;

                  }
                }
              }
                        //$arrangedRenderFormsData[$currentSubContentTypePropsArray[0]] = $arrangedTableContentTypeArray;

            }
                        //}

          }

        }

      } 

    }
  }
  //debugger($arrangedRenderFormsData);
  
  if(isset($_POST) && !empty($_POST)){

    foreach ($arrangedRenderFormsData as $arrangedRenderFormsDataKey => $arrangedRenderFormDataList) {
                        //debugger($arrangedRenderFormDataList);
      if(isset($arrangedRenderFormDataList['tableData']) && !empty($arrangedRenderFormDataList['tableData'])){
        // foreach ($arrangedRenderFormDataList['tableData'] as $tableDataKey => $tableDataItems) {
        // }
        $maxItemsOfContentTypeInsideStaticPage = max(array_keys($arrangedRenderFormDataList['tableData'])) + 1 ;
        $newItemIdOfContentTypeForStaticPage = $maxItemsOfContentTypeInsideStaticPage + 1 ;

        $nameAttrArrayOfContentTypeInsideStaticPage = array_column($arrangedRenderFormDataList['tableData'], 'nameAttr');
        $inputTypeArrayOfContentTypeInsideStaticPage = array_column($arrangedRenderFormDataList['tableData'], 'inputType');



        if($inputTypeArrayOfContentTypeInsideStaticPage[0] == 'url'){
          $newArrangedRenderFormDataList['tableData'] = $arrangedRenderFormDataList['tableData'];

          $currentNewItemIdOfContentTypeForStaticPage = $newItemIdOfContentTypeForStaticPage;

          do{

            $newNameAttrOfContentTypeForStaticPage = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $nameAttrArrayOfContentTypeInsideStaticPage[0]);

            $nameAttrForText = $newNameAttrOfContentTypeForStaticPage.'-text';
            $nameAttrForOpenInNewTab = $newNameAttrOfContentTypeForStaticPage.'-checkbox';
            $nameAttrForHref = $newNameAttrOfContentTypeForStaticPage.'-url';
                        //debugger($newNameAttrOfContentTypeForStaticPage);

            if((isset($_POST[$nameAttrForText]) && !empty($_POST[$nameAttrForText])) || (isset($_POST[$nameAttrForOpenInNewTab]) && !empty($_POST[$nameAttrForOpenInNewTab])) || (isset($_POST[$nameAttrForHref]) && !empty($_POST[$nameAttrForHref]))){

              $arrangedContentTypeDetail = array();
              $arrangedContentTypeDetail['label'] = str_replace('1', $currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['label']);
              $arrangedContentTypeDetail['inputType'] = $arrangedRenderFormDataList['tableData'][0]['inputType'];
              $arrangedContentTypeDetail['subContentType'] = $arrangedRenderFormDataList['tableData'][0]['subContentType'];
              $arrangedContentTypeDetail['nameAttr'] = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['nameAttr']);
              $arrangedContentTypeDetail['allClassName'] = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['allClassName']);
              $arrangedContentTypeDetail['action'] = $arrangedRenderFormDataList['tableData'][0]['action'];
              $arrangedContentTypeDetail['labelName'] = $arrangedRenderFormDataList['tableData'][0]['labelName'];
              $arrangedContentTypeDetail['addToDomEl'] = true ;

              if(isset($_POST[$nameAttrForHref]) && !empty($_POST[$nameAttrForHref])){
                $arrangedContentTypeDetail['href'] = sanitize($_POST[$nameAttrForHref]);
              }
              if(isset($_POST[$nameAttrForOpenInNewTab]) && !empty($_POST[$nameAttrForOpenInNewTab])){
                $openInNewTabVal = sanitize($_POST[$nameAttrForOpenInNewTab]);

                        //debugger($openInNewTabVal);
                if(isset($openInNewTabVal) && !empty($openInNewTabVal)){
                  $arrangedContentTypeDetail['target'] = '_blank';
                }
              }
              if(isset($_POST[$nameAttrForText]) && !empty($_POST[$nameAttrForText])){
                $textVal = sanitize($_POST[$nameAttrForText]);
                $arrangedContentTypeDetail['textArray'] = array($textVal);

              }
                        //debugger($arrangedContentTypeDetail);
              array_push($newArrangedRenderFormDataList['tableData'], $arrangedContentTypeDetail);
              $currentNewItemIdOfContentTypeForStaticPage++ ;
                        //debugger($currentNewItemIdOfContentTypeForStaticPage);
            }else{
              $currentNewItemIdOfContentTypeForStaticPage = null ;
            }
          }while(isset($currentNewItemIdOfContentTypeForStaticPage) && !empty($currentNewItemIdOfContentTypeForStaticPage));



        }elseif($inputTypeArrayOfContentTypeInsideStaticPage[0] == 'text' || $inputTypeArrayOfContentTypeInsideStaticPage[0] == 'textarea'){
          $newArrangedRenderFormDataList['tableData'] = $arrangedRenderFormDataList['tableData'];

          $currentNewItemIdOfContentTypeForStaticPage = $newItemIdOfContentTypeForStaticPage;
                        //debugger($currentNewItemIdOfContentTypeForStaticPage);

          do{

            $newNameAttrOfContentTypeForStaticPage = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $nameAttrArrayOfContentTypeInsideStaticPage[0]);

            if(isset($_POST[$newNameAttrOfContentTypeForStaticPage]) && !empty($_POST[$newNameAttrOfContentTypeForStaticPage])){
              $textVal = sanitize($_POST[$newNameAttrOfContentTypeForStaticPage]);

              $arrangedContentTypeDetail = array();
              $arrangedContentTypeDetail['textArray'] = array($textVal);

              $arrangedContentTypeDetail['label'] = str_replace('1', $currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['label']);
              $arrangedContentTypeDetail['inputType'] = $arrangedRenderFormDataList['tableData'][0]['inputType'];
              $arrangedContentTypeDetail['subContentType'] = $arrangedRenderFormDataList['tableData'][0]['subContentType'];
              $arrangedContentTypeDetail['nameAttr'] = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['nameAttr']);
              $arrangedContentTypeDetail['allClassName'] = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['allClassName']);
              $arrangedContentTypeDetail['action'] = $arrangedRenderFormDataList['tableData'][0]['action'];
              $arrangedContentTypeDetail['addToDomEl'] = true ;

              array_push($newArrangedRenderFormDataList['tableData'], $arrangedContentTypeDetail);
              $currentNewItemIdOfContentTypeForStaticPage++ ;

            }else{
              $currentNewItemIdOfContentTypeForStaticPage = null ;
            }
          }while(isset($currentNewItemIdOfContentTypeForStaticPage) && !empty($currentNewItemIdOfContentTypeForStaticPage));


        }elseif($inputTypeArrayOfContentTypeInsideStaticPage[0] == 'file'){
          $newArrangedRenderFormDataList['tableData'] = $arrangedRenderFormDataList['tableData'];

          $currentNewItemIdOfContentTypeForStaticPage = $newItemIdOfContentTypeForStaticPage;

          do{

            $newNameAttrOfContentTypeForStaticPage = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $nameAttrArrayOfContentTypeInsideStaticPage[0]);

            if(isset($_FILES[$newNameAttrOfContentTypeForStaticPage]) && !empty($_FILES[$newNameAttrOfContentTypeForStaticPage]) && $_FILES[$newNameAttrOfContentTypeForStaticPage]['error'] == 0){

                        //debugger($_FILES[$newNameAttrOfContentTypeForStaticPage]);
                        //debugger($arrangedRenderFormDataList['tableData'][0]);

              $arrangedContentTypeDetail = array();
              $arrangedContentTypeDetail['imageName'] = null ;
              $arrangedContentTypeDetail['src'] = null ;

              $arrangedContentTypeDetail['label'] = str_replace('1', $currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['label']);
              $arrangedContentTypeDetail['inputType'] = $arrangedRenderFormDataList['tableData'][0]['inputType'];
              $arrangedContentTypeDetail['subContentType'] = $arrangedRenderFormDataList['tableData'][0]['subContentType'];
              $arrangedContentTypeDetail['nameAttr'] = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['nameAttr']);
              $arrangedContentTypeDetail['allClassName'] = str_replace('_1', '_'.$currentNewItemIdOfContentTypeForStaticPage, $arrangedRenderFormDataList['tableData'][0]['allClassName']);
              $arrangedContentTypeDetail['action'] = $arrangedRenderFormDataList['tableData'][0]['action'];
              $arrangedContentTypeDetail['addToDomEl'] = true ;

                        //debugger($arrangedContentTypeDetail);
              array_push($newArrangedRenderFormDataList['tableData'], $arrangedContentTypeDetail);
              $currentNewItemIdOfContentTypeForStaticPage++ ;

            }else{
              $currentNewItemIdOfContentTypeForStaticPage = null ;
            }
          }while(isset($currentNewItemIdOfContentTypeForStaticPage) && !empty($currentNewItemIdOfContentTypeForStaticPage));
                        //debugger($arrangedRenderFormDataList['tableData']);

        }

                        //debugger($arrangedRenderFormDataList['tableData']);
        $arrangedRenderFormDataList['tableData'] = $newArrangedRenderFormDataList['tableData'];

      }
    }

    if(isset($arrangedRenderFormDataList) && !empty($arrangedRenderFormDataList)){
      $arrangedRenderFormsData[$arrangedRenderFormsDataKey] = $arrangedRenderFormDataList;

    }
    //debugger('$arrangedRenderFormsData');

    //debugger($arrangedRenderFormsData);

    return $arrangedRenderFormsData;
  }else{
                        //debugger($arrangedRenderFormsData);

    return $arrangedRenderFormsData;
  }

}

?>

<?php function renderFORMS($contentTypeProperties,$contentTypeName,$webpages = null,$subContentTypeNameToDisplay = null){ ?>
  <?php 

  $arrangedRenderFormData = createArrangedRenderFormData($contentTypeProperties,$contentTypeName,$subContentTypeNameToDisplay);
  /*arranging all data into main array before render*/
  //debugger($arrangedRenderFormData);
  ?>
  <?php if(isset($arrangedRenderFormData) && !empty($arrangedRenderFormData)){ ?>
    <?php foreach ($arrangedRenderFormData as $arrangedRenderFormDataKey => $arrangedRenderFormDataItems) { 
                        //debugger($arrangedRenderFormDataItems);
      ?>
      <?php if(isset($arrangedRenderFormDataItems['tableData']) && !empty($arrangedRenderFormDataItems['tableData'])){ 
        //debugger($arrangedRenderFormDataItems['tableData']);

        $inputType = $arrangedRenderFormDataItems['tableData'][0]['inputType'];
        $label = explode('-', $arrangedRenderFormDataItems['tableData'][0]['label'])[0];
        //debugger($arrangedRenderFormDataItems['tableData']);
        ?>
        <label for="themeName"><?php echo $label ?></label>

        <!-- display table -->
        <?php if($inputType == 'file'){ ?>
          <table class="table table-border table-xs">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Input</th>
                <th scope="col">File Image</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($arrangedRenderFormDataItems['tableData'] as $arrangedRenderFormDataItemsKey => $contentTypeTableDataItems) {  
                ?>
                <tr>
                  <th scope="row"><?php echo ($arrangedRenderFormDataItemsKey + 1) ?></th>
                  <td><input type="file" class="form-control-file imageFileInput" name="<?php echo $contentTypeTableDataItems['nameAttr'] ?>"></td>
                  <!-- <td><?php echo $contentTypeTableDataItems['imageName']; ?></td> -->
                  <td><img height="50px" width="auto" src="<?php echo $contentTypeTableDataItems['src']; ?>"></td>
                  <td>
                    <?php if($contentTypeTableDataItems['action']['canHide']){ ?>
                      <button type="button" class="btn btn default isCanHide"><i class="fas fa-eye-slash"></i></button>
                      <button type="button" class="btn btn default isCanShow" style="display: none"><i class="fas fa-eye"></i></button>
                    <?php } ?>
                    <?php if($contentTypeTableDataItems['action']['multiple']){ ?>
                      <button type="button" class="btn btn default isMultiple"><i class="fas fa-plus"></i></button>

                      <?php if($arrangedRenderFormDataItemsKey > 0){ ?>
                        <button type="button" class="btn btn default isRemoved" ><i class="fas fa-minus"></i></button>
                      <?php }else{ ?>
                        <button type="button" class="btn btn default isRemoved" style="display: none"><i class="fas fa-minus"></i></button>
                      <?php } ?>
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>

            </tbody>
          </table>
        <?php }elseif($inputType == 'text'){ ?>
          <table class="table table-border table-xs">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Input</th>
                <th scope="col">Heading Name</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($arrangedRenderFormDataItems['tableData'] as $arrangedRenderFormDataItemsKey => $contentTypeTableDataItems) {  
                        //debugger($contentTypeTableDataItems['textArray']);
                ?>
                <tr>
                  <td scope="row"><?php echo ($arrangedRenderFormDataItemsKey + 1) ?></td>
                  <td><input type="text" name="<?php echo $contentTypeTableDataItems['nameAttr'] ?>" value="<?php echo $contentTypeTableDataItems['textArray'][0] ?>"></td>
                  <td><?php echo $contentTypeTableDataItems['label'];  ?></td>
                  <td>
                    <?php if($contentTypeTableDataItems['action']['canHide']){ ?>
                      <button type="button" class="btn btn default isCanHide"><i class="fas fa-eye-slash"></i></button>
                      <button type="button" class="btn btn default isCanShow" style="display: none"><i class="fas fa-eye"></i></button>
                    <?php } ?>
                    <?php if($contentTypeTableDataItems['action']['multiple']){ ?>
                      <button type="button" class="btn btn default isMultiple"><i class="fas fa-plus"></i></button>

                      <?php if($arrangedRenderFormDataItemsKey > 0){ ?>
                        <button type="button" class="btn btn default isRemoved" ><i class="fas fa-minus"></i></button>
                      <?php }else{ ?>
                        <button type="button" class="btn btn default isRemoved" style="display: none"><i class="fas fa-minus"></i></button>
                      <?php } ?>
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        <?php }elseif($inputType == 'url'){ ?>
          <table class="table table-border table-xs">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Input</th>
                <th scope="col">Heading Name</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($arrangedRenderFormDataItems['tableData'] as $arrangedRenderFormDataItemsKey => $contentTypeTableDataItems) {  ?>
                <tr>
                  <td scope="row"><?php echo ($arrangedRenderFormDataItemsKey + 1) ?></td>
                  <td>
                    <?php if(isset($contentTypeTableDataItems['textArray']) && !empty($contentTypeTableDataItems['textArray'])){ ?>
                      <table class="col-lg-9">
                        <tr>
                          <td>Text: </td>
                          <td><input type="text" name="<?php echo $contentTypeTableDataItems['nameAttr'].'-text' ?>" value="<?php echo $contentTypeTableDataItems['textArray'][0] ?>" class="form-control"></td>
                        </tr>
                        <tr>
                          <td>Redirect: </td>
                          <td><input type="checkbox" name="<?php echo $contentTypeTableDataItems['nameAttr'].'-checkbox' ?>" <?php echo (isset($contentTypeTableDataItems['target']) && !empty($contentTypeTableDataItems['target'])) ? 'checked' : '' ?>>&nbsp; Open in new Tab</td>
                        </tr>
                        <tr>
                          <td>Url: </td>
                          <td>
                            <input list="pageNames" value="<?php echo $contentTypeTableDataItems['href'] ?>" name="<?php echo $contentTypeTableDataItems['nameAttr'].'-url' ?>" class="form-control">
                            <datalist id="pageNames">
                              <?php if(isset($webpages) && !empty($webpages)){ ?>
                                <?php foreach ($webpages as $webpagesKey => $currentWebpageName) {
                                  $pageName = '#'.explode('.', $currentWebpageName)[0];

                                  ?>
                                  <option value="<?php echo $pageName ?>"><?php echo $pageName ?></option>
                                <?php } ?>
                              <?php } ?>
                            </datalist>
                          </td>
                        </tr>
                      </table>
                    <?php }else{ ?>
                      <input type="text" class="form-control" name="<?php echo $contentTypeTableDataItems['nameAttr'] ?>" value="<?php echo $contentTypeTableDataItems['href'][0] ?>" placeholder="<?php echo $contentTypeTableDataItems['subContentType'] ?>">
                    <?php } ?>
                  </td>
                  <td><?php echo $contentTypeTableDataItems['label'];  ?></td>
                  <td>
                    <?php if($contentTypeTableDataItems['action']['canHide']){ ?>
                      <button type="button" class="btn btn default isCanHide"><i class="fas fa-eye-slash"></i></button>
                      <button type="button" class="btn btn default isCanShow" style="display: none"><i class="fas fa-eye"></i></button>
                    <?php } ?>
                    <?php if($contentTypeTableDataItems['action']['multiple']){ ?>
                      <button type="button" class="btn btn default isMultiple"><i class="fas fa-plus"></i></button>

                      <?php if($arrangedRenderFormDataItemsKey > 0){ ?>
                        <button type="button" class="btn btn default isRemoved" ><i class="fas fa-minus"></i></button>
                      <?php }else{ ?>
                        <button type="button" class="btn btn default isRemoved" style="display: none"><i class="fas fa-minus"></i></button>
                      <?php } ?>
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        <?php }elseif($inputType == 'textarea'){ ?>
          <table class="table table-border table-xs">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Input</th>
                <th scope="col">Heading Name</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($arrangedRenderFormDataItems['tableData'] as $arrangedRenderFormDataItemsKey => $contentTypeTableDataItems) {  
                        //debugger(sizeof($arrangedRenderFormDataItems['tableData']));
                ?>
                <tr>
                  <td scope="row"><?php echo ($arrangedRenderFormDataItemsKey + 1) ?></td>
                  <td>
                    <textarea class="form-control" name="<?php echo $contentTypeTableDataItems['nameAttr'] ?>" placeholder="<?php echo $contentTypeTableDataItems['subContentType'] ?>" rows="3" style="overflow-wrap: break-word;">
                      <?php if(isset($contentTypeTableDataItems['textArray']) && !empty($contentTypeTableDataItems['textArray'])){ ?>
                        <?php foreach ($contentTypeTableDataItems['textArray'] as $key => $items) { 
                          displayParagraphText($key,$items,$contentTypeTableDataItems);

                          ?>
                        <?php } ?>
                      <?php } ?>

                    </textarea>
                  </td>
                  <td><?php echo $contentTypeTableDataItems['label'];  ?></td>
                  <td>
                    <?php if($contentTypeTableDataItems['action']['canHide']){ ?>
                      <button type="button" class="btn btn default isCanHide"><i class="fas fa-eye-slash"></i></button>
                      <button type="button" class="btn btn default isCanShow" style="display: none"><i class="fas fa-eye"></i></button>
                    <?php } ?>
                    <?php if($contentTypeTableDataItems['action']['multiple']){ ?>
                      <button type="button" class="btn btn default isMultiple"><i class="fas fa-plus"></i></button>

                      <?php if($arrangedRenderFormDataItemsKey > 0){ ?>
                        <button type="button" class="btn btn default isRemoved" ><i class="fas fa-minus"></i></button>
                      <?php }else{ ?>
                        <button type="button" class="btn btn default isRemoved" style="display: none"><i class="fas fa-minus"></i></button>
                      <?php } ?>
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        <?php } ?>
      <?php }else{ ?>
        <!-- display form without table -->

        <?php 
        $inputType = $arrangedRenderFormDataItems[0]['inputType'];
                        //debugger($arrangedRenderFormDataItems);

        ?>
        <?php foreach ($arrangedRenderFormDataItems as $arrangedRenderFormDataItemsKey => $arrangedRenderFormDataItemsData) { 

          $subContentTypeName =  explode('-', $arrangedRenderFormDataItemsData['label'])[0];
          ?>
          <?php if($subContentTypeName == 'SocialbarItem'){ ?>
            <label for="themeName"><?php echo ucfirst($arrangedRenderFormDataItemsData['labelName']); ?></label>
          <?php }else{ ?>
            <label for="themeName"><?php echo $arrangedRenderFormDataItemsData['label'] ?></label>
          <?php } ?>
          <div class="row">
            <div class="<?php echo ($inputType == 'file') ? 'col-lg-9' : 'col-lg-11' ?>">
              <div class="form-group">
                <?php if($inputType == 'file'){ ?>

                  <input type="file" class="form-control" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'] ?>" placeholder="<?php echo $arrangedRenderFormDataItemsData['subContentType'] ?>">

                <?php }elseif($inputType == 'text'){ ?>
                  <input type="text" class="form-control" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'] ?>" value="<?php echo strip_tags(implode(' ', $arrangedRenderFormDataItemsData['textArray'])) ?>" placeholder="<?php echo $arrangedRenderFormDataItemsData['subContentType'] ?>">

                <?php }elseif($inputType == 'url'){ 
                    //debugger('$arrangedRenderFormDataItemsData');

                    //debugger($arrangedRenderFormDataItemsData);
                  ?>

                  <?php if(trim($arrangedRenderFormDataItemsData['subContentType']) == 'Video Url'){ ?>
                    <input type="url" class="form-control" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'] ?>" value="<?php echo $arrangedRenderFormDataItemsData['src'][0] ?>" placeholder="<?php echo $arrangedRenderFormDataItemsData['subContentType'] ?>">
                  <?php }else{ 
                    ?>
                    <?php if(isset($arrangedRenderFormDataItemsData['textArray']) && !empty($arrangedRenderFormDataItemsData['textArray'])){ ?>
                      <table class="col-lg-10">
                        <tbody>
                          <tr>
                            <td># Text: </td>
                            <td><input type="text" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'].'-text' ?>" value="<?php displayText($arrangedRenderFormDataItemsData['textArray']) ?>" class="form-control"></td>
                          </tr>
                          <tr>
                            <td># Redirect: </td>
                            <td><input type="checkbox" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'].'-checkbox' ?>" <?php echo (isset($arrangedRenderFormDataItemsData['target']) && !empty($arrangedRenderFormDataItemsData['target'])) ? 'checked' : '' ?>>&nbsp; Open in new Tab</td>
                          </tr>
                          <tr>
                            <td># Url: </td>
                            <td>
                              <input list="pageNames" value="<?php echo $arrangedRenderFormDataItemsData['href'] ?>" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'].'-url' ?>" class="form-control">
                              <datalist id="pageNames">
                                <?php if(isset($webpages) && !empty($webpages)){ ?>
                                  <?php foreach ($webpages as $webpagesKey => $currentWebpageName) {
                                    $pageName = '#'.explode('.', $currentWebpageName)[0];

                                    ?>
                                    <option value="<?php echo $pageName ?>"><?php echo $pageName ?></option>
                                  <?php } ?>
                                  <option value="<?php echo $arrangedRenderFormDataItemsData['href'] ?>"><?php echo $arrangedRenderFormDataItemsData['href'] ?></option>

                                <?php } ?>
                              </datalist>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    <?php }else{ ?>
                      <table class="col-lg-10">
                        <tbody>
                          <tr>
                            <td># Url: </td>
                            <td>
                              <input list="pageNames" value="<?php echo $arrangedRenderFormDataItemsData['href'][0] ?>" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'].'-url' ?>" class="form-control">
                              <datalist id="pageNames">
                                <?php if(isset($webpages) && !empty($webpages)){ ?>
                                  <?php foreach ($webpages as $webpagesKey => $currentWebpageName) {
                                    $pageName = '#'.explode('.', $currentWebpageName)[0];

                                    ?>
                                    <option value="<?php echo $pageName ?>"><?php echo $pageName ?></option>
                                  <?php } ?>
                                  <option value="<?php echo $arrangedRenderFormDataItemsData['href'] ?>"><?php echo $arrangedRenderFormDataItemsData['href'] ?></option>

                                <?php } ?>
                              </datalist>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                    <?php } ?>
                  <?php } ?>
                <?php }elseif($inputType == 'textarea'){ 
                        //debugger($arrangedRenderFormDataItemsData);

                  ?>

                  <textarea class="form-control" name="<?php echo $arrangedRenderFormDataItemsData['nameAttr'] ?>" placeholder="<?php echo $arrangedRenderFormDataItemsData['subContentType'] ?>" rows="3" style="overflow-wrap: break-word;">
                    <?php if(isset($arrangedRenderFormDataItemsData['textArray']) && !empty($arrangedRenderFormDataItemsData['textArray'])){ ?>
                      <?php foreach ($arrangedRenderFormDataItemsData['textArray'] as $key => $items) {  ?>
                        <?php   
                        displayParagraphText($key,$items,$arrangedRenderFormDataItemsData);
                        ?>
                      <?php } ?>
                    <?php } ?>

                  </textarea>
                <?php } ?>




              </div>
            </div>
            <?php if($inputType == 'file'){ 
              $arrangedRenderFormDataItemsDataSrcArr = explode('/', $arrangedRenderFormDataItemsData['src']);

                        //DEBUGGER($arrangedRenderFormDataItemsDataSrcArr);
              if($arrangedRenderFormDataItemsDataSrcArr[0] != 'http:' && $arrangedRenderFormDataItemsDataSrcArr[0] != 'https:'){

                if (($key = array_search('.', $arrangedRenderFormDataItemsDataSrcArr)) !== false) {
                  unset($arrangedRenderFormDataItemsDataSrcArr[$key]);
                }
                array_values($arrangedRenderFormDataItemsDataSrcArr);
                $arrangedRenderFormDataItemsData['src'] = implode('/', $arrangedRenderFormDataItemsDataSrcArr);
                        //$arrangedRenderFormDataItemsData['src'] = str_replace('.', '', $arrangedRenderFormDataItemsData['src']);

                $arrangedRenderFormDataItemsData['src'] = ALL_SITE_URL.'/'.$_SESSION['website_id'].'/'.$arrangedRenderFormDataItemsData['src'];
                        //debugger($arrangedRenderFormDataItemsDataSrcArr);


              }



              ?>
              <div class="col-lg-2">
                <img height="50px" width="auto" src="<?php echo $arrangedRenderFormDataItemsData['src']; ?>">
              </div>
            <?php } ?>
            <?php if($arrangedRenderFormDataItemsData['action']['canHide']){ ?>
              <div class="col-lg-1">
                <?php if($arrangedRenderFormDataItemsData['action']['canHide']){ ?>
                  <button type="button" class="btn btn default isCanHide"><i class="fas fa-eye-slash"></i></button>
                  <button type="button" class="btn btn default isCanShow" style="display: none"><i class="fas fa-eye"></i></button>
                <?php } ?>
              </div>
            <?php } ?>
          </div>
        <?php } ?>
      <?php } ?>
    <?php } ?>
  <?php } ?>
<?php } ?>

<?php 
function changeWebpageByInputType($inputType,$currentXml,$nameAttr,$subContentType,$arrangedRenderFormDataListItems){
  $currentXmlDom = dom_import_simplexml($currentXml);
  //debugger(787);
  ////debugger($_FILES[$nameAttr]);

  if($inputType == 'url'){
    //debugger($subContentType);
    if($subContentType == 'VideoUrl'){

      $srcVal = sanitize($_POST[$nameAttr]);

      $currentXmlDom->setAttribute('src',$srcVal);

    }elseif($subContentType == 'NavbarItem'){
      $nameAttrForText = $nameAttr.'-text';
      $nameAttrForOpenInNewTab = $nameAttr.'-checkbox';
      $nameAttrForHref = $nameAttr.'-url';

                        //debugger($currentXml);
      $anchorTagElDom = $currentXmlDom->getElementsByTagName('a');

      if(isset($_POST[$nameAttrForText]) && !empty($_POST[$nameAttrForText])){
                        //debugger($nameAttrForText);

        $textVal = sanitize($_POST[$nameAttrForText]);
        //debugger($textVal);
        if(isset($textVal) && !empty($textVal)){

          $anchorTagElDom[0]->textContent = $textVal;
        }


      }

      if(isset($_POST[$nameAttrForOpenInNewTab]) && !empty($_POST[$nameAttrForOpenInNewTab])){
        //debugger($nameAttrForOpenInNewTab);
        $openInNewTabVal = sanitize($_POST[$nameAttrForOpenInNewTab]);
        //debugger($openInNewTabVal);
        if(isset($openInNewTabVal) && !empty($openInNewTabVal)){
          $anchorTagElDom[0]->setAttribute('target','_blank');

        }else{
          ;

        }


      }
      if(isset($_POST[$nameAttrForHref]) && !empty($_POST[$nameAttrForHref])){
        //debugger($nameAttrForHref);
        $hrefVal = sanitize($_POST[$nameAttrForHref]);

        if(isset($hrefVal) && !empty($hrefVal)){

          $anchorTagElDom[0]->setAttribute('href',$hrefVal);

        }


      }


    }else{
      $nameAttrForHref = $nameAttr.'-url';
      $nameAttrForText = $nameAttr.'-text';

      if(isset($_POST[$nameAttrForHref]) && !empty($_POST[$nameAttrForHref])){
        $hrefVal = sanitize($_POST[$nameAttrForHref]);
        $currentXmlDom->setAttribute('href',$hrefVal);

      }
      if(isset($_POST[$nameAttrForText]) && !empty($_POST[$nameAttrForText])){
        $textVal = sanitize($_POST[$nameAttrForText]);

        $currentXmlDom->textContent = $textVal ;

      }

    }


    $changedSimpleXmlDomEl = simplexml_import_dom($currentXmlDom);
                        //debugger($changedSimpleXmlDomEl);

  }elseif($inputType == 'text' || $inputType == 'textarea'){

    $textVal = sanitize($_POST[$nameAttr]);
    if($subContentType == 'ParagraphGroup'){
      $pTagElDomList = $currentXmlDom->getElementsByTagName('p');
      //debugger($pTagElDomList[1]);
      //debugger($textVal);
      //pTagElDomList
      $textValArr = array_values(array_filter(array_map('trim', explode('                            ', $textVal))));
      //$pTagElDomList->removeChild();
      //debugger($pTagElDomList);
      //$currentXmlDom->removeChild($pTagElDomList);
      //$pTagElDomList->nodeValue = '';


      //$textValArr = implode(' ', $textValArr);
      // debugger('$pTagElDomList');
      // debugger($pTagElDomList);
      //debugger('gettype($pTagElDomList)');

      if(isset($textValArr) && !empty($textValArr)){
      //debugger('$textValArr');

      //debugger($textValArr);
        //$currentXmlDom->removeChild($pTagElDomList);
        //debugger($currentXmlDom);

        //debugger('$pTagElDomList');
        //debugger($pTagElDomList);

        $firstPTag = $pTagElDomList->item(0);
        if(isset($firstPTag) && !empty($firstPTag)){


          for ($i = $pTagElDomList->length; --$i >= 0; ) {
            $xmlEl = $pTagElDomList->item($i);
            $xmlEl->parentNode->removeChild($xmlEl);
          }
          foreach ($textValArr as $textValArrKey => $textValue) {
            if(isset($textValue) && !empty($textValue)){
              //debugger('$textValue');
              //debugger($textValue);
              $clonedPTag = $firstPTag->cloneNode(true);

              //debugger($pTagElDomList->item(0));
              $clonedPTag->textContent = $textValue;

              $currentXmlDom->appendChild($clonedPTag);

            }
          }
        }
        //debugger($currentXml->xpath(".//p"));

        //debugger('$currentXml');

        //debugger($currentXml);
      }
      //debugger($pTagElDom);
      ////$pTagElDom[0]->textContent = $textVal;
      /////debugger($textValArr);
    }elseif($subContentType == 'Heading'){
      $currentXmlDom->textContent = $textVal;
      //debugger($currentXml);

    }else{  


      $currentXmlDom->textContent = $textVal;

    }



    unset($textVal);

  }elseif($inputType == 'file'){

    if(isset($_FILES[$nameAttr]) && !empty($_FILES[$nameAttr])){
      $currentXmlDom->setAttribute('src',$arrangedRenderFormDataListItems['src']);


    }

  }

  $currentXmlDom = simplexml_import_dom($currentXml);


}

function removeDomElement($currentXmlWithIndex,$sectionElArray,$childNodesAllClassNameForClonedEl = null){
  if(isset($currentXmlWithIndex) && !empty($currentXmlWithIndex)){

    foreach ($currentXmlWithIndex as $currentXmlWithIndexKey => $currentXmlWithIndexElItem) {

      $currentXmlWithIndexDom = dom_import_simplexml($currentXmlWithIndexElItem);

      $currentXmlWithIndexDomParentDom = $currentXmlWithIndexDom->parentNode;
      $currentXmlWithIndexDomGrandParentDom = $currentXmlWithIndexDom->parentNode->parentNode;


      $currentXmlWithIndexDomParentDomClassAttr = $currentXmlWithIndexDomParentDom->getAttribute('class');
      $currentXmlWithIndexDomGrandParentDomClassAttr = $currentXmlWithIndexDomGrandParentDom->getAttribute('class');

      //debugger($currentXmlWithIndexDomGrandParentDomClassAttr);
      if (strpos($currentXmlWithIndexDomGrandParentDomClassAttr, 'multiple-encapsulation') !== false) {
      //has multiple encapsulation

        $currentXmlWithIndexDomGrandParentDom->removeChild($currentXmlWithIndexDomParentDom);
      //debugger('$childNodesAllClassNameForClonedEl');

      //debugger($childNodesAllClassNameForClonedEl);
        if(isset($childNodesAllClassNameForClonedEl) && !empty($childNodesAllClassNameForClonedEl)){
          $currentXmlWithIndexOfClonedEl = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassNameForClonedEl')]");
          //debugger($currentXmlWithIndexOfClonedEl);
          removeDomElement($currentXmlWithIndexOfClonedEl,$sectionElArray,null);

        }
        $dd = simplexml_import_dom($currentXmlWithIndexDomGrandParentDom);
      }else{
        if (strpos($currentXmlWithIndexDomParentDomClassAttr, 'single-encapsulation') !== false) {
                        //has single encapsulation
          $currentXmlWithIndexDomParentDom->removeChild($currentXmlWithIndexDom);

        //debugger('$childNodesAllClassNameForClonedEl');
          //debugger($childNodesAllClassNameForClonedEl);

          if(isset($childNodesAllClassNameForClonedEl) && !empty($childNodesAllClassNameForClonedEl)){
            $currentXmlWithIndexOfClonedEl = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassNameForClonedEl')]");
          //debugger($currentXmlWithIndexOfClonedEl);
            removeDomElement($currentXmlWithIndexOfClonedEl,$sectionElArray,null);

          }
          $dd = simplexml_import_dom($currentXmlWithIndexDomParentDom);

        }

      }
    //debugger($dd);
    //debugger($currentXmlWithIndexDomParentDom);
    }
    

  }
}

function addDomElement($currentXmlWithIndex,$parentXmlWithIndex,$sectionElArray,$subContentType,$arrangedRenderFormDataListItems,$websiteData,$childNodesAllClassNameForClonedFirstEl = null){
  if(isset($currentXmlWithIndex[0]) && !empty($currentXmlWithIndex[0])){

    $currentXmlWithIndexParent = $currentXmlWithIndex[0]->xpath("parent::*")[0];


    $firstChildXmlDom = dom_import_simplexml($currentXmlWithIndexParent[0]);
    //$firstChildXmlDom = dom_import_simplexml($currentXmlWithIndexParent[0]);
    //$firstChildXmlDom = dom_import_simplexml($currentXmlWithIndex[0]);


    $firstChildXmlDomClassName = $firstChildXmlDom->getAttribute('class');
    $firstChildXmlDomClassNameArr = explode(' ', $firstChildXmlDomClassName);
    
    if(in_array('single-encapsulation', $firstChildXmlDomClassNameArr)){
      /*code to add new dom elements with single encapsulation eg: new dom element inside same tag*/

      $i = 0;
      foreach ($currentXmlWithIndexParent as $currentXmlWithIndexParentKey => $currentXmlWithIndexParentChild) {

        $i++ ;

        if($i == 1){
          $firstChildXmlDom = dom_import_simplexml($currentXmlWithIndexParentChild);
          $currentXmlDom = $firstChildXmlDom->cloneNode(true);
          $newXmlEl = simplexml_import_dom($currentXmlDom);


          $newXmlElDom = dom_import_simplexml($newXmlEl[0]);
          $currentXmlWithIndexDom = dom_import_simplexml($currentXmlWithIndex[0]);


          $newXmlElDomClassName = $newXmlElDom->getAttribute('class');
          $currentXmlWithIndexDomClassName = $currentXmlWithIndexDom->getAttribute('class');

          


          if($newXmlElDomClassName == $currentXmlWithIndexDomClassName){
            $currentXmlWithIndexParentChildDom = dom_import_simplexml($newXmlEl[0]);

            if($arrangedRenderFormDataListItems['inputType'] == 'file'){
              $currentXmlWithIndexParentChildDom->setAttribute('class',$arrangedRenderFormDataListItems['allClassName']);
              $currentXmlWithIndexParentChildDom->setAttribute('src',$arrangedRenderFormDataListItems['src']);
            }elseif($arrangedRenderFormDataListItems['inputType'] == 'url'){
              $currentXmlWithIndexParentChildDom->setAttribute('class',$arrangedRenderFormDataListItems['allClassName']);

              if(isset($arrangedRenderFormDataListItems['target']) && !empty($arrangedRenderFormDataListItems['target'])){
                $currentXmlWithIndexParentChildDom->setAttribute('target',$arrangedRenderFormDataListItems['target']);

              }
              if(isset($arrangedRenderFormDataListItems['href']) && !empty($arrangedRenderFormDataListItems['href'])){
                $currentXmlWithIndexParentChildDom->setAttribute('href',$arrangedRenderFormDataListItems['href']);

              }

                        //debugger($currentXmlWithIndexParentChildDom->getElementsByTagName('a'));

              if($subContentType == 'NavbarItem'){
                $textVal = $arrangedRenderFormDataListItems['textArray'][0];

                        //debugger($currentXmlDom);

                $anchorTagElDom = $currentXmlDom->getElementsByTagName('a');
                $anchorTagElDom[0]->textContent = $textVal;


              }else{

              }

              $parentXmlWithIndex = $currentXmlWithIndexParent;
            }elseif($arrangedRenderFormDataListItems['inputType'] == 'text' || $arrangedRenderFormDataListItems['inputType'] == 'textarea'){
              $currentXmlWithIndexParentChildDom->setAttribute('class',$arrangedRenderFormDataListItems['allClassName']);

              $textVal = $arrangedRenderFormDataListItems['textArray'][0];
              $currentXmlWithIndexParentChildDom->textContent = $textVal;
                        //debugger('$parentXmlWithIndex');
              $parentXmlWithIndex = $currentXmlWithIndexParent;
                        //debugger($currentXmlWithIndexParent);
                        //debugger($currentXmlWithIndexParentChildDom);

            }
          }
          break ;
        }
      }
        //$currentXmlDom = $firstChildXmlDom->cloneNode(true);
    }else{

      $parentOfCurrentXmlWithIndexParent = $currentXmlWithIndexParent[0]->xpath("parent::*")[0];
      //$currentXmlWithIndexParent = $parentOfCurrentXmlWithIndexParent[0]->xpath("parent::*")[0];

      $parentOfCurrentXmlWithIndexParentDom = dom_import_simplexml($parentOfCurrentXmlWithIndexParent[0]);
      $parentXmlDomClassName = $parentOfCurrentXmlWithIndexParentDom->getAttribute('class');
      $firstChildXmlDomClassNameArr = explode(' ', $parentXmlDomClassName);

      if(in_array('multiple-encapsulation', $firstChildXmlDomClassNameArr)){
        /*code to add new dom elements with multiple encapsulation eg: new dom element inside new tag*/



        foreach ($parentOfCurrentXmlWithIndexParent as $newXmlElParentKey => $newXmlElParentChild) {


          $firstChildOfNewXmlElParentChild = $newXmlElParentChild->children()[0];



          $firstChildOfNewXmlElParentChildDom = dom_import_simplexml($firstChildOfNewXmlElParentChild[0]);
          $currentXmlWithIndexDom = dom_import_simplexml($currentXmlWithIndex[0]);


          $firstChildOfNewXmlElParentChildClassName = $firstChildOfNewXmlElParentChildDom->getAttribute('class');
          $currentXmlWithIndexDomClassName = $currentXmlWithIndexDom->getAttribute('class');

          /*checking if both variables are of first index where new dom element should be added */
          /*here to edit first index dom element to new element after cloning*/


          if($firstChildOfNewXmlElParentChildClassName == $currentXmlWithIndexDomClassName){


            $newXmlElParentChildDom = dom_import_simplexml($newXmlElParentChild[0]);

            $clonedNewXmlElParentChildDom = $newXmlElParentChildDom->cloneNode(true);
            $clonedNewXmlElParentChildEl = simplexml_import_dom($clonedNewXmlElParentChildDom);

            $currentXmlWithIndexParentChildEl = $clonedNewXmlElParentChildEl->children()[0];

            $currentXmlWithIndexParentChildDom = dom_import_simplexml($currentXmlWithIndexParentChildEl[0]);

              // debugger('$firstChildOfNewXmlElParentChildClassName');
              // debugger($firstChildOfNewXmlElParentChildClassName);
              // debugger('$currentXmlWithIndexDomClassName');
              // debugger($currentXmlWithIndexDomClassName);

            if($arrangedRenderFormDataListItems['inputType'] == 'file'){
                //debugger('$firstChildXmlDomClassName');

              $currentXmlWithIndexParentChildDom->setAttribute('class',$arrangedRenderFormDataListItems['allClassName']);
              $currentXmlWithIndexParentChildDom->setAttribute('src',$arrangedRenderFormDataListItems['src']);


            }elseif($arrangedRenderFormDataListItems['inputType'] == 'url'){
              $currentXmlWithIndexParentChildDom->setAttribute('class',$arrangedRenderFormDataListItems['allClassName']);
              $currentXmlWithIndexParentChildDom->setAttribute('target',$arrangedRenderFormDataListItems['target']);
              $currentXmlWithIndexParentChildDom->setAttribute('href',$arrangedRenderFormDataListItems['href']);

                //debugger($currentXmlWithIndexParentChildDom->getElementsByTagName('a'));

              if($subContentType == 'NavbarItem'){
                $textVal = $arrangedRenderFormDataListItems['textArray'][0];
                  //debugger($textVal);
                $anchorTagElDom = $currentXmlDom->getElementsByTagName('a');
                $anchorTagElDom[0]->textContent = $textVal;
              }else{

              }


            }else{

            }
              //debugger('$newXmlElParentChild');
              //debugger($clonedNewXmlElParentChildEl);
              //$parentXmlWithIndex = $newXmlEl;
            $newXmlEl = $clonedNewXmlElParentChildEl;
            $parentXmlWithIndex = $parentOfCurrentXmlWithIndexParent;
              //debugger($currentXmlWithIndex);
          }

        }
          //debugger($firstChildOfNewXmlElParentChild);

          //debugger('$firstChildOfNewXmlElParentChild');

          //debugger($firstChildOfNewXmlElParentChild);


      }

        //debugger('$newXmlEl');

        //debugger($newXmlEl[0]);
    }

    debugger('$parentXmlWithIndex');

    debugger($parentXmlWithIndex);

    /*checking if new element is added inside foreach*/
    /* appending newly created dom element above in main html tag*/
    if(isset($currentXmlWithIndexParentChildDom) && !empty($currentXmlWithIndexParentChildDom)){
      /*append to parent element after changing cloned first element */


      
      $newXmlElDom = dom_import_simplexml($newXmlEl[0]);

      $parentXmlWithIndexDom = dom_import_simplexml($parentXmlWithIndex[0]);

      

      $parentXmlWithIndexDom->appendChild($newXmlElDom);

      //debugger('$childNodesAllClassNameForClonedFirstEl');

        //debugger($childNodesAllClassNameForClonedFirstEl);
      $currentXmlWithIndexOfClonedFirstEl = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassNameForClonedFirstEl')]");

      if(isset($currentXmlWithIndexOfClonedFirstEl) && !empty($currentXmlWithIndexOfClonedFirstEl)){
        //debugger('$currentXmlWithIndexOfClonedFirstEl');

        //debugger($currentXmlWithIndexOfClonedFirstEl);
        foreach ($currentXmlWithIndexOfClonedFirstEl as $currentXmlWithIndexOfClonedFirstElKey => $currentXmlWithIndexOfClonedFirstElItem) {
          # code...
          $currentXmlWithIndexOfClonedFirstElDom = dom_import_simplexml($currentXmlWithIndexOfClonedFirstElItem);


        //debugger();
          $parentNodeClass = $currentXmlWithIndexOfClonedFirstElDom->parentNode->getAttribute('class');
          $parentNodeClassArr = explode(' ', $parentNodeClass);

          if(in_array('single-encapsulation', $parentNodeClassArr)){
            $parentNodeOfClonedElDom = $currentXmlWithIndexOfClonedFirstElDom->parentNode;
          }else{
            $parentOfParentNodeClass = $currentXmlWithIndexOfClonedFirstElDom->parentNode->parentNode->getAttribute('class');
            $parentOfParentNodeClassArr = explode(' ', $parentOfParentNodeClass);

            if(in_array('multiple-encapsulation', $parentOfParentNodeClassArr)){
              $parentNodeOfClonedElDom = $currentXmlWithIndexOfClonedFirstElDom->parentNode->parentNode;

            }
          }

          $contentTypeNameClassAttrNameForClonedEl = $subContentType.'-Cloned-'.$websiteData[0]->theme_name;
        //debugger($contentTypeNameClassAttrNameForClonedEl);
          $parentXmlWithIndexOfClonedEl = $sectionElArray->xpath(".//*[contains(@class, '$contentTypeNameClassAttrNameForClonedEl')]");
      //debugger('$parentXmlWithIndexOfClonedEl');
      //debugger($parentXmlWithIndexOfClonedEl);
          if(isset($parentNodeOfClonedElDom) && !empty($parentNodeOfClonedElDom)){
          //$newClonedElAllClassName = $arrangedRenderFormDataListItems['allClassName']
          //$parentXmlWithIndexOfClonedEl = $sectionElArray->xpath(".//*[contains(@class, '$contentTypeNameClassAttrNameForClonedEl')]");
            $parentXmlWithIndexOfClonedEl = simplexml_import_dom($parentNodeOfClonedElDom);

          //$newClonedFirstElAllClassName = $currentXmlWithIndexOfClonedFirstEl[0]->attributes()['class'];



            $firstElClonedAllClassName = $currentXmlWithIndexOfClonedFirstElDom->getAttribute('class');


            $labelArr = explode('-', $arrangedRenderFormDataListItems['label']);
            $currentID = end($labelArr);
            $newElClonedAllClassName = str_replace('_1', '_'.$currentID, $firstElClonedAllClassName);
            $arrangedRenderFormDataListItems['allClassName'] = $newElClonedAllClassName;

            addDomElement($currentXmlWithIndexOfClonedFirstEl,$parentXmlWithIndexOfClonedEl,$sectionElArray,$subContentType,$arrangedRenderFormDataListItems,$websiteData,null);

          }
          //addDomElement($currentXmlWithIndex,$parentXmlWithIndex,$sectionElArray,$subContentType,$arrangedRenderFormDataListItems,$websiteData,$childNodesAllClassNameForClonedFirstEl);

        }
        


      }
      
    }
  }
}

function editDomElement($contentTypeProperties,$contentTypeName,$sectionElArray,$subContentType,$inputType,$nameAttr,$arrangedRenderFormDataListItems,$childNodesAllClassNameForClonedFirstEl = null){
  foreach ($contentTypeProperties[$contentTypeName] as $currentContentTypeChildElIndex => $currentContentTypeChildEl) {
    if(is_int($currentContentTypeChildElIndex)){
        //debugger($currentContentTypeChildEl);
        //debugger($currentContentTypeChildEl);
        //
      if(isset($currentContentTypeChildEl[$subContentType]) && !empty($currentContentTypeChildEl[$subContentType])){
        $childNodes = $currentContentTypeChildEl[$subContentType]['childNodes'];
        $propsArr = $currentContentTypeChildEl[$subContentType]['props'];

        if(isset($childNodes) && !empty($childNodes)){
          $currentChildNodeEl = $childNodes;
          $allClassName = $arrangedRenderFormDataListItems['allClassName'];

            //$currentChildNodeElAllClassName = $currentChildNodeEl->attributes()['class'];


          $childNodesAllClassName =  implode('-', $propsArr); 

          $currentXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$allClassName')]");
            //debugger($arrangedRenderFormDataListItems);

          if(isset($currentXmlWithIndex[0]) && !empty($currentXmlWithIndex[0])){
            $currentXml = $currentXmlWithIndex[0];

            if(isset($currentXml) && !empty($currentXml)){
              //debugger($currentXml);

              changeWebpageByInputType($inputType,$currentXml,$nameAttr,$subContentType,$arrangedRenderFormDataListItems);

              if(isset($childNodesAllClassNameForClonedFirstEl) && !empty($childNodesAllClassNameForClonedFirstEl)){
                //debugger('$childNodesAllClassNameForClonedFirstEl');

                //debugger($childNodesAllClassNameForClonedFirstEl);
                $clonedElList = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassNameForClonedFirstEl')]");
                
                if(isset($clonedElList) && !empty($clonedElList)){

                  foreach ($clonedElList as $clonedElListKey => $clonedElListItem) {

                    $clonedElListItemDom = dom_import_simplexml($clonedElListItem);


                    $clonedElAllClassName = $clonedElListItemDom->getAttribute('class');

                    $parentNodeClass = $clonedElListItemDom->parentNode->getAttribute('class');
                    $parentNodeClassArr = explode(' ', $parentNodeClass);

                    if(in_array('single-encapsulation', $parentNodeClassArr)){
                      $parentNodeOfClonedElDom = $clonedElListItemDom->parentNode;
                    }else{
                      $parentOfParentNodeClass = $clonedElListItemDom->parentNode->parentNode->getAttribute('class');
                      $parentOfParentNodeClassArr = explode(' ', $parentOfParentNodeClass);

                      if(in_array('multiple-encapsulation', $parentOfParentNodeClassArr)){
                        $parentNodeOfClonedElDom = $clonedElListItemDom->parentNode->parentNode;

                      }
                    }

                    
                    if(isset($parentNodeOfClonedElDom) && !empty($parentNodeOfClonedElDom)){


                      $parentNodeOfClonedEl = simplexml_import_dom($parentNodeOfClonedElDom);


                      $labelArr = explode('-', $arrangedRenderFormDataListItems['label']);
                      $currentID = end($labelArr);
                      $newElClonedAllClassName = str_replace('_1', '_'.$currentID, $clonedElAllClassName);
                      $arrangedRenderFormDataListItems['allClassName'] = $newElClonedAllClassName;

                      $clonedCurrentXml = $parentNodeOfClonedEl->xpath(".//*[contains(@class, '$newElClonedAllClassName')]");
                    //debugger($arrangedRenderFormDataListItems);


                      if(isset($clonedCurrentXml) && !empty($clonedCurrentXml)){
                        changeWebpageByInputType($inputType,$clonedCurrentXml[0],$nameAttr,$subContentType,$arrangedRenderFormDataListItems);

                      }
                    }
                  }
                  
                }

              }
            }else{

            }
          }




        }
      }
    }
  }
}


function alterWebpage($contentTypeProperties,$contentTypeName,$sectionElArray,$arrangedRenderFormDataListItems,$websiteData){
  $inputType = $arrangedRenderFormDataListItems['inputType'];
  $nameAttr = $arrangedRenderFormDataListItems['nameAttr'];
  $subContentType = $arrangedRenderFormDataListItems['subContentType'];
  $subContentType = str_replace(' ', '', $subContentType) ; 

  $labelArr = explode('-', $arrangedRenderFormDataListItems['label']);
  $currentID = end($labelArr);
  $allClassName = str_replace('_'.$currentID, '_1', $arrangedRenderFormDataListItems['allClassName']);
  $subContentType = str_replace(' ', '', $arrangedRenderFormDataListItems['subContentType']);
  $contentTypeNameClassAttrName = $contentTypeName.'-'.$websiteData[0]->theme_name;

  $allClassNameArr = explode(' ', $allClassName);
  $clonedClassNameArr = explode('-', $allClassNameArr[0]);

  array_splice( $clonedClassNameArr, sizeof($clonedClassNameArr) - 1, 0, 'Cloned' );
  $childNodesAllClassNameForClonedFirstEl = implode('-', $clonedClassNameArr);
  //debugger($arrangedRenderFormDataListItems);
  /*$arrangedRenderFormDataListItems is simply all data retreived from webpage using file system */
  /*loop for removal of DOM element according to form data*/
  foreach ($contentTypeProperties[$contentTypeName] as $currentContentTypeChildElIndex => $currentContentTypeChildEl) {
    if(is_int($currentContentTypeChildElIndex)){


      if(isset($currentContentTypeChildEl[$subContentType]) && !empty($currentContentTypeChildEl[$subContentType])){

        $childNodes = $currentContentTypeChildEl[$subContentType]['childNodes'];
        $propsArr = $currentContentTypeChildEl[$subContentType]['props'];
        $propsArrForClonedEl = $propsArr;

        array_splice( $propsArrForClonedEl, sizeof($propsArrForClonedEl) - 1, 0, 'Cloned' );
        $childNodesAllClassNameForClonedEl =  implode('-', $propsArrForClonedEl); 

        $childNodesAllClassName =  implode('-', $propsArr); 
        $nameAttrForRemovalArr = explode('*', $propsArr[1]);

        //debugger($childNodesAllClassNameForClonedEl);

                        //unset($nameAttrForRemovalArr[0]);
                        //unset($nameAttrForRemovalArr[1]);

                        //$nameAttrForRemovalArr = array_values($nameAttrForRemovalArr);
                        //$sectionName = 
                        //$nameAttrForRemovalArr[0] = str_replace('*', '-', $nameAttrForRemovalArr[0]);

        $sectionArr = explode('_', $nameAttrForRemovalArr[0]);
        //debugger($sectionArr);
        if(isset($sectionArr[1]) && !empty($sectionArr[1])){
          $inputType = $sectionArr[1];
        }
                        //debugger($subContentType);
                        //debugger($nameAttrForRemovalArr);
        $sectionName = end($sectionArr);
        unset($nameAttrForRemovalArr[0]);
                        //debugger($sectionName);
        $nameAttrForRemovalArr = array_values($nameAttrForRemovalArr);

                        // $i = 0 ;
                        // if (strpos($nameAttributeOfCurrentContentType, 'Column') !== false) {

                        //   $nameAttributeOfCurrentContentType = $nameAttributeOfCurrentContentType.'_'.end($inputTypeDataArrayOfCurrentContentType).'_'.$i;
                        //   $i++ ;

                        // }
        $nameAttrForRemoval = $sectionName.'-'.implode('_', $nameAttrForRemovalArr);
        if(isset($_POST) && !empty($_POST)){
                        //debugger('$nameAttrForRemovalArr');
                        //debugger($currentContentTypeChildEl[$subContentType]);
                        //debugger($inputType);
                        //debugger($inputType);

          if($inputType != 'file'){

            if($inputType == 'url'){

              $removeDom = false;
              $hideDom = false;


              if($subContentType == 'NavbarItem' || $subContentType == 'SocialbarItem'){
                $nameAttrForText = $nameAttrForRemoval.'-text';
                $nameAttrForCheckbox = $nameAttrForRemoval.'-checkbox';
                $nameAttrForUrl = $nameAttrForRemoval.'-url';

                if((!isset($_POST[$nameAttrForText]) || empty($_POST[$nameAttrForText])) && (!isset($_POST[$nameAttrForCheckbox]) || empty($_POST[$nameAttrForCheckbox])) && (!isset($_POST[$nameAttrForUrl]) || empty($_POST[$nameAttrForUrl]))){
                        //optional
                  if(isset($_POST[$nameAttrForRemoval.'-canHide']) && !empty($_POST[$nameAttrForRemoval.'-canHide'])){
                    $hideDom = true;
                  }else{
                    $removeDom = true;

                  }

                }else{

                }

              }else{
                if(!isset($_POST[$nameAttrForRemoval]) || empty($_POST[$nameAttrForRemoval])){
                        //optional
                  $removeDom = true;

                }
              }


              if($removeDom){

                $currentXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassName')]");

                removeDomElement($currentXmlWithIndex,$sectionElArray,$childNodesAllClassNameForClonedEl);


              }
              if($hideDom){
                processingToHideDomElement($arrangedRenderFormDataListItems,$nameAttr,$sectionElArray);


              }
            }else{
              if(!isset($_POST[$nameAttrForRemoval]) || empty($_POST[$nameAttrForRemoval])){
                $currentXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassName')]");

                removeDomElement($currentXmlWithIndex,$sectionElArray,$childNodesAllClassNameForClonedEl);
              }
            }
          }else{

            if(!isset($_FILES[$nameAttrForRemoval]) || empty($_FILES[$nameAttrForRemoval])){
                        //debugger($currentXmlWithIndex);
              $currentXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassName')]");

              removeDomElement($currentXmlWithIndex,$sectionElArray,$childNodesAllClassNameForClonedEl);
                        //$dd = simplexml_import_dom($currentXmlWithIndexDomParentDom);
                        //debugger($dd);
                        //debugger($dd);

                        //debugger($dd);
                        //unset($currentContentTypeChildEl);

            }
          }
        }


      }
    }
  }

  /*for adding and editing DOM Element*/
  if(isset($arrangedRenderFormDataListItems['addToDomEl']) && !empty($arrangedRenderFormDataListItems['addToDomEl']) && $arrangedRenderFormDataListItems['addToDomEl']){
    /* adding as DOM element in webpage*/

     //debugger($arrangedRenderFormDataListItems);

    $currentXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$allClassName')]");
    $parentXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$contentTypeNameClassAttrName')]");
    
    
    //debugger('$currentXmlWithIndex[0]');
    //debugger($currentXmlWithIndex[0]);

    addDomElement($currentXmlWithIndex,$parentXmlWithIndex,$sectionElArray,$subContentType,$arrangedRenderFormDataListItems,$websiteData,$childNodesAllClassNameForClonedFirstEl);

  }else{
    //debugger($contentTypeProperties[$contentTypeName]);
    /* adding new element is done in if case.*/
    /*Editing current DOM element according to form data*/
    editDomElement($contentTypeProperties,$contentTypeName,$sectionElArray,$subContentType,$inputType,$nameAttr,$arrangedRenderFormDataListItems,$childNodesAllClassNameForClonedFirstEl);
    
  }


  //debugger($_POST[$nameAttr]);
  //debugger($parentXML);
}

function processingToHideDomElement($arrangedRenderFormDataListItems,$nameAttr,$sectionElArray){

  //debugger($arrangedRenderFormDataListItems);
  $canHide = $arrangedRenderFormDataListItems['action']['canHide'];
  $allClassName = $arrangedRenderFormDataListItems['allClassName'];
  if($canHide){

    $canHideNameAttr = $nameAttr.'-canHide';
    if(isset($_POST) && !empty($_POST)){
      if(isset($_POST[$canHideNameAttr]) && !empty($_POST[$canHideNameAttr])){

        $canHideNameAttrVal = sanitize($_POST[$canHideNameAttr]);

        if($canHideNameAttrVal == 1){
          $currentXml = $sectionElArray->xpath(".//*[contains(@class, '$allClassName')]")[0];

          $parentXML = $currentXml->xpath("parent::*")[0];
                                //debugger($currentXml);
          $currentXmlDom = dom_import_simplexml($currentXml);
          $allClassNameArr = explode(' ', $allClassName);

          if(!in_array('d-none', $allClassNameArr)){
            $newClassName = $allClassName.' '.'d-none';
            $currentXmlDom->setAttribute('class', $newClassName);

          }
          $clonedClassNameArr = explode('-', $allClassNameArr[0]);

          //debugger($clonedClassNameArr);
          
          array_splice( $clonedClassNameArr, sizeof($clonedClassNameArr) - 1, 0, 'Cloned' );
          $childNodesAllClassNameForClonedEl =  implode('-', $clonedClassNameArr); 

          
          $clonedXmlWithIndex = $sectionElArray->xpath(".//*[contains(@class, '$childNodesAllClassNameForClonedEl')]");

          //debugger('$clonedXmlWithIndex');
          if(isset($clonedXmlWithIndex) && !empty($clonedXmlWithIndex)){
            foreach ($clonedXmlWithIndex as $clonedXmlWithIndexKey => $clonedXmlWithIndexElItem) {
              $clonedXmlWithIndexElItemDom = dom_import_simplexml($clonedXmlWithIndexElItem);
              $clonedXmlWithIndexElItemAllClassName = $clonedXmlWithIndexElItemDom->getAttribute('class');
              $arrangedRenderFormDataListItems['allClassName'] = $clonedXmlWithIndexElItemAllClassName;

              $clonedXmlWithIndexElItemParentDom = $clonedXmlWithIndexElItemDom->parentNode;
              $clonedXmlWithIndexElItemParentEl = simplexml_import_dom($clonedXmlWithIndexElItemParentDom);

              processingToHideDomElement($arrangedRenderFormDataListItems,$nameAttr,$clonedXmlWithIndexElItemParentEl);
              //debugger($clonedXmlWithIndexElItemAllClassName);



            }
          }
          

        }

      }
    }

                            //debugger($nameAttrFormEl);
  }

}
function writeToPage($sectionElArray,$websiteData,$pageName){
  $pathToWriteTo = '';
  $pathToWritefrom = '';

                        //debugger($websiteData);
                        //debugger($sectionElArray);

  $pathToWriteTo = SITE_DIR.$websiteData[0]->website_id.'/'.$pageName;
                        //debugger($pathToWrite);
  $sectionElArrayDom = dom_import_simplexml($sectionElArray);
  $scriptDom = $sectionElArrayDom->getElementsByTagName('script') ;

                        //$scriptEl = simplexml_import_dom($ss);
  $scriptTags = $sectionElArray->xpath('//body//script');

  foreach ($scriptTags as $scriptTagsKey => $scriptTagsItems) {
    $scriptTagsItemsDom = dom_import_simplexml($scriptTagsItems);
    $sectionElArrayDom->removeChild($scriptTagsItemsDom);

  }
                        // foreach ($scriptDom as $scriptDomKey => $scriptDomItems) {
                        //   debugger($scriptDomItems);
                        //   $sectionElArrayDom->removeChild($scriptDomItems);
                        // }
                        // $sectionElArray = simplexml_import_dom($sectionElArrayDom);

                        //debugger($sectionElArray);
  $htmlString = $sectionElArray->asXML();

  $headerString = '<?php include "inc/header.php"; ?>';
  $footerString = '<?php include "inc/footer.php"; ?>';

  $htmlString = str_replace('<body>', $headerString, $htmlString);
  $htmlString = str_replace('</body>', $footerString, $htmlString);

                        //$allHtmlString = $headerString.'\r\n'.$htmlString.'\r\n'.$footerString;
                        //debugger('<?php include "inc/header.php";');
                        //


                        //echo($htmlString);

                        //$sectionElArrayDom->children();
                        //debugger(htmlspecialchars($htmlString));
  $newPageFile = fopen("$pathToWriteTo", "w") or die("Unable to open file!");
  fwrite($newPageFile, $htmlString);
  fclose($newPageFile);
  return $pathToWriteTo;


                        // $finalHtmlString = $topString.'

                        // '.$htmlString.'

                        // '.$bottomString;

                        //debugger($sectionElArray);

                        //echo(htmlspecialchars_decode($htmlString));
}
?>





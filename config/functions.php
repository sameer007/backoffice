<?php 


if(!function_exists('debugger')){
  function debugger($data, $is_die = false){
    echo "<pre style='color: #FF0000;'>";
    print_r($data);
    echo "</pre>";
    if($is_die){
      exit;
    }
  }
}

if(!function_exists('redirect')){
  function redirect($path = null, $session_status = null, $session_status_msg=null){
    if($session_status != NULL){
      $_SESSION[$session_status] = $session_status_msg;
    }
    if ($path != null) {
      @header('location: '.$path);
      exit;
    }
  }
}

if(!function_exists('getRoleByRoleId')){
  function getRoleByRoleId($roleId){
    if($roleId == 1){
      return 'Superadmin';
    }elseif($roleId == 2){
      return 'Admin';

    }elseif($roleId == 3){
      return 'User';

    }else{
      return '-';

    }
  }
}


if(!function_exists('displayRoutes')){
  function displayRoutes($routeArray){
    //debugger($routeArray);
    $currentUrl = SITE_URL;
    //$lastValue = sizeof($routeArray) - 2;
    foreach ($routeArray as $key => $value) {

      if($value == 'backoffice'){
          //debugger($value);

      }else{
        if(isset($currentUrl) && !empty($currentUrl)){
          $currentUrl .= $value.'/';
        }
      }
      
      
      if(getCurrentPage() == $value){
        echo "<li class=\"breadcrumb-item active\">$value</li>";

      }else{
        if($value != 'index'){
          if($value == 'backoffice'){
            if(isset($currentUrl) && !empty($currentUrl)){
              echo "<li class=\"breadcrumb-item\"><a href=\"$currentUrl\">$value</a></li>";
            }
          }else{
            if(isset($currentUrl) && !empty($currentUrl)){
              if($key == sizeof($routeArray) - 2){
                if(isset($_GET) && !empty($_GET)){
                  if(getCurrentPage() == 'index'){
                    $currentUrl = $currentUrl.'?'.$_SERVER['QUERY_STRING'];

                    
                  }else{
                    //DEBUGGER($_SERVER);
                    $explodedUrl = explode('?', $_SERVER['REQUEST_URI']);
                    $currentUrl = $currentUrl.'?'.$explodedUrl[1];
                  }
                  echo "<li class=\"breadcrumb-item\"><a href=\"$currentUrl\">$value</a></li>";

                }else{
                  echo "<li class=\"breadcrumb-item\"><a href=\"$currentUrl\">$value</a></li>";

                }

              }else{
                echo "<li class=\"breadcrumb-item\"><a href=\"$currentUrl\">$value</a></li>";

              }
            }
          }

        }
      }
    }
  }
}



if(!function_exists('getStatusByStatusId')){
  function getStatusByStatusId($statusId){
    if($statusId == 1){
      return 'Active';
    }elseif($statusId == 0){
      return 'Inactive';

    }else{
      return '-';

    }
  }
}

function processNode ( $base, SimpleXMLElement $node, &$output ,$contentTypeNameArray = NULL,$themeName = NULL,$contentTypeProperties = NULL)  {
  $base[] = $node->getName();
  $nodeName = implode("_", $base);
  $childNodes = $node->children();
  $classesArray = $node->attributes()['class'];
  $contentTypeNameArray = $contentTypeNameArray;

  global $contentTypeProperties;
  //$classesProperties = array();
  //debugger($node);
  //debugger($childNodes);

  //$contentTypeProperties = array();
  $subContentTypeProperties = array();

  if(isset($classesArray) && !empty($classesArray)){
    $classNameString = $classesArray[0];
    $classNameArray = explode(' ', $classNameString);

    //debugger($classNameArray);

    if(isset($classNameArray) && !empty($classNameArray)){

      foreach ($classNameArray as $key => $className) {


        if(strpos($className, '-'.$themeName) !== false){
          /*class name includes -THemeName*/
          $contentTypeProps = array();
          $subContentTypeProps = array();
          if(!in_array($className,$contentTypeProps)){
            $contentTypeProps = explode('-', $className);
            //debugger($contentTypeProps);
            if(isset($contentTypeNameArray) && !empty($contentTypeNameArray)){
              //debugger($contentTypeNameArray);
              foreach ($contentTypeNameArray as $contentTypeName => $subContentTypeNameArray) {
                    //debugger($contentTypeName);
                if($contentTypeName == $contentTypeProps[0]){
                  //debugger($contentTypeProps);
                  //$currentNodeContentType = $contentTypeProps[0];
                  $prevContentTypeProps = $contentTypeProps;
                  $contentTypeProperties[$contentTypeName]['props'] = $contentTypeProps;
                  $contentTypeProperties[$contentTypeName]['base'] = $nodeName;
                  //$contentTypeProperties[$contentTypeName]['childNodes'] = $childNodes;

                  //$contentTypeProperties[$contentTypeProps[0]]['props']
                    //$classesProperties[$nodeName] = $contentTypeProps;
                  $contentTypePropsArray[] = $contentTypeProps;

                  /*has content type*/
                }else{
                  /*doesn't have content type*/
                  //$classesProperties[$nodeName] = null ;
                }
                //debugger($node);
                //debugger($contentTypeProps);
                if(!in_array('Cloned', $contentTypeProps)){
                  if(isset($subContentTypeNameArray) && !empty($subContentTypeNameArray)){

                    //debugger($subContentTypeNameArray);
                    // foreach ($subContentTypeNameArray as $subContentTypeGroup => $subContentTypeName) {

                    //   if($subContentTypeGroup)
                    //   $contentTypeProperties[$contentTypeName][$subContentTypeGroup] = 
                    // }
                    //debugger($subContentTypeNameArray);
                    if(in_array($contentTypeProps[0], $subContentTypeNameArray)){
                      /*has sub content type*/

                      

                      //$a = $node->children();
                      if(isset($contentTypeProperties[$contentTypeName]) && !empty($contentTypeProperties[$contentTypeName])){

                        $subContentTypeProperties[$contentTypeProps[0]]['props'] = $contentTypeProps;
                        $subContentTypeProperties[$contentTypeProps[0]]['base'] = $nodeName;
                        $subContentTypeProperties[$contentTypeProps[0]]['childNodes'] = $node->children();
                        //$classesProperties[$nodeName] = $contentTypeProps;
                      //debugger($node->children());

                        array_push($contentTypeProperties[$contentTypeName], $subContentTypeProperties) ;

                      }

                    }else{
                        //debugger($contentTypeProperties);
                      //$classesProperties[$nodeName] = null ;

                    }

                    //debugger($node);
                    //debugger($contentTypeName);
                    //debugger($subContentTypeNameArray);
                    //debugger($classes);
                    if($contentTypeName == $className){
                    }

                  }
                }
                

                      //debugger($contentTypeProperties);

                //}
                
              }
            }else{

            }
            // if(in_array('canHide', $contentTypeProps)){
            //   array_push($contentTypeProps,'canHide');
            //   debugger($new)
            // debugger($contentTypeProps);

            // }
          //debugger($className);



          }

        } else{
          /*class name doesnt include -THemeName*/

        }
      # code...
      }
    }

    //debugger($classNameArray);
  }

  if ( count($childNodes) == 0 )  {
    $output[ $nodeName ] = (string)$node;
    //debugger('A');
    //return $contentTypeProperties ;
    //debugger($contentTypeProperties);
  }
  else    {
    foreach ( $childNodes as $newNode ) {
      processNode($base, $newNode, $output,$contentTypeNameArray,$themeName,$contentTypeProperties);
    }
  }
}

function processNodeForBackend ( $base, SimpleXMLElement $node, &$output ,$contentTypeNameArray = NULL,$themeName = NULL,$contentTypeProperties = NULL)  {
  $base[] = $node->getName();
  $nodeName = implode("_", $base);
  $childNodes = $node->children();
  $classesArray = $node->attributes()['class'];
  $contentTypeNameArray = $contentTypeNameArray;

  global $contentTypeProperties;
  //$classesProperties = array();
  //debugger($classesArray);
  //$contentTypeProperties = array();
  $subContentTypeProperties = array();

  if(isset($classesArray) && !empty($classesArray)){
    $classNameString = $classesArray[0];
    $classNameArray = explode(' ', $classNameString);

    //debugger($classNameArray);

    if(isset($classNameArray) && !empty($classNameArray)){

      foreach ($classNameArray as $key => $className) {


        if(strpos($className, '-'.$themeName) !== false){
          /*class name includes -THemeName*/
          $contentTypeProps = array();
          $subContentTypeProps = array();
          if(!in_array($className,$contentTypeProps)){
            $contentTypeProps = explode('-', $className);
            //debugger($contentTypeProps);
            if(isset($contentTypeNameArray) && !empty($contentTypeNameArray)){
              //debugger($contentTypeNameArray);
              foreach ($contentTypeNameArray as $contentTypeName => $subContentTypeNameArray) {
                    //debugger($contentTypeName);
                if($contentTypeName == $contentTypeProps[0]){
                  //debugger($contentTypeProps);
                  //$currentNodeContentType = $contentTypeProps[0];
                  $prevContentTypeProps = $contentTypeProps;
                  $contentTypeProperties[$contentTypeName]['props'] = $contentTypeProps;
                  $contentTypeProperties[$contentTypeName]['base'] = $nodeName;
                  //$contentTypeProperties[$contentTypeName]['childNodes'] = $childNodes;

                  //$contentTypeProperties[$contentTypeProps[0]]['props']
                    //$classesProperties[$nodeName] = $contentTypeProps;
                  $contentTypePropsArray[] = $contentTypeProps;

                  /*has content type*/
                }else{
                  /*doesn't have content type*/
                  //$classesProperties[$nodeName] = null ;
                }
                //debugger($node);
                //debugger($contentTypeProps);
                if(isset($subContentTypeNameArray) && !empty($subContentTypeNameArray)){

                    //debugger($subContentTypeNameArray);
                    // foreach ($subContentTypeNameArray as $subContentTypeGroup => $subContentTypeName) {

                    //   if($subContentTypeGroup)
                    //   $contentTypeProperties[$contentTypeName][$subContentTypeGroup] = 
                    // }
                    //debugger($subContentTypeNameArray);
                  if(in_array($contentTypeProps[0], $subContentTypeNameArray)){
                    /*has sub content type*/

                    

                      //$a = $node->children();
                    

                  }else{
                        //debugger($contentTypeProperties);
                      //$classesProperties[$nodeName] = null ;

                  }

                    //debugger($node);
                    //debugger($contentTypeName);
                    //debugger($subContentTypeNameArray);
                    //debugger($classes);
                  if($contentTypeName == $className){
                  }

                }

                      //debugger($contentTypeProperties);

                //}
                
              }
            }else{

            }
            // if(in_array('canHide', $contentTypeProps)){
            //   array_push($contentTypeProps,'canHide');
            //   debugger($new)
            // debugger($contentTypeProps);

            // }
          //debugger($className);



          }

        } else{
          /*class name doesnt include -THemeName*/

        }
      # code...
      }
    }

    //debugger($classNameArray);
  }

  if ( count($childNodes) == 0 )  {
    $output[ $nodeName ] = (string)$node;
    //debugger('A');
    //return $contentTypeProperties ;
    //debugger($contentTypeProperties);
  }
  else    {
    foreach ( $childNodes as $newNode ) {
      processNode($base, $newNode, $output,$contentTypeNameArray,$themeName,$contentTypeProperties);
    }
  }
}
if(!function_exists('dom_to_array')){
  function dom_to_array($root)
  {
    $result = array();

    if ($root->hasAttributes())
    {
      $attrs = $root->attributes;

      foreach ($attrs as $i => $attr)
        $result[$attr->name] = $attr->value;
    }

    $children = $root->childNodes;

    if ($children->length == 1)
    {
      $child = $children->item(0);

      if ($child->nodeType == XML_TEXT_NODE)
      {
        $result['_value'] = $child->nodeValue;

        if (count($result) == 1)
          return $result['_value'];
        else
          return $result;
      }
    }

    $group = array();

    for($i = 0; $i < $children->length; $i++)
    {
      $child = $children->item($i);

      if (!isset($result[$child->nodeName]))
        $result[$child->nodeName] = dom_to_array($child);
      else
      {
        if (!isset($group[$child->nodeName]))
        {
          $tmp = $result[$child->nodeName];
          $result[$child->nodeName] = array($tmp);
          $group[$child->nodeName] = 1;
        }

        $result[$child->nodeName][] = dom_to_array($child);
      }
    }

    return $result;
  }
}

function ordinal($number) {
  $ends = array('th','st','nd','rd','th','th','th','th','th','th');
  if ((($number % 100) >= 11) && (($number%100) <= 13))
    return $number. 'th';
  else
    return $number. $ends[$number % 10];
}

function word_digit($word) {
  $warr = explode(';',$word);
  $result = '';
  foreach($warr as $value){
    switch(trim($value)){
      case 'One':
      $result .= '1';
      break;
      case 'Two':
      $result .= '2';
      break;
      case 'Three':
      $result .= '3';
      break;
      case 'Four':
      $result .= '4';
      break;
      case 'Five':
      $result .= '5';
      break;
      case 'Six':
      $result .= '6';
      break;
      case 'Seven':
      $result .= '7';
      break;
      case 'Eight':
      $result .= '8';
      break;
      case 'Nine':
      $result .= '9';
      break;
      case 'Ten':
      $result .= '10';
      break;    
    }
  }
  return $result;
}

function digit_word($digit) {
  $dig = explode(';',$digit);
  $result = '';
  foreach($dig as $value){
    switch(trim($value)){
      case 1:
      $result .= 'One';
      break;
      case 2:
      $result .= 'Two';
      break;
      case 3:
      $result .= 'Three';
      break;
      case 4:
      $result .= 'Four';
      break;
      case 5:
      $result .= 'Five';
      break;
      case 6:
      $result .= 'Six';
      break;
      case 7:
      $result .= 'Seven';
      break;
      case 8:
      $result .= 'Eight';
      break;
      case 9:
      $result .= 'Nine';
      break;
      case 10:
      $result .= 'Ten';
      break;    
    }
  }
  return $result;
}

function sendEmail($msg,$sender,$email){
    // the message
    //$to = "sam.codename007@gmail.com";
    //$to = "sales@janasewatravels.com";

  $to = "rojen.maharjan89@gmail.com";
  $subject = "Janasewatravels message from : ".$sender;
  $header = "From: ".$email."\r\n";
    // use wordwrap() if lines are longer than 70 characters
  $msg = wordwrap($msg,70);

    // send email
  $result = mail($to,$subject,$msg,$header);

  return $result;
}

function addArray( array &$output, array $input ) {
  foreach( $input as $key => $value ) {
    if( is_array( $value ) ) {
      if( !isset( $output[$key] ) )
        $output[$key] = array( );
      addArray( $output[$key], $value );
    } else {
      $output[$key] = $value;
    }
  }
}

function cmp($a, $b){
  if ($a == $b) {
    return 0;
  }
  return ($a < $b) ? -1 : 1;
}

function replaceArrayKey($arr, $oldkey, $newkey) {
  if(array_key_exists( $oldkey, $arr)) {
    $keys = array_keys($arr);
    $keys[array_search($oldkey, $keys)] = $newkey;
    return array_combine($keys, $arr);  
  }
  return $arr;    
}

function checkWebpageValidity($currentPage,$currentWebpage){
  if ($currentPage == $currentWebpage) {
    redirect('./404');
  }
}

function setPhpmailerSMTPSetting($mail){
    //SMTP setting
  if(ENVIRONMENT == 'DEVELOPMENT'){
    $mail->isSMTP();

  }
  $mail->Host = "ssl://smtp.gmail.com";
  $mail->SMTPAuth = true;
  $mail->Username = "sam.codename007@gmail.com";
  $mail->Password = 'xyzzyspoon';
    $mail->Port = 465; //587
    $mail->SMTPSecure = "ssl";

    return $mail;
  }
// Function to recursively search for a given value 
  function array_search_id($search_value, $array, $id_path) { 
  //debugger($search_value);
    //if ($tempValue == 1) {
    if (isset($_SESSION['privileges'])) {
      unset($_SESSION['privileges']);
    }
    $privileges = array();
        //$tempValue++;
    //}
    if(is_array($array) && count($array) > 0) { 
      foreach($array as $key => $value) { 

        $temp_path = $id_path; 
        

            // Adding current key to search path 
        array_push($temp_path, $key); 
            //debugger($temp_path);
            // Check if this value is an array 
            // with atleast one element 
        if(is_array($value) && count($value) > 0) {

          $res_path = array_search_id( 
            $search_value, $value, $temp_path); 
                //debugger($temp_path);
          if ($res_path != null) { 
            return $res_path; 
          } 
        }elseif($value == $search_value) { 
              //debugger($temp_path);
                //return join(" --> ", $temp_path);
          foreach ($temp_path as $temp_path_key => $items) {
            if ($items != '$') {

              if(strstr( $items,'-')){
                $index = $temp_path[$temp_path_key];
                
                //$previlege = (int)trim(strstr( $items,'-'),'-');
                $previlege = trim(strstr($temp_path[3],'-'),'-');

                if ($previlege != 'NA') {
                  array_push($temp_path, $previlege);

                  return $temp_path;
                  
                }            
              }
            }
          }
        } 
      }
      
      //debugger($privileges);
      
    } 

    return null; 
  }


  function addWebpageCategory($currentArray,$categoryName,$categoryDetails = array(),$categoryGroup = null){

    $privilegeData[$categoryName] = array();
    $privilegeData[$categoryName] = $categoryDetails;
    if ($categoryGroup != null) {
      $privilegeData[$categoryGroup] = $privilegeData;

      unset($privilegeData[$categoryName]);

    }

    return $privilegeData;
  }

  function array_find_deep($array, $search, $keys = array())
  {
    foreach($array as $key => $value) {
      if (is_array($value)) {
        $sub = array_find_deep($value, $search, array_merge($keys, array($key)));
        if (count($sub)) {
          return $sub;
        }
      } elseif ($value === $search) {
        return array_merge($keys, array($key));
      }
    }

    return array();
  }

// Function to iteratively search for a given value 
  function searchForId($search_value, $array, $id_path) { 

    // Iterating over main array 
    foreach ($array as $key1 => $val1) { 

      $temp_path = $id_path; 

        // Adding current key to search path 
      array_push($temp_path, $key1); 

        // Check if this value is an array 
        // with atleast one element 
      if(is_array($val1) and count($val1)) { 

            // Iterating over the nested array 
        foreach ($val1 as $key2 => $val2) { 

          if($val2 == $search_value) { 

                    // Adding current key to search path 
            array_push($temp_path, $key2); 

            return join(" --> ", $temp_path); 
          } 
        } 
      } 

      elseif($val1 == $search_value) { 
        return join(" --> ", $temp_path); 
      } 
    } 

    return null; 
  }

  function array_swap(&$array,$swap_a,$swap_b){
   list($array[$swap_a],$array[$swap_b]) = array($array[$swap_b],$array[$swap_a]);
 }

 function convert_smart_quotes($string)
 {
  $search = array('"');

  $replace = array("&quot");

  return str_replace($search, $replace, $string);
}

if(!function_exists('getUserType')){
  function getUserType($currentUserData){
    if (isset($currentUserData) && !empty($currentUserData)) {

      if ($currentUserData[0]->user_type == 1) {
        $userType = 'Super Admin';
      }elseif($currentUserData[0]->user_type == 2){
        $userType = 'Admin';
      }elseif($currentUserData[0]->user_type == 3){
        $userType = 'Student';

      }elseif($currentUserData[0]->user_type == 4){
        $userType = 'Parents';

      }elseif($currentUserData[0]->user_type == 5){
        $userType = 'Teacher';

      }elseif($currentUserData[0]->user_type == 6){
        $userType = 'Accountant';

      }elseif($currentUserData[0]->user_type == 7){
        $userType = 'Receptionist';

      }elseif($currentUserData[0]->user_type == 8){
        $userType = 'Librarian';

      }elseif($currentUserData[0]->user_type == 9){
        $userType = 'General Staff';

      }

      return $userType;
    }
  }
}

if(!function_exists('notify')){
  function notify($session_status = null, $session_status_msg=null){
    if($session_status != NULL){
      $_SESSION[$session_status] = $session_status_msg;
    }
  }
}

if(!function_exists('in_array_r')){
  function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
        return true;
      }
    }

    return false;
  }
}

if(!function_exists('flash')){
 function flash(){
  if(isset($_SESSION['success']) && $_SESSION['success'] != ""){
   echo "<p class='alert alert-success'>".$_SESSION['success']."</p>";
   unset($_SESSION['success']);
 }
 if(isset($_SESSION['error']) && $_SESSION['error'] != ""){
   echo "<p class='alert alert-danger'>".$_SESSION['error']."</p>";
   unset($_SESSION['error']);
 }
 if(isset($_SESSION['warning']) && $_SESSION['warning'] != ""){
   echo "<p class='alert alert-warning'>".$_SESSION['warning']."</p>";
   unset($_SESSION['warning']);
 }
 if(isset($_SESSION['info']) && $_SESSION['info'] != ""){
   echo "<p class='alert alert-info'>".$_SESSION['info']."</p>";
   unset($_SESSION['info']);
 }
}
}


if(!function_exists('generateRandomString')){
 function generateRandomString($length = 30){
  $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $len = strlen($chars);
  $rand = "";
  for($i=0; $i<$length; $i++){
   $rand .= $chars[rand(0, $len-1)];
 }

 return $rand;
}
}


if(!function_exists('getCurrentPage')){
  function getCurrentPage(){
    $current_page = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
    return $current_page;
  }

}

if(!function_exists('sanitize')){
  function sanitize($str){
    if(is_array($str)){
      $strArr = array();
      foreach ($str as $key => $value) {

        $strVal = rtrim($value);
        $strVal = stripslashes($value);
        $strVal = addslashes($value);
          $strVal = strip_tags($value);  // htmlentities() => html_entity_decode()
          array_push($strArr, $strVal);
          
        }

        return $strArr;
      }else{
        $str = rtrim($str);
        $str = stripslashes($str);
        $str = addslashes($str);
        $str = strip_tags($str);  // htmlentities() => html_entity_decode()
        return $str;
      }
      
    }
  }


  if(!function_exists('sanitizeCheckbox')){
    function sanitizeCheckbox($postData = null){
      if (isset($postData) && !empty($postData)) {
        if(sanitize($postData) == 'on'){
          return 1;
        }else{
          return 0;
        }
      }else{
        return 0;
      }
    }
  }



/**
 * easy image resize function
 * @param  $file - file name to resize
 * @param  $string - The image data, as a string
 * @param  $width - new image width
 * @param  $height - new image height
 * @param  $proportional - keep image proportional, default is no
 * @param  $output - name of the new file (include path if needed)
 * @param  $delete_original - if true the original image will be deleted
 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
 * @param  $quality - enter 1-100 (100 is best quality) default is 100
 * @param  $grayscale - if true, image will be grayscale (default is false)
 * @return boolean|resource
 */
if(!function_exists('smart_resize_image')){
  function smart_resize_image($file,
    $string             = null,
    $width              = 0, 
    $height             = 0, 
    $proportional       = false, 
    $output             = 'file', 
    $delete_original    = true, 
    $use_linux_commands = false,
    $quality            = 100,
    $grayscale          = false
  ){

    if ( $height <= 0 && $width <= 0 ) return false;
    if ( $file === null && $string === null ) return false;
    # Setting defaults and meta
    $info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
    $image                        = '';
    $final_width                  = 0;
    $final_height                 = 0;
    list($width_old, $height_old) = $info;
    $cropHeight = $cropWidth = 0;
    # Calculating proportionality
    if ($proportional) {
      if      ($width  == 0)  $factor = $height/$height_old;
      elseif  ($height == 0)  $factor = $width/$width_old;
      else                    $factor = min( $width / $width_old, $height / $height_old );
      $final_width  = round( $width_old * $factor );
      $final_height = round( $height_old * $factor );
    }
    else {
      $final_width = ( $width <= 0 ) ? $width_old : $width;
      $final_height = ( $height <= 0 ) ? $height_old : $height;
      $widthX = $width_old / $width;
      $heightX = $height_old / $height;
      
      $x = min($widthX, $heightX);
      $cropWidth = ($width_old - $width * $x) / 2;
      $cropHeight = ($height_old - $height * $x) / 2;
    }
    # Loading image to memory according to type
    switch ( $info[2] ) {
      case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
      case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
      case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
      default: return false;
    }
    
    # Making the image grayscale, if needed
    if ($grayscale) {
      imagefilter($image, IMG_FILTER_GRAYSCALE);
    }    
    
    # This is the resizing/resampling/transparency-preserving magic
    $image_resized = imagecreatetruecolor( $final_width, $final_height );
    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
      $transparency = imagecolortransparent($image);
      $palletsize = imagecolorstotal($image);
      if ($transparency >= 0 && $transparency < $palletsize) {
        $transparent_color  = imagecolorsforindex($image, $transparency);
        $transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
        imagefill($image_resized, 0, 0, $transparency);
        imagecolortransparent($image_resized, $transparency);
      }
      elseif ($info[2] == IMAGETYPE_PNG) {
        imagealphablending($image_resized, false);
        $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
        imagefill($image_resized, 0, 0, $color);
        imagesavealpha($image_resized, true);
      }
    }
    imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
    
    
    # Taking care of original, if needed
    if ( $delete_original ) {
      if ( $use_linux_commands ) exec('rm '.$file);
      else @unlink($file);
    }
    # Preparing a method of providing result
    switch ( strtolower($output) ) {
      case 'browser':
      $mime = image_type_to_mime_type($info[2]);
      header("Content-type: $mime");
      $output = NULL;
      break;
      case 'file':
      $output = $file;
      break;
      case 'return':
      return $image_resized;
      break;
      default:
      break;
    }
    
    # Writing image according to type to the output destination and image quality
    switch ( $info[2] ) {
      case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
      case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
      case IMAGETYPE_PNG:
      $quality = 9 - (int)((0.9*$quality)/10.0);
      imagepng($image_resized, $output, $quality);
      break;
      default: return false;
    }
    return true;
  }
}

if(!function_exists('multi_attach_mail')){
  function multi_attach_mail($to, $subject, $message, $senderMail, $senderName, $files){
    $from = $senderName." <".$senderMail.">"; 
    $headers = "From: $from";
    // boundary 
    $semi_rand = md5(time()); 
    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
    // headers for attachment 
    $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 
    // multipart boundary 
    $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
    "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n"; 
    // preparing attachments
    debugger($files);
    debugger(count($files));
    if(count($files) > 0){
      for($i=0;$i<count($files);$i++){
        debugger($files[$i]);
        if(is_file($files[$i])){
          $message .= "--{$mime_boundary}\n";
          $fp =    @fopen($files[$i],"rb");
          $data =  @fread($fp,filesize($files[$i]));
          @fclose($fp);
          $data = chunk_split(base64_encode($data));
          $message .= "Content-Type: application/octet-stream; name=\"".basename($files[$i])."\"\n" . 
          "Content-Description: ".basename($files[$i])."\n" .
          "Content-Disposition: attachment;\n" . " filename=\"".basename($files[$i])."\"; size=".filesize($files[$i]).";\n" . 
          "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
        }
      }
    }

    $message .= "--{$mime_boundary}--";
    $returnpath = "-f" . $senderMail;

  //debugger($to);
  //debugger($subject);
    debugger($message);
  //debugger($headers);
  //debugger($returnpath);
    
  //send email
    $mail = @mail($to, $subject, $message, $headers); 
    if ($mail) {
      debugger(1,true);
    }else{
      debugger(2,true);
    }
  //function return true, if email sent, otherwise return fasle
    if($mail){ return TRUE; } else { return FALSE; }
  }
}

if(!function_exists('uploadSingleFile')){
  function uploadSingleFile($files, $destination){

    // if ($files['size'] >= 1000000) { //file size less than 1 mb or 1000000 bytes
    //   $_SESSION['warning'] = 'File not uploaded! Please try uploading file again.File size needs to be less than 1mb to upload.';
    //   return null;
    // }

    if($files['error'] == 0){
     $ext = pathinfo($files['name'], PATHINFO_EXTENSION);

     if(in_array(strtolower($ext), ALLOWED_EXTENSION)){
      $file_name = ucfirst($destination)."-".date('Ymdhis').rand(0,999).".".$ext;

      debugger($ext,true);
      $folder = UPLOAD_DIR.''.$destination;

      if(!is_dir($folder)){
       mkdir($folder, '0777', true);
     }

     $success = move_uploaded_file($files['tmp_name'], $folder.'/'.$file_name);

     debugger($success);
        /**
 * easy image resize function
 * @param  $file - file name to resize
 * @param  $string - The image data, as a string
 * @param  $width - new image width
 * @param  $height - new image height
 * @param  $proportional - keep image proportional, default is no
 * @param  $output - name of the new file (include path if needed)
 * @param  $delete_original - if true the original image will be deleted
 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
 * @param  $quality - enter 1-100 (100 is best quality) default is 100
 * @param  $grayscale - if true, image will be grayscale (default is false)
 * @return boolean|resource
 */
        if($success){
          debugger('success');
          if ($destination == 'user') {
            smart_resize_image($folder."/".$file_name,null,120,120,false, $folder."/".$file_name, true, false,100,false);
          }
          return $file_name;
        } else {
         return null;
       }

     } else {
      return null;
    }
  } else {
   return null;
 }
}
if(!function_exists('uploadSingleThemeFile')){
  function uploadSingleThemeFile($files, $destination,$allwoedExtention){

    // if ($files['size'] >= 1000000) { //file size less than 1 mb or 1000000 bytes
    //   $_SESSION['warning'] = 'File not uploaded! Please try uploading file again.File size needs to be less than 1mb to upload.';
    //   return null;
    // }

    if($files['error'] == 0){
     $ext = pathinfo($files['name'], PATHINFO_EXTENSION);

     if(in_array(strtolower($ext), $allwoedExtention)){
      //$file_name = $files['name'];
      //debugger(explode('/', $destination));

      $urlArr = explode('/', $destination);
      $folder = '/'.end($urlArr);
      $file_name_with_dir = ucfirst($destination).$folder."-".date('Ymdhis').rand(0,999).".".$ext;
      //debugger($file_name);
      //debugger($destination);

      if(!is_dir($destination)){
       mkdir($destination, '0777', true);
     }
     debugger($destination.'/'.$file_name_with_dir);


     $file_name_with_dir_arr = explode('/', $file_name_with_dir);
     $file_name = end($file_name_with_dir_arr);
     $success = move_uploaded_file($files['tmp_name'], $file_name_with_dir);

     if($success){
      debugger('success');
      return $file_name;
    } else {
     return null;
   }

 } else {
  return null;
}
} else {
 return null;
}
}
}
function deleteDir($dirPath) {
  if (!is_dir($dirPath)) {
    throw new InvalidArgumentException("$dirPath must be a directory");
  }
  if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
    $dirPath .= '/';
  }
  $files = glob($dirPath . '*', GLOB_MARK);
  foreach ($files as $file) {
    if (is_dir($file)) {
      deleteDir($file);
    } else {
      unlink($file);
    }
  }
  rmdir($dirPath);
}

function deleteDirectory($dir) {
  if (!file_exists($dir)) return true;
  if (!is_dir($dir)) return unlink($dir);
  foreach (scandir($dir) as $item) {
    if ($item == '.' || $item == '..') continue;
    if (!deleteDirectory($dir.DIRECTORY_SEPARATOR.$item)) return false;
  }
  return rmdir($dir);
}

function uploadAndExtractZip($file,$uploadDirectory){


  if($file['type'] == 'application/x-zip-compressed'){
                  // $data['folderName'] = uploadSingleFile($_FILE['themeZipFIle'], 'user');
      //debugger(PATHINFO_DIRNAME);
                  // assuming file.zip is in the same directory as the executing script.
    $zipfile = $file['name'] ;
    $zip = new ZipArchive();
    $res = $zip->open($file['tmp_name']);
    if ($res === TRUE) {
                      // extract it to the path we determined above
      $path = $uploadDirectory.'/';
      $zip->extractTo($path);
      $zip->close();
      return true;
                    //echo "WOOT! $zipfile extracted to $path";
    } else {
                    //echo "Doh! I couldn't open $zipfile";
      return false;
    }
  }else{
    return false;
  }
}

if(!function_exists('readMoreHelper')){
  function readMoreHelper($id,$story_desc,$sender, $chars = 100) {
    $story = substr($story_desc,0,$chars);  
    
      //$story = $story.' ... <a href="#" onclick="openPopupText(\''.$id.'\',\''.$story_desc.'\');">Read More</a>'; 
      //$story = $story.' ... <a href="#" onclick="openPopupText(\''.$id.'\');">Read More</a>'; 
    if (strlen($story_desc) >= $chars) {
      $story = $story.' ... <a href="#" data-toggle="modal" data-target="#'.$id.'" style="color:#001dff" onclick="openPopupText(\''.$id.'\',\''.$story_desc.'\',\''.$sender.'\');">Read More</a>';
    }else{
          //debugger(strrpos($story,' '));
      if(is_int(strrpos($story,' '))){
        $story = substr($story,0,strrpos($story,' '));
      }
    }
    $story = preg_replace("/[\r\n]*/","",$story);
    return $story;
  }
}
}

if(!function_exists('isJson')){
  function isJson($string) {
   json_decode($string);
   
   if (json_last_error() === 0) {
    return true;
  }else{
    return false;
  }
   //return (json_last_error() == JSON_ERROR_NONE);
}
}

//processing JSON data for OR operation
if (!function_exists('processJsonData')) {
  function processJsonData($id,$column,$operation){
    $idArray = json_decode($id);
    $id = '';
    foreach ($idArray as $key => $items) {

      if ($key == sizeof($idArray) - 1) {
        $id .= $items;        
      }else{
        $id .= $items.' '.$operation.' '.$column.' = ';

      }
    }
    return $id;
  }
}

if (!function_exists('cors')) {
  function cors() {

    header("Access-Control-Allow-Origin: *");

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
      header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
      header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
      }

    // Access-Control headers are received during OPTIONS requests
      if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
          header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
          header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
      }

      echo "You have CORS!";
    }
  }


  if(!function_exists('isLoginPage')){
    function isLoginPage(){
        //debugger('http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['SCRIPT_NAME']);
        //debugger(SITE_URL.'index.php');
      if($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['SCRIPT_NAME'] == SITE_URL.'index.php'){
        return true;
      }else{
        return false;
      }
    }
  }

  function custom_copy($src, $dst ,$execptionFilesAndFolders = null ) { 

    // open the source directory
    $dir = opendir($src); 
    //debugger($dir);

    // Make the destination directory if not exist
    @mkdir($dst); 

    // Loop through the files in source directory
    while( $file = readdir($dir) ) { 

      if (( $file != '.' ) && ( $file != '..' )) { 
        if ( is_dir($src . '/' . $file)) 
        { 
            // copy folders
          if(isset($execptionFilesAndFolders) && !empty($execptionFilesAndFolders)){
            if(!in_array($file, $execptionFilesAndFolders['folders'])){
                // Recursively calling custom copy function
                // for sub directory 
              custom_copy($src . '/' . $file, $dst . '/' . $file); 
            }
          }else{
            custom_copy($src . '/' . $file, $dst . '/' . $file); 
          }
          
        } 
        else { 
            // copy files
          if(isset($execptionFilesAndFolders) && !empty($execptionFilesAndFolders)){
            if(!in_array($file, $execptionFilesAndFolders['files'])){
              copy($src . '/' . $file, $dst . '/' . $file); 
              
            }
          }else{
            copy($src . '/' . $file, $dst . '/' . $file); 

          }
          
        } 
      } 
    } 
    
    
    closedir($dir);
  } 

  function xml_attribute($object, $attribute)
  {
    if(isset($object[$attribute]))
      return (string) $object[$attribute];
  }

  function returnKeyOfXmlDoc($arr,$currentSubContentTypeProps){
    foreach ($arr as $key => $value) {
        //debugger($currentSubContentTypePropsArray[0]);
      $classNameArray = explode(' ', $value);
      $currentSubContentTypePropsArray = explode('-', $currentSubContentTypeProps);
      //debugger($currentSubContentTypeProps);

      if($currentSubContentTypePropsArray[0] == 'SocialbarItem' || $currentSubContentTypePropsArray[0] == 'NavbarItem' || $currentSubContentTypePropsArray[0] == 'Image'){

        if((isset($classNameArray[0]) && !empty($classNameArray[0])) || (isset($classNameArray[1]) && !empty($classNameArray[1]))){

          if(isset($classNameArray[0]) && !empty($classNameArray[0])){
            if($classNameArray[0] == $currentSubContentTypeProps){

              return $key;

            }
          }
          if(isset($classNameArray[1]) && !empty($classNameArray[1])){
            if($classNameArray[1] == $currentSubContentTypeProps){
          //debugger($classNameArray);

              return $key;

            }
          }
          
        }

      }else{
        if($classNameArray[0] == $currentSubContentTypeProps){
          return $key;
        }
      }
      
    }
  }
  function simpleXMLElement_innerXML($xml)
  {
    $innerXML= array();
    if (isset(dom_import_simplexml($xml)->childNodes) && !empty(dom_import_simplexml($xml)->childNodes)) {
      foreach (dom_import_simplexml($xml)->childNodes as $child)
      {
        $innerXML[] = $child->ownerDocument->saveXML( $child );
        //$innerXML = array_filter(array_map('trim', $innerXML));

      }
      return $innerXML;
    }
    
  };
  function displayText($textArray){
    $text = '';
    if(is_array($textArray)){
      foreach ($textArray as $key => $items) {
        $text = $text.$items.' ';
      }
    }else{
      $text = $textArray ;

    }
    echo $text;
    
  }
  function displayParagraphText($key,$items,$mainArray){
    $text = strip_tags(ltrim(rtrim(preg_replace('/\s+/', ' ', $items))));
  //debugger($items);

    if(strlen($text) > 0){
      echo $text;

    }else{
      if($key != 0 && $key != (sizeof($mainArray['textArray']) - 1)){
        echo '&#013;&#010;';

      }

    }
  }
  function loadHtmlToXml($currentPageURL){
    $url = $currentPageURL;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);

    $data = curl_exec($ch);
            //debugger($data,true);

    curl_close($ch);
    $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
    return $xml ;

  }
  function getContentTypeNames(){
    $contentTypeNameArray = array();
    $contentTypeNameArray['Navbar'] = array('NavbarItem','NavbarTitle','Image','Heading');
    $contentTypeNameArray['SideBar'] = array('NavbarItem','NavbarTitle','Image','Heading');
    
    $contentTypeNameArray['Navbar']['NavbarBrand'] = array('Image','Heading');
    $contentTypeNameArray['Socialbar'] = array('SocialbarItem');
    $contentTypeNameArray['Carousel'] = array('Image','Heading','Paragraph','ParagraphGroup','SocialbarItem');

    $contentTypeNameArray['TitleHeading'] = array();
    $contentTypeNameArray['Heading'] = array('TitleHeading','ParagraphGroup','Paragraph');

    $contentTypeNameArray['Footer'] = array('FooterHeading','TitleHeading','SocialbarItem','Heading');

    $contentTypeNameArray['Content'] = array('Paragraph','ParagraphGroup','Heading','Image','SocialbarItem','VideoUrl','TitleHeading');
    $contentTypeNameArray['Video'] = array('VideoUrl');
    $contentTypeNameArray['Gallery'] = array('Image');
    return $contentTypeNameArray ;
  }


  function simplexml_import_xml(SimpleXMLElement $parent, $xml, $before = false){
    $xml = (string)$xml;

    // check if there is something to add
    if ($nodata = !strlen($xml) or $parent[0] == NULL) {
      return $nodata;
    }

    // add the XML
    $node     = dom_import_simplexml($parent);
    $fragment = $node->ownerDocument->createDocumentFragment();
    $fragment->appendXML($xml);

    if ($before) {
      return (bool)$node->parentNode->insertBefore($fragment, $node);
    }

    return (bool)$node->appendChild($fragment);
  }

  function simplexml_import_simplexml(SimpleXMLElement $parent, SimpleXMLElement $child, $before = false){
    // check if there is something to add
    if ($child[0] == NULL) {
      return true;
    }

    // if it is a list of SimpleXMLElements default to the first one
    $child = $child[0];

    // insert attribute
    if ($child->xpath('.') != array($child)) {
      $parent[$child->getName()] = (string)$child;
      return true;
    }

    $xml = $child->asXML();

    // remove the XML declaration on document elements
    if ($child->xpath('/*') == array($child)) {
      $pos = strpos($xml, "\n");
      $xml = substr($xml, $pos + 1);
    }

    return simplexml_import_xml($parent, $xml, $before);
  }
  function _remove_empty_internal($value) {
    return !empty($value) || $value === 0;
  }
  
  function getUploadedFileUrlForWeb($websiteData,$uploaded_file,$uploadType){
    return SITE_URL.'manage-website/site/'.$websiteData[0]->website_id.'/uploads/'.$uploadType.'/'.$uploaded_file;
  }
  
// debugger($_SERVER);

// debugger($_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
// debugger(SITE_URL.'index.php');


